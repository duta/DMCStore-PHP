<?php
    use yii\helpers\Url;
?>

<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
<title>Sign Up | Aisyah Collection</title>
<!-- Bootstrap Core CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/css/animate.css" rel="stylesheet">
<!-- Wizard CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/register-steps/steps.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/css/colors/default.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="step-register">
  <div class="register-box">
    <div class="">
        <a href="javascript:void(0)" class="text-center db m-b-40">
            <img style="height: 90px" src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/hatobe.png" alt="Home" />
        </a>

      <!-- multistep form -->
        <form id="msform">
        <!-- progressbar -->
        <ul id="eliteregister">
        <li class="active">Account Setup</li>
        <!-- <li>Social Profiles</li> -->
        <li>Personal Details</li>
        </ul>
        <!-- fieldsets -->
        <fieldset>
        <h2 class="fs-title">Create your account</h2>
        <h3 class="fs-subtitle">This is step 1</h3>
        <input type="text" name="reg[email]" placeholder="Email" />
        <input type="password" name="reg[pass]" placeholder="Password" />
        <input type="password" name="reg[cpass]" placeholder="Confirm Password" />
        <input type="button" name="reg[next]" class="next action-button" value="Next" />
        </fieldset>
     <!--    <fieldset>
        <h2 class="fs-title">Social Profiles</h2>
        <h3 class="fs-subtitle">Your presence on the social network</h3>
        <input type="text" name="reg[twitter]" placeholder="Twitter" />
        <input type="text" name="reg[facebook]" placeholder="Facebook" />
        <input type="text" name="reg[gplus]" placeholder="Google Plus" />
        <input type="button" name="reg[previous]" class="previous action-button" value="Previous" />
        <input type="button" name="reg[next]" class="next action-button" value="Next" />
        </fieldset> -->
        <fieldset>
            <h2 class="fs-title">Personal Details</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <input type="text" name="reg[fname]" placeholder="Full Name" />
            <input type="text" name="reg[username]" placeholder="Nick name" />
            <input type="text" name="reg[phone]" placeholder="Phone" />
            <!-- <textarea name="reg[address]" placeholder="Address"></textarea> -->
            <input type="button" name="reg[previous]" class="previous action-button" value="Previous" />
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>" >

            <button type="submit" name="reg[submit]" class=" action-button" />Submit</button>
        </fieldset>
        </form>
        <div class="clear"></div>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/register-steps/jquery.easing.min.js"></script>
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/register-steps/register-init.js"></script>
<!--slimscroll JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $("form#msform").submit(function(e){
            e.preventDefault();
            if($("input[name='reg[username]']").val()!=''){
                $.post("<?=Url::toRoute('site/register')?>",$(this).serialize(),function(){
                    window.location.href="<?=Url::toRoute('site/login')?>";
                });
            }
        });
    })
</script>
</body>
</html>
