<?php
    use yii\helpers\Url;
    use yii\authclient\widgets\AuthChoice;
?>
<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/logo_ac.png">
<title>Aisyah Collection</title>
<!-- Bootstrap Core CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?=Url::to('@web/web/ampleadmin')?>/css/colors/blue.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box">
      <form class="form-horizontal form-material login-form" name="formLogin" id="formLogin" action="<?=Url::to('@web/site/login')?>" method="POST">
        <a href="<?=Yii::$app->homeUrl?>" class="text-center db">
        <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/logo_ac_text.png" style="height: 100px" alt="Home" />
        <br/>
        
        <!-- <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/admin-text-dark.png" alt="Home" /> -->
      </a>  
        
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->csrfToken?>" >
        
              <?=Yii::$app->session->getFlash('error');?>
            
        <div class="form-group m-t-40">
          
          <div class="col-xs-12">
            <input class="form-control" type="text" name="LoginForm[username]" required="required" placeholder="Username" autofocus>
          </div>
        </div>
        <div class="form-group">
          <div class="col-xs-12">
            <input class="form-control" type="password" name="LoginForm[password]" required="required" placeholder="Password">
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input type="hidden" name="LoginForm[rememberMe]" value="0">
              
              <input id="checkbox-signup" type="checkbox" name="LoginForm[rememberMe]" value="0">
              <label for="checkbox-signup"> Remember me </label>
            </div>

            <!-- <div class="col-lg-offset-1 col-lg-3">
              <input type="hidden" name="LoginForm[rememberMe]" value="0">
              <input type="checkbox" id="loginform-rememberme" name="LoginForm[rememberMe]" value="1" checked=""> <label for="loginform-rememberme">Remember Me</label></div> -->
            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button type="submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="login-button">Log In </button>
          </div>
        </div>
        <div class="row">
 


          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center" style="">
            <div class="social">
              <!-- <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>  -->

              <a href="<?=Url::toRoute(['site/auth','authclient'=>'google'])?>" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> 
               <div class="form-group">
              <?= yii\authclient\widgets\AuthChoice::widget([
                   'baseAuthUrl' => ['site/auth'],
                   'popupMode' => false,
              ]) ?></div> 
            </div>
             
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <!-- <p>Don't have an account? <a href="<?=Url::toRoute('site/register')?>" class="text-primary m-l-5 "><b>Sign Up</b></a></p> -->
          </div>
        </div>
      </form>
      <form class="form-horizontal" id="recoverform" action="index.html">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="<?=Url::to('@web/web/ampleadmin')?>/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script type="text/javascript">
    function testAnim(x) {
        $('#animationSandbox').removeClass().addClass(x + ' animated  alert alert-danger').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
            // $(this).removeClass();
        });

    };

    testAnim('shake');
</script>
</body>
</html>
