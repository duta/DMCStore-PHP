<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="mdi mdi-puzzle fa-fw"></i>PRODUCT </h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active">PRODUCT</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <i class="mdi mdi-puzzle fa-fw"></i>Featured Product

                <!-- <button class="add_material btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah Barang" data-placement="left">
                    <span class="ti-plus"></span> 
                </button> -->
            </h3>
             <hr>
            <div class="table-body"></div>
        </div>
    </div>
</div>