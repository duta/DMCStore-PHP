<div class="row">
    <div class="col-sm-12 col-xs-12">
        <form id="form-change-pass">
            
            <div class="form-group">
                <label for="new_pass">New Password</label>
                <input type="password" class="form-control" id="new_pass" placeholder="New Password" required="required"> 
            </div>

            <div class="form-group">
                <label for="conf_pass">Confirm Password</label>
                <input type="password" class="form-control" id="conf_pass" name="new_pass" placeholder="Confirm Password" required="required"> 
            </div>
       
            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
            <button type="button" data-dismiss="modal" class="btn btn-inverse waves-effect waves-light">Cancel</button>
        </form>
    </div>
</div>