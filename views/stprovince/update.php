<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\StProvince */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'St Province',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'St Provinces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="st-province-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
