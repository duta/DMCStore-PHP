<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StProvince */

$this->title = Yii::t('app', 'Create St Province');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'St Provinces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="st-province-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
