
<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use app\models\Menu;
use app\components\Help;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$link=Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
$menu=Menu::find()->where(['link'=>$link])->one();
$this->title = Yii::t('app', strtolower($menu->name)); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi '.$menu->icon.' fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/

$user=Yii::$app->user->identity;
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <?php
                if(isset($menu->parent)){
            ?>
                    <li class=""><?=$menu->parent->name?></li>
            <?php
                }
            ?>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                <div class="btn-group  pull-right">
                    <button class="add_<?=$this->title?> btn btn-info waves-effect waves-light" id="0">
                        <span class="fa fa-plus fa-fw"></span> Pengeluaran
                    </button>
                    <?php
                        // if($user->id_role==1){
                    ?>
                   <!--  <button class="paid_<?=$this->title?> btn btn-success waves-effect waves-light" id="0">
                        <span class="fa fa-check fa-fw"></span> Telah dibayar
                    </button> -->
                    <?php
                        // }
                    ?>
                </div>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                            <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'responsive'=>true,
                                    'responsiveWrap'=>false,
                                    'condensed'=>true,
                                    'bordered'=>false,
                                    'export'=>false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        [   
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                            'class' => 'yii\grid\ActionColumn',
                                            'template'=>'<center>{action}</center>',
                                            'buttons'=>[
                                                // 'bukti'=>function($url,$model){
                                                //     if($model->bukti !== null){
                                                //         return '<button type="button" class="btn btn-success btn-circle btn-sm view_bukti lihat_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip"  data-filename="'.$model->bukti.'" data-original-title="Lihat bukti" data-placement="top"><i class="fa fa-photo"></i></button>';
                                                //     }
                                                // },
                                                // 'update'=>function($url,$model){
                                                //     return '<button type="button" class="btn btn-primary btn-circle btn-sm update_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Update '.$this->title.'" data-placement="top"><i class="fa fa-pencil"></i></button>';
                                                // },
                                                // 'delete'=>function($url,$model){
                                                //     return '<button type="button" class="btn btn-danger btn-circle btn-sm del_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Hapus '.$this->title.'" data-placement="top"><i class="fa fa-trash"></i></button>';
                                                // },
                                                'action'=> function($url,$model){
                                                    $btn_if="";
                                                    if($model->bukti !== null){
                                                        $btn_if.='<li>
                                                                <a href="javascript:void(0)" id="'.$model->id.'" class="view_bukti" data-filename="'.$model->bukti.'">
                                                                <i class="fa fa-photo fa-fw"></i>Lihat bukti
                                                                </a>
                                                        </li>';
                                                    }
                                                    return '
                                                            <div class="btn-group dropup">
                                                                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light" type="button">
                                                                        <i class="fa fa-cog m-r-5"></i>
                                                                        <span class="caret"></span>
                                                                </button>
                                                                <ul role="menu" class="dropdown-menu animated flipInX">
                                                                    <li class="disabled"><a href="javascript:void(0)">ID : #'.$model->id.'</a></li>
                                                                    <li class="divider"></li>
                                                                    <li>
                                                                            <a href="javascript:void(0)" id="'.$model->id.'" class="update_'.$this->title.'">
                                                                            <i class="fa fa-pencil fa-fw"></i>Edit '.$this->title.'
                                                                            </a>
                                                                    </li>
                                                                     <li>
                                                                            <a href="javascript:void(0)" id="'.$model->id.'" class="del_'.$this->title.'">
                                                                            <i class="fa fa-trash fa-fw"></i>Hapus '.$this->title.'
                                                                            </a>
                                                                    </li>
                                                                    
                                                                    '.$btn_if.'
                                                                </ul>
                                                            </div>';
                                                }
                                            ],

                                        ],
                                         // 'id',
                                        // 'date',
                                        // 'user_id',
                                        [
                                            'header'=> 'Tanggal',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return Help::dateindoshort($model->date);
                                            }
                                        ],
                                        // 'id',
                                        // 'date',
                                        // 'user_id',
                                        'fullname:ntext',
                                        // 'coa_id',

                                        'coa_name',
                                        [
                                            'header'=> 'Debit',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return Help::toRp($model->debit);
                                            }
                                        ],
                                        [
                                            'header'=> 'Kredit',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return Help::toRp($model->kredit);
                                            }
                                        ],
                                        // 'debit',
                                        // 'kredit',
                                        'keterangan:ntext',
                                         // checkbox
                                        // [
                                        //     'class' => 'yii\grid\CheckboxColumn',
                                        //     'checkboxOptions' => function($model,$key,$i){
                                        //             return ['value'=>$model->id,'class'=>'check_item'];
                                        //     },
                                           
                                        // ],
                                        // 'status_id',
                                        // 'bukti',
                                        // 'is_cf',
                                        // 'created_at',
                                        // 'updated_at',
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                          
                                    <!-- 'tableOptions'=>['class'=>'table table-condensed table-hover'], -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

