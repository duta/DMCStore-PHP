<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vspending */

$this->title = 'Create Vspending';
$this->params['breadcrumbs'][] = ['label' => 'Vspendings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vspending-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
