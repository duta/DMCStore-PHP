
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<form class="">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <!-- <label class="control-label">First Name</label> -->
                    <input type="text" id="firstName" class="form-control" placeholder="John doe"> 
                    <!-- <span class="help-block"> This is inline help </span>  -->
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <!-- <label class="control-label">Last Name</label> -->
                    <input type="text" id="lastName" class="form-control" placeholder="12n"> 
                    <!-- <span class="help-block"> This field has error. </span>  -->
                </div>
            </div>
            <!--/span-->
        </div>
</form>

