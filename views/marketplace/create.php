<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marketplace */

$this->title = Yii::t('app', 'Create Marketplace');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Marketplaces'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marketplace-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
