<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    use app\models\Role;
?>

<form class="form-horizontal" id="form_sup" action="<?=($model->isNewRecord) ? Url::toRoute('brsupplier/create') : Url::toRoute('brsupplier/update').'/'.$model['id']; ?>" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="BrSupplier[nm_sup]" value="<?=$model['nm_sup']?>" placeholder="Nama Supplier" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="BrSupplier[tlp]" value="<?=$model['tlp']?>" placeholder="no Tlp/Hp" >
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="BrSupplier[qq]" value="<?=$model['qq']?>" placeholder="QQ" >
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="BrSupplier[wechat]" value="<?=$model['wechat']?>" placeholder="wechat" >
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
   
    </div>
        <hr class="light-grey-hr">
    
    <div class="form-actions">
        <div class="row">
            <div class="text-center col-md-12">
                <button type="submit" class="btn btn-success"><?=($model->isNewRecord ? 'Create' : 'Save'); ?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>