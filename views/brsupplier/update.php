<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BrSupplier */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Br Supplier',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Br Suppliers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="br-supplier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
