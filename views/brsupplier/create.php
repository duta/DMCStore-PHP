<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BrSupplier */

$this->title = Yii::t('app', 'Create Br Supplier');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Br Suppliers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="br-supplier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
