<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="customers-form">

    <?php $form = ActiveForm::begin([
        'id' => 'customers-form',
        // 'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('name')])->label(false) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('email')])->label(false) ?>

    <?= $form->field($model, 'tlp')->textInput(['maxlength' => true,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('tlp')])->label(false) ?>

    <?= $form->field($model, 'prov')->textInput(['maxlength' => true,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('prov')])->label(false) ?>

    <?= $form->field($model, 'kota_kab')->textInput(['maxlength' => true,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('kota_kab')])->label(false) ?>

    <?= $form->field($model, 'kec')->textInput(['maxlength' => true,'class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('kec')])->label(false) ?>

    <?= $form->field($model, 'pos')->textInput(['class'=>'form-control input-sm','placeholder'=>$model->getAttributeLabel('pos')])->label(false) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6,'placeholder'=>$model->getAttributeLabel('alamat')])->label(false) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

