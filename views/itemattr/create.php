<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ItemAttr */

$this->title = Yii::t('app', 'Create Item Attr');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Item Attrs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-attr-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
