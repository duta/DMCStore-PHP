<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use app\components\Help;
?>
<form id="form-updatedvarian">
    <input type="hidden" name="id_attr" value="<?=$model->id?>">

        <div class="well" style="padding: 1px !important">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <h4>
                    <b><?=strtoupper($model->nm_attr);?></b>
                    </h4>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <!-- <a href="#" class="pull-right btn btn-primary btn-xs add-hg"><i class="fa fa-plus fa-fw"></i>Varian</a> -->
                <div class="hg-wrap" style="<?=isset($model->varian)?'':'display: none';?>">
                    <h6 class="text-center font-bold"><i class="icon-info"></i>Daftar Varian
                        <!-- <a class="btn btn-danger btn-xs pull-right">Hapus Harga grosir</a> -->
                    </h6>
                    <hr class="light-grey-hr">
                        
                     <?php
                        if(count($model->varian)>0){
                            $c=count($model->varian)-1;
                            
                            foreach ($model->varian as $key => $var) {
                    ?>
                        <div class="row row-hg">
                            <div class="col-md-10 col-sm-9 col-xs-8">
                                <div class="form-group">
                                    <!-- <label class="col-md-6 control-label">mi</label> -->
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span style="background: white" class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                            
                                            <input type="text" 
                                            name="set[<?=$var['id']?>]" 
                                            class="form-control"  
                                            placeholder="nama varian" 
                                            value="<?=$var['nm_varian']?>"  
                                            data-proses="set_row"
                                            required="required">
                                            
                                        </div>
                                        <span class="help-block"></span>
                                     </div>
                                </div>
                            </div>
                           
                            <div class="col-md-2 col-sm-3 col-xs-4">
                                <button type="button" id="<?=$var['id']?>" class="btn btn-xs btn-warning btn-icon-anim btn-circle rm-row-hg"><i class="fa fa-trash"></i></button>
                               
                                <button type="button" class="btn btn-xs btn-info btn-icon-anim btn-circle add-row-hg 
                                  " style="<?=($key==$c)?'':'display: none';?>"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    <?php
                            }

                        }else{
                    ?>     
                        <div class="row row-hg">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <!-- <label class="col-md-6 control-label">mi</label> -->
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span style="background: white" class="input-group-addon">
                                                <i class="fa  fa-plus"></i>
                                            </span>
                                            <input type="text" 
                                            name="new[]" 
                                            class="form-control"  
                                            placeholder="nama varian" 
                                            data-proses="new_row"
                                            required="required">
                                            
                                        </div>
                                    <span class="help-block">
                                    </span>
                                </div>
                                </div>
                                
                            </div>
                           
                            <div class="col-md-2">
                                <button type="button" class="btn btn-xs btn-warning btn-icon-anim btn-circle rm-row-hg"><i class="fa fa-trash"></i></button>
                             
                                <button type="button" class="btn btn-xs btn-info btn-icon-anim btn-circle add-row-hg"><i class="fa fa-plus"></i></button>
                               
                            </div>
                        </div>
                    <?php  
                        }
                    ?>
                </div>
            </div>
        </div>

    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
                <button type="button" class="btn  btn-danger " data-dismiss="modal">
                    <i class="fa fa-close"></i> Cancel
                </button>
            </div>
        </div>
    </div>
</form>

