<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    use app\models\Role;
?>

<form class="form-horizontal" id="form_attr" action="<?=($model->isNewRecord) ? Url::toRoute('itemattr/create') : Url::toRoute('itemattr/update').'/'.$model['id']; ?>" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="ItemAttr[nm_attr]" value="<?=$model['nm_attr']?>" placeholder="Nama Atribut" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
   
    </div>
        <hr class="light-grey-hr">
    
    <div class="form-actions">
        <div class="row">
            <div class="text-center col-md-12">
                <button type="submit" class="btn btn-success"><?=($model->isNewRecord ? 'Create' : 'Save'); ?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>