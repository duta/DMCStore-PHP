<?php

use yii\helpers\Html;
use yii\helpers\Url;

// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Penjualan');
$icon = Yii::t('app', '<i class="mdi mdi-cart-plus fa-fw"></i>');
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>

                <button class="add_od btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="fa fa-plus"></span> 
                </button>


                <button class="add_od_dropship btn m-r-5 btn-warning btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?> Dropship" data-placement="left">
                    <span class="fa fa-plus-square"></span> 
                </button>
                               

                <button class=" add_gift btn-primary m-r-5  btn-icon-anim btn-circle pull-right  waves-effect waves-light" data-toggle="tooltip" data-original-title="Tambah Hadiah" data-placement="left">
                    <span class="fa fa-gift"></span> 
                </button>                         


            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                               <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'tableOptions'=>['class'=>'table table-condensed table-hover'],
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'responsive'=>true,
                                    'responsiveWrap'=>false,
                                    'condensed'=>true,
                                    'bordered'=>false,
                                    'export'=>false,
                                    'columns' => [
                                        // action Column
                                        [
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                            // 'headerOptions' => ['width' => '100'],
                                            
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<center> {action}</center>',
                                            // {pay} {view} {print} {del}
                                            'buttons' => [
                                                'action'=> function($url,$model){
                                                    $btn_if="";

                                                    if($model->id_status==0){
                                                        $btn_if.='<li>
                                                                <a href="javascript:void(0)" id="'.$model->kd_order.'" class="pay_od">
                                                                <i class="fa fa-fw"><b>Rp</b></i>Sudah dibayar ?</a>
                                                            </li>';
                                                    }
                                                    if($model->id_status==3){
                                                        $btn_if.='<li>
                                                                <a href="javascript:void(0)" id="'.$model->kd_order.'"  class="retur_od">
                                                                    <i class="fa fa-reply fa-fw"></i>Retur ?
                                                                </a>
                                                             </li>';
                                                    }
                                                    if($model->id_status==16){
                                                        $btn_if.='<li>
                                                                <a href="javascript:void(0)" id="'.$model->kd_order.'"  class="retur_acc">
                                                                    <i class="fa fa-check-square-o fa-fw"></i>Terima barang
                                                                </a>
                                                             </li>';
                                                    }
                                                    if($model->id_status==24 || $model->id_status==0){
                                                        $btn_if.=' <li>
                                                                            <a href="javascript:void(0)" id="'.$model->kd_order.'" class="cancel_tr">
                                                                            <i class="fa fa-close fa-fw"></i>Batalkan Transaksi
                                                                            </a>
                                                                    </li>';
                                                    }

                                                    if($model->resi_photo!=null || $model->resi_photo!=''){
                                                        
                                                        $btn_if.=' <li>
                                                                            <a href="javascript:void(0);" data-filename="'.$model->resi_photo.'" id="'.$model->kd_order.'"  class="view_resi">
                                                                            <i class="fa fa-file-image-o fa-fw"></i>Lihat Bukti Resi
                                                                            </a>
                                                                    </li>';
                                                    }

                                                    return '
                                                            <div class="btn-group dropup">
                                                                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light" type="button">
                                                                        <i class="fa fa-cog m-r-5"></i>
                                                                        <span class="caret"></span>
                                                                </button>
                                                                <ul role="menu" class="dropdown-menu animated flipInX">
                                                                    <li class="disabled"><a href="javascript:void(0)">Order : #'.$model->kd_order.'</a></li>
                                                                    <li class="divider"></li>
                                                                    <li>
                                                                            <a href="javascript:void(0)" id="'.$model->kd_order.'" class="edit_od">
                                                                            <i class="fa fa-pencil fa-fw"></i>Perbarui Penjualan
                                                                            </a>
                                                                    </li>
                                                                    
                                                                    '.$btn_if.'

                                                                   
                                                                    <li>
                                                                            <a href="javascript:void(0)" id="'.$model->kd_order.'" class="detail_od"><i class="fa fa-search fa-fw"></i>Detail Penjualan</a>
                                                                    </li>

                                                                    <li>
                                                                            <a href="javascript:void(0)" id="'.$model->kd_order.'" class="change_sender"><i class="fa fa-user fa-fw"></i>Atur pengirim</a>
                                                                    </li>
                                                                    <li>
                                                                            <a href="javascript:void(0)" id="'.$model->kd_order.'" class="print_tr_od"><i class="fa fa-print fa-fw"></i>Cetak Penjualan</a>
                                                                    </li>

                                                                    <li>
                                                                            <a href="javascript:void(0)" id="'.$model->kd_order.'" class="del_od">
                                                                            <i class="fa fa-trash fa-fw"></i>Hapus Penjualan
                                                                            </a>
                                                                    </li>
                                                                   
                                                                    
                                                                </ul>
                                                            </div>';
                                                }
                                            ]
                                        ],
                                        ['class' => 'yii\grid\SerialColumn'],
                                        // Barang
                                        
                                        [
                                            'header'=> 'Barang ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                $arr=[];

                                                foreach ($model->md as $item) {
                                                    $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;
                                                }
                                               
                                                return implode(array_unique($arr), '<br>');
                                            },
                                            'filter'=>'<input type="text" class="form-control input-sm" name="VOrdersSearch[barang]" value="'.$searchModel->barang.'" placeholder="By items" >'
                                        ],
                                        [
                                            'header'=> '(QTY) Varian ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                $val='';
                                                foreach ($model->md as $item) {
                                                    $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                                                    $val.='('.$item['qty'].') '.$item->v_material->varianname.' <br>';
                                                }
                                                return $val;
                                            },
                                            // 'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                        ],

                                         // Pembeli
                                        [
                                            'header'=> 'Pembeli',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                               
                                                return $model->plg->nama;
                                            },
                                            'filter'=>Html::activeDropDownList($searchModel,"buyer",$cm,['class'=>'form-control select2 input-sm','prompt' => 'By Customers']),
                                            
                                        ],

                                         // Tagihan
                                        [
                                            'header'=> 'Tagihan',
                                            'value'=>function($model,$key,$i){
                                                if($model->is_gived!=1){
                                                    return Yii::$app->help->toRp($model->total_tagihan_buyer);
                                                }
                                            },
                                            'filter'=>'<input type="text" class="form-control input-sm" name="VOrdersSearch[total_tagihan_buyer]" value="'.$searchModel->total_tagihan_buyer.'" placeholder="By bill" >'
                                        ],

                                        // Market
                                        [
                                            'header'=> 'Market',
                                            'value'=>function($model,$key,$i){
                                                if($model->is_gived!=1){
                                                    if(isset($model->mkt->nm_market)){
                                                        return $model->mkt->nm_market;
                                                    }
                                                }
                                            },
                                         
                                            'filter'=>Html::activeDropDownList($searchModel,"id_market",$mp,['class'=>'form-control select2 input-sm','prompt' => 'By Marketplace']),
                                         
                                        ],

                                        // Status
                                        [
                                            'header'=> 'Status',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                if($model->is_gived!=1){
                                                    $resi='';
                                                    if($model->id_status==3 || $model->id_status==4){
                                                        $resi='<br>Resi : <i class="text-danger">'.$model->resi.'</i>';
                                                    }
                                                    return '<span class="label label-sm popovers '.$model->status->class.'"  data-trigger="hover" data-placement="left" data-content="'.$model->status->ket_status.'" data-original-title="'.$model->status->nm_status.'">'.$model->status->nm_status.' </span>'.$resi;
                                                }else{
                                                    return '<span class="label label-info"><i class="fa fa-gift"></i> Gift</span>';
                                                }
                                            },
                                            // 'filter'=>'<input type="text" class="form-control input-sm" name="VBmSearch[nm_sup]" value="'.$searchModel->nm_sup.'" placeholder="By suppliers" >'
                                        ],


                                        // 'kd_order',
                                        // 'tipe_transfer',
                                        // 'tipe_transaksi',
                                        // 'qty',
                                        // 'ongkir_buyer',
                                        // 'total_belanja',
                                        // 'total_tagihan_buyer',
                                        // 'buyer',
                                        // 'buyer_note:ntext',
                                        // 'id_market',
                                        // 'id_kurir',
                                        // 'resi',
                                        // 'status',
                                        // 'created_at',
                                        // 'created_by',
                                        
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
