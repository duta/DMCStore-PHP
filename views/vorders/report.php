<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use app\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$c_name=Yii::$app->controller->id;
$link=Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
$menu=Menu::find()->where(['link'=>$link])->one();
$this->title = Yii::t('app', strtolower($menu->name)); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi '.$menu->icon.' fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <?php
                if(isset($menu->parent)){
            ?>
                    <li class=""><?=$menu->parent->name?></li>
            <?php
                }
            ?>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?><?=$this->title?>
<!-- 
                <button class="add_<?=$c_name?> btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="ti-plus"></span> 
                </button> -->
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            
                             <div class="example">
                                	<form id="form-vorders-report">
                                		<div class="col-md-3">
                                			<?=Html::DropDownList("item",null,$item,['id'=>'item','class'=>'filter form-control','prompt'=>'-- Pilih Item --']);?>
                                		</div>
                                		<div class="col-md-3">
                                			<?=Html::DropDownList("cs",null,$cs,['id'=>'cs','class'=>'filter form-control','prompt'=>'-- Pilih CS --']);?>
                                		</div>
                                		<div class="col-md-6">
			                                <div class="input-daterange input-group" id="date-range" data-date-format="dd-mm-yyyy">
			                                    <input type="text" class="form-control"  name="start" value="<?=date('01-m-Y')?>" required="required" /> 
			                                    <span class="input-group-addon bg-info b-0 text-white">to</span>
			                                    <input type="text" class="form-control"  name="end" value="<?=date('t-m-Y')?>" required="required" /> 
			                                    <span class="input-group-btn bg-info b-0 text-white">
			                                    	<button type="submit" class="btn btn-success" type="button"><i class="fa fa-search fa-fw"></i> Search</button>
			                                    </span>
			                                </div>
		                                </div>
                                    </form>
                            </div>

                        </div>
                        <div class="col-md-12">
                        	<hr>
                        	<a href="javascript:;" class="btn btn-block btn-primary print_a " style="display: none"><i class="fa fa-print fa-fw"></i> Pratinjau</a>

                        	<div class="response m-t-15"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

