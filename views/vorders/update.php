<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VOrders */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vorders',
]) . $model->kd_tagihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_tagihan, 'url' => ['view', 'id' => $model->kd_tagihan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vorders-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
