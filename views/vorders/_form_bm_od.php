<?php
    use yii\helpers\Url;
    use yii\helpers\Html;

	$action=Yii::$app->controller->action->id;

    // \kartik\select2\Select2Asset::register($this);
?>
<form class="form-horizontal" id="form_odropship" role="form">

		<div class="form-group form-group-mat">

			<label class="col-md-3 control-label" style="text-align: left !important">Pilih barang </label>
			<div class="col-md-7">
			
				<?=Html::DropDownList("md[id]",null,$Barang,['id'=>'id_dropship','multiple'=>'multiple','class'=>'select2','required'=>'required','empty'=>'-- Pilih Barang --']);?>

				<span class="help-block pull-right">
					<!-- <a href="javascript:;" class="text-danger">Piih material dari lokasi lain ?</a> -->
				</span>
			</div>
			<div class="col-md-2">
				
			</div>
		</div>
	
		<div class="load_inout"></div>
		
	</div>
	<hr>
	<div class="form-actions">
		<div class="row">
			<div class="col-md-12 text-center">
				<button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
				<button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
			</div>
		</div>
	</div>
</form>