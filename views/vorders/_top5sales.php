<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>#RANK</th>
                <th>PRODUCT</th>
                <th>SOLD</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $rank=1;
                foreach ($models as $key => $model) {
                    if($key==0){
                        $class="label-success";
                    }elseif ($key==1) { 
                        $class="label-info";
                    }elseif ($key==2) {
                        $class="label-primary";
                    }elseif ($key==3) {
                        $class="label-warning";
                    }elseif ($key==4) {
                        $class="label-danger";
                    }
            ?>
                    <tr>
                        <td style="width: 1%">#<?=$rank?></td>
                        <td class="txt-oflo"><?=$model['item']?></td>
                        <td>
                            <span class="label <?=$class?> label-rouded"><?=$model['sold']?></span> 
                        </td>
                    </tr>
            <?php
                $rank++;
                }
            ?>
            
        </tbody>
    </table>
</div>