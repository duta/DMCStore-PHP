<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VOrders */

$this->title = Yii::t('app', 'Create Vorders');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vorders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vorders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
