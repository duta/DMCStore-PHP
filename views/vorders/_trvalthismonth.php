<?php
    use app\components\Help;
?>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>#NO</th>
                <th>PRODUCT</th>
                <th>QTY</th>
                <th>TOTAL OMZET</th>
                <th>TOTAL MARGIN</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $rank=1;
                foreach ($models as $key => $model) {
              
            ?>
                    <tr>
                        <td style="width: 1%"><?=$rank?></td>
                        <td class="txt-oflo"><?=$model['item']?></td>
                        <td>
                            <span class="label label-success label-rouded"><?=$model['qty_sales']?></span> 
                        </td>
                        <td class="txt-oflo"><?=Help::toRp($model['total_sales'])?></td>
                        <td class="txt-oflo"><?=Help::toRp($model['total_earnings'])?></td>
                    </tr>
            <?php
                $rank++;
                }
            ?>
            
        </tbody>
    </table>
</div>