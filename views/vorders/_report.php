<?php
use app\components\Help;
?>
<table class="table table-bordered">
		
	<thead>
		<tr>
			<th><i class="fa fa-user fa-fw"></i>CS</th>
			<th>Marketplace</th>
			<th>Tgl & waktu</th>
			<th>Barang</th>
			<th>(QTY) Varian </th>
			<th>Omzet</th>
			<th>Margin</th>
			
		</tr>
		
	</thead>
	<tbody>
	<?php
		$no=1;
		foreach ($model as $data) {
	?>
		<tr>
			<td><b><?=$data->csby->fullname;?></b></td>
			<td><?=$data->mkt->nm_market?></td>
			<td><?=Help::datetimeindoShort($data->created_at);?></td>
			<td>
				<?php
					$arr=[];

                    foreach ($data->md as $item) {
                        $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;
                    }
                   
                    echo implode(array_unique($arr), '<br>');
				?>
			</td>
			<td>
				<?php
					$val='';
                    foreach ($data->md as $item) {
                        $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                        $val.='('.$item['qty'].') '.$item->v_material->varianname.' <br>';
                    }
                    echo $val;
				?>
			</td>
			<td><?=Help::toRp($data->total_belanja)?></td>
			<td><?=Help::toRp($data->total_margin)?></td>
		</tr>
	<?php
			$no++;
		}
	?>
	</tbody>

</table>