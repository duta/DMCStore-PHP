<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="mdi mdi-av-timer fa-fw"></i>Dashboard </h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active">Dashboard </li>
        </ol>
    </div>
</div>

<input type="hidden" name="have_store" value=1>


<div class="row">
    <div class="col-sm-12">
        <div class="white-box" id="box-statistik">
            <div id="response-statistik"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="white-box" id="box-trvalthismonth">
                    <h3 class="box-title">
                        <!-- <i class="mdi mdi-av-timer fa-fw"></i> -->sales report this month
                    </h3>
                    <hr>
                    <div id="response-trvalthismonth"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="white-box" id="box-top5sales">
                    <h3 class="box-title">
                        <!-- <i class="mdi mdi-av-timer fa-fw"></i> -->top 5 sales
                    </h3>
                    <hr>
                    <div id="response-top5sales"></div>
                </div>
            </div>
             <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="white-box" id="box-top5stockout">
                    <h3 class="box-title">
                        <!-- <i class="mdi mdi-av-timer fa-fw"></i> -->top 5 stock out
                    </h3>
                    <hr>
                    <div id="response-top5stockout"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
         <div class="white-box" id="box-salestoday">
            <h3 class="box-title">sales today</h3><hr>
            <!-- <div>
                <select class="form-control">
                    <option>Lampu smartcharge</option>
                </select>
            </div> -->
            <div id="morris-donut-chart"></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="white-box" id="box-salesthisyear">
            <h3 class="box-title">sales this year</h3><hr>
            <div class="container-fluid">
                <div class="col-md-6">
                   <!--  <select class="form-control">
                    <option>Lampu smartcharge</option>
                    </select> -->
                </div>
                <div class="col-md-6">
                    <ul class="list-inline text-right" id="legend-chart-year">
                    
                    </ul>
                </div>
            </div>
                        
            <div id="morris-bar-chart"></div>        
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
        <div class="white-box" id="box-salesthisweek">
            <h3 class="box-title">sales this week</h3><hr>
            <div class="container-fluid">
                <div class="col-md-6">
                   <!--  <select class="form-control">
                    <option>Lampu smartcharge</option>
                    </select> -->
                </div>
                <div class="col-md-6">
                    <ul class="list-inline text-right" id="legend-chart">
                    <!--    <li><h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>12 watt</h5> </li> 
                        <li><h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>9 watt</h5> </li>
                        <li><h5><i class="fa fa-circle m-r-5" style="color: #6164C1;"></i>5 watt</h5> </li> -->
                    </ul>
                </div>
            </div>
                        
            <div id="morris-area-chart2"></div>        
        </div>
    </div>
   <!--  <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" >
       <div class="white-box" id="box-salestoday">
            <h3 class="box-title">sales today</h3><hr>
            <div>
                <select class="form-control">
                    <option>Lampu smartcharge</option>
                </select>
            </div>
            <div id="morris-donut-chart"></div>
        </div>
    </div> -->
</div>

<!-- <div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <i class="mdi mdi-av-timer fa-fw"></i>Lihat Statistik

                
            </h3>
             <hr>
        </div>
    </div>
</div> -->