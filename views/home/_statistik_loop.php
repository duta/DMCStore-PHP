 <?php
    use app\models\Statistik;
    use app\components\Help;
    $user=Yii::$app->user->identity;
 ?>

 <div class="row row-in">
    <?php
        foreach ($models as $key => $val) {
    ?>

    <div class="col-lg-4 col-sm-6 row-in-br">
        <ul class="col-in">
            <li>
                <span class="circle circle-md bg-<?=$val['bg_class']?>"><i class="mdi <?=$val['icon']?>"></i></span>
            </li>
            <li class="col-last">
                <h4 class=" text-right m-t-15 font-bold"> 
                    <?=($val['type']=='rp')?'Rp ':'';?><span class="counter"><?=Statistik::getScalar($val['sql']."  WHERE store_id=".$user->store_id);?></span>
                </h4>
                
            </li>
            <li class="col-middle">
                <h4><?=$val['nama']?></h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-<?=$val['bg_class']?>" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only">100% Complete (success)</span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <?php
        }
    ?>
</div>