 <div class="row row-in">
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-danger"><i class="mdi mdi-puzzle"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15">0</h3>
                        </li>
                        <li class="col-middle">
                            <h4>Total Barang</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-info"><i class="mdi mdi-timer-sand-empty"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15">0</h3>
                        </li>
                        <li class="col-middle">
                            <h4>Stok Habis</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6  b-0">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-warning"><i class="mdi mdi-basket-fill"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15">0</h3>
                        </li>
                        <li class="col-middle">
                            <h4>Barang masuk</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-6 row-in-br">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-success"><i class=" mdi mdi-basket-unfill"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15">0</h3>
                        </li>
                        <li class="col-middle">
                            <h4>Barang Terjual</h4>
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>