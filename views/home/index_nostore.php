<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="mdi mdi-av-timer fa-fw"></i>Dashboard </h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active">Dashboard </li>
        </ol>
    </div>
</div>

<input type="hidden" name="have_store" value=0>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box" id="box-statistik">
            <div class="alert alert-danger">Anda belum punya toko </div>
        </div> 
    </div>
</div>
