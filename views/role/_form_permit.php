<div class="row">
	<div class="col-md-12">
		<form id="form-permit">
			<center>
				<div class="checkbox checkbox-success">
				    <input id="check-all" value="all" type="checkbox">
				    <label for="check-all" > Check all</label>
				</div>
				<hr>
			</center>

			<input type="hidden" name="role_id" value="<?=$role_id?>">
		<?php
			foreach ($menus as $menu) {
		?>
			<div class="checkbox checkbox-danger">
			    <input id="checkbox-<?=$menu->id?>" name="menu[]" value="<?=$menu->id?>" class="checkbox_parent" type="checkbox" <?=isset($model[$menu->id])?'checked=checked':'';?> >
			    <label for="checkbox-<?=$menu->id?>"> <?=$menu->name;?></label>
			</div>
			<div class="row">
				<?php
					foreach (array_chunk($menu->child_pure, 3) as $key) {
				?>	
					<div class="col-md-3">
						
						<?php
							foreach ($key as $c) {
						?>	
							<div class="checkbox checkbox-warning">
								<i  class="fa fa-chevron-right fa-fw"></i>&nbsp;&nbsp;&nbsp;
							    <input id="checkbox-<?=$c->id?>" name="menu[]" class="checkbox_child-<?=$c->parent_id?>" type="checkbox" value="<?=$c->id?>" <?=isset($model[$c->id])?' checked=checked':'';?> >
							    <label for="checkbox-<?=$c->id?>"> <?=$c->name;?></label>
							</div>
						<?php
							}
						?>
					</div>
				<?php
					}
				?>
				</div>
			<hr>
		<?php
			}
		?>
		<!-- <hr> -->
		<center>
			<button type="submit" class="btn btn-info btn-block"><i class="fa fa-check fa-fw"></i>Simpan</button>
		</center>
		</form>
	</div>
	
</div>

