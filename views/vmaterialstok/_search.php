<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VMaterialStokSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vmaterial-stok-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kd_stok') ?>

    <?= $form->field($model, 'kd_tagihan') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'varianname') ?>

    <?php // echo $form->field($model, 'id') ?>

    <?php // echo $form->field($model, 'kode') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'hg') ?>

    <?php // echo $form->field($model, 'id_attr') ?>

    <?php // echo $form->field($model, 'id_varian') ?>

    <?php // echo $form->field($model, 'stok_awal') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'qty_in') ?>

    <?php // echo $form->field($model, 'qty_out') ?>

    <?php // echo $form->field($model, 'stok_akhir') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
