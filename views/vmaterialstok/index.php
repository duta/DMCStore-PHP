<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Stok Barang'); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi mdi-file fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="">Laporan</li>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Laporan <?=$this->title?>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                          <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'tableOptions'=>['class'=>'table table-condensed table-hover'],

                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'kd_stok',
                                        // 'kd_tagihan',
                                        // 'nama',
                                          // Nama
                                        [
                                            'header'=>'Nama',
                                            'attribute'=>'nama',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return "<a href='javascript:;' class='foto_item' id='".$model->kode."'>".ucfirst($model->nama)."</a>";
                                            },
                                            // 'filter'=>'<input type="text" class="form-control input-sm" name="VMaterialSearch[nama]" value="'.$searchModel->nama.'" placeholder="By Barang" >',
                                            'enableSorting'=>true,
                                        ],
                                        // 'fullname:ntext',
                                        'varianname',
                                        // 'id',
                                        // 'kode',
                                        // 'foto',
                                        // 'hg',
                                        // 'id_attr',
                                        // 'id_varian',
                                        // 'stok_awal',
                                        // 'created_by',
                                        // 'created_at',
                                        // 'qty_in',
                                        // 'qty_out',
                                        'stok_akhir',

                                        // ['class' => 'yii\grid\ActionColumn'],
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>