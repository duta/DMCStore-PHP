<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VMaterialStok */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vmaterial-stok-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_stok')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_tagihan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fullname')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'varianname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hg')->textInput() ?>

    <?= $form->field($model, 'id_attr')->textInput() ?>

    <?= $form->field($model, 'id_varian')->textInput() ?>

    <?= $form->field($model, 'stok_awal')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'qty_in')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qty_out')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'stok_akhir')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
