<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VMaterialStok */

$this->title = $model->kd_stok;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vmaterial Stoks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vmaterial-stok-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->kd_stok], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->kd_stok], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_stok',
            'kd_tagihan',
            'nama',
            'fullname:ntext',
            'varianname',
            'id',
            'kode',
            'foto',
            'hg',
            'id_attr',
            'id_varian',
            'stok_awal',
            'created_by',
            'created_at',
            'qty_in',
            'qty_out',
            'stok_akhir',
        ],
    ]) ?>

</div>
