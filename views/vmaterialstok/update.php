<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VMaterialStok */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vmaterial Stok',
]) . $model->kd_stok;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vmaterial Stoks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_stok, 'url' => ['view', 'id' => $model->kd_stok]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vmaterial-stok-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
