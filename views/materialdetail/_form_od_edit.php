<form id="form_od_edit">
    <?php
    use yii\helpers\Html;
    use app\components\Help;


        foreach ($model->md as $od) {
    ?>  
        <input type="hidden" name="md_id[]" value="<?=$od->id?>">
         <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><?=$od->v_materialbyinv['kd_tagihan_view']?></span>

                        <b class="form-control"><?=$od->v_materialbyinv['fullname']?></b>
                        <span class="input-group-addon">Stok : <?=$od->v_materialbyinv['stok_akhir']?></span>
                    </div>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group">
                    <input type="hidden" name="hgmod[<?=$od['id']?>]" value="<?=$od['harga_mod']?>">

                    <input type="text" id="hgsat_<?=$od['id']?>" placeholder="Harga satuan" name="hgsat_jual[<?=$od['id']?>]" class="form-control kal_or hgsat MaskMoney" required="required"  data-id="<?=$od['id']?>" value="<?=Help::toRp($od['hgsat_jual'])?>"> 

                </div>      
            </div>

            <div class="col-md-2">
               <div class="form-group">
                    <!-- <label class="col-md-1 control-label">QTY</label> -->
                        <input  id="qty_<?=$od['id']?>"  data-id="<?=$od['id']?>" type="text"  value="<?=$od['qty']?>" data-value="<?=$od['qty']?>" name="qty[<?=$od['id']?>]" class="touchspin" data-bts-button-down-class="btn btn-default btn-outline " data-bts-button-up-class="btn btn-default btn-outline" required="required" readonly="readonly" >
                        <span class="help-block">
                        </span>
                </div>
            </div>
        </div>

     <?php
        }
    ?>
    <hr>
        <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                    <div class="input-group" >
                        <span class="input-group-addon" style="background: white">Pelanggan</span>
                        <?=Html::DropDownList('md[id_c]',$model->plg->id,$Customers,['id'=>'c_id','class'=>'form-control select2','prompt'=>'-- Pilih Pelanggan --'])?>

                    </div>
               </div>
            </div>

           

            <div class="col-md-6">
               <div class="form-group">
                    <div class="input-group" >
                        <span class="input-group-addon" style="background: white">Marketplace</span>
                        <?=Html::DropDownList("md[id_market]", $model->mkt->id,$Market,['id'=>'id_market','class'=>'form-control select2','prompt'=>'-- Pilih Marketplace --']);?>
                    </div>
               </div>
            </div>
        </div>
        <div class="row">
             <div class="col-md-6">
               <div class="form-group">
                    <div class="input-group" >
                        <span class="input-group-addon" style="background: white">Kurir</span>
                        <?=Html::DropDownList("md[id_kurir]", $model->kurir->id,$Kurir,['id'=>'id_kurir','class'=>'form-control select2','data-allow-clear'=>true,'prompt'=>'-- Pilih Kurir --']);?>
                    </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                    <div class="input-group" >
                        <span class="input-group-addon" style="background: white">Biaya kirim</span>
                        <input type="text" id="ongkir_buyer" placeholder="Biaya kirim" name="md[ongkir_buyer]" class="form-control MaskMoney kal_or" value="<?=Help::toRp($od->ongkir_buyer);?>"  > 

                    </div>
               </div>
            </div>

        </div>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
                <button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            </div>
        </div>
    </div>
</form>