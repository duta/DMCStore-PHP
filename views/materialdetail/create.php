<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MaterialDetail */

$this->title = Yii::t('app', 'Create Material Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Material Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
