<?php
	$uri=explode("=", $_SERVER['REQUEST_URI']);
	
?>
<form id="form-checked">
<div class="table-responsive">
<table class="table  table-hover product-overview" id="MaterialDetailTable">
	<thead>
		<tr>
		
			<th>No</th> 
			
			<!-- <th>Pembeli</th> -->
			
			<th>Barang</th>
			<th>Qty</th>
			<?php
				if(end($uri)=='od'){
			?>
				<th>Pembeli</th>
				<th>Tagihan</th>
			<?php
				}else{
			?>
				<th>Pengiriman</th>
			<?php
				}

			?>
			<th>Market</th>
			<th>Status</th>
			
			<?php
				if(end($uri)=='bk'){
			?>
			<th style="background: grey" class=" text-center" data-orderable="false">
				<div class="checkbox checkbox-success checkbox-circle" style="margin:0px !important">
                    <input name="select_all" id="all" type="checkbox" >
                    <label for="all"></label>
                </div>
			</th>
			<?php } ?>
			<th data-orderable=false ><center><i class="fa fa-cogs"></i></center></th>
			
		</tr>
	</thead>
	<tbody>
		<?php
			$no=1;
			foreach ($data as $item) {
		?>
				<tr id="<?=$item['kd_tagihan']?>" class="odd gradeX">
					
					<td><?=$no?></td>
										
					<!-- <td><?=$item->Buyer['nama']?></td> -->

					<td>
						<?php
							$no_item=1;
							foreach ($item->MD as $md_item) {
								echo $no_item.'. '.$md_item->VMaterial['nama'].'<br>';
								$no_item++;
							}
						?>
					</td>
					<td>
						<?php
							foreach ($item->MD as $md_q) {
								echo $md_q['qty'].'<br>';
							}
						?>
					</td>
					<!-- <td><?=$item['qty']?></td> -->
					<?php
						if(end($uri)=='od'){
					?>
					<td><?=$item->Buyer['nama']?></td>
					<td><?=Yii::app()->help->toRp($item['total_tagihan_buyer'])?></td>
					<?php
						}else{
					?>
						<td>
							<b><?=$item->Buyer['nama']?></b><br>
							<?=$item->Buyer['prov']?>, <?=$item->Buyer['kota_kab']?>, <?=$item->Buyer['kec']?>	<br>
							<i class="text-danger"><?=(end($uri)=='bk')? 'NO. RESI :'.$item->resi:'';?>	</i>
						</td>
					<?php
						}
					?>

					<td><?=$item->Market['nm_market']?></td>
					<td>
						<span class="label label-sm popovers <?=$item->Status->class;?>"  data-trigger="hover" data-placement="left" data-content="<?=$item->Status['ket_status']?> " data-original-title="<?=$item->Status->nm_status;?>">
							<?=$item->Status->nm_status;?> </span>
					</td>
					<?php 
					if(end($uri)=='bk'){
						?>
						<td style="background: grey" align="center">
							<div class="checkbox checkbox-danger checkbox checkbox-circle">
                                <input id="checkbox_<?=$item['kd_tagihan']?>" class="checkboxes" name="checked[]" value="<?=$item['kd_tagihan']?>" type="checkbox" >
                                <label for="checkbox_<?=$item['kd_tagihan']?>"></label>
                            </div>
						</td>
					<?php } ?>
					<td>
						<center>
						<?php
							if(end($uri)=='od'){
						?>
							<button type="button" class="btn btn-primary btn-pay btn-circle btn-sm "  data-toggle="tooltip" data-original-title="Sudah dibayar ?" data-placement="top" id="<?=$item['kd_tagihan']?>"><i class="fa">Rp</i></button>
						
						<?php }?>
							<button type="button" class="btn btn-info btn-circle btn-sm detail_materialdetail" id="<?=ltrim($item['kd_tagihan'],'#')?>" data-toggle="tooltip" data-original-title="Detail Transaksi" data-placement="top"><i class="fa fa-search"></i></button>
							<button type="button" class="btn btn-success btn-circle btn-sm cetak_transaksi" id="<?=$item['kd_tagihan']?>" data-toggle="tooltip" data-original-title="Bukti Transaksi" data-placement="top"><i class="fa fa-print"></i></button>
						
							<button type="button" class="btn btn-danger btn-circle btn-sm del_materialdetail" id="<?=$item['kd_tagihan']?>" data-toggle="tooltip" data-original-title="Hapus Transaksi" data-placement="top"><i class="fa fa-trash"></i></button>
						</center>

					</td>
					
					
				</tr>
		<?php
			$no++;
			}
		?>
	</tbody>
</table>
</div>
</form>