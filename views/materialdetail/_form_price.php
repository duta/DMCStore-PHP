<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use app\components\Help;
?>
<form id="form-price">

        <input type="hidden" name="price[id]" value="<?=$model->id?>">
        <input type="hidden" name="price[kd]" value="<?=$model->kode?>">
        <div class="well" style="padding: 5px!important">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <h3>
                    <b>Harga Modal : </b><b class="text-danger"><?=Help::toRp($model->harga_mod)?></b>
                    </h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group ">
                    <label class="col-md-12 control-label " style="text-align: left !important">Harga retail</label>
                    <div class="col-md-12">
                        <input type="text" name="price[hgsat_retail]" class="form-control MaskMoney hrg-s" placeholder="retail price" value="<?=$model->hgsat_retail;?>" required="required">
                        <span class="help-block ">
                            
                        </span>
                    </div>
                </div>
            </div>

            <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group ">
                    <label class="col-md-12 control-label " style="text-align: left !important">Harga reseller</label>
                    <div class="col-md-12">
                        <input type="text" name="price[hgsat_reseller]" class="form-control MaskMoney hrg-res" placeholder="reseller price" value="<?=$model->hgsat_reseller;?>" required="required">
                        <span class="help-block ">
                        </span>
                    </div>
                </div>
            </div> -->
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <a href="#" class="pull-right btn btn-primary btn-xs add-hg"><i class="fa fa-plus fa-fw"></i>GROSIR</a>
                <div class="hg-wrap" style="<?=($model['is_grosir']==1)?'':'display: none';?>">
                    <h6 class="text-center font-bold"><i class="icon-info"></i> Harga Grosir 
                        <!-- <a class="btn btn-danger btn-xs pull-right">Hapus Harga grosir</a> -->
                    </h6>
                    <hr class="light-grey-hr">
                        
                     <?php
                        if(count($model->grosir)>0){
                            $c=count($model->grosir)-1;

                            foreach ($model->grosir as $key => $hg_item) {
                    ?>
                        <div class="row row-hg">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <!-- <label class="col-md-6 control-label">mi</label> -->
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <span style="background: white" class="input-group-addon">QTY :</span>
                                            <input type="number" name="set_qty[<?=$hg_item['id']?>]" class="form-control input-qty "  placeholder="jumlah minimum beli" value="<?=$hg_item['qty']?>"
                                            data-proses="set_row">
                                            <span style="background: white" class="input-group-addon input-price">Price :</span>
                                            <input type="text" name="set_price[<?=$hg_item['id']?>]" class="form-control input-price MaskMoney hrg-g" placeholder="Harga per barang"  value="<?=$hg_item['harga']?>" 
                                            data-proses="set_row">
                                        </div>
                                    <span class="help-block">
                                    </span>
                                </div>
                                </div>
                                
                            </div>
                           
                            <div class="col-md-2">
                                <button type="button" id="<?=$hg_item['id']?>" class="btn btn-xs btn-warning btn-icon-anim btn-circle rm-row-hg" ><i class="fa fa-trash"></i></button>
                                <button type="button" style="<?=($key==$c)?'':'display: none';?>" class="btn btn-xs btn-info btn-icon-anim btn-circle add-row-hg"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    <?php
                            }

                        }else{
                    ?>     
                    <div class="row row-hg">
                        <div class="col-md-10">
                            <div class="form-group">
                                <!-- <label class="col-md-6 control-label">mi</label> -->
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span style="background: white" class="input-group-addon ">QTY :</span>
                                        <input type="number" name="new_qty[]" class="form-control input-qty"  placeholder="jumlah minimum beli" data-proses="new_row">
                                        <span style="background: white" class="input-group-addon input-price">Price :</span>
                                        <input type="text" name="new_price[]" class="form-control MaskMoney hrg-g" placeholder="Harga per barang" data-proses="new_row">
                                    </div>
                                    <span class="help-block">
                                    </span>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-xs btn-warning btn-icon-anim btn-circle rm-row-hg"><i class="fa fa-trash"></i></button>
                            <button type="button" class="btn btn-xs btn-info btn-icon-anim btn-circle add-row-hg"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <?php  
                        }
                    ?>
                </div>
            </div>
        </div>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
                <button type="button" class="btn  btn-danger btn-backtovarian" id="<?=$model->kd_tagihan?>" >
                    <i class="fa fa-mail-reply"></i> Kembali
                </button>
            </div>
        </div>
    </div>
</form>

