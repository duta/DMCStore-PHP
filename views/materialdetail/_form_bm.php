
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
?>
<div class="form-horizontal">
<div class="row ">
<?php
	foreach ($Barang as $item) {
?>	
        <div class="col-md-7">
            <div class="form-group ">
                <div class="col-md-12">
                    <div class="input-group">
                            <b class="form-control input-sm"><?=$item['nama']?> : <u class="text-danger"><?=$item['varianname']?></u></b>
                            <span class="input-group-addon"> <b> = </b></span>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-12">
                    <input type="text" placeholder="Harga beli " class="form-control MaskMoney kalkulasi input-sm" id="hb_<?=$item['id']?>" data-id="<?=$item['id']?>" name="md[harga_beli][<?=$item['id']?>]"  >
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12">
                    <input type="number" placeholder="QTY " class="form-control  kalkulasi input-sm" id="qty_<?=$item['id']?>" data-id="<?=$item['id']?>" name="md[qty][<?=$item['id']?>]"  >

                    <input type="hidden" id="tpb_<?=$item['id']?>" class="tpb" value=0 data-id="<?=$item['id']?>">

                    <input type="hidden" name="md[hgmod][<?=$item['id']?>]" id="hgmod_<?=$item['id']?>" class="hgmod" value=0 data-id="<?=$item['id']?>">


                </div>
            </div>
        </div>

<?php 
	}
?>
</div>
<hr style="border-color: rgb(0, 0, 0)">
<div class="row m-b-10">
    <div class="col-md-12">
         <div class="form-group">
            <div class="col-xs-3 col-sm-2 col-md-2">
                <input type="number" placeholder="PPN(%)" id="ppn_"  class="form-control kalkulasi input-sm " name="md[ppn]" >
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2">
                  <input type="number" placeholder="PPH(%)" id="pph_"  class="form-control kalkulasi input-sm " name="md[pph]" >
            </div>
            <div class="col-xs-3 col-sm-2 col-md-2">
                <input type="number"  placeholder="BM(%)" class="form-control kalkulasi input-sm " id="bm_"  name="md[bm]" >
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                <input type="text"  placeholder="DLL(Rp)" class="form-control kalkulasi input-sm MaskMoney " id="dll_"  name="md[dll]" >
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3">
                 <input type="text"  placeholder="Biaya kirim(Rp)" class="form-control kalkulasi input-sm MaskMoney " id="ongkir_"  name="md[ongkir]" >
            </div>
        </div>
    </div>
</div>

<input type="hidden"  id="hatot_dasar"  name="md[hatot_dasar]" >

<div class="row">
    <div class="col-md-7">
        <div class="form-group">   
            <div class="col-md-6">
                <div class="input-group input-group-sm">
                    <span class="input-group-btn"> 
                        <button class="btn btn-info add_sup" type="button"><i class="fa fa-plus"></i></button> 
                    </span>
                    <?=Html::DropDownList("md[id_sup]",$model['id_sup'],$Supplier,['id'=>'id_sup','class'=>'form-control select2 ','prompt'=>'-- Pilih Supplier --']);?>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="input-group input-group-sm">
                    <span class="input-group-btn"> 
                        <button class="btn btn-primary add_fw" type="button"><i class="fa fa-plus"></i></button> 
                    </span>
                    <?=Html::DropDownList("md[id_fw]",$model['id_fw'],$Forwarder,['id'=>'id_fw','class'=>'form-control select2 input-sm','prompt'=>'-- Pilih Forwarder --']);?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group">
            <!-- <label class="col-md-12 control-label" style="text-align: left">TOTAL HARGA</label> -->
            <div class="col-md-12">
                <input type="text" placeholder="TOTAL PEMBELIAN" name="md[hatot]" class="form-control input-sm" id="ht_" readonly>        
            </div>
        </div>
    </div>
</div>
<hr style="border-color: rgb(0, 0, 0)">
<?php
    foreach ($Barang as $itemod) {
?>  
<div class="row">
    <div class="col-md-12">
        <div class="form-group">   
            <div class="col-md-12">
                <div class="input-group">
                    <span class="input-group-addon" style="width:25%"><span class="pull-left"><?=$itemod->varianname?></span> <span class="pull-right font-bold">:</span></span>
                    <input type="text" placeholder="harga beli + (ongkos/total qty)" name="" style="background: white" class="form-control " id="hpb_<?=$itemod['id']?>" data-id="<?=$itemod['id']?>" readonly>    
                   
                    <span class="input-group-addon">=</span>
                    <input type="text" placeholder="Harga modal" name="" style="background: white" class="form-control " id="modpb_<?=$itemod['id']?>" data-id="<?=$itemod['id']?>" readonly>
                </div>             
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="row">
    
</div>
</div>
