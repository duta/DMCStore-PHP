<?php

	foreach ($Barang as $item) {
?>	

	 <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                   <!--  <label class="col-md-12" >Nama Barang
                        <span class="help"> </span>
                    </label> -->
                    <div class="col-md-12">
                        <div class="input-group">
                            <b class="form-control"><?=$item['nama']?></b>
                            <span class="input-group-addon">Stok : <?=$item['stok_akhir']?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" id="hgsat_<?=$item['id']?>" placeholder="Harga satuan" name="MaterialDetail[hgsat_jual][<?=$item['id']?>]" class="form-control kal_or hgsat MaskMoney" required="required"  data-id="<?=$item['id']?>"> 

                    <input type="hidden" id="price_total_<?=$item['id']?>" class="price_total" data-id="<?=$item['id']?>"> 
                </div>      
            </div>
            <div class="col-md-2">
               <div class="form-group">
                    <!-- <label class="col-md-1 control-label">QTY</label> -->
                    <div class="col-md-12">
                     <input  id="qty_<?=$item['id']?>"  data-id="<?=$item['id']?>" type="text"  value="1" name="MaterialDetail[qty][<?=$item['id']?>]" class="touchspin" data-bts-button-down-class="btn btn-default btn-outline " data-bts-button-up-class="btn btn-default btn-outline" required="required" readonly="readonly">
                    <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>

 <?php
	}
?>
        <hr style="margin-top: 5px !important;margin-bottom: 5px !important;border-color:black !important">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <?=CHtml::DropDownList("customer[id_c]", null,Chtml::listData($Customers, 'id', 'nama'),['id'=>'id_c','class'=>'form-control select2me','prompt'=>'-- Pilih Pelanggan --']);?>
            </div>
           <!--  <div class="col-md-3">
                <a href="javascript:;" class="btn btn-block new-c btn-primary">Pelanggan baru ?</a>
            </div> -->
        </div>
        
            <hr>
            <h4><i class="mdi mdi-account-edit fa-fw"></i>Data Pembeli</h4><hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <!-- <label class="col-md-12" >Nama Pembeli 
                            <span class="help"> </span>
                        </label> -->
                        <div class="col-md-12">
                            <input type="text" id="c_nama" name="customer[nama]" class="form-control" placeholder="Nama Lengkap Pembeli" required="required"> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <!-- <label class="col-md-12 control-label" style="text-align: left">Kurir</label> -->
                        <div class="col-md-12">
                          <?=CHtml::DropDownList("customer[id_kurir]", null,Chtml::listData($Kurir, 'id', 'service','kurir'),['id'=>'id_kurir','class'=>'form-control select2me','data-allow-clear'=>true,'empty'=>'-- Pilih Kurir --']);?>
                        </div>
                            <span class="help-block">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                     <div class="form-group">
                        <!-- <label class="col-md-12" >Telepon/Handphone
                            <span class="help"> </span>
                        </label> -->
                        <div class="col-md-12">
                            <input type="number" id="c_tlp" name="customer[tlp]" class="form-control" placeholder="Telepon/Handphone Pembeli"> 
                        </div>
                    </div>   
                </div>
                <div class="col-md-6">
                     <div class="form-group">
                        <!-- <label class="col-md-12 control-label" style="text-align: left">Dari Market</label> -->
                        <div class="col-md-12">
                          <?=CHtml::DropDownList("customer[id_market]", null,Chtml::listData($Market, 'id', 'nm_market'),['id'=>'id_market','class'=>'form-control select2me','empty'=>'-- Pilih Marketplace --']);?>
                        </div>
                            <span class="help-block">
                            </span>
                    </div>
                       
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                      <div class="form-group">
                        <!-- <label class="col-md-12" >Email pembeli 
                            <span class="help"> </span>
                        </label> -->
                        <div class="col-md-12">
                            <input type="email" id="c_email" name="customer[email]" class="form-control" placeholder="E-mail Pembeli"> 
                        </div>
                    </div>
                    
                   
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                         <!-- <label class="col-md-12 control-label" style="text-align: left">Biaya kirim</label> -->
                        <div class="col-md-12">
                            <input t ype="text" id="ongkir_buyer" placeholder="Biaya kirim" name="customer[ongkir_buyer]" class="form-control MaskMoney kal_or"  > 
                        </div>
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <!-- <label class="col-md-12" >Alamat Pengiriman 
                                <span class="help"> </span>   
                        </label> -->
                        <div class="col-md-12">
                            <textarea name="customer[alamat]" class="form-control" id="c_alamat" rows="6" placeholder="Alamat Lengkap Pembeli (pengiriman) "></textarea>
                        </div>  
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <!-- <label class="col-md-12" >Total 
                                <span class="help"> </span>   
                        </label> -->
                        <div class="col-md-12 m-b-5">
                             <input type="text" id="c_prov" name="customer[prov]" class="form-control" placeholder="Provinsi"> 
                        </div>  
                         <div class="col-md-12 m-b-5">
                             <input type="text" id="c_kota" name="customer[kota]" class="form-control" placeholder="Kota/Kab"> 
                        </div>  
                         <div class="col-md-12">
                             <input type="text" id="c_kec" name="customer[kec]" class="form-control" placeholder="Kecamatan"> 
                        </div>  
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <!-- <label class="col-md-12" >Catatan Pembeli
                                <span class="help"> </span>   
                        </label> -->
                        <div class="col-md-12">
                            <textarea name="customer[c_buyer_note]" class="form-control" id="c_buyer_note" rows="3" placeholder="Catatan Pembeli"></textarea>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <!-- <label class="col-md-12" >Catatan Pembeli
                                <span class="help"> </span>   
                        </label> -->
                         <div class="col-md-12">
                            <input name="customer[pay_total]" placeholder="TOTAL" class="form-control" id="total-pay" readonly="" >
                        </div> 
                    </div>
                </div>
            </div>
        
