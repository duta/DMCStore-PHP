<form class="form-horizontal" id="form_shipping" role="form">
	<?php
		foreach ($models as $model) {
	?>

	<!-- <div class="form-body b-t-1"> -->

		<div class="row">

		<div class="col-md-12">
			<div class="container-fluid">
				<div class="col-md-6">
					<div class="well">	
						<b><?=$model->plg['nama']?></b><br>
						<span class="m-l-20"><?=$model->plg['prov']?>, <?=$model->plg['kota_kab']?>, <?=$model->plg['kec']?></span><br>
						<b>Jasa Pengiriman</b><br>
						<span class="m-l-20"><?=$model->kurir['nm_kurir']?> ( <?=$model->kurir['layanan']?> )</span>
						<br>
						<hr>

						<input type="text" name="inv[<?=$model['kd_order']?>][resi]" maxlength="150" class="form-control" placeholder="Nomor Resi"  required="required">
						<br>
						<input type="file" name="resi_photo_<?=$model['kd_order']?>" class="MultiFile">

					</div>
				</div>
				<div class="col-md-6">
					<div class="well">
					<b>Barang :</b><br>
					<?php
							$no=1;
							foreach ($model->md as $item) {
						?>
							<?=$no.'. (#'.$item->kd_tagihan.') '.$item->v_material['fullname']?> <br>
						<?php
							$no++;
							}
						?>
					</div>
				</div>
			</div>
		</div>
	

		
	
		</div>
	<!-- </div> -->

	<hr style="border-color: black !important;margin-top:0px !important">

	<?php
		}
	?>
	<div class="form-actions">
		<div class="row">
			<div class="col-md-12">
				<center>
					<button type="submit" class="btn btn-success green">Konfirmasi</button>
					<button type="button" class="btn btn-danger red" data-dismiss="modal">Cancel</button>
				</center>
			</div>
		</div>
	</div>
</form>