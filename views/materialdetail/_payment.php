<form id="form-paid">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="white-box">
            <h6 style="text-transform:normal !important" class="box-title m-b-0"><center>NO. INVOICE : <b class="text-danger"><?=$model['kd_tagihan']?></b></center></h6><hr>
            <!-- <p class="text-muted m-b-30 font-13">Masukan data anda sebagai pembeli </p> -->
            <div class="form-horizontal">
                
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-12">
                            <h5>
                                <b class="text-info pull-left">DETAIL TRANSAKSI & TAGIHAN</b>
                                <b class=" pull-right">STATUS TRANSAKSI : <span class="label <?=$model->status['class']?>"><?=$model->status['nm_status']?></span></b>
                            </h5>
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    
                    
                    <div class="form-group">
                        <label class="col-md-3" >Waktu Transaksi
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=Yii::$app->help->datetimeindoShort($model['created_at'])?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Pembeli
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=$model->plg['nama']?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Metode Pembayaran
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            Transfer
                        </label>
                    </div>   
                    <div class="form-group">
                        <label class="col-md-3" >Produk
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=$model->v_material['nama']?> 
                        </label>
                    </div> 
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-md-3" >Harga satuan
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=Yii::$app->help->toRp($model->v_material['hgsat_jual'])?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Kuantitas
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=$model['qty']?> <?=$model->v_material['satuan']?> 
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Biaya kirim
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            Rp 0
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Total tagihan
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=Yii::$app->help->toRp($model['total_pay'])?>
                        </label>
                    </div>    
                </div>

                <div class="col-md-12">
                <div class="well">
                    <div class="form-group"><center>

                        <label class="col-md-12" >
                            Lakukan pembayaran sebesar :<br>
                            <h1 class="text-primary bold"><strong><?=Yii::$app->help->toRp($model['total_pay'])?></strong></h1><br>
                            Pembayaran dapat ditransfer ke nomor rekening a/n PT Duta Media Cipta
                            <span class="help"> </span>
                        </label>
                        <div class="col-md-12 text-center bold">
                           BANK BRI : 00000000000000000000000000
                        </div></center>
                    </div>
                </div>  
                <hr>

                <div class="form-group">
                    <label class="col-md-6" >Bukti Pembayaran
                        <span class="help"> </span>
                    </label>
                    <div class="col-md-6">
                        <input type="hidden" name="md_id" value="<?=$model['id']?>">
                        <input type="hidden" name="inv" value="<?=$model['kd_tagihan']?>">

                        <input type="file" name="paid_photo" class="MultiFile">
                   </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12">
                        <center>
                            <button type="submit" class="btn btn-info payment-conf" > 
                            Konfirmasi Pembayaran</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"> Bayar nanti</button>
                        </center>
                    </div>
                </div>
                </div>
          </div>
        </div>
    </div>
</div>
</form>
