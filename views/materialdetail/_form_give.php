<?php
use yii\helpers\Html;
	foreach ($Barang as $item) {
?>	

	 <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                   <!--  <label class="col-md-12" >Nama Barang
                        <span class="help"> </span>
                    </label> -->
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon"><?=$item['kd_tagihan_view']?></span>
                            <b class="form-control"><?=$item['fullname']?></b>
                            <span class="input-group-addon">Stok : <?=$item['stok_akhir']?></span>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="inv_<?=$item['id']?>"  name="inv[<?=$item['kd_md']?>]" value="<?=$item['kd_tagihan']?>" data-id="<?=$item['id']?>"> 

            <div class="col-md-2">
               <div class="form-group">
                    <!-- <label class="col-md-1 control-label">QTY</label> -->
                    <div class="col-md-12">
                     <input  id="qty_<?=$item['id']?>"  data-id="<?=$item['id']?>" type="text"  value="1" name="qty[<?=$item['kd_md']?>]" class="touchspin" data-bts-button-down-class="btn btn-default btn-outline " data-bts-button-up-class="btn btn-default btn-outline" required="required" readonly="readonly">
                    <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>

 <?php
	}
?>

        <hr style="margin-top: 5px !important;margin-bottom: 5px !important;border-color:black !important">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <?=Html::DropDownList('customer[id_c]',null,$Customers,['id'=>'c_id','class'=>'form-control select2','prompt'=>'-- Pilih Penerima --'])?>
            </div>
           <!--  <div class="col-md-3">
                <a href="javascript:;" class="btn btn-block new-c btn-primary">Pelanggan baru ?</a>
            </div> -->
        </div>
        
            <hr>
            <h4><i class="mdi mdi-account-edit fa-fw"></i>Data Penerima</h4><hr>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <!-- <label class="col-md-12" >Nama Pembeli 
                            <span class="help"> </span>
                        </label> -->
                        <div class="col-md-12">
                            <input type="text" id="c_nama" name="customer[nama]" class="form-control" placeholder="Nama Lengkap Pembeli" required="required"> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <!-- <label class="col-md-12 control-label" style="text-align: left">Kurir</label> -->
                        <div class="col-md-12">
                             <input type="number" id="c_tlp" name="customer[tlp]" class="form-control" placeholder="Telepon/Handphone Pembeli" required="required"> 
                        </div>
                            <span class="help-block">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
   
          
        
