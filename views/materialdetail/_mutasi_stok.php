<hr class="hidden-print">
<div class="form-group btn-tools">
 <a href="javascript:;" class="btn btn-success hidden-print pull-right btn-cetak" onclick="window.print()"><span class="icon-printer"></span></a>
</div>
<img src="<?=Yii::app()->theme->baseUrl; ?>/assets/custom/img/Logo PGN Com - Big.png"  height="70" alt=""/>
<br>
<br>
<table class="table table-bordered table-hover table-striped" id="MutasiStok">
	<caption>
		<center>MUTASI STOK<br>
			Periode : <?=$periode?>
		</center>
	</caption>
	<thead>
		<tr>
			<th>NO</th> 
			<th>TANGGAL MUTASI</th>
			<th>LOKASI ASAL</th>
			<th>LOKASI TUJUAN</th>
			<th>MATERIAL</th> 
			<th>MERK</th>
			<th>QTY</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$no=1;
			foreach ($data as $item) {
		?>
				<tr>
					<td><?=$no?></td>
					<td><?=Yii::app()->help->dateindoShort($item['sent_at'])?></td>
					<td><?=$item->Dr_lokasi['nm_lokasi']?></td>
					<td><?=$item->Ke_lokasi['nm_lokasi']?></td>
					<td><?=$item->Material['nama']?></td>
					<td><?=$item->Material['merk']?></td>
					<td><?=$item['qty']?> <?=$item->Material['satuan']?></td>
					
				</tr>
		<?php
			$no++;
			}
		?>
	</tbody>
</table>