
<form class="form-horizontal" id="form_cek_tr" role="form">

        <div class="form-group form-group-mat">

            <label class="col-md-3 control-label" style="text-align: left !important">KODE TRANSAKSI </label>
            <div class="col-md-9">
                <input type="text" class="form-control" name="kd_trans" placeholder="" required="required">
                <span class="help-block pull-right text-danger">
                    <?=$note?>
                    <!-- <a href="javascript:;" class="text-danger">Piih material dari lokasi lain ?</a> -->
                </span>
            </div>
        </div>
    
        
    </div>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Lihat</button>
                <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Nanti</button>
            </div>
        </div>
    </div>
</form>
