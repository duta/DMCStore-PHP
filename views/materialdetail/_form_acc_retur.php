
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
?>
<form id="form-retur-acc">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class=""> Solusi yang diinginkan pembeli ?</label>
                    <input type="hidden" name="kd_order" value="<?=$kd_order?>">

                    <div class="radio radio-success">
                        <input type="radio" name="tipe_retur" value="30" id="radio14">
                        <label for="radio14"> Ganti barang baru</label>
                    </div>

                    <div class="radio radio-warning">
                        <input type="radio" name="tipe_retur" value="31" id="radio15">
                        <label for="radio15"> Pengembalian dana</label>
                    </div>
                </div>
            </div>
        </div>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Retur</button>
                <button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            </div>
        </div>
    </div>
</form>

