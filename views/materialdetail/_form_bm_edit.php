<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use app\components\Help;
?>
<form class="form-horizontal" id="form_bm_edit">
<div class="row">
<?php
    $qty_total=[];
    $tpb_total=[];
    foreach ($model->md as $item) {
        $qty_total[]=$item->qty;
        $tpb_total[]=floatval($item->hgsat_dasar)*$item->qty;
?>  
        <div class="col-md-7">
            <div class="form-group ">
                <div class="col-md-12">
                    <div class="input-group">
                            <b class="form-control input-sm"><?=$item->v_material['nama']?> : <u class="text-danger"><?=$item->v_material['varianname']?></u></b>
                            <span class="input-group-addon"> <b> = </b></span>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="col-md-12">
                    <input type="text" placeholder="Harga beli " class="form-control MaskMoney kalkulasi input-sm" id="hb_<?=$item['id']?>" data-id="<?=$item['id']?>"  name="md[harga_beli][<?=$item['id']?>]"  required="required" value="<?=Help::toRp($item->hgsat_dasar)?>">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12">
                   <input type="number" placeholder="QTY " class="form-control  kalkulasi input-sm" id="qty_<?=$item['id']?>" data-id="<?=$item['id']?>" value="<?=$item->qty?>" name="md[qty][<?=$item['id']?>]"  >


                    <!-- total per barang -->
                    <input type="hidden" id="tpb_<?=$item['id']?>" class="tpb" value="<?=ceil($item->hgsat_dasar*$item->qty)?>" data-id="<?=$item['id']?>">

                    <input type="hidden" name="md[hgmod][<?=$item['id']?>]" id="hgmod_<?=$item['id']?>" class="hgmod" value="<?=$item['harga_mod']?>" data-id="<?=$item['id']?>" value="<?=$item['harga_mod']?>">
                </div>
            </div>
        </div>

<?php 
    }
?>
<hr>
<div class="form-actions">
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
            <button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
        </div>
    </div>
</div>
</form>
