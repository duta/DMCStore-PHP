<?php
	$uri=explode("=", $_SERVER['REQUEST_URI']);
	
?>
<form id="form-checked">
<table class="table  table-hover product-overview" id="StokBarangTable">
	<thead>
		<tr>
		
			<th>No</th> 
			<th>Barang</th>
			<th>In</th>
			<th>Out</th>
			<th>Stok akhir</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$no=1;
			foreach ($data as $item) {
		?>
				<tr id="<?=$item['id']?>" class="odd gradeX">
					
					<td><?=$no?></td>
					
					<td>
						<?=$item['nama']?> ( <?=$item['merk']?> )
					</td>
					<td><?=$item['qty_in']?></td>
					<td><?=$item['qty_out']?></td>
					<td><?=$item['stok_akhir']?></td>

					
				</tr>
		<?php
			$no++;
			}
		?>
	</tbody>
</table>
</form>