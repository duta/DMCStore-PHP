<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Material Details');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-detail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Material Detail'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kd_tagihan',
            'kd_transaksi',
            'buyer',
            'id_material',
            // 'id_sup',
            // 'id_market',
            // 'hgsat_dasar',
            // 'hgtot_beli',
            // 'hgsat_jual',
            // 'keterangan:ntext',
            // 'qty',
            // 'status',
            // 'read',
            // 'id_kurir',
            // 'resi',
            // 'sent_at',
            // 'return_at',
            // 'done_at',
            // 'total_pay',
            // 'paid_photo',
            // 'tipe_transfer',
            // 'tipe_transaksi',
            // 'buyer_note:ntext',
            // 'sent_photo',
            // 'done_photo',
            // 'created_by',
            // 'approved_by',
            // 'rejected_by',
            // 'created_at',
            // 'approved_at',
            // 'rejected_at',
            // 'updated_at',
            // 'ppn',
            // 'pph',
            // 'bea_masuk',
            // 'ongkir_impor',
            // 'ongkir_buyer',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
