<a href="javascript:;" onclick="window.print()" class="hidden-print btn btn-block btn-info"><i class="mdi mdi-printer fa-fw"></i> Cetak</a>
<div class="">
<h3 class="p-t-0"><b>INVOICE <span class="pull-right">#<?=$model['kd_order']?></span></h3>
<hr>
<table class="table table-bordered">
    <caption class="text-center">Detail Transaksi</caption>
    <tr>
        <td style="width: 40%">No. Invoice</td>
        <td class="font-normal">#<?=$model['kd_order']?></td>
    </tr>
    <tr>
        <td style="width: 40%">Waktu Transaksi</td>
        <td class="font-normal"><?=Yii::$app->help->datetimeindoShort($model['created_at'])?></td>
    </tr>
    <tr>
        <td style="width: 40%">Pembeli</td>
        <td class="font-normal"><?=$model->plg->nama?></td>
    </tr>
    <tr>
        <td style="width: 40%">Tujuan pengiriman</td>
        <td>
            <b><?=$model->plg['nama']?></b><br>
            <span class="font-normal"><?=$model->plg['alamat']?></span>
            <br>
            No. Tlp/Handphone : <?=$model->plg['tlp']?>
        </td>
    </tr>
    <tr>
        <td style="width: 40%">Jasa pengiriman</td>
        <td  class="font-normal" >
            <?=$model->kurir['nm_kurir']?> > <b><?=$model->kurir['layanan']?></b>
        </td >
    </tr>
<?php
    if($model['id_status'] == 3 || $model['id_status'] == 4){
?>
    <tr>
        <td style="width: 40%">No resi</td>
        <td class="font-normal">
            <?=$model['resi']?>
        </td>
    </tr>
<?php
    }
?>
</table>
<hr>

<table class="table table-bordered">
    <caption class="text-center">Detail Tagihan</caption>
   
    <tr>
        <td style="width: 40%">Catatan pembeli</td>
        <td class="font-normal" colspan="4"><?=$model['buyer_note']?></td>
    </tr>
    <tr class="active">
        <th>(TR) Barang</th>
        <th>Kuantitas</th>
        <th>Harga</th>
        <th>Total</th>
    </tr>
    <?php
        foreach ($model->md as $md_item) {
    ?>

    <tr>
        
        <td class="font-normal">
            <small>(#<?=$md_item->kd_tagihan?>) </small><br>
            <?=$md_item->v_material['fullname']?>
            
        </td>
        <td class="font-normal"><?=$md_item['qty']?></td>
        <td class="font-normal"><?=Yii::$app->help->toRp($md_item['hgsat_jual'])?></td>
        <td><?=Yii::$app->help->toRp($md_item['hgsat_jual']*$md_item['qty'])?></td>
    </tr>
    <?php
        }
    ?>
    <tr>
        <th colspan="3">Biaya Kirim</th>
        <td><?=Yii::$app->help->toRp($model['ongkir_buyer'])?></td>
    </tr>
    <tr class="active">
        <th colspan="3">Total tagihan</th>
        <td ><b class="text-danger"><?=Yii::$app->help->toRp($model['total_tagihan_buyer'])?></b></td>
    </tr>
</table>
<?php
    if($model['id_status'] <1){
?>
<hr>
<div class="well">
<center>         
    Lakukan pembayaran sebesar :<br>
        <h1 class="text-primary bold"><strong><?=Yii::$app->help->toRp($model['total_tagihan_buyer'])?></strong></h1><br>
        Pembayaran dapat ditransfer ke nomor rekening a/n PT Duta Media Cipta<br>
       BANK BRI : 00000000000000000000000000
</center>
</div>
<?php } ?>