<?php
    use app\components\Help;
?>
<div class="well"> <center>
    TRANSAKSI <b>#<?=$model->kd_tagihan?></b>
    </center>
</div>
<div class="row">
    <div class="col-md-12">
         <div class="form-group">
            <label class="control-label col-md-3">Supplier </label>
            <div class="col-md-9">
                <b>:</b>&nbsp;<?=$model->supplier->nm_sup?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
         <div class="form-group">
            <label class="control-label col-md-3">Forwarder </label>
            <div class="col-md-9">
                <b>:</b>&nbsp;<?=($model->fw=='')?'':$model->fw->nm_fw;?>
            </div>
        </div>
    </div>
</div>

<div class="row">
     <div class="col-md-12">
         <div class="form-group">
            <label class="control-label col-md-3">Order at </label>
            <div class="col-md-9">
                <b>:</b>&nbsp;<?=Help::DateIndoShort($model->order_at);?>
            </div>
        </div>
    </div>
</div>

<!-- <hr> -->


<div class="row">
     <div class="col-md-12">
         <div class="form-group">
            <!-- <label class="control-label col-md-12">(QTY) Varian </label> -->
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>Barang</th>
                        <th>(QTY) Varian</th>
                        <th>Harga modal</th>
                    </tr>
                    <tr>
                        <td>
                            <?php
                            $arr=[];

                            foreach ($model->md as $item) {
                                $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;
                            }
                           
                            echo implode(array_unique($arr), '<br>');
                            ?>
                        </td>
                        <td>
                            <?php
                             $val='';
                                foreach ($model->md as $item) {
                                    $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                                    $val.='('.$item->qty_order_bm.') '.$item->v_material->varianname.' <br>';
                                }
                                echo $val;
                            ?>
                        </td>
                        <td>
                            <?php
                            $val='';
                            foreach ($model->md as $item) {
                                $val.=Help::toRp($item->harga_mod).'<br>';
                                $val.="";
                            }
                            echo $val;
                            ?>
                        </td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="row">
     <div class="col-md-12">
         <div class="form-group">
            <!-- <label class="control-label col-md-12">(QTY) Varian </label> -->
            <div class="col-md-12">
                <table class="table">
                    <tr>
                        <th>PPN</th>
                        <th>PPH</th>
                        <th>BEA MASUK</th>
                        <th>DLL</th>
                        <th>ONGKIR</th>
                    </tr>
                    <tr>
                      <td><?=$model->ppn;?>%</td>
                      <td><?=$model->pph;?>%</td>
                      <td><?=$model->bea_masuk;?>%</td>
                      <td><?=Help::toRp($model->dll);?></td>
                      <td><?=Help::toRp($model->ongkir_impor)?></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
</div>


