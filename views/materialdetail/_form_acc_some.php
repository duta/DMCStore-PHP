<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    use app\models\Role;
?>

<form class="form-horizontal" id="form_acc_some" role="form">
    <div class="form-body">
        <div class="well">
            <b class="pull-left">Barang : </b>
            <span class="pull-right">
        <?php
             $arr=[];

                foreach ($model->md as $item) {
                    $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;
                }
                                           
            echo implode(array_unique($arr), '<br>');


        ?> 
        </span>
        <div class="clearfix"></div>
        </div>
         <div class="form-group">
            <div class="col-md-12">
               
                <div class="input-group">
                <input type="text" class="form-control datepicker" name='accept_at' placeholder="Tanggal kedatangan barang" required="required"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                </div>
            </div>
        </div>
        <?php
            foreach ($model->md as $md) {
                if($md->qty>0){
        ?>
            <div class="form-group">
                <div class="col-md-12">
                    <div class="input-group" style="width: 100%;">
                        <span class="input-group-addon" style="width: 60%;background: white;text-align:left">
                            <i class="fa fa-chevron-right fa-fw" style="color:<?=$md->v_material->kode_warna?>"></i>
                            <?=$md->v_material->varianname?>
                        </span>
                        <input type="number"  class="form-control" name="md[<?=$md->id?>]" max="<?=$md->qty?>" value="" placeholder="QTY">
                    </div>
                    <span class="help-block">
                    </span>
                </div>
            </div>
        <?php
                }
            }
        ?>
      
    </div>
        <hr class="light-grey-hr">
    
    <div class="form-actions">
        <div class="row">
            <div class="text-center col-md-12">
                <button type="submit" class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>