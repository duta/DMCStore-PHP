<?php
	$uri=explode("=", $_SERVER['REQUEST_URI']);
	
?>
<form id="form-checked">
<div class="table-responsive">
<table class="table  table-hover product-overview" id="MaterialDetailTable">
	<thead>
		<tr>
		
			<th>No</th> 
			
			<!-- <th>Pembeli</th> -->
			
			<th>Barang</th>
			<th>Qty</th>
			<?php
				if(end($uri)!='bm'){
			?>
			<th>Market</th>
			<th style="width: 30% !important">Pengiriman</th>
			
			<th>Status</th>
			<?php
				}
				if(end($uri)=='bm'){
			?>
			<th>Harga satuan</th>
			<th>Supplier</th>
			<?php
				}
			?>
		
			<th data-orderable=false ><center><i class="fa fa-cogs"></i></center></th>
				<?php
				if(end($uri)=='bk'){
			?>
			<th class="active text-center" data-orderable="false">
				<div class="checkbox checkbox-success checkbox-circle" style="margin:0px !important">
                    <input name="select_all" id="all" type="checkbox" >
                    <label for="all"></label>
                </div>
			</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<?php
			$no=1;
			foreach ($data as $item) {
		?>
				<tr id="<?=$item['id']?>" class="odd gradeX">
					
					<td><?=$no?></td>
										
					<!-- <td><?=$item->Buyer['nama']?></td> -->

					<td>
						<?=$item->VMaterial['nama']?> ( <?=$item->VMaterial['merk']?> )
					</td>
					
					

					<td><?=$item['qty']?> item</td>

					<?php
					if(end($uri )!= 'bm'){
					?>
						<td><?=$item->Market['nm_market']?></td>
						<td>
							<b><?=$item->Buyer['nama']?></b><br>
							<?=$item->Buyer['prov']?>, <?=$item->Buyer['kota_kab']?>, <?=$item->Buyer['kec']?>	<br>

							<i class="text-danger"><?=(end($uri)=='bk')? 'NO. RESI :'.$item->resi:'';?>	</i>
						</td>
						<td>
							<span class="label label-sm popovers <?=$item->Status->class;?>"  data-trigger="hover" data-placement="left" data-content="<?=$item->Status['ket_status']?> " data-original-title="<?=$item->Status->nm_status;?>">
								<?=$item->Status->nm_status;?> </span>
						</td>
					<?php
					}	
					if(end($uri)=='bm'){
					?>
						
						<td><?=Yii::app()->help->toRp($item['hgtot_beli']/$item['qty'])?></td>
						<td><?=$item->Supplier['nm_sup']?></td>
					<?php
						}
					?>
					<td>
						<center>
						<?php
							if(end($uri)=='od'){
						?>
							<button type="button" class="btn btn-primary btn-pay btn-circle btn-sm "  data-toggle="tooltip" data-original-title="Sudah dibayar ?" data-placement="top" id="<?=$item['id']?>"><i class="fa">Rp</i></button>
						<?php
							}if(end($uri)!='bm'){
						?>

							<button type="button" class="btn btn-info btn-circle btn-sm detail_materialdetail" id="<?=$item['id']?>" data-toggle="tooltip" data-original-title="Detail Transaksi" data-placement="top"><i class="fa fa-search"></i></button>
							<button type="button" class="btn btn-success btn-circle btn-sm cetak_transaksi" id="<?=$item['id']?>" data-toggle="tooltip" data-original-title="Invoice" data-placement="top"><i class="fa fa-print"></i></button>
						<?php
						}
						?>
							<button type="button" class="btn btn-danger btn-circle btn-sm del_materialdetail" id="<?=$item['id']?>" data-toggle="tooltip" data-original-title="Hapus Transaksi" data-placement="top"><i class="fa fa-trash"></i></button>
						</center>

					</td>
					<?php
					
					if(end($uri)=='bk'){
						?>
						<td class="active" align="center">
							<div class="checkbox checkbox-danger checkbox checkbox-circle">
                                <input id="checkbox_<?=$item['id']?>" class="checkboxes" name="checked[]" value="<?=$item['id']?>" type="checkbox" >
                                <label for="checkbox_<?=$item['id']?>"></label>
                            </div>
						</td>
					<?php } ?>
					
				</tr>
		<?php
			$no++;
			}
		?>
	</tbody>
</table>
</div>
</form>