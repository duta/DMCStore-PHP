<?php
/* @var $this MaterialDetailController */
/* @var $model MaterialDetail */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'material-detail-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_material'); ?>
		<?php echo $form->textField($model,'id_material',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'id_material'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harga_jual'); ?>
		<?php echo $form->textField($model,'harga_jual',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'harga_jual'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stok_awal'); ?>
		<?php echo $form->textField($model,'stok_awal',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'stok_awal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'in'); ?>
		<?php echo $form->textField($model,'in',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'out'); ?>
		<?php echo $form->textField($model,'out',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'out'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stok_akhir'); ?>
		<?php echo $form->textField($model,'stok_akhir',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'stok_akhir'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keterangan'); ?>
		<?php echo $form->textArea($model,'keterangan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'keterangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'request_out'); ?>
		<?php echo $form->textField($model,'request_out',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'request_out'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved_by'); ?>
		<?php echo $form->textField($model,'approved_by',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'approved_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rejected_by'); ?>
		<?php echo $form->textField($model,'rejected_by',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'rejected_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved_at'); ?>
		<?php echo $form->textField($model,'approved_at'); ?>
		<?php echo $form->error($model,'approved_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rejected_at'); ?>
		<?php echo $form->textField($model,'rejected_at'); ?>
		<?php echo $form->error($model,'rejected_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->