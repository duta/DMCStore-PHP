<form id="form-status-transaksi">
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="white-box">
            <h6 style="text-transform:normal !important" class="box-title m-b-0"><center>KODE TRANSAKSI : <b class="text-danger"><?=$kode_transaksi?></b></center></h6><hr>
            <!-- <p class="text-muted m-b-30 font-13">Masukan data anda sebagai pembeli </p> -->
            <div class="form-horizontal">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="col-md-12">
                            <h5>
                                <b class="text-info pull-left">DETAIL TRANSAKSI & TAGIHAN</b>
                                <b class=" pull-right">STATUS TRANSAKSI : <span class="label <?=$item->Status['class']?>"><?=$item->Status['nm_status']?></span></b>
                            </h5>
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">
                    
                    
                    <div class="form-group">
                        <label class="col-md-3" >Waktu Transaksi
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=Yii::$app->help->datetimeindoShort($item['created_at'])?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Pembeli
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=$item->Buyer['nama']?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Metode Pembayaran
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            Transfer
                        </label>
                    </div>   
                    <div class="form-group">
                        <label class="col-md-3" >Produk
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=$item->VMaterial['nama']?> 
                        </label>
                    </div> 
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-md-3" >Harga satuan
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=Yii::$app->help->toRp($item->VMaterial['hgsat_jual'])?>
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Kuantitas
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=$item['qty']?> <?=$item->VMaterial['satuan']?> 
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Biaya kirim
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            Rp 0
                        </label>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3" >Total tagihan
                            <span class="help"> </span>
                        </label>
                        <label class="col-md-1 ">:</label>
                        <label class="col-md-8 ">
                            <?=Yii::$app->help->toRp($item['total_pay'])?>
                        </label>
                    </div>    
                </div>
                


                <div class="col-md-12">
                <div class="well">
                    <div class="form-group"><center>

                        <label class="col-md-12" >
                            Status transaksi :<br>
                            <h1 class="text-primary bold"><strong><?=$item->Status['nm_status']?></strong></h1><br>
                            
                            <span class="help"><?=$item->Status['ket_status']?></span>
                        </label>
                        <div class="col-md-12 text-center bold">
                           <!-- BANK BRI : 00000000000000000000000000 -->
                        </div></center>
                    </div>
                </div>  
                <hr>

                <input type="hidden" name="id" value="<?=$item['id']?>">
                <input type="hidden" name="status" value="<?=$item->Status['id']?>">
                <div class="form-group">
                    <div class="col-md-12">
                        <center>
                            <?php
                                if($item['status']==3){
                            ?>
                                <button type="submit" class="btn btn-info payment-conf" > Terima barang</button>
                            <?php
                                }
                            ?>
                            
                            <button type="button" class="btn btn-large btn-success" data-dismiss="modal"> Close</button>
                        </center>
                    </div>
                </div>
                </div>
          </div>
        </div>
    </div>
</div>
</form>
