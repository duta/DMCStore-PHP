<form class="form-horizontal" id="form-paid"> 
	<div class="form-group">
	    <label class="col-md-3" >Bukti Pembayaran
	        <span class="help"> </span>
	    </label>
	    <div class="col-md-9">
	        <input type="hidden" name="paid" value="<?=$model['kd_order']?>">
	        <input type="file" name="paid_photo" class="MultiFile">
	   </div>
	</div>
	<div class="form-group">
	    <label class="col-md-3" >Keterangan
	        <span class="help"> </span>
	    </label>
	    <div class="col-md-9">
	        <textarea name="paid_note" class="form-control" placeholder=""></textarea>
	   </div>
	</div>
<hr>
    <div class="form-group">
        <div class="col-md-12">
            <center>
                <button type="submit" class="btn btn-info payment-conf" > 
                Konfirmasi Pembayaran</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal"> Cancel</button>
            </center>
        </div>
    </div>
</form>
