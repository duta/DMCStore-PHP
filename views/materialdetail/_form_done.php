<form class="form-horizontal" id="form_md_done" role="form">
	<div class="form-body">
		<input type="hidden" name="md_done[id]" value="<?=$model['id']?>">

		
		<div class="form-group">
			<label class="col-md-3 control-label">Bukti Foto</label>
			<div class="col-md-9">
				<input type="file" name="md_done_file" class="form-control MultiFile" placeholder="Bukti Foto">
				<span class="help-block">foto : packing, nota, dll
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Kondisi Material</label>
			<div class="col-md-9">
				

				<?=CHtml::DropDownList("md_done[kondisi_done]",null,Chtml::listData($status, 'id', 'nm_status'),['id'=>'id_status','class'=>'form-control select2me','required'=>'required','empty'=>'-- Kondisi ? --']);?>
				<span class="help-block">
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label ">Retur material</label>
			<div class="col-md-9">
				<select class="form-control select-retur-material" name="md_done[retur]">
					<option value="2">TIDAK</option>
					<option value="1">YA</option>

				</select>
				<span class="help-block">
				</span>
			</div>
		</div>

		<div class="form-group div-ket-retur display-hide">
			<label class="col-md-3 control-label ">Keterangan</label>
			<div class="col-md-9">
				<textarea class="form-control" placeholder="Kenapa material ini di kembalikan ?" name="md_done[ket_retur]" ></textarea>
				<span class="help-block">
				</span>
			</div>
		</div>
	</div>
	<hr>
	<div class="form-actions">
		<div class="row">
			<div class="col-md-12">
				<center>
					<button type="submit" class="btn purple btn-retur display-hide">Proses Retur</button>
					<button type="submit" class="btn green btn-konfir">Konfirmasi</button>
					<button type="button" class="btn red" data-dismiss="modal">Cancel</button>
				</center>
			</div>
		</div>
	</div>
</form>