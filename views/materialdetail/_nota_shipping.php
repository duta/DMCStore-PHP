<a href="javascript:;" onclick="window.print()" class="hidden-print btn btn-block btn-info"><i class="mdi mdi-printer fa-fw"></i> Cetak</a><br class="hidden-print">

<?php
    $i=count($model)-1;
    foreach ($model as $key => $data) {

?>
<div class="container-fluid" style="border: 1px solid black;">

    <table class="m-t-10">
        <!-- <caption class="text-center">Detail Transaksi</caption> -->
        <tr>
            <td style="width: 40% !important">No. Order</td>
            <td style="width: 60% !important" class="font-normal">#<?=$data['kd_order']?></td>
        </tr>
       
        <tr>
            <td style="width: 40% !important">Waktu Transaksi</td>
            <td style="width: 60% !important" class="font-normal"><?=Yii::$app->help->datetimeindoShort($data['created_at'])?></td>
        </tr>
        <tr>
            <td style="width: 40% !important" valign="top">Pengirim</td>
            <td style="width: 60% !important" class="font-normal">
                <b><?=$data->sender['nm_ds']?></b><br>
                 No. Tlp/HP : <?=$data->sender['tlp']?>
            </td>
        </tr>
       <!--  <tr>
            <td style="width: 40% !important">Pembeli</td>
            <td style="width: 60% !important" class="font-normal"><?=$data->plg->nama?></td>
        </tr> -->
        <tr>
            <td style="width: 40% !important" valign="top">Tujuan pengiriman</td>
            <td style="width: 60% !important">
                <b><?=$data->plg['nama']?></b><br>
                <span class="font-normal "><?=$data->plg['alamat']?></span>
                <br>
                No. Tlp/HP : <?=$data->plg['tlp']?>
            </td>
        </tr>
        <tr>
            <td style="width: 40% !important">Jasa pengiriman</td>
            <td style="width: 60% !important"  class="font-normal" >
                <?=$data->kurir['nm_kurir']?> > <b><?=$data->kurir['layanan']?></b>
            </td >
        </tr>
    <?php
        if($data['id_status'] == 3 || $data['id_status'] == 4){
    ?>
        <tr>
            <td style="width: 40% !important">No. Resi</td>
            <td style="width: 60% !important" class="font-normal">
                <?=$data['resi']?>
            </td>
        </tr>
    <?php
        }
    ?>
    </table>
</div>
    <?php
        if($key<$i){
    ?>
            <table style="width: 100%">
                <tr>
                    <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4 class="font-bold"><i class="fa fa-cut"></i></h4></td>
                    <td><hr style="border: 1px dashed black;"></td>
                </tr>
            </table>
    <?php
        }
    }
?>
