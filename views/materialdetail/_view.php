<?php
    use yii\helpers\Url;
?>

                            <!-- Nav tabs -->
<ul class="nav nav-pills m-b-30 pull-right " role="tablist">

    <li role="presentation" class="active">
        <a href="#profile1" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-zoom-in"></i></span> <span class="hidden-xs">Detail</span></a>
    </li>
    <li role="presentation" class="">
        <a href="#messages1" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Pembeli</span></a>
    </li>
   
</ul>
<!-- Tab panes -->
<div class="tab-content">

    <div role="tabpanel" class="tab-pane fade active in" id="profile1">
        <div class="clearfix"></div>

        <div class="col-md-7">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-5">Total belanja <span class="pull-right">:</span></label>
                    <div class="col-md-7">
                        <p class="form-control-static p-t-0 pull-right"> 
                            <?=Yii::$app->help->toRp($model['total_belanja'])?> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-5">Biaya kirim <span class="pull-right">:</span></label>
                    <div class="col-md-7">
                        <p class="form-control-static p-t-0 pull-right"> 
                            <?=Yii::$app->help->toRp($model['ongkir_buyer'])?> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table width="100%">
                  <td><hr style="border: 1px dashed black;" /></td>
                  <td style="width:1px; padding: 0 10px; white-space: nowrap;"><h4 class="font-bold">+</h4></td>
                </table>
                <div class="form-group">
                    <label class="control-label col-md-5">Total Tagihan <span class="pull-right">:</span></label>
                    <div class="col-md-7">
                        <p class="form-control-static p-t-0 pull-right"> 
                            <?=Yii::$app->help->toRp($model['total_tagihan_buyer'])?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12"><hr></div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-5">No. Invoice <span class="pull-right">:</span></label>
                    <div class="col-md-7">
                        <p class="form-control-static p-t-0 pull-right"> 
                            #<?=$model['kd_order']?> </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-5">Jasa pengiriman <span class="pull-right">:</span></label>
                    <div class="col-md-7">
                        <p class="form-control-static p-t-0 pull-right"> 
                            <?=$model->kurir['nm_kurir']?> > <b><?=$model->kurir['layanan']?></b>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-5">Metode Pembayaran <span class="pull-right">:</span></label>
                    <div class="col-md-7">
                        <p class="form-control-static p-t-0 pull-right"> 
                           Transfer </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group ">
                    <label class="control-label col-md-5">Catatan pembeli <span class="pull-right">:</span></label>
                    <div class="col-md-7  well">
                        <p class="form-control-static p-t-0 pull-right"> 
                           <?=$model['buyer_note']?> </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 well" >
            <?php
                foreach ($model->md as $md_item) {
            ?>
                <div class="row">
                    <div class="col-md-4">
                      <img height="100" class="img-responsive img-thumbnail" style="height: 100px" src="<?=Url::base()?>/file_uploaded/material/<?=$md_item->v_material['foto']?>?<?=strtotime(date('H:i:s'));?>" />
                    </div>
                    <div class="col-md-7">
                          <div class="form-group">
                             <b><?=$md_item->v_material['fullname']?></b><br>
                             <b class="text-danger"><?=Yii::$app->help->toRp($md_item['hgsat_jual'])?></b><br>
                             <b class="text-muted" style="font-size: 12px">TR : #<?=$md_item['kd_tagihan']?></b ><br>
                             <b class="text-muted" style="font-size: 12px">QTY : <?=$md_item['qty']?></b ><br>
                             <b class="text-muted" style="font-size: 12px">BERAT : <?=$md_item->v_material['berat'].' '.$md_item->v_material['satuan_berat']?></b >
                              <!-- <div class="col-md-8"> -->
                                  <!-- <p class="form-control-static p-t-0"> <?=$model->plg['nama']?> </p> -->
                              <!-- </div> -->
                          </div>
                    </div>
                </div>
                <hr>
            <?php
                }
            ?>
        </div>
            <!--/span-->
        <div class="clearfix"></div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="messages1">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4">Nama Lengkap <span class="pull-right">:</span></label>
                    <div class="col-md-8">
                        <p class="form-control-static p-t-0"> <?=$model->plg['nama']?> </p>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4">Tlp/Hp <span class="pull-right">:</span></label>
                    <div class="col-md-8">
                        <p class="form-control-static p-t-0"> <?=$model->plg['tlp']?> </p>
                    </div>
                </div>
            </div>
            <!--/span-->
    
       
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4">Email <span class="pull-right">:</span></label>
                    <div class="col-md-8">
                        <p class="form-control-static p-t-0"> <?=$model->plg['email']?></p>
                    </div>
                </div>
            </div>
            <!--/span-->
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label col-md-4">Alamat <span class="pull-right">:</span></label>
                    <div class="col-md-8">
                        <p class="form-control-static p-t-0"> <?=$model->plg['alamat']?></p>
                    </div>
                </div>
            </div>
        <div class="clearfix"></div>
    </div>
   
</div>
