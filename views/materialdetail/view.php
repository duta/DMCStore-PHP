<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MaterialDetail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Material Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="material-detail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_tagihan',
            'kd_transaksi',
            'buyer',
            'id_material',
            'id_sup',
            'id_market',
            'hgsat_dasar',
            'hgtot_beli',
            'hgsat_jual',
            'keterangan:ntext',
            'qty',
            'status',
            'read',
            'id_kurir',
            'resi',
            'sent_at',
            'return_at',
            'done_at',
            'total_pay',
            'paid_photo',
            'tipe_transfer',
            'tipe_transaksi',
            'buyer_note:ntext',
            'sent_photo',
            'done_photo',
            'created_by',
            'approved_by',
            'rejected_by',
            'created_at',
            'approved_at',
            'rejected_at',
            'updated_at',
            'ppn',
            'pph',
            'bea_masuk',
            'ongkir_impor',
            'ongkir_buyer',
        ],
    ]) ?>

</div>
