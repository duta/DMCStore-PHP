
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
?>
<form id="form-cancel-note">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="">Alasan dibatalkan ?</label>
                    <input type="hidden" name="kd_order" value="<?=$kd_order?>">
                    <textarea class="form-control" name="cancel_note" placeholder="cth: Stok habis, dll"></textarea>
                </div>
            </div>
        </div>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Batalkan</button>
                <button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            </div>
        </div>
    </div>
</form>

