<hr>
<div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label" style="text-align: left">Harga beli</label>
                <div class="col-md-12">
                    <input type="text" placeholder="Rp 0" class="form-control MaskMoney kalkulasi" id="hb_<?=$item['id']?>" data-id="<?=$item['id']?>" name="MaterialDetail[harga_beli][<?=$item['id']?>]"   required="required">
                </div>
            </div>
        </div>
     
        <div class="col-md-6">
             <div class="form-group">
                <label class="col-md-3 control-label" style="text-align: left">PPN & PPH</label>
                <div class="col-md-12">
                    <div class="input-group">
                        <input type="number" placeholder="PPN (%)" id="ppn_<?=$item['id']?>" data-id="<?=$item['id']?>" class="form-control kalkulasi" name="MaterialDetail[ppn][<?=$item['id']?>]" required="required">

                        <span class="input-group-addon">%</span>
                        <input type="number" placeholder="PPH (%)" id="pph_<?=$item['id']?>" data-id="<?=$item['id']?>" class="form-control kalkulasi" name="MaterialDetail[pph][<?=$item['id']?>]" required="required">
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                 <label class="col-md-1 control-label">QTY</label>
                <div class="col-md-12">
                     <input readonly="readonly" id="qty_<?=$item['id']?>"  data-id="<?=$item['id']?>" type="text"  value="1" name="MaterialDetail[qty][<?=$item['id']?>]" class="touchspin" data-bts-button-down-class="btn btn-default btn-outline " data-bts-button-up-class="btn btn-default btn-outline" required="required">
                </div>
            </div>
        </div>
        <div class="col-md-6">
           <div class="form-group">
                <label class="col-md-12 control-label" style="text-align: left">Bea masuk (BM)</label>
                <div class="col-md-12">
                    <div class="input-group">
                    <input type="number"  placeholder="BM(%)" class="form-control kalkulasi" id="bm_<?=$item['id']?>" data-id="<?=$item['id']?>" name="MaterialDetail[bm][<?=$item['id']?>]" required="required">

                        <span class="input-group-addon">%</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-1 control-label">Supplier</label>
                <div class="col-md-12">
                    <?=CHtml::DropDownList("MaterialDetail[id_sup][".$item['id']."]", null,Chtml::listData($Supplier, 'id', 'nm_sup'),['id'=>'id_sup','class'=>'form-control select2me','empty'=>'-- Pilih Supplier --','required'=>'required']);?>         
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label" style="text-align: left">Biaya kirim</label>
                <div class="col-md-12">
                    <input type="text"  placeholder="Rp 0" class="form-control MaskMoney kalkulasi" id="ongkir_<?=$item['id']?>" data-id="<?=$item['id']?>" name="MaterialDetail[ongkir][<?=$item['id']?>]" required="required">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-12 control-label" style="text-align: left">TOTAL HARGA</label>
                <div class="col-md-12">
                    <input type="text" name="MaterialDetail[hatot][<?=$item['id']?>]" class="form-control" id="ht_<?=$item['id']?>" readonly>        
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-12 control-label" style="text-align: left">HARGA SATUAN (BELI)</label>
                <div class="col-md-12">
                    <input type="text" class="form-control MaskMoney" id="hs_<?=$item['id']?>" readonly >
                </div>
            </div>
        </div>
    </div>