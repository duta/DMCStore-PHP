
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
?>
<form id="form-ds">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="">Pilih Dropshipper</label>
                    <input type="hidden" name="kd_order" value="<?=$kd_order?>">
                    <?=Html::DropDownList("ds[ds_found]",null,$Dropshipper,['class'=>'ds_combo form-control','prompt'=>'-- Pilih Dropshipper --']);?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="">Nama Pengirim</label>
                    <input class="form-control" id="ds_send" type="text" value="" name="ds[nm_ds]">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label class="">No. Tlp/Hp</label>
                    <input class="form-control" id="ds_tlp" type="text" value="" name="ds[tlp]">
                </div>
            </div>
        </div>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
                <button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            </div>
        </div>
    </div>
</form>

