<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Forwarder */

$this->title = Yii::t('app', 'Create Forwarder');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forwarders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forwarder-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
