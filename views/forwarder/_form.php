<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    use app\models\Role;
    
?>

<form class="form-horizontal" id="form_fw" action="<?=($model->isNewRecord) ? Url::toRoute('forwarder/create') : Url::toRoute('forwarder/update').'/'.$model['id']; ?>" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="Forwarder[nm_fw]" value="<?=$model['nm_fw']?>" placeholder="Nama Forwarder" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="Forwarder[tlp]" value="<?=$model['tlp']?>" placeholder="no Tlp/Hp" >
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="Forwarder[qq]" value="<?=$model['qq']?>" placeholder="QQ" >
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="Forwarder[wechat]" value="<?=$model['wechat']?>" placeholder="wechat" >
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
   
    </div>
        <hr class="light-grey-hr">
    
    <div class="form-actions">
        <div class="row">
            <div class="text-center col-md-12">
                <button type="submit" class="btn btn-success"><?=($model->isNewRecord ? 'Create' : 'Save'); ?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>