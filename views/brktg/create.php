<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BrKtg */

$this->title = Yii::t('app', 'Create Br Ktg');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Br Ktgs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="br-ktg-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
