
<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Barang'); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi mdi-puzzle fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                <button class="add_material btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="fa fa-plus"></span> 
                </button>
                <button class="m-r-5 imp_xls btn btn-warning btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Import excel" data-placement="left">
                    <i class="fa fa-file-excel-o"></i>
                </button>
                <a href="<?=Url::toRoute(['mastermaterial/ex_xls'])?>" class="m-r-5 exp_xls btn btn-info btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Export excel" data-placement="left">
                    <i class="fa  fa-file-archive-o"></i>
                </a>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                            <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'tableOptions'=>['class'=>'table table-condensed table-hover'],
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'columns' => [

                                        [   'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                            'class' => 'yii\grid\ActionColumn',
                                            'contentOptions' => ['style' => 'width:1%;'],
                                            'template'=>'<center>{action}</center>',
                                            'buttons'=>[
                                                // {view_foto} {foto} {update} {delete}
                                                // 'view_foto'=>function($url,$model){
                                                //     return '<button type="button" class="btn btn-success btn-circle btn-sm foto_item" id="'.$model->kode.'" data-toggle="tooltip" data-original-title="Lihat foto" data-placement="top"><i class="fa fa-image"></i></button>';
                                                // },
                                                // 'foto'=>function($url,$model){
                                                //     return '<button type="button" class="btn btn-info btn-circle btn-sm var_item" id="'.$model->kode.'" data-toggle="tooltip" data-original-title="Atur foto" data-placement="top"><i class="fa fa-star-half-empty"></i></button>';
                                                // },
                                                // 'update'=>function($url,$model){
                                                //     return '<button type="button" class="btn btn-primary btn-circle btn-sm edit_item" id="'.$model->kode.'" data-toggle="tooltip" data-original-title="Update Barang" data-placement="top"><i class="fa fa-pencil"></i></button>';
                                                // },
                                                // 'delete'=>function($url,$model){
                                                //     return '<button type="button" class="btn btn-danger btn-circle btn-sm del_item" id="'.$model->kode.'" data-toggle="tooltip" data-original-title="Hapus Barang" data-placement="top"><i class="fa fa-trash"></i></button>';
                                                // }
                                                 'action'=>function($url,$model){

                                                    return ' <div class="btn-group dropup">
                                                            <button aria-expanded="false" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light" type="button">
                                                                    <i class="fa fa-cog m-r-5"></i>
                                                                    <span class="caret"></span>
                                                            </button>
                                                            <ul role="menu" class="dropdown-menu animated flipInX">
                                                                <li class="disabled"><a href="javascript:void(0)">KODE : #'.$model->kode.'</a></li>
                                                                <li class="divider"></li>
                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kode.'" class="foto_item"><i class="fa fa-image fa-fw"></i>Lihat foto</a>
                                                                </li>
                                                                
                                                                 <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kode.'" class="var_item"><i class="fa  fa-star-half-empty fa-fw"></i>Atur foto</a>
                                                                </li>

                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kode.'" class="edit_item"><i class="fa fa-pencil fa-fw"></i>Update Barang / Tambah varian</a>
                                                                </li>

                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kode.'" class="del_item"><i class="fa fa-trash fa-fw"></i>Hapus Barang</a>
                                                                </li>
                                                              
                                                                
                                                            </ul>
                                                        </div>';
                                                    }
                                            ],
                                        ],
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'kode',
                                        [
                                            'header'=>'Kategori',
                                            'attribute'=>'master_material.nama',
                                            'value'=>function($model,$key,$i){
                                                return $model->ktg->nm_ktg;
                                            },
                                            // 'enableSorting'=>true,
                                        ],
                                        'nama',
                                        [
                                            'header'=>'Varian',
                                            // 'attribute'=>'master_material.nama',
                                            'value'=>function($model,$key,$i){
                                                $ktg=(count($model->v_material)>0)?$model->v_material[0]->attr->nm_attr:'';
                                                
                                                $var="";
                                                if(count($model->v_material)>0){
                                                    $var=ucwords($ktg)." (";
                                                    foreach ($model->v_material as $varian) {
                                                        $var.=$varian->varian->nm_varian.',';
                                                    }

                                                    $var=rtrim($var,',').")";
                                                }else{
                                                    $var="Belum ada varian";
                                                }

                                                return $var;
                                            },
                                            // 'enableSorting'=>true,
                                        ],
                                        // 'nama',
                                        

                                        // 'created_at',

                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>