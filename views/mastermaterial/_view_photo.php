<?php
    use yii\helpers\Url;

?>
 
<center>
<div id="myCarousel" class="carousel slide carousel-fit" data-ride="carousel">
  <!-- Indicators -->
    <ol class="carousel-indicators">
       <?php
        $slide=0;
        foreach ($model->v_material as $key=>$item) {
        ?>
            <li data-target="#myCarousel" data-slide-to="<?=$slide?>" class="<?=($key==0)?'active':'';?>"></li>
        <?php
        $slide++;
        }
        ?>
    </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php
    foreach ($model->v_material as $key=>$item) {
    ?>
    <div class="item <?=($key==0)?'active':'';?>">
        <img src="<?=Url::base(); ?>/file_uploaded/material/<?=$item->foto?>" alt="First slide">
        <div class="carousel-caption" style="background: rgba(0, 0, 0, 0.8);">
            <h3 style="color:white"><?=$item->fullname?></h3>
            <p>&nbsp;</p>
        </div>
    </div>
    <?php 
    } ?>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
</center>
