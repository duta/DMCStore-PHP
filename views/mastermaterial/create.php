<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterMaterial */

$this->title = Yii::t('app', 'Create Master Material');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Master Materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-material-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
