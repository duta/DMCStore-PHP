<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use app\models\BrGrosir;
?>
<form class="form-horizontal" id="form_material" action="<?=($model->isNewRecord) ? Url::toRoute('mastermaterial/create') : Url::toRoute('mastermaterial/update').'/'.$model['kode']; ?>" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- <label class="col-md-1 control-label">Nama</label> -->
                    <div class="col-md-12">
                        <input type="hidden" name="Material[kd]" value="<?=$model['kode']?>">
                        <input type="text" name="Material[nama]"  class="form-control" placeholder="Nama barang" value="<?=$model['nama']?>" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
          
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                   <!--  <label class="col-md-1 control-label">Merk</label>
                    <div class="col-md-12">
                        <input type="text" name="Material[merk]"  class="form-control" placeholder=" Merk barang" value="<?=$model['merk']?>" required="required">
                        <span class="help-block">
                        </span>
                    </div> -->
                    <!-- <label class="col-md-1 control-label">Foto </label> -->
                    <div class="col-md-12">
                      
                        <?=Html::DropDownList("Material[id_ktg]",$model['id_ktg'],ArrayHelper::map($ktg, 'id', 'nm_ktg'),['id'=>'nm_ktg','class'=>'form-control select2me','required'=>'required','prompt'=>'-- Pilih Kategori --']);?>
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <!-- <label class="col-md-1 control-label">Berat</label> -->
                    <div class="col-md-12">
                        <div class="input-group">
                            <input type="text" name="Material[berat]"  class="form-control" placeholder="Berat" value="<?=$model['berat']?>" required="required">       
                            <span class="input-group-addon"><i class="icon-bag"></i></span>

                            <select class="form-control" name="Material[satuan_berat]" required="required">
                                <option value="g" <?=($model['satuan_berat']=='g')?"selected='selected'":'';?> >Gram (g)</option>
                                <option value="kg" <?=($model['satuan_berat']=='g')?"selected='selected'":'';?> >Kilogram (kg)</option>
                            </select>
                        </div>
                    </div>
                </div>
                     
            </div>
        </div>
        <?php
            /*if($model->isNewRecord){*/
                $attrval=isset($model->v_material[0]->id_attr)?$model->v_material[0]->id_attr:null;
        ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <!-- <label class="col-md-1 control-label">Atribut</label> -->
                    <div class="col-md-12">
                       <?=Html::DropDownList("Material[attr]",$attrval,ArrayHelper::map($attr,'id','nm_attr'),['id'=>'attr','class'=>'form-control select2me','prompt'=>'-- Pilih Atribut --']);?>
                   </div>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="form-group">

                    <!-- <label class="col-md-1 control-label" id="varian_label">Varian</label> -->
                    <div class="col-md-12" id="varian_combo">
                    <?php if(!$model->isNewRecord){ ?> 

                       <?=Html::DropDownList("Material[varian]",ArrayHelper::getColumn($model->v_material,'id_varian'),ArrayHelper::map($varian,'id','nm_varian'),['id'=>'varian','class'=>'itemselect2','multiple'=>'multiple','placeholder'=>'-- Pilih Varian --']);?>
                    <?php } ?>
                       
                    </div>

                </div>
               
            </div>
        </div>
<!-- 
        <div class="row">
            <div class="col-md-6">
                <div class="checkbox checkbox-success">
                    <input id="check-1" name="Material[is_dropship]" value="1" <?=($model->is_dropship==1)?'checked=checked':'';?> type="checkbox">
                    <label for="check-1" > Barang dropship ?</label>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div><br> -->
        <?php  /*}*/ ?> 
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <!-- <label class="col-md-1 control-label">Deskripsi</label> -->
                    <div class="col-md-12">
                        <textarea class="form-control" name="Material[keterangan]" placeholder="Deskripsi barang " rows="5"><?=$model['keterangan']?></textarea>
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <hr class="light-grey-hr">
    
    <div class="form-actions">
        <div class="row">
            <div class="text-center col-md-12">
                <button type="submit" class="btn btn-success"><?=($model->isNewRecord ? 'Create' : 'Save'); ?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>