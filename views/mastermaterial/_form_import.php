
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
?>
<form id="form-impxls">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon" style="width: 60%;background: white;text-align: none;border:none">
                       File Excel 
                    </span>     
                    <input type="file" name="xls_file" class="MultiFile">    
                </div>
            </div>
        </div>

    </div>

    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Import</button>
            </div>
        </div>
    </div>
</form>

