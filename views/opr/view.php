<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Opr */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oprs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opr-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'user_id',
            'coa_id',
            'project_id',
            'debit',
            'kredit',
            'keterangan:ntext',
            'status_id',
            'sppd',
            'bukti',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
