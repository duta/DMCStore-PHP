<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Opr */

$this->title = 'Update Opr: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Oprs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="opr-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
