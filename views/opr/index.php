<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use app\models\Menu;
use app\components\Help;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$link=Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
$menu=Menu::find()->where(['link'=>$link])->one();
$this->title = Yii::t('app', strtolower($menu->name)); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi '.$menu->icon.' fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <?php
                if(isset($menu->parent)){
            ?>
                    <li class=""><?=$menu->parent->name?></li>
            <?php
                }
            ?>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                <div class="btn-group  pull-right">
                    <button class="add_<?=$this->title?> btn  btn-info " id="0">
                        <span class="fa fa-plus fa-fw"></span> Pengeluaran
                    </button>

                    <button class="add_<?=$this->title?> btn  btn-success " id="1">
                        <span class="fa fa-plus fa-fw"></span> Cashflow
                    </button>
                </div>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                            <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'responsive'=>true,
                                    'responsiveWrap'=>false,
                                    'condensed'=>true,
                                    'bordered'=>false,
                                    'export'=>false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                            // 'id',
                                            // 'date',
                                            // 'user_id',
                                            [
                                                'header'=> 'Tanggal',
                                                'format'=>'raw',
                                                'value'=>function($model,$key,$i){
                                                    return Help::dateindoshort($model->date);
                                                }
                                            ],
                                            [
                                                'header'=> 'Pegawai',
                                                'format'=>'raw',
                                                'value'=>function($model,$key,$i){

                                                    return isset($model->user->fullname)?$model->user->fullname:'';
                                                }
                                            ],
                                            [
                                                'header'=> 'COA',
                                                'format'=>'raw',
                                                'value'=>function($model,$key,$i){
                                                    return isset($model->coa->name)?$model->coa->name:'';
                                                }
                                            ],
                                            [
                                                'header'=> 'Keterangan',
                                                'format'=>'raw',
                                                'value'=>function($model,$key,$i){
                                                    return $model->keterangan;
                                                }
                                            ],
                                            // 'coa_id',
                                            // 'keterangan:ntext',
                                            [
                                                'header'=> 'Debit',
                                                'format'=>'raw',
                                                'value'=>function($model,$key,$i){
                                                    return Help::toRp($model->debit);
                                                }
                                            ],
                                            [
                                                'header'=> 'Kredit',
                                                'format'=>'raw',
                                                'value'=>function($model,$key,$i){
                                                    return Help::toRp($model->kredit);
                                                }
                                            ],
                                            // 'debit',
                                            // 'kredit',
                                            // 'status_id',
                                            // 'bukti',
                                            // 'created_at',
                                            // 'updated_at',

                                        [   
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',

                                            'class' => 'yii\grid\ActionColumn',
                                            'template'=>'<center>{bukti} {update} {delete}</center>',
                                            'buttons'=>[
                                                'bukti'=>function($url,$model){
                                                    return '<button type="button" class="btn btn-success btn-circle btn-sm lihat_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Lihat bukti" data-placement="top"><i class="fa fa-photo"></i></button>';
                                                },
                                                'update'=>function($url,$model){
                                                    return '<button type="button" class="btn btn-primary btn-circle btn-sm update_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Update '.$this->title.'" data-placement="top"><i class="fa fa-pencil"></i></button>';
                                                },
                                                'delete'=>function($url,$model){
                                                    return '<button type="button" class="btn btn-danger btn-circle btn-sm del_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Hapus '.$this->title.'" data-placement="top"><i class="fa fa-trash"></i></button>';
                                                }
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                          
                                    <!-- 'tableOptions'=>['class'=>'table table-condensed table-hover'], -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

