<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vcashflow */

$this->title = 'Create Vcashflow';
$this->params['breadcrumbs'][] = ['label' => 'Vcashflows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vcashflow-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
