<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vcashflow */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vcashflows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vcashflow-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_cf',
            'id',
            'date',
            'user_id',
            'fullname:ntext',
            'coa_id',
            'debit',
            'kredit',
            'keterangan:ntext',
            'status_id',
            'is_cf',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
