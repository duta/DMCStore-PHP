<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="opr-form">
    <?php $form = ActiveForm::begin(); ?>
    <!-- <input type="hidden" name="opr[is_cf]" value="0"> -->

    <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                
                     <?=Html::DropDownList("opr[user_id]",$model->user_id,$user,['class'=>'form-control itemselect2','prompt'=>'-- Pilih Pegawai --']);?>
                    <!-- <span class="help-block"> This is inline help </span>  -->
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <input type="text" name="opr[date]" class="form-control datepicker" placeholder="Tanggal" value="<?=date('d-m-Y')?>" required="required"> 
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <input type="text" name="opr[debit]" value="<?=$model->debit?>" class="form-control MaskMoney" placeholder="Nominal">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <textarea class="form-control" rows="5" name=opr[keterangan] placeholder="Keterangan..."><?=$model->keterangan?></textarea>
                </div>
            </div>
        
        </div>
       
    <div class="form-group">
        <hr>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
