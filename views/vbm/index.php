<?php


use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use kartik\select2\Select2;
use app\components\Help;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Barang Masuk');
$icon = Yii::t('app', '<i class="mdi mdi-playlist-check fa-fw"></i>');
// $icon = Yii::t('app', '<i class="mdi mdi-basket-fill fa-fw"></i>');
?>


<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                
                <!-- <button class="add_bm btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="ti-plus"></span> 
                </button> -->
                
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                                
                                <?php Pjax::begin(); ?>    
                                    <?= GridView::widget([
                                    'tableOptions'=>['class'=>'table table-condensed table-hover'],
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'responsive'=>true,
                                    'responsiveWrap'=>false,
                                    'condensed'=>true,
                                    'bordered'=>false,
                                    'export'=>false,
                                    // 'rowOptions'=>function($model, $key, $index, $grid){
                                    //     if($model->status==3){
                                    //         return array('key'=>$key,'index'=>$index,'class'=>"advance-table-row active");
                                    //     }
                                    // },
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                       // INVOICE
                                    // [
                                    //     'header'=> 'KODE TR',
                                    //     'format'=>'raw',
                                    //     'value'=>function($model,$key,$i){
                                    //         $bm='<b>TR :</b> #'.$model->kd_tagihan;
                                    //         $bm.='<br><b>By :</b> '.$model->supplier->nm_sup;
                                    //         $bm.='<br><b>Order at :</b> '.Help::dateindoShort($model->order_at);
                                    //         return $bm;
                                    //     },
                                    //     'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[kd_tagihan]" value="'.$searchModel->kd_tagihan.'" placeholder="Search by invoices" >'
                                    // ],
                                         [
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                            'headerOptions' => ['width' => '100'],

                                            'class' => 'yii\grid\ActionColumn',
                                            'template'=>'<center>{acc_some} {acc}</center>',
                                            'buttons'=>[
                                                'acc_some'=>function($url,$model){
                                                    if($model->id_status==3){
                                                        return '<button type="button" class="btn btn-info btn-circle btn-sm acc_some_bm" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Terima barang" data-placement="top"><i class="fa fa-check"></i></button>';
                                                    }
                                                },
                                                'acc'=>function($url,$model){
                                                    if($model->id_status==3){
                                                        // return '<button type="button" class="btn btn-primary btn-circle btn-sm acc_bm" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Terima Semua" data-placement="top"><i class="fa fa-check-square"></i></button>';
                                                    }
                                                },
                                                // 'delete'=>function($url,$model){
                                                  
                                                //     return '<button type="button" class="btn btn-danger btn-circle btn-sm del_bm" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Hapus Transaksi" data-placement="top"><i class="fa fa-trash"></i></button>';
                                                // }

                                            ],
                                        ],
                                        // Barang
                                         [
                                            'header'=> 'Barang ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                $arr=[];
                                                $val='';
                                                foreach ($model->md as $item) {
                                                    if(isset($item->v_material->kode_warna)){
                                                        $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;

                                                        $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                                                        $val.='('.$item->qty.') '.$item->v_material->varianname.' <br>';
                                                    }
                                                }
                                               
                                                return implode(array_unique($arr), '<br>');
                                            },
                                            'filter'=>'<input type="text" class="form-control input-sm" name="VBmSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                        ],

                                        // Varian
                                        [
                                            'header'=> '(QTY) Varian ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                $val='';
                                                foreach ($model->md as $item) {
                                                    $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                                                    $val.='('.$item->qty.') '.$item->v_material->varianname.' <br>';
                                                }
                                                return $val;
                                            },
                                            // 'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                        ],
                                        // HARGA SATUAN
                                        // [
                                        //     'header'=> 'Harga Satuan',
                                        //     'value'=>function($model,$key,$i){
                                        //         return Yii::$app->help->toRp($model->hgsat_modal);
                                        //     },
                                        //     'filter'=>'<input type="text" class="form-control input-sm" name="VBmSearch[hgsat_modal]" value="'.$searchModel->hgsat_modal.'" placeholder="By price" >'
                                        // ],

                                        // Supplier
                                        // [
                                        //     'header'=> 'Supplier',
                                        //     'value'=>function($model,$key,$i){
                                        //         return $model->supplier->nm_sup;
                                        //     },
                                        //     'filter'=>'<input type="text" class="form-control input-sm" name="VBmSearch[nm_sup]" value="'.$searchModel->nm_sup.'" placeholder="Search by suppliers" >'
                                        // ],

                                        // 'id',
                                        // 'kd_tagihan',
                                        // 'kd_transaksi',
                                        // 'tipe_transfer',
                                        // 'tipe_transaksi',
                                        // 'ppn',
                                        // 'pph',
                                        // 'bea_masuk',
                                        // 'hgtot_beli',
                                        // 'qty_total',
                                        // 'ongkir_impor',
                                        // 'id_sup',
                                        // 'hgsat_modal',
                                        // 'status',
                                        // 'created_at',

                                       
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>