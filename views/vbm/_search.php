<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VBmSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vbm-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kd_tagihan') ?>

    <?= $form->field($model, 'kd_transaksi') ?>

    <?= $form->field($model, 'tipe_transfer') ?>

    <?= $form->field($model, 'tipe_transaksi') ?>

    <?php // echo $form->field($model, 'ppn') ?>

    <?php // echo $form->field($model, 'pph') ?>

    <?php // echo $form->field($model, 'bea_masuk') ?>

    <?php // echo $form->field($model, 'hgtot_beli') ?>

    <?php // echo $form->field($model, 'qty_total') ?>

    <?php // echo $form->field($model, 'ongkir_impor') ?>

    <?php // echo $form->field($model, 'id_sup') ?>

    <?php // echo $form->field($model, 'hgsat_modal') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
