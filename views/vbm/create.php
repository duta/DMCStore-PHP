<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vbm */

$this->title = Yii::t('app', 'Create Vbm');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vbms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vbm-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
