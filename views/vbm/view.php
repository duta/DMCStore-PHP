<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Vbm */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vbms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vbm-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_tagihan',
            'kd_transaksi',
            'tipe_transfer',
            'tipe_transaksi',
            'ppn',
            'pph',
            'bea_masuk',
            'hgtot_beli',
            'qty_total',
            'ongkir_impor',
            'id_sup',
            'hgsat_modal',
            'status',
            'created_at',
        ],
    ]) ?>

</div>
