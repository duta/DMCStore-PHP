
        <div class="row">
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="col-md-12 control-label " style="text-align: left !important">Harga retail</label>
                    <div class="col-md-12">
                        <input type="text" name="Material[hgsat_retail]" class="form-control MaskMoney hrg-s" placeholder="Masukan Harga retail" value="<?=$model['hgsat_retail']?>" required="required">
                        <span class="help-block ">
                            <a href="#" class="pull-right btn btn-primary btn-xs add-hg">harga grosir ?</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group ">
                    <label class="col-md-12 control-label " style="text-align: left !important">Harga reseller</label>
                    <div class="col-md-12">
                        <input type="text" name="Material[hgsat_reseller]" class="form-control MaskMoney hrg-res" placeholder="Masukan Harga reseller" value="<?=$model['hgsat_reseller']?>" required="required">
                        <span class="help-block ">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="hg-wrap" style="<?=($model['hg']==1)?'':'display: none';?>">
                    <h6 class="text-center"><i class="icon-info"></i> Harga Grosir 
                        <!-- <a class="btn btn-danger btn-xs pull-right">Hapus Harga grosir</a> -->
                    </h6>
                    <hr class="light-grey-hr">
                    <?php
                        if($model['hg']==1){
                            $hg=BrGrosir::find()->where(['id_brg'=>$model['id']])->all();
                            foreach ($hg as $hg_item) {
                    ?>
                        <div class="row row-hg">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Jumlah minimum beli</label>
                                    <div class="col-md-6">
                                        <input type="number" name="grosir[jml][]" class="form-control " value="<?=$hg_item['jml']?>" placeholder="jumlah minimum" >
                                        <span class="help-block">
                                        </span>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Harga <strong class="text-danger">per barang</strong></label>
                                    <div class="col-md-6">
                                        <input type="text" name="grosir[hg][]" class="form-control MaskMoney hrg-g" placeholder="Harga/barang" value="<?=$hg_item['hrg']?>" >
                                        <span class="help-block">
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn btn-xs btn-warning btn-icon-anim btn-circle rm-row-hg"><i class="fa fa-trash"></i></button>
                                <button type="button" class="btn btn-xs btn-info btn-icon-anim btn-circle add-row-hg"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    <?php
                            }

                        }else{
                    ?>     
                    <div class="row row-hg">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Jumlah minimum beli</label>
                                <div class="col-md-6">
                                    <input type="number" name="grosir[jml][]" class="form-control " placeholder="jumlah minimum" >
                                    <span class="help-block">
                                    </span>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Harga <strong class="text-danger">per barang</strong></label>
                                <div class="col-md-6">
                                    <input type="text" name="grosir[hg][]" class="form-control MaskMoney hrg-g" placeholder="Harga/barang"  >
                                    <span class="help-block">
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-xs btn-warning btn-icon-anim btn-circle rm-row-hg"><i class="fa fa-trash"></i></button>
                            <button type="button" class="btn btn-xs btn-info btn-icon-anim btn-circle add-row-hg"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <?php  
                        }
                    ?>
                </div>
            </div>
        </div>