<?php
    use yii\helpers\Url;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="white-box p-10">
            <!-- <div class=""> -->
                <!-- <h2 class="m-b-0 m-t-0">Rounded Chair</h2> <small class="text-muted db">globe type chair for rest</small> -->
                <!-- <hr> -->
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="white-box p-10 text-center"> 
                        	<img src="<?=Url::to('@web')?>/file_uploaded/material/<?=$item['foto']?>?<?=strtotime(date('H:i:s'));?>" class="img-responsive" />

                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-6">
                        <h4 class="box-title m-t-10">Deskripsi</h4>
                        <p><?=nl2br($item['keterangan'])?></p>
     
                        <h2 class="m-t-40"><?=Yii::$app->help->toRp($item['hgsat_retail'])?> <small class="text-success"><!-- (36% off) --></small></h2>
                        <button class="btn btn-danger btn-rounded m-r-5" data-toggle="tooltip" data-original-title="Tambahkan ke keranjang" data-placement="top" ><i class="mdi mdi-cart-plus"></i> </button>

	                         <a href="javascript:void(0)" id="<?=$item['id']?>" class="btn btn-info buy_product btn-rounded m-r-5" data-toggle="tooltip" data-original-title="Beli sekarang" data-placement="top">
	                        <i class="ti-shopping-cart"></i> Beli sekarang
	                    </a> 
                        <div class="row">
                        	<div class="col-md-6">
                        		<h3 class="box-title m-t-40">Terjual</h3>
                                <ul class="list-icons">
                                    <li><i data-icon="b" class="linea-icon linea-ecommerce text-info"></i> <?=$item['qty_out']?> <?=$item['satuan']?></li>
                                </ul>
                        	</div>
                        	<div class="col-md-6">
                        		<h3 class="box-title m-t-40">Tersisa</h3>
                                <ul class="list-icons">
                                    <li><i class="fa fa-puzzle-piece text-info"></i> <?=$item['stok_akhir']?> <?=$item['satuan']?></li>
                                    
                                </ul>
                        	</div>
                        </div>
                        
                    </div>
                   
                </div>
            <!-- </div> -->
        </div>
    </div>
</div>