
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
?>
<form id="form-phovar-changed">
    <?php
        foreach ($model->v_material as $item) {
    ?>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" style="width: 60%;background: white;text-align: none;border:none">
                                <b class="pull-left text-primary"><?=$item->nama?></b> <b class="pull-right"><?=$item->attr->nm_attr?> : <?=$item->varian->nm_varian?></b>
                            </span>     
                            <input type="file" name="varian[<?=$item->id?>]" class="MultiFile">    
                            <input type="hidden" name="varian[]" value="<?=$item->id?>">
                        </div>
                        <!-- <label class="control-label">First Name</label> -->
                        <!-- <input type="text" id="firstName" class="form-control" placeholder="John doe">  -->
                        <!-- <span class="help-block"> This is inline help </span>  -->
                    </div>
                </div>
                <!--/span-->
            </div>
    <?php
        }
    ?>
    <hr>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" class="btn  btn-success"><i class="fa fa-check"></i> Simpan</button>
                <button type="button" class="btn  btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
            </div>
        </div>
    </div>
</form>

