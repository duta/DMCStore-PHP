
<?php
    use yii\helpers\Url;
    
	foreach (array_chunk($data,4) as $key => $items) {
?>
<div class="row">
<?php

	foreach ($items as $item) {
?>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="white-box">
            <div class="product-img">
                <img src="<?=Url::base(); ?>/file_uploaded/material/<?=$item['foto']?>?<?=strtotime(date('H:i:s'));?>" />
                <div class="pro-img-overlay">
                	<a href="javascript:void(0)" id="<?=$item['id']?>" class="bg-info buy_product" data-toggle="tooltip" data-original-title="Beli " data-placement="top">
                        <i class=" ti-shopping-cart"></i>
                    </a> 
                    <a href="javascript:void(0)" id="<?=$item['id']?>" data-nama="<?=$item['nama']?>" class="bg-success detail_material" data-toggle="tooltip" data-original-title="Lihat sekilas" data-placement="top"><i class="ti-search"></i></a>
                </div>
            </div>
            <div class="product-text" style="height:50px !important">
                <?php
				if($item['hg']==1){
				?>
                <span class="pro-price bg-danger" style="top:-260px !important">Grosir</span>
									
				<?php } ?>
                <!-- <span class="pro-price bg-danger"></span> -->
                <h6 class="box-title m-b-0 font-normal text-center" style="font-size: 12px !important"><?=$item['fullname']?></h6>
                <small class="text-danger text-center db"><strong><?=Yii::$app->help->toRp($item['hgsat_retail'])?></strong></small>
            </div>
        </div>
    </div>
<?php
	}
?>
</div>
<?php
	}
?>