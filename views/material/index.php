<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Materials');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="mdi mdi-puzzle fa-fw"></i>Barang </h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active">Barang</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <i class="mdi mdi-puzzle fa-fw"></i>Kelola Barang

                <button class="add_material btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah Barang" data-placement="left">
                    <span class="ti-plus"></span> 
                </button>
            </h3>
             <hr>
            <div class="table-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-pills m-b-30 pull-right">
                            <li class="active"> <a href="#navpills-11" data-toggle="tab" aria-expanded="true">Barang dijual</a> </li>
                            <li class=""> <a href="#navpills-21" data-toggle="tab" aria-expanded="false">Stok habis</a> </li>
                        </ul>
                        <div class="tab-content br-n pn">
                            <div id="navpills-11" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-12">

                                            <?php Pjax::begin(); ?>    <?= GridView::widget([
                                                    'dataProvider' => $dataProvider,
                                                    'filterModel' => $searchModel,
                                                    'columns' => [
                                                        ['class' => 'yii\grid\SerialColumn'],

                                                         // Nama
                                                        [
                                                            'header'=>'Barang',
                                                            'attribute'=>'material.nama',
                                                            'value'=>'nama',
                                                            'filter'=>'<input type="text" class="form-control input-sm" name="MaterialSearch[nama]" value="'.$searchModel->nama.'" placeholder="By Barang" >',
                                                            'enableSorting'=>true,
                                                        ],

                                                        // HARGA RETAIL
                                                        [
                                                            'header'=>'Harga',
                                                            'attribute'=>'material.harga',
                                                            'value'=>function($model,$key,$index){
                                                                return Yii::$app->help->toRp($model->hgsat_retail);  
                                                            },
                                                            'filter'=>'<input type="number" class="form-control input-sm" name="MaterialSearch[hgsat_retail]" value="'.$searchModel->hgsat_retail.'" placeholder="By Harga" >',
                                                            'enableSorting'=>true,
                                                        ],

                                                        // Merk                                                        
                                                        [
                                                            'header'=>'Merk',
                                                            'attribute'=>'material.merk',

                                                            'value'=>function($model,$key,$index){
                                                                return $model->merk;  
                                                            },
                                                            'filter'=>'<input type="text" class="form-control input-sm" name="MaterialSearch[merk]" value="'.$searchModel->merk.'" placeholder="By Merk" >',
                                                            'enableSorting'=>true,
                                                        ],

                                                        // 'id',
                                                        // 'kode',
                                                        // 'nama:ntext',
                                                        
                                                        // 'foto',
                                                        // 'model',
                                                        // 'hgsat_retail',
                                                        // 'hgsat_reseller',
                                                        // 'berat',
                                                        // 'satuan_berat',
                                                        // 'satuan',
                                                        // 'diskon',
                                                        // 'buyer_id',
                                                        // 'id_ktg',
                                                        // 'id_lokasi',
                                                        // 'id_grup',
                                                        // 'id_tipe',
                                                        // 'stok_awal',
                                                        // 'created_by',
                                                        // 'created_at',
                                                        // 'updated_at',
                                                        // 'transaksi',
                                                        // 'hg',
                                                        // 'kondisi',
                                                        // 'buyer_note:ntext',
                                                        // 'note:ntext',
                                                        // 'keterangan:ntext',
                                                        // 'warna',
                                                        // 'size',

                                                        ['class' => 'yii\grid\ActionColumn'],
                                                    ],
                                                  
                                                ]); ?>
                                            <?php Pjax::end(); 
                                                function toRp($number){
                                                    
                                                }
                                            ?>
                                                
                                        </div>
                                </div>
                            </div>
                            <div id="navpills-21" class="tab-pane">
                                 <div class="row">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>
                           
                        </div>     
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>