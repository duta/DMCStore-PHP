
<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
?>
<form class="">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <?=Html::DropDownList("Material[attr]",$model->v_material[0]['id_attr'],$attr,['id'=>'attr_json','class'=>'form-control select2me','required'=>'required',''=>'','placeholder'=>'-- Pilih Kategori --'])?>
                    <!-- <label class="control-label">First Name</label> -->
                    <!-- <input type="text" id="firstName" class="form-control" placeholder="John doe">  -->
                    <!-- <span class="help-block"> This is inline help </span>  -->
                </div>
            </div>
            <!--/span-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group " id="varian_json">
                    <?=Html::DropDownList("Material[varian]",null,$varian,['id'=>'varian','data-kd'=>$model->kode,'class'=>'select2ww','multiple'=>'multiple','placeholder'=>'-- Pilih Varian --']);?>
                    <!-- <label class="control-label">Last Name</label> -->
                    <!-- <input type="text" id="lastName" class="form-control" placeholder="12n">  -->
                    <!-- <span class="help-block"> This field has error. </span>  -->
                </div>
            </div>
            <!--/span-->
        </div>
<hr>
    <div id="load-varian-changed">
    <?php
        foreach ($model->v_material as $item) {
    ?>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon" style="width: 60%;background: white;text-align: none;border:none">
                                <b class="pull-left text-primary"><?=$item->nama?></b> <b class="pull-right"><?=$item->attr->nm_attr?> : <?=$item->varian->nm_varian?></b>
                            </span>     
                            <input type="file" name="foto[<?=$item->id?>]" class="MultiFile">    
                        </div>
                        <!-- <label class="control-label">First Name</label> -->
                        <!-- <input type="text" id="firstName" class="form-control" placeholder="John doe">  -->
                        <!-- <span class="help-block"> This is inline help </span>  -->
                    </div>
                </div>
                <!--/span-->
              
            </div>
    <?php
        }
    ?>
    </div>
</form>

