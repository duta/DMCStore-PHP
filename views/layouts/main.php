<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use app\assets\AppAsset;

    AppAsset::register($this);
    $user=Yii::$app->user->identity;
    $uriMaster=explode('?',Url::current());
    $uriMain = explode('/',ltrim($uriMaster[0],"/"));
    $uriget = explode('=',Url::current());
    if($uriMain[0]!=''){
        $index2=count($uriMain)-2;
        $index3=count($uriMain)-1;
        $urimain2=strtolower($uriMain[$index2]);
        $urimain3=strtolower($uriMain[$index3]);
    }else{
        $urimain2='';
        $urimain3='';

    }
    $getval=(count($uriget)>0)?strtolower(end($uriget)):'';
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/logo_ac.png">
    <?= Html::csrfMetaTags() ?>
    <title>Aisyah Collection</title>
    <?php $this->head() ?>
    
    <style type="text/css">
        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th {
            border: none;
        }
        /*general styles*/
            div#printable{display:none;}
        /* print styles*/
        @media print {
            div#printable {display:block;}
            div#screen {display:none;}
        }

        @media (max-width: 767px) {
            .table-responsive .dropdown-menu {
                position: static !important;
            }
        }
        @media (min-width: 768px) {
            .table-responsive {
                overflow: visible;
            }
        }
    </style>
</head>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
 


<body class="fix-header">


    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <?php $this->beginBody() ?>

    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="<?=Yii::$app->homeUrl?>">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon-->
                        <img height="35" src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/logo_ac1.png" alt="home" class="dark-logo" /><!--This is light logo icon-->

                        <!-- <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/admin-logo-dark.png" alt="home" class="light-logo" /> -->
                     </b>
                        <!-- Logo text image you can use text also -->
                        <span class="hidden-xs">
                            <!-- <B>DMC </B> --><!--  -->
                        <!--This is dark logo text-->
                        <!-- <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/admin-text.png" alt="home" class="dark-logo" /> -->
                        <!--This is light logo text-->

                        <!-- <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/admin-text-dark.png" alt="home" class="light-logo" /> -->
                     </span> </a>
                </div>
                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
               
                
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
                <?php
                if(Yii::$app->user->isGuest){
                ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light " id="modal-login" href="<?=Url::toRoute('site/login')?>"> 
                            <i class="mdi mdi-login"></i> Login
                        </a>
                    </li>
                    <li class="dropdown">
                        <a class="waves-effect waves-light cek-transaksi"  href="javascript:void(0)"> 
                            <i class="mdi mdi-shuffle"></i> Cek Transaksi
                        </a>
                    </li>
                <?php
                }else{
                ?>
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="javascript:void(0)"> <i class="mdi mdi-gmail"></i>
                            <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                        </a>
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <a href="javascript:void(0)">
                                        <div class="user-img"> <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li> -->
                    <!-- .Task dropdown -->
                   <!--  <li class="dropdown">
                        <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="javascript:void(0)"> <i class="mdi mdi-check-circle"></i>
                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <li>
                                <a href="javascript:void(0)">
                                    <div>
                                        <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:void(0)">
                                    <div>
                                        <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:void(0)">
                                    <div>
                                        <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="javascript:void(0)">
                                    <div>
                                        <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="javascript:void(0)"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                    </li> -->
                 <?php
                }
                ?>  
                </ul>

                <ul class="nav navbar-top-links navbar-right pull-right">

                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                <?php
                    if(!Yii::$app->user->isGuest){
                        
                ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)"> <img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/patrick.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?=$user->username?></b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/patrick.png" alt="user" /></div>
                                    <div class="u-text">
                                        <h4><?=$user->fullname?></h4>
                                        <p class="text-muted"><?=$user->email?></p><a  href="javascript:void(0)" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0)" class="change-pass"><i class="fa fa-key fa-fw"></i>Change password</a></li>
                           
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=Url::toRoute('site/logout')?>"><i class="fa fa-power-off fa-fw"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                    <?php
                    } 
                    ?>
                </ul>
              
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                <ul class="nav" id="side-menu">

                <?php 
                    if(Yii::$app->user->isGuest){
                        include('side_menu_guest.php');
                    }else{
                ?>
                    <li class="user-pro">
                        <a href="javascript:void(0)" class="waves-effect"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/patrick.png" alt="user-img" class="img-circle"> <span class="hide-menu"> <?=$user->fullname?><span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                            <li><a href="javascript:void(0)" class="change-pass"><i class="fa fa-key fa-fw"></i>Change password</a></li>
                            <li><a href="<?=Url::toRoute('site/logout')?>"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>

                        </ul>
                    </li>
                <?php
                        include('side_menu.php');
                    }
                ?>

                   
                   
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Starter Page</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        
                        <ol class="breadcrumb">
                            <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)">Dashboard</a></li>
                            <li class="active">Starter Page</li>
                        </ol>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title">Blank Starter page</h3> </div>
                    </div>
                </div> -->
                <div id="screen">
                <?=$content?>
                </div>
                <div  id="printable"> </div>
                <!-- CONTENT -->

                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="<?=Url::to('@web/web/ampleadmin')?>/javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme working">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?=Url::to('@web/web/ampleadmin')?>/plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center hidden-print"> 2017 - <?=date('Y')?> &copy; PT DUTA MEDIA CIPTA </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <?php $this->endBody() ?>
    
    <!-- /#wrapper -->
    <!-- jQuery -->
  
    <?php   
    include('script/HelperScript.php');
    include('script/SiteScript.php');  
    include('script/MainScript.php');   
    include('script/HomeScript.php');   
    include('script/MastermaterialScript.php');
    include('script/VpbScript.php');
    include('script/VbmScript.php');
    include('script/VbkScript.php');
    include('script/VordersScript.php');
    include('script/UserScript.php');
    include('script/BrsupplierScript.php');
    include('script/ItemattrScript.php');
    include('script/ForwarderScript.php');
    include('script/CustomerScript.php');
    include('script/KategoriBrgScript.php');
    include('script/KurirScript.php');
    include('script/DropshipperScript.php');
    include('script/MarketplaceScript.php');
    include('script/RoleScript.php');
    include('script/OprScript.php');
    include('script/VspendingScript.php');
    include('script/VcashflowScript.php');
    include('script/VmaterialstokScript.php');
  

    ?>
    <script type="text/javascript">
        $(document).ready(function(){
            var c="<?=$urimain2?>";
            // Main.init();
            if(c=='home'){
                Home.init();
            }else if(c=='mastermaterial'){
                Mastermaterial.init();
            }else if(c=='vpb'){
                Vpb.init();
            }else if(c=='vbm'){
                Vbm.init();
            }else if(c=='vorders'){
                VOrders.init();
            }else if(c=='vbk'){
                Vbk.init();
            }else if(c=='user'){
                User.init();
            }else if(c=='brsupplier'){
                Brsupplier.init();
            }else if(c=='forwarder'){
                Forwarder.init();
            }else if(c=='itemattr'){
                Itemattr.init();
            }else if(c=='customers'){
                Customer.init();
            }else if(c=='brktg'){
                brktg.init();
            }else if(c=='kurir'){
                kurir.init();
            }else if(c=='marketplace'){
                marketplace.init();
            }else if(c=='dropshipper'){
                dropshipper.init();
            }else if(c=='role'){
                role.init();
            }else if(c=='opr'){
                opr.init();
            }else if(c=='vspending'){
                vspending.init();
            }else if(c=='vcashflow'){
                vcashflow.init();
            }else if(c=='vmaterialstok'){
                vmaterialstok.init();
            }



            // alert(c);

            

            // MaterialDetail.init();
            // MaterialGrup.init();
            // MaterialLokasi.init();
            // MaterialHistory.init();
            // alert('hai');
            Site.init();
            Main.init();
            // alert(c);
        })
    </script>

</body>

<!-- MODAL -->
    <div class="modal fade hidden-print" id="basic" tabindex="-1" role="basic" aria-hidden="true" style="background: rgba(0, 0, 0, 0.8);">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-close"></i>
                    </button>
                    <h3 class="modal-title bold"></h3>
                </div>
                <div class="modal-body"></div>
                <!-- <div class="modal-footer">
                    
                </div> -->
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</html>
<?php $this->endPage() ?>

