<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Mastermaterial = function () {
    var listVarian = function(kd_material){
       
            start();
            $("#basic .modal-dialog").removeClass("modal-lg");
            modalPost("<?=Url::toRoute(['material/variantable'])?>",{kd:kd_material},"List barang varian");
        
    }

	var crudMaterial = function(){
		// create material
		$("button.add_material").click(function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('mastermaterial/create')?>",{},"Tambah Barang");
        });

        // creating material
        $("#basic .modal-content .modal-body").on("submit","form#form_material",function(e){
            e.preventDefault();
            start("#basic .modal-content");

            var form = $(this);
            var url_val = $(this).attr('action');
            $.ajax({
                type:'POST',
                url:url_val,
                data: new FormData(form[0]),
                contentType:false,
                processData:false,  
                dataType:'json',
                success:function(response){
                    $("#basic").modal('hide');
                    // toast(response.pesan,"success","check");
                    reloadGridview(response.pesan,response.status);
                }
            });
        });

        // Del barang
        $("div#p0").on("click","a.del_item",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['mastermaterial/delete'])?>/"+id_val,{kd:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "User berhasil di hapus.", "success"); 
                });
            });
           
        });

       // Edit Material
        $("div#p0").on("click","a.edit_item",function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('mastermaterial/update')?>/"+$(this).attr('id'),{},"Update Barang");
        });

        //Load varian combo
        $("#basic .modal-content .modal-body").on("change","select#attr",function(){
            attr=$(this).val();
            $("div#varian_combo").html("<i class='fa fa-refresh fa-spin'></i>"); 

            e_post("<?=Url::toRoute(['material/variancombo'])?>",{id_attr:attr},function(response){
                $("div#varian_combo").html(response);
                $("select.itemselect2").select2({allowClear:true});
            });
        });


        //Load varian combo json
        $("#basic .modal-content .modal-body").on("change","select#attr_json",function(){
            attr=$(this).val();
            $(".load-varian-changed").html("<i class='fa fa-refresh fa-spin'></i>"); 

            e_post("<?=Url::toRoute(['material/variancombo'])?>",{id_attr:attr,json:1},function(response){
                // $("div#varian_json").html(response);
                $("div#varian_json").find("select.itemselect2").select2({allowClear:true});
                // alert(response);
            },'json');
        });

        // Change Varian
        $("div#p0").on("click","a.var_item",function(){
            start();
            id_val=$(this).attr('id');
            $("#basic .modal-dialog").removeClass("modal-lg");
            modalPost("<?=Url::toRoute(['mastermaterial/changephoto'])?>",{kd:id_val},"Atur foto");
        });

      

        $("#basic .modal-content .modal-body").on("submit","form#form-phovar-changed",function(e){
            e.preventDefault();
            startModal();
            var form = $(this);
            var url_val = "<?=Url::toRoute(['mastermaterial/changephoto'])?>";
            $.ajax({
                type:'POST',
                url:url_val,
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    toast(response.pesan,"success","check");
                    endModal();
                    closeModal();
                }
            });
        });

        $("div#p0").on("click","a.price_item",function(){
            id_val=$(this).attr('id');
            listVarian(id_val);
        });

        $("#basic .modal-content .modal-body").on("click","button.btn-price",function(){
            id_val=$(this).attr('id');
            $("#basic .modal-dialog").addClass("modal-lg");
            // alert('hai');
            startModal();
            modalPost("<?=Url::toRoute(['mastermaterial/changeprice'])?>",{id:id_val},"Atur Harga");
        });
        
        $("#basic .modal-content .modal-body").on("click","button.btn-backtovarian",function(){
            id_val=$(this).attr('id');
            listVarian(id_val);
        });

            
        $("#basic .modal-content .modal-body").on("submit","form#form-price",function(e){
            e.preventDefault();
            startModal();
            e_post("<?=Url::toRoute(['mastermaterial/changeprice'])?>",$(this).serialize(),function(response){
                listVarian(response.kd);
                ok(response.pesan);
            },'json');

        });

        
	}

	var input_grosir= function(){
        // HARGA GROSIR
        $("#basic .modal-content .modal-body").on("click","a.add-hg",function(){
            if($("input.hrg-s").val()==''){
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                $(".hg-wrap").fadeToggle('fast');

            }
            // console.log($("input.hrg-s").val());
        });

        $("#basic .modal-content .modal-body").on("click","button.add-row-hg",function(){
            $("div.row-hg:last").clone().appendTo("div.hg-wrap").find("input").val('');
            $(this).last().hide();
            $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});

        });

        $("#basic .modal-content .modal-body").on("click","button.rm-row-hg",function(){
            var this_el=$(this);
            // alert($("button.rm-row-hg").length);
            if($("button.rm-row-hg").length>1){
                this_el.parents('.row-hg').remove();
                $("button.add-row-hg:last").show();
            }
        });

        $("#basic .modal-content .modal-body").on("keyup","input.hrg-g",function(){
            var hrg_s=toNumberOnly($("input.hrg-s").val());
            var hrg_g=$(this);
            if($("input.hrg-s").val()==''){
                hrg_g.val('Rp 0');
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                if(toNumberOnly(hrg_g.val())>=hrg_s){
                    hrg_g.val('Rp 0');
                }
            }
        });

        $("div#p0").on("click",".foto_item",function(){
            // alert($(this).attr('id'));
            // $("#basic .modal-dialog").addClass("modal-lg");

            id_val=$(this).attr('id');
            start();
            e_post("<?=Url::toRoute(['mastermaterial/viewphoto'])?>",{kd:id_val},function(response){
                $("#basic .modal-content .modal-title").html('Lihat Foto');
                $("#basic .modal-content .modal-body").html(response);
                // $("#bzoom").zoom({
                //   zoom_area_width: 300,
                //     autoplay_interval :3000,
                //     small_thumbs : 4,
                //     autoplay : true
                // });
                $("#basic").modal({backdrop:false});     
                end();
            });  
        });

        $("button.imp_xls").click(function(){
            // alert("clicked !"); 
            normalmodal();
            modalGet("<?=Url::toRoute(['mastermaterial/imp_xls'])?>",{},"Import Excel");
        });

        $(".modal-body").on("submit","#form-impxls",function(e){
            e.preventDefault();
            var form=$(this);
            var url="<?=Url::toRoute(['mastermaterial/imp_xls'])?>";
            var data=new FormData(form[0]);
            startModal();
            post_upload(url,data,function(response){
                endModal();
                closeModal();
                reloadGridview("Import sukses !",1);
            },'json');
        })


    }


	return {
		init: function () {
            // listVarian();
			crudMaterial();
			input_grosir();

		}
	}	
}();
</script>