<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var brktg= function () {
	var crudbrktg = function(){
		// add brktg
		$("button.add_ktg").click(function(){
			modalGet("<?=Url::toRoute('brktg/create')?>",{},'<i class="mdi mdi-arrange-send-backward fa-fw"></i>Tambah Kategori');
		});
		// update brktg
		$("div#p0").on("click","button.update_ktg",function(){
			modalGet("<?=Url::toRoute('brktg/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-arrange-send-backward fa-fw"></i>Update Kategori');
		});

        // adding/updating brktg
		$("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
            	// alert(response.errors.length);
            	if(response.length <1){
            		reloadGridview('Action success',1);
            	}else{
            		error(response.errors);
            		endModal();
            	}
            },'json');
        });

        // Del brktg
        $("div#p0").on("click","button.del_ktg",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['brktg/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Kategori berhasil di hapus.", "success"); 
                });
            });
           
        });
	}

	return {
		init: function () {
			crudbrktg();
		}
	}	
}();
</script>