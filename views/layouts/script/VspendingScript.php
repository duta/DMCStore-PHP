<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var vspending = function () {
	  var crudpengeluaran = function(){
        // add pengeluaran
        $("button.add_pengeluaran").click(function(){
            // lgmodal();
            modalGet("<?=Url::toRoute('vspending/create')?>",{},'<i class="mdi mdi-plus fa-fw"></i>Tambah Pengeluaran');
        });
        // update pengeluaran
        $("div#p0").on("click","a.update_pengeluaran",function(){
            modalGet("<?=Url::toRoute('vspending/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-lead-pencil fa-fw"></i>Update Pengeluaran');
        }); 

        // adding/updating pengeluaran
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            var form=$(this);
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            var data=new FormData(form[0]);
            post_upload(url,data,function(response){
                if(response.status == 1){
                    reloadGridview(response.pesan,1);
                }else{
                    error(response.pesan);
                    endModal();
                }
            },'json');
        });
        // view_bukti
        $("div#p0").on("click","a.view_bukti",function(){
            window.open("<?=Url::base()?>/file_uploaded/opr/spending/"+$(this).attr('data-filename')).focus();
        });
        // Del pengeluaran
        $("div#p0").on("click","a.del_pengeluaran",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute('vspending/delete')?>/"+id_val,{id:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Pengeluaran berhasil di hapus.", "success"); 
                });
            });
           
        });

         // Del pengeluaran
        $("button.paid_pengeluaran").click(function(){

            id_val=$(this).attr('id');
            var checked=$("div#p0").find("input.check_item:checked");
            if(checked.length > 0){
                var data=checked.serialize();
                
                swal_help("Apakah anda yakin ?","Pengeluaran terpilih, akan diklaim 'sudah dibayar'","warning",function(){
                    e_post("<?=Url::toRoute('vspending/paid')?>",data,function(){
                        swal("Dibayar !", "Pengeluaran terpilih telah dibayar.", "success"); 
                    });
                });

            }else{
                error(" Anda belum pilih ceklist di tabel !");
            }
           
        });

	}
	return {
		init: function () {
			crudpengeluaran();
		}
	}	
}();
</script>