<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var vcashflow = function () {
	  var crudcashflow = function(){
        // add cashflow
        $("button.add_cashflow").click(function(){
            // lgmodal();
            modalGet("<?=Url::toRoute('vcashflow/create')?>",{},'<i class="mdi mdi-plus fa-fw"></i>Tambah Cashflow');
        });
        // update cashflow
        $("div#p0").on("click","a.update_cashflow",function(){
            modalGet("<?=Url::toRoute('vcashflow/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-lead-pencil fa-fw"></i>Update Cashflow');
        });

        // adding/updating cashflow
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            var form=$(this);
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            var data=new FormData(form[0]);
            post_upload(url,data,function(response){
                if(response.status == 1){
                    reloadGridview(response.pesan,1);
                }else{
                    error(response.pesan);
                    endModal();
                }
            },'json');
        });
        // view_bukti
        $("div#p0").on("click","a.view_bukti",function(){
            window.open("<?=Url::base()?>/file_uploaded/opr/spending/"+$(this).attr('data-filename')).focus();
        });
        // Del cashflow
        $("div#p0").on("click","a.del_cashflow",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute('vcashflow/delete')?>/123",{kd_cf:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Cashflow berhasil di hapus.", "success"); 
                });
            });
           
        });

	}
	return {
		init: function () {
			crudcashflow();
            // alert('hai');
		}
	}	
}();
</script>