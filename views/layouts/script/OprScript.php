<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var opr = function () {
	  var crudoperasional = function(){
        // add operasional
        $("button.add_operasional").click(function(){
            // lgmodal();
            title = ($(this).attr('id')==1)?'Cash Flow':'Pengeluaran';
            modalGet("<?=Url::toRoute('opr/create')?>",{cf:$(this).attr('id')},'<i class="mdi mdi-plus fa-fw"></i>Tambah '+title);
        });
        // update operasional
        $("div#p0").on("click","button.update_operasional",function(){
            modalGet("<?=Url::toRoute('opr/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-lead-pencil fa-fw"></i>Update operasional');
        });

        // adding/updating operasional
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                if(response.status == 1){
                    reloadGridview(response.pesan,1);
                }else{
                    error(response.pesan);
                    endModal();
                }
            },'json');
        });

        // Del operasional
        $("div#p0").on("click","button.del_operasional",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['opr/delete'])?>/"+id_val,{id:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "operasional berhasil di hapus.", "success"); 
                });
            });
           
        });

	}
	return {
		init: function () {
			crudoperasional();
		}
	}	
}();
</script>