<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var role= function () {
    var crudrole = function(){
        // add role
        $("button.add_role").click(function(){
            modalGet("<?=Url::toRoute('role/create')?>",{},'<i class="mdi mdi-truck-delivery fa-fw"></i>Tambah role');
        });
        // update role
        $("div#p0").on("click","button.update_role",function(){
            modalGet("<?=Url::toRoute('role/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-truck-delivery fa-fw"></i>Update role');
        });

        // adding/updating role
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                // alert(response.errors.length);
                if(response.length <1){
                    reloadGridview('Action success',1);
                }else{
                    error(response.errors);
                    endModal();
                }
            },'json');
        });

        // Del role
        $("div#p0").on("click","button.del_role",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['role/delete'])?>/"+id_val,{id:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "role berhasil di hapus.", "success"); 
                });
            });
           
        });


        // Permission role
        $("div#p0").on("click","button.permission_role",function(){
            id_val=$(this).attr('id');
            lgmodal();
            modalPost("<?=Url::toRoute('role/change_permit')?>",{role_id:id_val},"HAK AKSES "+$(this).attr('data-nama'));
        });
        
        $("#basic .modal-content .modal-body").on("submit","form#form-permit",function(e){
            e.preventDefault();
            startModal();
            e_post("<?=Url::toRoute('role/permit_added')?>",$(this).serialize(),function(response){
                ok("Hak akses telah diperbarui !");
                closeModal();
                endModal();
            });
        });


        $("#basic .modal-content .modal-body").on("change",".checkbox_parent",function(e){
            var check_child=$(".checkbox_child-"+$(this).val());
            check_child.prop('checked',$(this).prop('checked'));
        });


        $("#basic .modal-content .modal-body").on("change","#check-all",function(e){
            $("input:checkbox").prop('checked',$(this).prop('checked'));
        });

        $("#basic .modal-content .modal-body").on("change","input:checkbox",function(e){
            if($(this).prop('checked')==false){
                $("#check-all").prop('checked',$(this).prop('checked'));
            }
        });
    }

    return {
        init: function () {
            crudrole();
        }
    }   
}();
</script>