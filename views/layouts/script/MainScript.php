<?php
    use yii\helpers\Url;
	
?>
<script type="text/javascript">
var Main = function () {
	var changePass = function(){
		// $('.tooltip').tooltip();
		// $(".counter").counterUp({
  //            delay: 100,
  //            time: 1200
  //       }); 
  		// alert('hai');
  		$("a.change-pass").click(function(){
  			// alert('hai');
  			start();
  			modalPost("<?=Url::toRoute('site/changepass')?>",{},"<i class='fa fa-key fa-fw'></i> Change Password");
  		})

  		$("#basic .modal-content .modal-body").on("submit","form#form-change-pass",function(e){
            e.preventDefault();
            // alert('hai');
            if($("input#conf_pass").val().length < 6 ){
            	error("Password minimal 6 karakter !");
            }else{
            	if($("input#conf_pass").val()===$("input#new_pass").val()){
            		startModal();
            		e_post("<?=Url::toRoute('site/changepass')?>",{new_pass:$("input#conf_pass").val()},function(){
            			ok("Password anda berhasil di perbarui.");
            			endModal();
            			closeModal();
            		});
            	}else{
            		error("Gagal konfirmasi password, silahkan cek kembali !");
            	}
            }
        });
	}


    return {
        //main function to initiate map samples
        init: function () {
            // Notice();
         //    setInterval(function(){
         //    	Notice();    	
	        // },10000);
            changePass();
            // alert('hai')

            $('#w0-container .table-responsive').on('show.bs.dropup', function () { 
                $('.table-responsive').css( "overflow", "visible !important " ); 
            });
// 
            $('#w0-container .table-responsive').on('hide.bs.dropup', function () { 
              $('.table-responsive').css( {"overflow":"auto"} ); 
            }) 
        }

    };

}();
</script>