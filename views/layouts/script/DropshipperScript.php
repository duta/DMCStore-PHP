<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var dropshipper= function () {
    var cruddrop = function(){
        // add dropshipper
        $("button.add_drop").click(function(){
            modalGet("<?=Url::toRoute('dropshipper/create')?>",{},'<i class="mdi di-account-star fa-fw"></i>Tambah Dropshipper');
        });

        // update dropshipper
        $("div#p0").on("click","button.update_drop",function(){
            modalGet("<?=Url::toRoute('dropshipper/update')?>/"+$(this).attr('id'),{},'<i class="mdi di-account-star fa-fw"></i>Update Dropshipper');
        });

        // adding/updating dropshipper
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                // alert(response.errors.length);
                if(response.length <1){
                    reloadGridview('Action success',1);
                }else{
                    error(response.errors);
                    endModal();
                }
            },'json');
        });

        // Del dropshipper
        $("div#p0").on("click","button.del_drop",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
            e_post("<?=Url::toRoute(['dropshipper/delete'])?>/"+id_val,{id:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Kurir berhasil di hapus.", "success"); 
                });
            });
        });
    }

    return {
        init: function () {
            cruddrop();
        }
    }   
}();
</script>