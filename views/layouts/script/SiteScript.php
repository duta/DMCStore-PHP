<?php
    use yii\helpers\Url;

    $token=Yii::$app->request->csrfToken;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Site = function () {


    var loadData = function(pesan,status,withConfig=1){
        if(withConfig<1){
            start();
        }
        e_post("<?=Url::toRoute('material/product')?>",{_csrf:"<?=$token?>"},function(response){
            $("div.table-body").html(response);
            if(withConfig>0){
                end("#basic .modal-content");
                resetForm();
                $("#basic").modal('hide');   
            }else{
                end();
            }


            if(status==1){
                toast(pesan,"success","check");
            }else{
                toast(pesan,"danger","close");
            }
            $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
              popout:true
            });
            $('[data-toggle="tooltip"]').tooltip(); 
        });
    }

    var transaksi = function(){

        $("div.table-body").on("click","a.buy_product",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('material/buy')?>",{id:$(this).attr('id'),_csrf:"<?=$token?>"},"Data Pembelian");
        })
        $("div.table-body").on("click","a.detail_material",function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/material/view')?>/"+$(this).attr('id'),{_csrf:"<?=$token?>"},$(this).attr('data-nama'));
        });

        $("#basic .modal-content .modal-body").on("click","button.bootstrap-touchspin-up,button.bootstrap-touchspin-down",function(){
            val=parseFloat($('input.touchspin').val());
            price=parseFloat($("input.hidden_price").val());
            result=price*val;
            $(".label-price").text(toRp(result));
            $(".label-price-total").val(result);
        });

        $("#basic .modal-content .modal-body").on("submit","#form-checkout",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            e_post("<?=Url::toRoute('materialdetail/createtransaksinotlogin')?>",$(this).serialize(),function(response){
                if(response.status==1){
                    kd_t=response.kode_transaksi;
                    modalPost("<?=Url::toRoute('materialdetail/payment')?>",{kd_trans:kd_t,_csrf:"<?=$token?>"},"Tagihan Pembayaran");
                }else{
                    toast(" Gagal melanjutkan ke pembayaran ! ","danger");
                }
            },'json');
        })
        $("#basic .modal-content .modal-body").on("submit","#form-paid",function(e){
            e.preventDefault();
            start("#basic .modal-content");
           
            var form = $(this);
            $.ajax({
                type:'POST',
                url:"<?=Url::toRoute('materialdetail/payment')?>",
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                   if(response.status==1){
                        $("#basic").modal('hide');
                        swal("Berhasil !", "Konfirmasi Pembayaran telah berhasil, Mohon tunggu sebentar. Kami akan segera merespon transaksi anda. Silahkan cek transaksi anda di menu 'CEK TRANSAKSI'", "success"); 
                   }else{
                        $("#basic").modal('hide');
                        swal("Gagal !", "Konfirmasi Pembayaran gagal. Hubungi CS kami ", "error");
                   }
                }
            });
          
        });

        $("a.cek-transaksi").click(function(){
            modalPost("<?=Url::toRoute('materialdetail/cek_transaksi')?>",{_csrf:"<?=$token?>"},"Cek Transaksi");
        });

        $("#basic .modal-content .modal-body").on("submit","#form_cek_tr",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/cek_transaksi')?>",$(this).serialize(),"Status Transaksi");
        });

        $("#basic .modal-content .modal-body").on("submit","form#form-status-transaksi",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            e_post("<?=Url::toRoute('materialdetail/terimabrg')?>",$(this).serialize(),function(response){
                if(response.status==1){
                    swal("Berhasil !",response.pesan,"success");
                }else{
                    swal("Gagal !",response.pesan,"error");

                }
                end("#basic .modal-content");
                $("#basic").modal('hide');

            },'json');
        });        
    }

    var site = function(){
        // alert('hai');
        $("a#modal-login").click(function(){
            modalPost("<?=Url::toRoute('site/login')?>",{_csrf:"<?=$token?>"},"LOGIN");
        })
    }
    var register = function(){

    }
    return {
        //main function to initiate map samples
        init: function () {
            // alert("<?=Url::current()?>");
            <?php
                if( $urimain2=='site' && $urimain3=='index' ){
            ?>
                    loadData("Data Berhasil di muat !",1,0);
                    transaksi();
                    site();
            <?php
                }
                $user = Yii::$app->user->identity;

            ?>
            // alert('hai');
            

        }

    };

}();
</script>