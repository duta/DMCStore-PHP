<?php
  use Yii\helpers\Url;
?>
<script type="text/javascript">
    var init_plug = function(){
        $("select.select2me,select.select2,select.init_select2").select2({
              'placeholder' : function() {
                              $(this).data('placeholder');
                            }
            });
            $("select.itemselect2").select2({allowClear:true});

            $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
            $("input.noRp").maskMoney({prefix:'',thousands:'.',decimal:',',precision:0});
            $('input.MultiFile').MultiFile({
                max: 1,
                accept: 'jpg|jpeg|png|gif|zip|rar|xls|xlsx|ppt|pptx|doc|docx|pdf',
                STRING:{
                      remove:"<i title='Klik untuk hapus' class='text-danger fa fa-remove'></i>", 
                      denied:"Ekstensi file '$ext' tidak di perbolehkan ! ekstensi file yang di perbolehkan : 'jpg', 'jpeg', 'gif', 'png','rar','zip','xls','xlsx','ppt','pptx','doc','docx','pdf'",
                      toomuch:"Ukuran file harus kurang dari 2 MB !",
                      duplicate:"File : '$file', Sudah anda pilih sebelumnya ! ",
                    toomany: 'Hanya diperbolehkan upload maksimal $max file saja !',
                  }
            });
            $("[data-toggle='tooltip']").tooltip();
            $("input.touchspin").TouchSpin({ min: 1});
            
            $('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy',
            });

            // $("select.select2me").select2();
            // $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
            // $('input.MultiFile').MultiFile({
            //     max: 1,
            //     accept: 'jpg|jpeg|png|gif|zip|rar|xls|xlsx|ppt|pptx|doc|docx|pdf',
            //     STRING:{
            //           remove:"<i title='Klik untuk hapus' class='text-danger fa fa-remove'></i>", 
            //           denied:"Ekstensi file '$ext' tidak di perbolehkan ! ekstensi file yang di perbolehkan : 'jpg', 'jpeg', 'gif', 'png','rar','zip','xls','xlsx','ppt','pptx','doc','docx','pdf'",
            //           toomuch:"Ukuran file harus kurang dari 2 MB !",
            //           duplicate:"File : '$file', Sudah anda pilih sebelumnya ! ",
            //         toomany: 'Hanya diperbolehkan upload maksimal $max file saja !',
            //       }
            // });
            // $("input.touchspin").TouchSpin({ min: 1});
            // $("[data-toggle='tooltip']").tooltip();
            // $("input.touchspin").TouchSpin({ min: 1});
            // $('.datepicker').datepicker({
            //     autoclose: true,
            //     todayHighlight: true,
            //     format: 'dd-mm-yyyy',
            // });
    }


    var resetForm = function (){
        $("button,a").removeAttr('disabled');
        $("input,textarea").val("");
    }
    
    var start = function (selector=null){
        if(selector!=null){
          $(selector).block({message: '<i class="fa fa-spin fa-circle-o-notch"></i> Loading'});  
        }else{
          $.blockUI({message: '<i class="fa fa-spin fa-circle-o-notch"></i> Loading'});   
        }
      
    }
    var end = function (selector=null){
        if(selector!=null){
          $(selector).unblock();
        }else{
          $.unblockUI();
        }
    }

    var startModal = function(){
        start("#basic .modal-content");
    }

    var endModal = function(){
        end("#basic .modal-content");
    }
    var closeModal=function(){
        $("#basic").modal('hide');  
      
    }

    var lgmodal=function(){
      $("#basic .modal-dialog").addClass("modal-lg");
    }

    var normalmodal=function(){
      $("#basic .modal-dialog").removeClass("modal-lg modal-sm");
    }

    var fail_act = function(errors, textStatus, errorThrown) {
          lgmodal();
          $("#basic .modal-content .modal-title").html(errors.statusText+' ('+errors.status+') <i class="m-l-15 fa fa-quote-left fa-fw"></i><small class="text-danger m-l-5 m-r-5">Silahkan screenshot error ini, lalu kirimkan ke developer terkait !</small><i class="fa fa-quote-right fa-fw"></i>');
          $("#basic .modal-content .modal-body").html(errors.responseText);
          $("#basic").modal('show');  
          endModal();
          end();   
    }
    var toast = function(messagenya,typenya,iconnya,cont=null){
        if(cont==null){
            var cont_val=".note-alert";
        }else{
            var cont_val=cont;
        }
        // $.alert({
        //     container: cont_val, // alerts parent container(by default placed after the page breadcrumbs)
        //     place: "prepend", // append or prepent in container 
        //     type: typenya,  // alert's type
        //     message: messagenya,  // alert's message
        //     close: true, // make alert closable
        //     reset: true, // close all previouse alerts first
        //     focus: true, // auto scroll to the alert after shown
        //     closeInSeconds: 5, // auto close after defined seconds
        //     icon: iconnya // put icon before the message
        // });
     

        // $.toast({
        //   "closeButton": true,
        //   "debug": false,
        //   "positionClass": "toast-top-right",
        //   "onclick": null,
        //   "showDuration": "1000",
        //   "hideDuration": "1000",
        //   "timeOut": "10000",
        //   "extendedTimeOut": "1000",
        //   "showEasing": "swing",
        //   "hideEasing": "linear",
        //   "showMethod": "fadeIn",
        //   "hideMethod": "fadeOut"
        // }
        if(typenya=="success"){
          $.toast({
            heading: 'Pemberitahuan :',
            text: messagenya,
            position: 'top-right',
            loaderBg:'#fec107',
            icon: 'success',
            hideAfter: 5000, 
            stack: 6
          });
        }else if(typenya=="danger"){
          $.toast({
            heading: 'Pemberitahuan :',
            text: messagenya,
            position: 'top-right',
            loaderBg:'#fec107',
            icon: 'error',
            hideAfter: 5000, 
            stack: 6
          });
          
        }

    }

    var error=function(messagenya){
        toast(messagenya,'danger','warning');
    }
    var ok=function(messagenya){
        toast(messagenya,'success','check');
    }
    var CmodalPost = function(url,data,title,OnSuccess,backdroparam=false){
      start("#basic .modal-content");
      start();
      $.post(url,data,OnSuccess).fail(fail_act);
    }  
    var modalPost = function(url,data,title,backdroparam=false){
        start("#basic .modal-content");
        start();
        $.post(url,data,function(response){
            $("#basic .modal-content .modal-title").html(title);
            $("#basic .modal-content .modal-body").html(response);
            
            init_plug();

            $("#basic").modal({backdrop:backdroparam});     

            end("#basic .modal-content");
            end();
            //Load varian combo json
            $("select#attr_json").change(function(){
                attr=$(this).val();
                $("load-varian-changed").html("<i class='fa fa-refresh fa-spin'></i>"); 

                $.post("<?=Url::toRoute(['material/variancombo'])?>",{id_attr:attr,json:1},function(response){
                    // $("div#varian_json").html(response);
                    $("select.itemselect2").select2({allowClear:true});
                    // alert(response);
                },'json');
            });

            
        }).fail(fail_act);
    }

    var modalPostPrint = function(url,data,title,backdroparam=false){
        start("#basic .modal-content");
        $.post(url,data,function(response){
            $("#basic .modal-content .modal-title").html(title);
            $("#basic .modal-content .modal-body").html(response);
            $("#basic").modal({backdrop:backdroparam});     
            $("div#printable").html($("div.modal-body").html());
            end("#basic .modal-content");
            end();
        }).fail(fail_act);
    }
    var modalGet = function(url,data,title,backdroparam=false){
        start();
        start("#basic .modal-content");
        
        $.get(url,data,function(response){
            $("#basic .modal-content .modal-title").html(title);
            $("#basic .modal-content .modal-body").html(response);
            init_plug();
            $("#basic").modal({backdrop:backdroparam});     

            end("#basic .modal-content");
            end();
        }).fail(fail_act);
    }

    var modalHtml = function($html,title,backdroparam=false){
        // start();
        // start("#basic .modal-content");
        

        $("#basic .modal-content .modal-title").html(title);
        $("#basic .modal-content .modal-body").html($html);
        $("#basic").modal({backdrop:backdroparam});
        // end("#basic .modal-content");
        // end();
 
    }

    var timeSinceFunc = function(dateparm) {
      var date=new Date(dateparm);
      var seconds = Math.floor((new Date() - date) / 1000);

      var months = ["Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember"];
      if (seconds < 5){
          return "baru saja";
      }else if (seconds < 60){
          return seconds + " detik yang lalu";
      }
      else if (seconds < 3600) {
          minutes = Math.floor(seconds/60)
          if(minutes > 1)
              return minutes + " menit yang lalu";
          else
              return "1 menit yang lalu";
      }
      else if (seconds < 86400) {
          hours = Math.floor(seconds/3600)
          if(hours > 1)
              return hours + " hours yang lalu";
          else
              return "1 jam yang lalu";
      }
      //2 days and no more
      else if (seconds < 172800) {
          days = Math.floor(seconds/86400)
          if(days > 1)
              return days + " hari yang lalu";
          else
              return "1 hari yang lalu";
      }
      else{

          //return new Date(time).toLocaleDateString();
          return date.getDate().toString() + " " + months[date.getMonth()] + ", " + date.getFullYear();
      }
    }

    var toRp = function (angka,rp=1){
      var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
      var rev2    = '';
      for(var i = 0; i < rev.length; i++){
          rev2  += rev[i];
          if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
              rev2 += '.';
          }
      }
      var Rp=(rp==0)?'':'Rp ';
      return Rp + rev2.split('').reverse().join('');
  }

  var windowCetak = function (htmlContent)
  {
    myWindow=window.open('','_blank',',width=1000,height=500,top=100,left=150,toolbars=no,scrollbars=yes,status=no,resizable=no');
    if(myWindow==null){
      toast("Mohon izinkan kami membuka jendela POP-UP !","danger");
    }else{
      myWindow.document.writeln(htmlContent);
      myWindow.document.close(); //missing code
      myWindow.focus();
      setTimeout(function(){
        myWindow.print(); 
      },500);
    }
  }

  var swal_help=function(title_v,text_v,type_v ,onConfirm,canceltext="TIDAK",confirmtext="YA"){
    swal({   
        title: title_v,   
        text: text_v,   
        type: type_v,   
        showCancelButton: true, 
        cancelButtonText: canceltext,     
        confirmButtonColor: "#f0c541",   
        confirmButtonText: confirmtext,   
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        allowOutsideClick: false,
    },onConfirm
    // function(){   
    //     swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
    // }
    );
  }


  var toNumberOnly=function(stringVal='0'){
    if( (stringVal=='' || stringVal==null) ){
      return 0;
    }else{
      var a=stringVal.replace(/[^0-9]/g,'');
      return parseFloat(a);
    }
  }

  var checkAll = function(selector_checkbox_all,selector_table){
    $(selector_checkbox_all).click(function (e) {
        $(selector_table+' tbody :checkbox').prop('checked', $(this).is(':checked'));
        e.stopImmediatePropagation();
    });
  }
  
  function get_random_color() {
    function c() {
      var hex = Math.floor(Math.random()*256).toString(16);
      return ("0"+String(hex)).substr(-2); // pad with zero
    }
    return "#"+c()+c()+c();
  }
  function reloadGridview(pesan,status,withConfig=1){
    if(withConfig>0){
        end("#basic .modal-content");
        resetForm();
        $("#basic").modal('hide');  
    }
    
    $('#w0').yiiGridView('applyFilter');

    if(status==1){
        toast(pesan,"success","check");
    }else{
        toast(pesan,"danger","close");
    }
  }

  function e_post(url,data,OnSuccess,responseType=null){
      $.post(url,data,OnSuccess,responseType)
      .fail(fail_act);
  };
  function post_upload(url,data,OnSuccess,responseType=null){
      $.ajax({
          type:'POST',
          url:url,
          data: data,
          contentType:false,
          processData:false,
          dataType:responseType,
          success:OnSuccess,
          error:fail_act
      });
  }
</script>