<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Itemattr = function () {
	var crudItemattr = function(){
		// add Atribut
		$("button.add_attr").click(function(){
			modalGet("<?=Url::toRoute('itemattr/create')?>",{},'<i class="mdi mdi-arrange-bring-to-front fa-fw"></i>Tambah Atribut');
		});
		// update Atribut
		$("div#p0").on("click","button.update_attr",function(){
			modalGet("<?=Url::toRoute('itemattr/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-arrange-bring-to-front fa-fw"></i>Update Atribut');
		});

        // adding/updating Atribut
		$("#basic .modal-content .modal-body").on("submit","form#form_attr",function(e){
            e.preventDefault();
            start("#basic .modal-content"); 
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

        // Del Atribut
        $("div#p0").on("click","button.del_attr",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Jika atribut dihapus, maka varian jg ikut terhapus !","warning",function(){
                e_post("<?=Url::toRoute(['itemattr/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Atribut berhasil di hapus.", "success"); 
                });
            });
           
        });
        // Atur varian
        $("div#p0").on("click","button.atur_varian",function(){
            modalPost("<?=Url::toRoute('itemattr/changevarian')?>",{id:$(this).attr('id')},'<i class="mdi mdi-arrange-bring-to-front fa-fw"></i>Atur Varian');
        });
	}

    var varian= function(){
     
        $("#basic .modal-content .modal-body").on("click","a.add-hg",function(){
            $(".hg-wrap").fadeToggle('fast');
            // console.log($("input.hrg-s").val());
        });

        $("#basic .modal-content .modal-body").on("click","button.add-row-hg",function(){
            $("div.row-hg:last").clone().appendTo("div.hg-wrap");
            $("div.row-hg:last").find("input").val('').attr({'name':'new[]','data-proses':'new_row'});
            $("div.row-hg:last").find("span.input-group-addon").html('<i class="fa fa-plus"></i>');

            $(this).last().hide();    
        });

        $("#basic .modal-content .modal-body").on("click","button.rm-row-hg",function(){
            var this_el=$(this);
            action=this_el.parents("div.row-hg").find("input").attr('data-proses');
            // alert(action);
            if($("button.rm-row-hg").length>1){
                if(action=='set_row'){
                    var varian=$("input[name='set["+this_el.attr('id')+"]']").val();
                    if(confirm('Hapus varian : '+varian+' ?')){
                        startModal();
                        e_post('<?=Url::toRoute(["itemattr/delvar"])?>',{id:this_el.attr('id')},function(){
                            endModal();
                            this_el.parents('.row-hg').remove();
                            $("button.add-row-hg:last").show();    
                        });
                    }
                }else{
                    this_el.parents('.row-hg').remove();
                    $("button.add-row-hg:last").show();
                }
            }
        });

        // $("#basic .modal-content .modal-body").on("keyup","input.input-var",function(){
        //     $('.span-addon').show();
        //     $('.span-btn').hide();
        //     this_el=$(this);
        //     if(this_el.val()!=$(this).attr('data-val') && this_el.val()!=''){
        //         if(this_el.attr('data-val')!='new_row'){
        //             $(this).parent('.input-group').find('.span-addon').hide();
        //             $(this).parent('.input-group').find('.span-btn').show();
        //         }
        //     }else{
        //         $('.span-addon').show();
        //         $('.span-btn').hide();
        //     }
        // }); 


        $("#basic .modal-content .modal-body").on("submit","form#form-updatedvarian",function(e){
            e.preventDefault();
            startModal();
            e_post("<?=Url::toRoute(['itemattr/updatedvarian'])?>",$(this).serialize(),function(response){
                endModal();
                ok('Varian berhasil di perbarui !');
                $("#basic").modal('hide');
            },'json');
             // alert();
        });
    }

	return {
		init: function () {
			crudItemattr();
            varian();
		}
	}	
}();
</script>