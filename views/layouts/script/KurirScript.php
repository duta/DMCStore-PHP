<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var kurir= function () {
    var crudkurir = function(){
        // add kurir
        $("button.add_kurir").click(function(){
            modalGet("<?=Url::toRoute('kurir/create')?>",{},'<i class="mdi mdi-truck-delivery fa-fw"></i>Tambah kurir');
        });
        // update kurir
        $("div#p0").on("click","button.update_kurir",function(){
            modalGet("<?=Url::toRoute('kurir/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-truck-delivery fa-fw"></i>Update kurir');
        });

        // adding/updating kurir
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                // alert(response.errors.length);
                if(response.length <1){
                    reloadGridview('Action success',1);
                }else{
                    error(response.errors);
                    endModal();
                }
            },'json');
        });

        // Del kurir
        $("div#p0").on("click","button.del_kurir",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['kurir/delete'])?>/"+id_val,{id:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "kurir berhasil di hapus.", "success"); 
                });
            });
           
        });
    }

    return {
        init: function () {
            crudkurir();
        }
    }   
}();
</script>