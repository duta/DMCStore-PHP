<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Vbm = function () {
	var plugin = function(){
      $('#defaultrange').daterangepicker({
              opens: ('left'),
              format: 'DD-MM-YYYY',
              separator: ' TO ',
              startDate: "<?=date('01-m-Y')?>",
              endDate: "<?=date('t-m-Y')?>",
          },
          function (start, end) {
              $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
          }
      );
      $("select.select2meFilter,select.select2").select2({allowClear: true});
      $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
      $("input.touchspin").TouchSpin({ min: 1,max:10000,step:1});
      $(".datepicker").datepicker();
  }

	var crudBm = function(){
       // ADD BM
        $("button.add_bm").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/create_bm')?>",{},'<i class="mdi mdi-basket-fill fa-fw"></i> Barang masuk');
            // $("span")
        });

    		// SELECT MATERIAL EVENT
        $("#basic .modal-content .modal-body").on("change","select#id_material",function(){
            var value=($(this).val()=='')?'':$(this).val();
            e_post("<?=Url::toRoute('materialdetail/form_bm')?>",{id_material:value},function(response){
                $("div.load_inout").html(response);

                plugin();
            });
        });

        // DEL BM
        $("div#p0").on("click","button.del_bm",function(){
            inv_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Hapus Transaksi ini !","warning",function(){
                start();
                e_post("<?=Url::toRoute('/materialdetail/del_byinv')?>/",{inv:inv_val},function(){
                    swal("Dihapus!", "Transaksi telah di hapus.", "success"); 
                    end();
                    $('#w0').yiiGridView('applyFilter');
                });
            });
        });

 
          // ACC BM
        $("div#p0").on("click","button.acc_bm",function(){
            inv_val=$(this).attr('id');
            swal_help("Terima Barang ?","Apakah anda yakin telah menerima semua barang.","info",function(){
                start();
                e_post("<?=Url::toRoute('/materialdetail/acc_bm')?>/",{inv:inv_val},function(){
                    swal("Barang diterima !", "Jumlah barang yang anda di terima, akan mempengaruhi jumlah stok", "success"); 
                    end();
                    $('#w0').yiiGridView('applyFilter');
                });
            },"TIDAK","TERIMA");
        });
        
        // ACC SOME
        $("div#p0").on("click","button.acc_some_bm",function(){
            $("#basic .modal-dialog").removeClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/acc_some')?>",{inv:$(this).attr('id')},'<i class="fa fa-check fa-fw"></i> Terima sebagian');
        });

        $("#basic .modal-content .modal-body").on("submit","form#form_acc_some",function(e){
            e.preventDefault();
            startModal();
            var url="<?=Url::toRoute('materialdetail/acc_some')?>";
            e_post(url,$(this).serialize(),function(response){
                // toast(response.pesan);
                reloadGridview(response.pesan,response.status);
            },'json');
        })
	}


  
  return {
		init: function () {
			crudBm();
			// kalkulasiBM();
			// plugin();

		}
	}	
}();
</script>