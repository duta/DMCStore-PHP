<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Vpb = function () {

	var plugin = function(){
         $('#defaultrange').daterangepicker({
                opens: ('left'),
                format: 'DD-MM-YYYY',
                separator: ' TO ',
                startDate: "<?=date('01-m-Y')?>",
                endDate: "<?=date('t-m-Y')?>",
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
            }
        );
        $("select.select2meFilter,select.select2").select2({allowClear: true});
        $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
        $("input.touchspin").TouchSpin({ min: 1,max:10000,step:1});

        // $("select.select2meFilter,select.select2").select2({allowClear: true});
    }
    var listVarian = function(kd_inv){
   
        startModal();
        $("#basic .modal-dialog").removeClass("modal-lg");
        modalPost("<?=Url::toRoute(['materialdetail/variantable'])?>",{inv:kd_inv},"Daftar barang varian");
    }
	var crudPB = function(){
        // ADD PB
		$("button.add_bm").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/create_bm')?>",{},'<i class="mdi mdi-basket-fill fa-fw"></i> Pembelian');
            // $("span")
        });

         // UPDATE PB
        $("div#p0").on("click","a.edit_bm",function(){

            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/edit_bm')?>",{inv:$(this).attr('id')},'<i class="mdi mdi-basket-fill fa-fw"></i> Update Pembelian');
       
        });

		// SELECT MATERIAL EVENT
        $("#basic .modal-content .modal-body").on("change","select#kd_md",function(){
            var value=($(this).val()=='')?'':$(this).val();
            e_post("<?=Url::toRoute('materialdetail/form_bm')?>",{id_material:value},function(response){
                $("div.load_inout").html(response);

                plugin();
            });
        });
        // DEL PB
        $("div#p0").on("click","a.del_bm",function(){
            inv_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Hapus Transaksi ini !","warning",function(){
                start();
                e_post("<?=Url::toRoute('materialdetail/del_byinv')?>",{inv:inv_val},function(){
                    swal("Dihapus!", "Transaksi telah di hapus.", "success"); 
                    end();
                    $('#w0').yiiGridView('applyFilter');
                });
            });
        });

        // DETAIL PB
        $("div#p0").on("click","a.detail_bm",function(){
            // alert('hai');
              $("#basic .modal-dialog").removeClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/detail_bm')?>",{inv:$(this).attr('id')},'<i class="mdi mdi-search fa-fw"></i> Detail Pembelian');
        });
        
        // CREATE BM
        $("#basic .modal-content .modal-body").on("submit","form#form_materialdetail",function(e){
            e.preventDefault();
            if($("select#id_sup").select2().val()==''){
                error("supplier harus di isi !");
            }else{
                start("#basic .modal-content");
                var url="<?=Url::toRoute('materialdetail/create_bm')?>";
                e_post(url,$(this).serialize(),function(response){
                    
                    swal_help("Notifikasi ?","Apakah semua barang tersebut sudah ada di rak anda?","warning",function(isConfirm){
                    if (isConfirm) {     
                        start("#basic .modal-content");
                        var url="<?=Url::toRoute('materialdetail/acc_direct')?>";
                        e_post(url,{inv:response.inv},function(response){
                            swal("Sukses!", "Stok telah bertambah.", "success");
                            reloadGridview(response.pesan,response.status);       
                        },'json');
                    } else {
                        toast(response.pesan);
                        reloadGridview(response.pesan,response.status);   
                    }
                });
                },'json');
                // swal_help("Notifikasi ?","Apakah semua barang tersebut sudah ada di rak anda?","warning",function(isConfirm){
                //     if (isConfirm) {     
                //         // swal("Deleted!", "Your imaginary file has been deleted.", "success");  

                //     } else {     
                //         start("#basic .modal-content");
                //         var url="<?=Url::toRoute('materialdetail/create_bm')?>";
                //         e_post(url,$(this).serialize(),function(response){
                //             // toast(response.pesan);
                //             reloadGridview(response.pesan,response.status);
                //         },'json');
                //         // swal("Cancelled", "Your imaginary file is safe :)", "error");   
                //     }
                // });
                // start("#basic .modal-content");
                // var url="<?=Url::toRoute('materialdetail/create_bm')?>";
                // e_post(url,$(this).serialize(),function(response){
                //     // toast(response.pesan);
                //     reloadGridview(response.pesan,response.status);
                // },'json');
            }
        });

        // update BM
        $("#basic .modal-content .modal-body").on("submit","form#form_bm_edit",function(e){
            e.preventDefault();
            if($("select#id_sup").val()==''){
                alert("supplier harus di isi !");
            }else{
                start("#basic .modal-content");
                var url="<?=Url::toRoute('materialdetail/edit_bm')?>";
                e_post(url,$(this).serialize(),function(response){
                    // toast(response.pesan);
                    reloadGridview(response.pesan,response.status);
                },'json');
            }
        });

        // Atur harga
        $("div#p0").on("click","a.price_item",function(){
            id_val=$(this).attr('id');
            listVarian(id_val);
        });

        $("#basic .modal-content .modal-body").on("click","button.btn-price",function(){
            id_val=$(this).attr('id');
            $("#basic .modal-dialog").addClass("modal-lg");
            // alert('hai');
            var title=$(this).attr('data-nama')+'<span class="font-bold pull-right m-r-15">TR : <span class=" text-danger ">'+$(this).attr('data-inv')+'</span></span>';
            modalPost("<?=Url::toRoute(['materialdetail/changeprice'])?>",{id:id_val},title);
        });
        
        $("#basic .modal-content .modal-body").on("click","button.btn-backtovarian",function(){
            id_val=$(this).attr('id');
            listVarian(id_val);
        });

            
        $("#basic .modal-content .modal-body").on("submit","form#form-price",function(e){
            e.preventDefault();
            startModal();
            e_post("<?=Url::toRoute(['materialdetail/changeprice'])?>",$(this).serialize(),function(response){
                listVarian(response.inv);
                ok(response.pesan);
            },'json');

        });

	}
    var input_grosir= function(){
        // HARGA GROSIR
        $("#basic .modal-content .modal-body").on("click","a.add-hg",function(){
            if($("input.hrg-s").val()==''){
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                $(".hg-wrap").fadeToggle('fast');
            }
            // console.log($("input.hrg-s").val());
        });

        $("#basic .modal-content .modal-body").on("click","button.add-row-hg",function(){

            $("div.row-hg:last").clone().appendTo("div.hg-wrap");
            $("div.row-hg:last").find("input.input-qty").val('').attr({'name':'new_qty[]','data-proses':'new_row'});
            $("div.row-hg:last").find("input.input-price").val('').attr({'name':'new_price[]','data-proses':'new_row'});
            $(this).last().hide();

            $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
            // alert('hai')

        });

        $("#basic .modal-content .modal-body").on("click","button.rm-row-hg",function(){
            var this_el=$(this);
            action=this_el.parents('div.row-hg').find('input.input-price').attr('data-proses');
            // alert(action);
            if($("button.rm-row-hg").length>1){
                if(action=='set_row'){
                    // alert('set');
                    var qty=this_el.parents('div.row-hg').find('input.input-qty').val();
                    var grosir=$("input[name='set_price["+this_el.attr('id')+"]']").val();
                    if(confirm('Hapus harga grosir : ('+qty+') '+grosir+' ?')){
                        // alert('confirmed');
                        startModal();
                        e_post('<?=Url::toRoute(["materialdetail/delgrosir"])?>',{id:this_el.attr('id')},function(){
                            endModal();
                            this_el.parents('.row-hg').remove();
                            $("button.add-row-hg:last").show();    
                        });
                    }
                }else{
                    this_el.parents('.row-hg').remove();
                    $("button.add-row-hg:last").show();
                }
            }
        });

        $("#basic .modal-content .modal-body").on("keyup","input.hrg-g",function(){
            var hrg_s=toNumberOnly($("input.hrg-s").val());
            var hrg_g=$(this);
            if($("input.hrg-s").val()==''){
                hrg_g.val('Rp 0');
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                if(toNumberOnly(hrg_g.val())>=hrg_s){
                    hrg_g.val('Rp 0');
                }
            }
        });
    }
    var kalkulasiBM = function(){
        $("#basic .modal-content .modal-body").on("keyup",".kalkulasi",function(){
            id=$(this).attr('data-id');
            qty=toNumberOnly($("#qty_"+id).val());
            hb=toNumberOnly($("#hb_"+id).val());
            
            tpb=hb*qty;
            $("input#tpb_"+id).val(tpb);
            
            tot_tpb=0;
            tot_qty=0;
            $("input.tpb").each(function(i,item){
                tot_tpb+=toNumberOnly($(item).val());
                tot_qty+=toNumberOnly($("#qty_"+$(item).attr('data-id')).val());
            });
            
            ppn=tot_tpb*(Math.ceil(toNumberOnly($("#ppn_").val()))/100);
            pph=tot_tpb*(Math.ceil(toNumberOnly($("#pph_").val()))/100);
            bm=tot_tpb*(Math.ceil(toNumberOnly($("#bm_").val()))/100);

            dll=Math.ceil(toNumberOnly($("#ongkir_").val()));
            ongkir=Math.ceil(toNumberOnly($("#dll_").val()));

            ht_fix=Math.ceil(tot_tpb+(ppn+pph+bm+dll+ongkir));
            ongkos=Math.ceil(ppn+pph+bm+dll+ongkir);

            $("input.hgmod").each(function(i,item){
                id_mod=$(item).attr('data-id');
                hbmod=toNumberOnly($("#hb_"+id_mod).val());

                hgmod_fix=Math.ceil(hbmod+(ongkos/tot_qty));
                $(item).val(Math.ceil(hgmod_fix));

                $("input#hpb_"+id_mod).val(toRp(hbmod,0)+" + "+"( "+toRp(Math.ceil(ongkos),0)+"/"+tot_qty+" )");
                $("input#modpb_"+id_mod).val(toRp(hgmod_fix));
                
            });
            // console.log($("#hb_"+id).val());
            $("input#ht_").val(toRp(ht_fix));
            // alert(tot_qty);
        });
      }

    var diffval= function(){
        span = $("span.child").val();
        console.log(span);

    }

    var add_supplier = function(){

        var value=($("select#kd_md").val()=='')?'':$("select#kd_md").val();

        // add supplier
        $("#basic .modal-content .modal-body").on('click','button.add_sup',function(){
            modalGet("<?=Url::toRoute('brsupplier/create')?>",{},'<i class="mdi mdi-account-network fa-fw"></i>Tambah Supplier');
        });
        // adding/updating Supplier
        $("#basic .modal-content .modal-body").on("submit","form#form_sup",function(e){
            e.preventDefault();
            start("#basic .modal-content"); 
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                modalPost("<?=Url::toRoute('materialdetail/create_bm')?>",{},'<i class="mdi mdi-basket-fill fa-fw"></i> Pembelian');
            },'json');
        });

         $("#basic .modal-content .modal-body").on('click','button.btn-danger',function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/create_bm')?>",{},'<i class="mdi mdi-basket-fill fa-fw"></i> Pembelian');
        });
    }

    var add_fw = function(){
        // add Forwarder
        $("#basic .modal-content .modal-body").on('click','button.add_fw',function(){
            modalGet("<?=Url::toRoute('forwarder/create')?>",{},'<i class="mdi mdi-account-convert fa-fw"></i>Tambah Forwarder');
        });
         // adding/updating Forwarder
        $("#basic .modal-content .modal-body").on("submit","form#form_fw",function(e){
            e.preventDefault();
            start("#basic .modal-content"); 
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                modalPost("<?=Url::toRoute('materialdetail/create_bm')?>",{},'<i class="mdi mdi-basket-fill fa-fw"></i> Pembelian');
            },'json');
        });

        $("#basic .modal-content .modal-body").on('click','button.btn-danger',function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/create_bm')?>",{},'<i class="mdi mdi-basket-fill fa-fw"></i> Pembelian');
        });
    }
	return {
		init: function () {
			crudPB();
			kalkulasiBM();
            input_grosir();
            diffval();
            add_supplier();
            add_fw();

			// plugin();
			// alert('ee');
		}
	}	
}();
</script>