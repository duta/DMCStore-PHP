<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
    $sess = Yii::$app->session;
    $menu = $sess->get('menu');
?>
<script type="text/javascript">
var User = function () {
	var crudUser = function(){
		// Add User
		$("button.add_user").click(function(){
			modalGet("<?=Url::toRoute('user/create')?>",{},'<i class="mdi mdi-account-plus fa-fw"></i>Tambah User');
		});

		// update User
		 $("div#p0").on("click","button.update_user",function(){
			modalGet("<?=Url::toRoute('user/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-account-edit fa-fw"></i>Update User');
		});

        // adding/updating user
		$("#basic .modal-content .modal-body").on("submit","form#form_user",function(e){
            e.preventDefault();
            start("#basic .modal-content"); 
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

        // Del User
        $("div#p0").on("click","button.del_user",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['user/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "User berhasil di hapus.", "success"); 
                });
            });
           
        });
	}

	return {
		init: function () {
			crudUser();
		}
	}	
}();


</script>