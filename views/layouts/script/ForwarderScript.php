<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Forwarder = function () {
	var crudfW = function(){
		// add Forwarder
		$("button.add_fw").click(function(){
			modalGet("<?=Url::toRoute('forwarder/create')?>",{},'<i class="mdi mdi-account-convert fa-fw"></i>Tambah Forwarder');
		});
		// update Forwarder
		$("div#p0").on("click","button.update_fw",function(){
			modalGet("<?=Url::toRoute('forwarder/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-account-convert  fa-fw"></i>Update Forwarder');
		});

        // adding/updating Forwarder
		$("#basic .modal-content .modal-body").on("submit","form#form_fw",function(e){
            e.preventDefault();
            start("#basic .modal-content"); 
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

        // Del Forwarder
        $("div#p0").on("click","button.del_fw",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['forwarder/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Forwarder berhasil di hapus.", "success"); 
                });
            });
           
        });
	}

	return {
		init: function () {
			crudfW();
		}
	}	
}();
</script>