<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;

    $uriMaster=explode('?',Url::current());
    $uriMain = explode('/',ltrim($uriMaster[0],"/"));
    $uriget = explode('=',Url::current());
    if($uriMain[0]!=''){
        $index2=count($uriMain)-2;
        $index3=count($uriMain)-1;
        $urimain2=strtolower($uriMain[$index2]);
        $urimain3=strtolower($uriMain[$index3]);
    }else{
        $urimain2='';
        $urimain3='';

    }
    $getval=(count($uriget)>0)?strtolower(end($uriget)):'';
?>
<script type="text/javascript">
var VOrders = function () {
	var plugin = function(){
         $('#defaultrange').daterangepicker({
                opens: ('left'),
                format: 'DD-MM-YYYY',
                separator: ' TO ',
                startDate: "<?=date('01-m-Y')?>",
                endDate: "<?=date('t-m-Y')?>",
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
            }
        );
        $("select.select2meFilter,select.select2").select2({allowClear: true});
        $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
        $("input.touchspin").TouchSpin({ min: 1});

        // $("select.select2meFilter,select.select2").select2({allowClear: true});
    }
    
	var crudOd = function(){
		 // ADD OD
		$("button.add_od").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/create_od')?>",{},'<i class="mdi mdi-basket-unfill fa-fw"></i> Order');
        });

		$("#basic .modal-content .modal-body").on("change","select#c_id",function(){
            id=$(this).val();
            if(id!=null && id!=''){
                e_post("<?=Url::toRoute('materialdetail/cm_json')?>",{c_id:id},function(response){
                    $("#c_nama").val(response.nama);
                    $("#c_tlp").val(response.tlp);
                    $("#c_email").val(response.email);
                    $("#c_alamat").val(response.alamat);
                    // $("#c_prov").select2("val",response.prov);
                    // $("#c_kota").select2("val",response.kota_kab);
                    $("#c_prov").val(response.prov).trigger('change');
                    $("#c_kota").val(response.kota_kab).trigger('change');
                    $("#c_kec").val(response.kec);
                },'json');
            }else{
                resetForm();
            }
        });

		// SELECT MATERIAL EVENT
        $("#basic .modal-content .modal-body").on("change","select#kd_md",function(){
            var value=($(this).val()=='')?'':$(this).val();
            var give_val=$("input:hidden[name='give']").val();
            var give_val=$("input:hidden[name='give']").val();

            e_post("<?=Url::toRoute('materialdetail/form_od')?>",{kd_md:value,give:give_val},function(response){
                $("div.load_inout").html(response);
                plugin();
            });
        });

        $("#basic .modal-content .modal-body").on("change","input:checkbox#give",function(){
            
            var value=($("select#kd_md").val()=='')?'':$("select#kd_md").val();
            var give_val=$("input:hidden[name='give']").val();
            e_post("<?=Url::toRoute('materialdetail/form_od')?>",{kd_md:value,give:give_val},function(response){
                $("div.load_inout").html(response);
                plugin();
            });
        });

        // onSubmit created OD
        $("#basic .modal-content .modal-body").on("submit","form#form_materialdetail",function(e){
            e.preventDefault();
            startModal();
            var give_val=$("input:hidden[name='give']").val();
            var tipe='';
            if(give_val==1){
                tipe='give';
                var url="<?=Url::toRoute('materialdetail/create_gift')?>";
            }else{
                tipe='od';
                var url="<?=Url::toRoute('materialdetail/create_od')?>";
            }
            e_post(url,$(this).serialize(),function(response){
                swal_help("Notifikasi ?","Apakah order ini sudah dibayar ?","warning",function(isConfirm){
                    if(isConfirm){  
                        swal.close();
                         modalPost("<?=Url::toRoute('materialdetail/payment')?>",{inv:response.inv},"Konfirmasi Pembayaran");
                    }else{
                        reloadGridview(response.pesan,response.status);
                    }
                },"BELUM","SUDAH");
                
            },'json');
        });

        // Del OD
        $("div#p0").on("click","a.del_od",function(){
            inv_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Hapus Transaksi ini !","warning",function(){
                e_post("<?=Url::toRoute('/materialdetail/del_byod')?>",{inv:inv_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Transaksi telah di hapus.", "success"); 
                });
            });
           
        });

        // View Nota
        $("div#p0").on("click","a.print_tr_od",function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPostPrint("<?=Url::toRoute('materialdetail/nota')?>",{inv:$(this).attr('id'),tr:'od'},"Bukti Transaksi");
        });

        // View Detail OD
         $("div#p0").on("click","a.detail_od",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/detail_tr')?>",{inv:$(this).attr('id'),tr:'od'},"Detail Transaksi");
        });

       // Konfirmasi Pembayaran
        $("div#p0").on("click","a.pay_od",function(){

            modalPost("<?=Url::toRoute('materialdetail/payment')?>",{inv:$(this).attr('id')},"Konfirmasi Pembayaran");
        });


        $("#basic .modal-content .modal-body").on("submit","form#form-paid",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            var form = $(this);
            $.ajax({
                type:'POST',
                url:"<?=url::toroute('materialdetail/payment')?>",
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                	reloadGridview(response.pesan,response.status);
                }
            });
        });
        // Batalkan Transaksi
        $("div#p0").on("click","a.cancel_tr",function(){
            modalPost("<?=Url::toRoute('materialdetail/trcancel')?>",{kd:$(this).attr('id')},"Batalkan Transaksi");
        });

        $("#basic .modal-content .modal-body").on("submit","form#form-cancel-note",function(e){
            e.preventDefault();
            startModal();
            var url="<?=Url::toRoute('materialdetail/trcancel')?>";

            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

         // Retur Transaksi
        $("div#p0").on("click","a.retur_od",function(){
            modalPost("<?=Url::toRoute('materialdetail/retur')?>",{kd:$(this).attr('id')},"Retur");
        });

        $("#basic .modal-content .modal-body").on("submit","form#form-retur-note",function(e){
            e.preventDefault();
            startModal();
            var url="<?=Url::toRoute('materialdetail/retur')?>";

            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });


         // Retur Terima barang
        $("div#p0").on("click","a.retur_acc",function(){
            modalPost("<?=Url::toRoute('materialdetail/returacc')?>",{kd:$(this).attr('id')},"Barang telah diterima");
        });

        $("#basic .modal-content .modal-body").on("submit","form#form-retur-acc",function(e){
            e.preventDefault();
            startModal();
            var url="<?=Url::toRoute('materialdetail/returacc')?>";
            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

        // add gift
        $("button.add_gift").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/create_gift')?>",{},'<i class="mdi mdi-basket-unfill fa-fw"></i> Hadiah');
        });

        // Change sender
        $("div#p0").on("click","a.change_sender",function(){
            modalPost("<?=Url::toRoute('materialdetail/senderchanged')?>",{kd:$(this).attr('id')},'<i class="mdi mdi-basket-unfill fa-fw"></i> Atur Pengirim');
         });

        // COMBO DS
        $("#basic .modal-content .modal-body").on("change","select.ds_combo",function(){
            id_val=$(this).val();
            // alert(id_val);
            if(id_val!==''){
                 e_post('<?=Url::toRoute('dropshipper/findonejson')?>',{id:id_val},function(response){
                    // console.log(response);
                    $("#ds_send").val(response.nm_ds);
                    $("#ds_tlp").val(response.tlp);
                },'json');
            }else{
                $("#ds_send,#ds_tlp").val('');
            }
        
        });

        $("#basic .modal-content .modal-body").on("submit","form#form-ds",function(e){
            e.preventDefault();
            startModal();
            var url="<?=Url::toRoute('materialdetail/senderchanged')?>";
            e_post(url,$(this).serialize(),function(response){
                endModal();
                closeModal();
                ok("Pengirim berhasil di perbarui !");
            });
        });


          // UPDATE OD
        $("div#p0").on("click","a.edit_od",function(){

            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/edit_od')?>",{kd_order:$(this).attr('id')},'<i class="mdi mdi-cart-plus fa-fw"></i> Perbarui Penjualan');
       
        });

          // update OD
        $("#basic .modal-content .modal-body").on("submit","form#form_od_edit",function(e){
            e.preventDefault();
            startModal();
            var url="<?=Url::toRoute('materialdetail/edit_od')?>";
                e_post(url,$(this).serialize(),function(response){
                    toast(response.pesan);
                    reloadGridview(response.pesan,response.status);
                    closeModal();
                    endModal();
                },'json');
        });

        $("#basic .modal-content .modal-body").on("change","select#id_prov",function(e){
            startModal();
            e_post("<?=Url::toRoute('stprovince/findcity')?>",{province_id:$(this).val()},function(response){
                // console.log(response);
                $("select#c_kota").empty();
                $("select#c_kota").append("<option value=''>--Pilih Kab/kota--</option>");

                $.each(response,function(i,item){
                    $("select#c_kota").append("<option value='"+item.id+"'>"+item.city+"</option>");
                });
                $("select#c_kota").select2();
                endModal();

            },'json');
        });

        // view_resi
        $("div#p0").on("click","a.view_resi",function(){
            window.open("<?=Url::base()?>/file_uploaded/no_resi/"+$(this).attr('id')+"/"+$(this).attr('data-filename')).focus();

        });

         // ADD OD DROPSHIP
        $("button.add_od_dropship").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('vorders/create_od_dropship')?>",{},'<i class="mdi mdi-basket-unfill fa-fw"></i> Order Dropship');
        });

        $("#basic .modal-content .modal-body").on("submit","form#form_odropship",function(e){
            e.preventDefault();
            startModal();
     
            var url="<?=Url::toRoute('vorders/create_od_dropship')?>";
            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

        // SELECT MATERIAL EVENT DROPSHIP
        $("#basic .modal-content .modal-body").on("change","select#id_dropship",function(){
            var value=($(this).val()=='')?'':$(this).val();

            e_post("<?=Url::toRoute('vorders/form_od')?>",{id:value},function(response){
                $("div.load_inout").html(response);
                plugin();
            });
        });

	}

	var kalkulasiOD = function(){

           // kalkulasi barang orders
        $("#basic .modal-content .modal-body").on("click","button.bootstrap-touchspin-up,button.bootstrap-touchspin-down",function(){
            id=$(this).parents('div.row').find('input.hgsat').attr('data-id');

            hg=toNumberOnly($("input#hgsat_"+id).val());
            qty=parseFloat($('input#qty_'+id).val());
            $("input#price_total_"+id).val(qty*hg);

            ongkir=($("#ongkir_buyer").val()=='')?0:toNumberOnly($("#ongkir_buyer").val());
            tot=0;
            $("input.price_total").each(function(){
                tot+=($(this).val()!=null && $(this).val()!='')?toNumberOnly($(this).val()):0;
            });
            $("input#total-pay").val(toRp(tot+ongkir));
        });

        $("#basic .modal-content .modal-body").on("keyup","input.kal_or",function(){
            id=$(this).attr('data-id');
            hg=toNumberOnly($("input#hgsat_"+id).val());
            qty=parseFloat($('input#qty_'+id).val());
            $("input#price_total_"+id).val(qty*hg);

            ongkir=($("#ongkir_buyer").val()=='')?0:toNumberOnly($("#ongkir_buyer").val());
            tot=0;
            $("input.price_total").each(function(){
                tot+=($(this).val()!=null && $(this).val()!='')?toNumberOnly($(this).val()):0;
            });
            $("input#total-pay").val(toRp(tot+ongkir));
        });
    }
    var report = function(){
        $("form#form-vorders-report").submit(function(e){
            e.preventDefault();
            start();
            e_post("<?=Url::toRoute(['vorders/report'])?>",$(this).serialize(),function(response){
                $(".response").html(response);
                $(".print_a").slideDown('fast');
                end();
            });
        });

        $("a.print_a").click(function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPostPrint("<?=Url::toRoute(['vorders/report'])?>",$("form#form-vorders-report").serialize(),"Pratinjau <center><button type='button' class='btn btn-info' onclick='print()'><i class='fa fa-print'> </button></center>");
        });

    }


	return {
		init: function () {
            
            var m="<?=$urimain3?>";
            if(m=='index'){
        		crudOd();
        		kalkulasiOD();
            }else if(m=='report'){
                report();
                $("select.filter").select2({allowClear: true});
                $('#date-range').datepicker({
                    toggleActive: true,
                });
            }

		}
	}	
}();
</script>