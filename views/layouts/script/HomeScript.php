<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Home = function () {

    var init_piechart =  function(IDselector,data){
        var ctx3 = document.getElementById(IDselector).getContext("2d");
        // var data3 = [
        //     {
        //         value: 300,
        //         color:"#2cabe3",
        //         highlight: "#2cabe3",
        //         label: "Blue"
        //     },
        //     {
        //         value: 50,
        //         color: "#edf1f5",
        //         highlight: "#edf1f5",
        //         label: "Light"
        //     },
        //      {
        //         value: 50,
        //         color: "#b4c1d7",
        //         highlight: "#b4c1d7",
        //         label: "Dark"
        //     },
        //      {
        //         value: 50,
        //         color: "#53e69d",
        //         highlight: "#53e69d",
        //         label: "Megna"
        //     },
        //     {
        //         value: 100,
        //         color: "#ff7676",
        //         highlight: "#ff7676",
        //         label: "Orange"
        //     }
        // ];
        
        var myPieChart = new Chart(ctx3).Pie(data,{
            segmentShowStroke : true,
            segmentStrokeColor : "#fff",
            segmentStrokeWidth : 0,
            animationSteps : 100,
            tooltipCornerRadius: 0,
            animationEasing : "easeOutBounce",
            animateRotate : true,
            animateScale : false,
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
            responsive: true
        });
        
    }
    var chart_market = function(){
        e_post("<?=Url::toRoute('Home/home')?>",function(response){
            console.log(response);
            init_piechart("chart3");

        },'json');
    }
    var salestoday = function(){
        start("div#box-salestoday");
        e_post("<?=Url::toRoute(['vorders/salestoday'])?>",{kode:4},function(response){ 
             Morris.Donut({
                element: 'morris-donut-chart',
                data:response.data,
                resize: true,
                colors: response.colors
            });
            end("div#box-salestoday");

        },'json');
    }

    var salesthisweek=function(){
        start("div#box-salesthisweek");
        e_post("<?=Url::toRoute(['vorders/salesthisweek'])?>",{kode:4},function(response){    
            $("ul#legend-chart").html('');
            $.each(response.legends,function(i,item){
                $("ul#legend-chart").append(item);
            });
            Morris.Area({
                element: 'morris-area-chart2',
                data: response.data,
                parseTime: false,
                xkey: "period",
                ykeys: response.keys, 
                labels: response.labels, 
                pointSize: 3,
                fillOpacity: 0,
                pointStrokeColors:response.colors, 
                behaveLikeLine: true,
                gridLineColor: '#e0e0e0',
                lineWidth: 1,
                hideHover: 'auto',
                lineColors: response.colors, 
                resize: true,
                continuousLine:true,
            }); 
            end("div#box-salesthisweek");
        },'json');
    }

    var salesthisyear=function(){
        // Morris bar chart
        start("div#box-salesthisyear");
        e_post("<?=Url::toRoute(['vorders/salesthisyear'])?>",{},function(response){    
            $("ul#legend-chart-year").html('');
            $.each(response.legends,function(i,item){
                $("ul#legend-chart-year").append(item);
            });
                // var data_val=[{
                //     month_field: '2006',
                //     qty: "0",
                //     OMZET: "90",
                //     MARGIN: "60"
                // }, {
                //     month_field: '2007',
                //     qty: "75",
                //     OMZET: "65",
                //     MARGIN: "40"
                // }, {
                //     month_field: '2008',
                //     qty: "50",
                //     OMZET: "40",
                //     MARGIN: "30"
                // }, {
                //     month_field: '2009',
                //     qty: "75",
                //     OMZET: "65",
                //     MARGIN: "40"
                // }, {
                //     month_field: '2010',
                //     qty: "50",
                //     OMZET: "40",
                //     MARGIN: "30"
                // }, {
                //     month_field: '2011',
                //     qty: "75",
                //     OMZET: "65",
                //     MARGIN: "40"
                // }, {
                //     month_field: '2012',
                //     qty: "100",
                //     OMZET: "90",
                //     MARGIN: "40"
                // }];
            // Morris.Bar({
            //     element: 'morris-bar-chart',
            //     data: response.data,
            //     xkey: response.xkey,
            //     ykeys: response.ykeys,
            //     labels: response.labels,
            //     barColors:response.colors,
            //     hideHover: 'auto',
            //     gridLineColor: '#eef0f2',
            //     resize: true
            // });
             Morris.Bar({
                element: 'morris-bar-chart',
                // data:data_val,
                data:response.data,
                xkey: response.xkey,
                ykeys: response.ykeys,
                labels: response.labels,
                barColors:response.colors,
                hideHover: 'auto',
                gridLineColor: '#eef0f2',
                resize: true
            });
            end("div#box-salesthisyear");
        },'json');

        
    }

    var top5sales=function(){
        start("div#box-top5sales");
        e_post("<?=Url::toRoute(['vorders/top5sales'])?>",{kode:4},function(response){
            $("div#response-top5sales").html(response);
            end("div#box-top5sales");
        });

    }

    var top5stockout=function(){
        start("div#box-top5stockout");
        e_post("<?=Url::toRoute(['vmaterial/top5stockout'])?>",{},function(response){
            $("div#response-top5stockout").html(response);
            end("div#box-top5stockout");
        });

    }

    var trvalthismonth=function(){
        start("div#box-trvalthismonth");
        e_post("<?=Url::toRoute(['vorders/trvalthismonth'])?>",{},function(response){
            $("div#response-trvalthismonth").html(response);
            end("div#box-trvalthismonth");
        });
    }

    var statistik=function(){
        start("div#box-statistik");
        e_post("<?=Url::toRoute(['home/statistik'])?>",{},function(response){
            $("div#response-statistik").html(response);
            end("div#box-statistik");
            // $(".counter").counterUp({
            //      delay: 100,
            //      time: 1200,
                
            //  });
        });
    }

    
    var init_plug = function(){
        $('#defaultrange').daterangepicker({
                opens: ('left'),
                format: 'DD-MM-YYYY',
                separator: ' TO ',
                startDate: "<?=date('01-m-Y')?>",
                endDate: "<?=date('t-m-Y')?>",
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
            }
        );
       
    }

    var create_store = function(){
        modalPost("<?=Url::toRoute('store/create')?>",{},"Buat Toko");
         // adding/updating kurir
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                // alert(response.errors.length);
                if(response.length <1){
                    // reloadGridview('Action success',1);
                    ok('Toko berhasil dibuat !');
                    // closeModal();
                    window.location.href="<?=Url::toRoute('home/index')?>";
                }else{
                    error(response.errors);
                    endModal();
                }
            },'json');
        });

    }   
    return {
        //main function to initiate map samples
        init: function () {
            // MaterialGrupTable();
             <?php
                if($urimain2=='home' && $user->id_role!=3){
            ?>
            // chart_market();
            have_store=$("input:hidden[name='have_store']").val();
            if(have_store==1){

                init_plug();
                salestoday();
                salesthisweek();
                salesthisyear();
                top5sales();
                top5stockout();
                trvalthismonth();
                statistik();

            }else{
                create_store();
            }
            <?php } ?>
           
        }

    };

}();
</script>