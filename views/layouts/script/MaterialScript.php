<?php
    use yii\helpers\Url;
    $user='';
?>
<script type="text/javascript">
var Material = function () {

    var MaterialTable = function () {

       $("#MaterialTable").dataTable({
                "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Indonesian.json"
            },
            // "columns":[
                // {width:'5%',orderable:true},
                // {orderable:true},
                // {width:'10%',orderable:false},
            // ],
            "lengthMenu": [
                [5,10, 15, 20, -1],
                [5,10, 15, 20, "All"] // change per page values here
            ],

       });

    }
    var loadData = function(pesan,status,withConfig=1){
        if(withConfig<1){
            start();
        }
        e_post("<?=Url::toRoute('material/admin')?>",function(response){
            $("div.table-body").html(response);
            MaterialTable();
            if(withConfig>0){
                end("#basic .modal-content");
                resetForm();
                $("#basic").modal('hide');   
            }else{
                end();
            }


            if(status==1){
                toast(pesan,"success","check");
            }else{
                toast(pesan,"danger","close");
            }
            $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
              popout:true
            });
            $('[data-toggle="tooltip"]').tooltip(); 
        });
    }

    var scriptMaterial = function(){
        $("button.add_material").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/material/create')?>",{},"Tambah  Barang");
        });

        $("div.table-body").on("click","a.edit_material",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('material/update')?>/"+$(this).attr('id'),{id:$(this).attr('id')},"Update Barang");
        });

      

        $("div.table-body").on("click","a.del_material",function(){
            var id=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Hapus Barang !","warning",function(){
                
                e_post("<?=Url::toRoute('/material/delete')?>/"+id,function(){
                    loadData("Data Berhasil di hapus !",1,0);
                    swal("Dihapus!", "Barang telah di hapus.", "success"); 
                });
            });
        });

        $("div.table-body").on("click","a.detail_material",function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/material/view')?>/"+$(this).attr('id'),{},$(this).attr('data-nama'));
        });

        $("#basic .modal-content .modal-body").on("submit","form#form_material",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            var form = $(this);
            var url_val = $(this).attr('action');
            $.ajax({
                type:'POST',
                url:url_val,
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    $("#basic").modal('hide');
                    toast(response.pesan,"success","check");

                    // loadData(response.pesan,response.status);
                }
            });
            // console.log(form[0]);
            // e_post(url,$(this).serialize(),function(response){
                
            // },'json');
        });

        

   
    }

    var input_grosir= function(){
        // HARGA GROSIR
        $("#basic .modal-content .modal-body").on("click","a.add-hg",function(){
            if($("input.hrg-s").val()==''){
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                $(".hg-wrap").fadeToggle('fast');
            }
            // console.log($("input.hrg-s").val());
        });

        $("#basic .modal-content .modal-body").on("click","button.add-row-hg",function(){
            $("div.row-hg:last").clone().appendTo("div.hg-wrap").find("input").val('');
            $(this).last().hide();
            $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});

        });

        $("#basic .modal-content .modal-body").on("click","button.rm-row-hg",function(){
            var this_el=$(this);
            // alert($("button.rm-row-hg").length);
            if($("button.rm-row-hg").length>1){
                this_el.parents('.row-hg').remove();
                $("button.add-row-hg:last").show();
            }
        });

        $("#basic .modal-content .modal-body").on("keyup","input.hrg-g",function(){
            var hrg_s=toNumberOnly($("input.hrg-s").val());
            var hrg_g=$(this);
            if($("input.hrg-s").val()==''){
                hrg_g.val('Rp 0');
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                if(toNumberOnly(hrg_g.val())>=hrg_s){
                    hrg_g.val('Rp 0');
                }
            }
        });
    }

    var table_export = function(){
        $("table#MaterialReport").tableExport({position: "top",formats: ["xls"],  bootstrap:true,trimWhitespace:false});
        $("button.xls").addClass('hidden-print btn-sm pull-right').detach().appendTo("div.btn-tools");
        // $("a.btn-xls").attr("data-fileblob",$("button.xls").attr("data-fileblob"));
    }
    var scriptReport = function(){
        $('#defaultrange').daterangepicker({
                opens: ('left'),
                format: 'DD-MM-YYYY',
                separator: ' TO ',
                startDate: "<?=date('01-m-Y')?>",
                endDate: "<?=date('t-m-Y')?>",
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
            }
        );
        $("select.select2meFilter").select2({allowClear: true});

        $("button.btn-clear").click(function(){
            $("select.select2meFilter").select2("val","");
        });

        $("#form-filter").submit(function(e){
            e.preventDefault();
            start();
            // $("div.table-print").empty();
            e_post("<?=Url::toRoute('material/report')?>",$(this).serialize(),function(response){
                $("div.table-report").html(response);
                table_export();
                end();
                // $("div.table-print").html(response);
            });
        });

        $("div.table-report").on("click","a.btn-cetak",function(){
            start();
            
            setTimeout(function(){
                end(); 
            }, 1500); 
            setTimeout(function(){
                $("table#MaterialReport").removeClass($("table#MaterialReport").attr('class')).addClass('tg');

                window.print();    
                $("table#MaterialReport").removeClass($("table#MaterialReport").attr('class')).addClass('table table-bordered table-hover table-striped');

            }, 2000); 

            // setTimeout(function(){
            //     $("table#MaterialReport").removeClass($("table#MaterialReport").attr('class')).addClass('tg');
                // window.print();    
            // }, 3000); 
            
        });
    }
    
    return {
        //main function to initiate map samples
        init: function () {
            <?php
                if($urimain2=='material' && $urimain3=='index'){
            ?>
                    // loadData("Data Berhasil di muat !",1,0);
                    scriptMaterial();
            <?php
                }elseif($urimain2=='material' && $urimain3=='report'){
            ?>
                    scriptReport();
                    // table_export();
            <?php
                }
            ?>
                    input_grosir();

        }

    };

}();
</script>