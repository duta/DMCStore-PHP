<?php
    use yii\helpers\Url;
    $uri=explode("=", $_SERVER['REQUEST_URI']);
    $params='';
    if(end($uri)=='od'){
        $params='?status=od';
    }elseif(end($uri)=='bm'){
        $params='?status=bm';
    }elseif(end($uri)=='bk'){
        $params='?status=bk';
    }
?>
<script type="text/javascript">
var MaterialDetail = function () {
    var plugin = function(){
         $('#defaultrange').daterangepicker({
                opens: ('left'),
                format: 'DD-MM-YYYY',
                separator: ' TO ',
                startDate: "<?=date('01-m-Y')?>",
                endDate: "<?=date('t-m-Y')?>",
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
            }
        );
        $("select.select2meFilter,select.select2").select2({allowClear: true});
    }
    var MaterialDetailTable = function () {

       $("#MaterialDetailTable").dataTable({
                "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Indonesian.json"
            },
            // "columns":[
          
            //     // {orderable:false},
            //     {orderable:true},
            //     {orderable:true},
            //     {orderable:true},
            
            //     {orderable:true},
            //     {orderable:true},
            //     {orderable:true},
           
                
            // ],

            "lengthMenu": [
                [5,10, 15, 20, -1],
                [5,10, 15, 20, "All"] // change per page values here
            ],

       });

       
        $("#MaterialDetailTable").on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        // tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    var StokBarangTable = function () {

       $("#StokBarangTable").dataTable({
                "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Indonesian.json"
            },
            // "columns":[
          
            //     // {orderable:false},
            //     {orderable:true},
            //     {orderable:true},
            //     {orderable:true},
            
            //     {orderable:true},
            //     {orderable:true},
            //     {orderable:true},
           
                
            // ],

            "lengthMenu": [
                [5,10, 15, 20, -1],
                [5,10, 15, 20, "All"] // change per page values here
            ],

       });

       
        $("#StokBarangTable").on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });

        // tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
    }
    var loadData = function(pesan,status,withConfig=1){
        if(withConfig<1){
            start();
        }
        
        e_post("<?=Url::toRoute('MaterialDetail/Admin').$params;?>",function(response){
            $("div.table-body").html(response);
            MaterialDetailTable();
            checkAll("#all","#MaterialDetailTable");
            if(withConfig>0){
                end("#basic .modal-content");
                resetForm();
                $("#basic").modal('hide');   
            }else{
                end();
            }


            if(status==1){
                toast(pesan,"success","check");
            }else{
                toast(pesan,"danger","close");
            }
            $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
            });
            $('[data-toggle="tooltip"]').tooltip(); 
            $('.popovers').popover(); 
            $('.pulsate-repeat').pulsate({
                color: "#000000",
                reach: 50,
                speed: 100,
                glow: true
            });
            
        });
    }

    var scriptMaterialDetail = function(){
        $("#basic .modal-content .modal-body").on("submit","form#form_materialdetail",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            var url=$(this).attr('action')+"<?=$params;?>";
            e_post(url,$(this).serialize(),function(response){
                // toast(response.pesan);
                loadData(response.pesan,response.status);
            },'json');
        });

        $("button.out_materialdetail").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/MaterialDetail/Create').$params;?>",{},"Orders");
        });

        $("button.stok_materialdetail").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/MaterialDetail/Create').$params;?>",{},'<?=(end($uri)=='bm')?'<i class="mdi mdi-basket-fill fa-fw"></i> Barang masuk':'<i class="mdi mdi-basket-unfill fa-fw"></i> Order';?>');
        });

        $("#basic .modal-content .modal-body").on("change","select#id_material",function(){
            var value=($(this).val()=='')?'':$(this).val();
            e_post("<?=Url::toRoute('MaterialDetail/Load_inout').$params;?>",{id_material:value},function(response){
                $("div.load_inout").html(response);
                $("input.touchspin").TouchSpin({ min: 1});
                
                $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
                $("select.select2me,select.select2").select2();
            });
        });

       
        $("#basic .modal-content .modal-body").on("click","a.a-mat-lain",function(){
            $("div.form-group-matlain").removeClass("display-none");
        });

        $("button.approve_materialdetail").click(function(){
            var checked=$("div.table-body").find("input.checkboxes:checked").length;
            if(checked > 0){
                var data=$("div.table-body").find("form#form-checked").serialize()+"&status=1";
                e_post("<?=Url::toRoute('MaterialDetail/Checked').$params;?>",data,function(){
                    loadData(checked+" pesanan berhasil diproses, silahkan lakukan konfirmasi pengiriman barang !",1,0);
                });
            }else{
                toast(" Anda belum pilih ceklist di table !",'danger','warning')
            }
            
            // toast('clicked');
        });

        $("button.reject_materialdetail").click(function(){
           var checked=$("div.table-body").find("input.checkboxes:checked").length;
            if(checked > 0){
                var data=$("div.table-body").find("form#form-checked").serialize()+"&status=2";
                e_post("<?=Url::toRoute('MaterialDetail/Checked').$params;?>",data,function(){
                    loadData(checked+" order material berhasil di tolak !",1,0);
                });
            }else{
                toast(" Anda belum pilih ceklist di table !",'danger','warning')
            }
            
            // toast('clicked');
        });

        <?php
            if(end($uri)=='od' || end($uri)=='bk'){
        ?>
            $("#basic .modal-content .modal-body").on("keyup","input.stock-input",function(){
                var this_var = $(this);
                var value = $(this).val();
                var stok = $(this).attr('data-stok');
                if(parseInt(value)>parseInt(stok)){
                    this_var.val(null);
                }
            });
        <?php
            }
        ?>
        $("div.table-body").on("click","button.detail_material",function(){
            modalPost("<?=Url::toRoute('Material/View')?>/"+$(this).attr('id'),{},"Detail Barang");
        });

        $("div.table-body").on("click","button.detail_materialdetail",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('MaterialDetail/View')?>/"+$(this).attr('id'),{},"Detail Transaksi");
            
        });

        $("div.table-body").on("click","button.cetak_transaksi",function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPostPrint("<?=Url::toRoute('MaterialDetail/Nota').$params?>",{id:$(this).attr('id')},"Bukti Transaksi");
        });

        // $("div.table-body").on("click","buttoaston.md_sent",function(){
            $("button.md_sent").click(function(){
                $("#basic .modal-dialog ").addClass("modal-lg");

                var checked=$("div.table-body").find("input.checkboxes:checked").length;
                if(checked > 0){
                    var data=$("div.table-body").find("form#form-checked").serialize();
                    modalGet("<?=Url::toRoute('MaterialDetail/Md_sent').$params?>",data,"Konfirmasi Pengiriman");
                }else{
                    toast(" Anda belum pilih ceklist di table !",'danger','warning');
                }
            });
            
            $("button.md_trdone").click(function(){
                var checked=$("div.table-body").find("input.checkboxes:checked").length;
                if(checked > 0){
                    start();
                    var data=$("div.table-body").find("form#form-checked").serialize();
                    e_post("<?=Url::toRoute('MaterialDetail/DoneByAdmin')?>",data,function(response){
                        loadData(response.pesan,response.status,0);
                    },"json");
                }else{
                    toast(" Anda belum pilih ceklist di table !",'danger','warning')
                }
            });
           
        // });

        $("#basic .modal-content .modal-body").on("submit","form#form_md_sent",function(e){
            e.preventDefault();
            start("#basic .modal-content");
           
            var form = $(this);
            $.ajax({
                type:'POST',
                url:"<?=Url::toRoute('MaterialDetail/Md_sent').$params?>",
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    loadData(response.pesan,response.status);
                }
            });
        });

  

        $("div.table-body").on("click","button.del_materialdetail",function(){
            id=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Hapus Transaksi ini !","warning",function(){
                start();
                e_post("<?=Url::toRoute('/MaterialDetail/Delete')?>/"+id,function(){
                    loadData("Data Berhasil di hapus !",1,0);
                    swal("Dihapus!", "Transaksi telah di hapus.", "success"); 
                });
            });
           
        });

         $("#basic .modal-content .modal-body").on("change","select.select-retur-material",function(){
            // alert($(this).val());
            if($(this).val()==1){
                $("div.div-ket-retur").fadeIn('fast');
                $("button.btn-konfir").addClass("display-hide");
                $("button.btn-retur").removeClass("display-hide");
            }else{
                $("div.div-ket-retur").fadeOut('fast');
                $("button.btn-retur").addClass("display-hide");
                $("button.btn-konfir").removeClass("display-hide");

            }

         });

        $("#basic .modal-content .modal-body").on("submit","form#form_md_done",function(e){
            e.preventDefault();
            // alert('hai');

            start("#basic .modal-content");
            var form = $(this);
            $.ajax({
                type:'POST',
                url:"<?=Url::toRoute('MaterialDetail/Md_done').$params?>",
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    loadData(response.pesan,response.status);
                }
            });
        });

       


        $("div.table-body").on("click",".btn-pay",function(){
            id=$(this).attr('id');
            // alert('hai');
            $("#basic .modal-dialog").removeClass("modal-lg");
            modalPost("<?=Url::toRoute('MaterialDetail/Payment')?>",{md_id_pay_form:id},"Bukti Pembayaran");
        });
        
        $("#basic .modal-content .modal-body").on("submit","form#form-pay-admin",function(e){
            e.preventDefault();
           
            start("#basic .modal-content");
           
            var form = $(this);
            $.ajax({
                type:'POST',
                url:"<?=Url::toRoute('MaterialDetail/Payment')?>",
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    loadData(response.pesan,response.status);
                }
            });
        });
        $("#basic .modal-content .modal-body").on("click",".new-c",function(){
            $("#form-c").removeClass('hidden');
        });

        $("#basic .modal-content .modal-body").on("change","select#id_c",function(){
            id=$(this).val();
            if(id!=null && id!=''){
                $.getJSON("<?=Url::toRoute('Customers/FindByPkJSON')?>?c_id="+id,function(response){
                    $("#c_nama").val(response.nama);
                    $("#c_tlp").val(response.tlp);
                    $("#c_email").val(response.email);
                    $("#c_alamat").val(response.alamat);
                    $("#c_prov").val(response.prov);
                    $("#c_kota").val(response.kota_kab);
                    $("#c_kec").val(response.kec);
                });
            }else{
                resetForm();
            }
        });

        // add brg
        $("#basic .modal-content .modal-body").on("click",".new_item",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/Material/Create')?>",{},"Tambah  Barang");
        });

        $("#basic .modal-content .modal-body").on("submit","form#form_material",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            var form = $(this);
            var url_val = $(this).attr('action');
            $.ajax({
                type:'POST',
                url:url_val,
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    $("#basic .modal-dialog").addClass("modal-lg");
                    
                    modalPost("<?=Url::toRoute('/MaterialDetail/Create').$params;?>",{},'<?=(end($uri)=='bm')?'<i class="mdi mdi-basket-fill fa-fw"></i> Barang masuk':'<i class="mdi mdi-basket-unfill fa-fw"></i> Order';?>');
                    // loadData(response.pesan,response.status);
                }
            });
            // console.log(form[0]);
            // e_post(url,$(this).serialize(),function(response){
                
            // },'json');
        });
    }

    var loadMutasiStok = function(){
        start();
        e_post("<?=Url::toRoute('MaterialDetail/MutasiStok')?>",$("form#form-filter-mutasi-stok").serialize(),function(response){
            $("div.table-report").html(response);
            end();
            // $("div.table-print").html(response);
        });
    }
    var mutasiStok = function(){
        $("button.btn-clear").click(function(){
            // alert('hai');
            $("select.select2meFilter,select.select2me,select.select2").select2("val","");
        });

        $("#form-filter").submit(function(e){
            e.preventDefault();
            start();
            // $("div.table-print").empty();
            e_post("<?=Url::toRoute('MaterialDetail/MutasiStok')?>",$(this).serialize(),function(response){
                $("div.table-report").html(response);
                end();
                // $("div.table-print").html(response);
            });
        });
    }

    var table_export = function(){
        $("table#MaterialDetailReport").tableExport({position: "top",formats: ["xls"],  bootstrap:true,trimWhitespace:false});
        $("button.xls").addClass('hidden-print btn-sm pull-right').detach().appendTo("div.btn-tools");
        // $("a.btn-xls").attr("data-fileblob",$("button.xls").attr("data-fileblob"));
    }

    var loadDataStok = function(pesan,status,withConfig=1){
        if(withConfig<1){
            start();
        }
        
        e_post("<?=Url::toRoute('MaterialDetail/Stok_item')?>",{VMaterial:1},function(response){
            $("div.table-body").html(response);
            StokBarangTable();
            checkAll("#all","#StokBarangTable");
            if(withConfig>0){
                end("#basic .modal-content");
                resetForm();
                $("#basic").modal('hide');   
            }else{
                end();
            }


            if(status==1){
                toast(pesan,"success","check");
            }else{
                toast(pesan,"danger","close");
            }
            $('[data-toggle=confirmation]').confirmation({
              rootSelector: '[data-toggle=confirmation]',
            });
            $('[data-toggle="tooltip"]').tooltip(); 
            $('.popovers').popover(); 
            $('.pulsate-repeat').pulsate({
                color: "#000000",
                reach: 50,
                speed: 100,
                glow: true
            });
            
        });
    }

    var kalkulasiBM = function(){
         // kalkulasi barang masuk
        $("#basic .modal-content .modal-body").on("click","button.bootstrap-touchspin-up,button.bootstrap-touchspin-down",function(){
            id=$(this).parents('div.row').find('input').attr('data-id');
            qty=($("#qty_"+id).val()=='' || $("#qty_"+id).val()==null)? 0 : toNumberOnly($("#qty_"+id).val());
            hb=($("#hb_"+id).val()=='' || $("#hb_"+id).val()==null)? 0 : toNumberOnly($("#hb_"+id).val());
      
            tpb=hb*qty;
            $("input#tpb_"+id).val(tpb);
            
            tot_tpb=0;
            $("input.tpb").each(function(i,item){
                tot_tpb+=toNumberOnly($(item).val());
            });
            
            ppn=tot_tpb*(parseFloat(($("#ppn_").val()=='' || $("#ppn_").val()==null)?0:$("#ppn_").val())/100);
            pph=tot_tpb*(parseFloat(($("#pph_").val()=='' || $("#pph_").val()==null)?0:$("#pph_").val())/100);
            bm=tot_tpb*(parseFloat(($("#bm_").val()=='' || $("#bm_").val()==null)?0:$("#bm_").val())/100);
            ongkir=($("#ongkir_").val()=='' || $("#ongkir_").val()==null)? 0 : toNumberOnly($("#ongkir_").val());

            ht_fix=(tot_tpb+ppn+pph+bm)+ongkir;
            $("input#ht_").val(toRp(ht_fix));
            // console.log(id);
            // $("input#hs_"+id).val(toRp(ht_fix/qty));
        });

        $("#basic .modal-content .modal-body").on("keyup",".kalkulasi",function(){
            id=$(this).attr('data-id');
            qty=($("#qty_"+id).val()=='' || $("#qty_"+id).val()==null)? 0 : toNumberOnly($("#qty_"+id).val());
            hb=($("#hb_"+id).val()=='' || $("#hb_"+id).val()==null)? 0 : toNumberOnly($("#hb_"+id).val());
            
            tpb=hb*qty;
            $("input#tpb_"+id).val(tpb);
            
            tot_tpb=0;
            $("input.tpb").each(function(i,item){
                tot_tpb+=toNumberOnly($(item).val());
            });
            
            ppn=tot_tpb*(parseFloat(($("#ppn_").val()=='' || $("#ppn_").val()==null)?0:$("#ppn_").val())/100);
            pph=tot_tpb*(parseFloat(($("#pph_").val()=='' || $("#pph_").val()==null)?0:$("#pph_").val())/100);
            bm=tot_tpb*(parseFloat(($("#bm_").val()=='' || $("#bm_").val()==null)?0:$("#bm_").val())/100);
            ongkir=($("#ongkir_").val()=='' || $("#ongkir_").val()==null)? 0 : toNumberOnly($("#ongkir_").val());

            ht_fix=(tot_tpb+ppn+pph+bm)+ongkir;
            $("input#ht_").val(toRp(ht_fix));
            // alert(qty);
        });
    }
    var kalkulasiOD = function(){

           // kalkulasi barang orders
        $("#basic .modal-content .modal-body").on("click","button.bootstrap-touchspin-up,button.bootstrap-touchspin-down",function(){
            id=$(this).parents('div.row').find('input.hgsat').attr('data-id');

            hg=toNumberOnly($("input#hgsat_"+id).val());
            qty=parseFloat($('input#qty_'+id).val());
            $("input#price_total_"+id).val(qty*hg);

            ongkir=($("#ongkir_buyer").val()=='')?0:toNumberOnly($("#ongkir_buyer").val());
            tot=0;
            $("input.price_total").each(function(){
                tot+=($(this).val()!=null && $(this).val()!='')?toNumberOnly($(this).val()):0;
            });
            $("input#total-pay").val(toRp(tot+ongkir));
        });

        $("#basic .modal-content .modal-body").on("keyup","input.kal_or",function(){
            id=$(this).attr('data-id');
            hg=toNumberOnly($("input#hgsat_"+id).val());
            qty=parseFloat($('input#qty_'+id).val());
            $("input#price_total_"+id).val(qty*hg);

            ongkir=($("#ongkir_buyer").val()=='')?0:toNumberOnly($("#ongkir_buyer").val());
            tot=0;
            $("input.price_total").each(function(){
                tot+=($(this).val()!=null && $(this).val()!='')?toNumberOnly($(this).val()):0;
            });
            $("input#total-pay").val(toRp(tot+ongkir));
        });
    }
    return {
        //main function to initiate map samples
        init: function () {
            <?php
                if($urimain2=='materialdetail' && ($urimain3=='index?status=od' || $urimain3=='index?status=bm'|| $urimain3=='index?status=bk')){
                    if($urimain3=='index?status=od'){
                        echo "kalkulasiOD();";
                    }
                    if($urimain3=='index?status=bm'){
                        echo "kalkulasiBM();";
                    }

                    echo 
                    "loadData('Data Berhasil di muat !',1,0);
                    scriptMaterialDetail();";
            
                }
                if($urimain2=='materialdetail' && $urimain3=='mutasistok'){
                    echo "
                    loadMutasiStok();
                    mutasiStok();
                    ";
            
                }
                if($urimain2=='materialdetail' && $urimain3=='stok_item'){
                    echo "loadDataStok('Data Berhasil di muat !',1,0);";
                }
            ?>
            plugin();
            
        }

    };

}();
</script>