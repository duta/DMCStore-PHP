<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Vmaterial = function () {
	var crudMaterial = function(){
		// create material
		$("button.add_material").click(function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('/material/create')?>",{},"Tambah Barang");
        });

        // creating material
        $("#basic .modal-content .modal-body").on("submit","form#form_material",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            var form = $(this);
            var url_val = $(this).attr('action');
            $.ajax({
                type:'POST',
                url:url_val,
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    $("#basic").modal('hide');
                    // toast(response.pesan,"success","check");
                    reloadGridview(response.pesan,response.status);
                }
            });
        });

        // Del barang
        $("div#p0").on("click","button.del_item",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['material/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "User berhasil di hapus.", "success"); 
                });
            });
           
        });

       // Edit Material
        $("div#p0").on("click","button.edit_item",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('material/update')?>/"+$(this).attr('id'),{},"Update Barang");
        });

        //Load varian combo
        $("#basic .modal-content .modal-body").on("change","select#attr",function(){
            attr=$(this).val();
            $("div#varian_combo").html("<i class='fa fa-refresh fa-spin'></i>");            
            e_post("<?=Url::toRoute(['material/variancombo'])?>",{id_attr:attr},function(response){
                $("div#varian_combo").html(response);
                $("select.itemselect2").select2({allowClear:true});
            });

        });

	}

	var input_grosir= function(){
        // HARGA GROSIR
        $("#basic .modal-content .modal-body").on("click","a.add-hg",function(){
            if($("input.hrg-s").val()==''){
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                $(".hg-wrap").fadeToggle('fast');
            }
            // console.log($("input.hrg-s").val());
        });

        $("#basic .modal-content .modal-body").on("click","button.add-row-hg",function(){
            $("div.row-hg:last").clone().appendTo("div.hg-wrap").find("input").val('');
            $(this).last().hide();
            $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});

        });

        $("#basic .modal-content .modal-body").on("click","button.rm-row-hg",function(){
            var this_el=$(this);
            // alert($("button.rm-row-hg").length);
            if($("button.rm-row-hg").length>1){
                this_el.parents('.row-hg').remove();
                $("button.add-row-hg:last").show();
            }
        });

        $("#basic .modal-content .modal-body").on("keyup","input.hrg-g",function(){
            var hrg_s=toNumberOnly($("input.hrg-s").val());
            var hrg_g=$(this);
            if($("input.hrg-s").val()==''){
                hrg_g.val('Rp 0');
                toast("Harga retail harus di isi lebih dulu !","danger","close");             
            }else{
                if(toNumberOnly(hrg_g.val())>=hrg_s){
                    hrg_g.val('Rp 0');
                }
            }
        });
    }


	return {
		init: function () {
			crudMaterial();
			input_grosir();
		}
	}	
}();
</script>