<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var marketplace= function () {
	var crudmarketplace = function(){
		// add marketplace
		$("button.add_marketplace").click(function(){
			modalGet("<?=Url::toRoute('marketplace/create')?>",{},'<i class="mdi mdi-store fa-fw"></i>Tambah market');
		});
		// update marketplace
		$("div#p0").on("click","button.update_marketplace",function(){
			modalGet("<?=Url::toRoute('marketplace/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-store fa-fw"></i>Update market');
		});

        // adding/updating marketplace
		$("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
            	// alert(response.errors.length);
            	if(response.length <1){
            		reloadGridview('Action success',1);
            	}else{
            		error(response.errors);
            		endModal();
            	}
            },'json');
        });

        // Del marketplace
        $("div#p0").on("click","button.del_marketplace",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['marketplace/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "marketplace berhasil di hapus.", "success"); 
                });
            });
           
        });
	}

	return {
		init: function () {
			crudmarketplace();
		}
	}	
}();
</script>