<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Brsupplier = function () {
	var crudSup = function(){
		// add supplier
		$("button.add_sup").click(function(){
			modalGet("<?=Url::toRoute('brsupplier/create')?>",{},'<i class="mdi mdi-account-network fa-fw"></i>Tambah Supplier');
		});
		// update Supplier
		$("div#p0").on("click","button.update_sup",function(){
			modalGet("<?=Url::toRoute('brsupplier/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-account-network  fa-fw"></i>Update Supplier');
		});

        // adding/updating Supplier
		$("#basic .modal-content .modal-body").on("submit","form#form_sup",function(e){
            e.preventDefault();
            start("#basic .modal-content"); 
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                reloadGridview(response.pesan,response.status);
            },'json');
        });

        // Del Supplier
        $("div#p0").on("click","button.del_sup",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['brsupplier/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Supplier berhasil di hapus.", "success"); 
                });
            });
           
        });
	}

	return {
		init: function () {
			crudSup();
		}
	}	
}();
</script>