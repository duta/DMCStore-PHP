<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Customer = function () {
	var crudfW = function(){
		// add customers
		$("button.add_plg").click(function(){
			modalGet("<?=Url::toRoute('customers/create')?>",{},'<i class="mdi mdi-account-multiple fa-fw"></i>Tambah Pelanggan');
		});
		// update customers
		$("div#p0").on("click","button.update_plg",function(){
			modalGet("<?=Url::toRoute('customers/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-account-multiple fa-fw"></i>Update Pelanggan');
		});

        // adding/updating customers
		$("#basic .modal-content .modal-body").on("submit","form#customers-form",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
            	// alert(response.errors.length);
            	if(response.length <1){
            		reloadGridview('Action success',1);
            	}else{
            		error(response.errors);
            		endModal();
            	}
            },'json');
        });

        // Del customers
        $("div#p0").on("click","button.del_plg",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['customers/delete'])?>/"+id_val,{id:id_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Pelanggan berhasil di hapus.", "success"); 
                });
            });
           
        });
	}

	return {
		init: function () {
			crudfW();
		}
	}	
}();
</script>