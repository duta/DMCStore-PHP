<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var starter = function () {
	  var crudstarter = function(){
        // add starter
        $("button.add_starter").click(function(){
            modalGet("<?=Url::toRoute('starter/create')?>",{},'<i class="mdi mdi-plus fa-fw"></i>Tambah starter');
        });
        // update starter
        $("div#p0").on("click","button.update_starter",function(){
            modalGet("<?=Url::toRoute('starter/update')?>/"+$(this).attr('id'),{},'<i class="mdi mdi-lead-pencil fa-fw"></i>Update starter');
        });

        // adding/updating starter
        $("#basic .modal-content .modal-body").on("submit","form#w0",function(e){
            e.preventDefault();
            // alert('hai');
            startModal() ;
            var url=$(this).attr('action');
            e_post(url,$(this).serialize(),function(response){
                // alert(response.errors.length);
                if(response.length <1){
                    reloadGridview('Action success',1);
                }else{
                    error(response.errors);
                    endModal();
                }
            },'json');
        });

        // Del starter
        $("div#p0").on("click","button.del_starter",function(){
            id_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Data yang dihapus tidak bisa dikembalikan !","warning",function(){
                e_post("<?=Url::toRoute(['starter/delete'])?>/"+id_val,{id:id_val},function(){
                    $('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "starter berhasil di hapus.", "success"); 
                });
            });
           
        });

	}
	return {
		init: function () {
			crudstarter();
		}
	}	
}();
</script>