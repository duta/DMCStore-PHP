<?php
    use yii\helpers\Url;
    $user=Yii::$app->user->identity;
?>
<script type="text/javascript">
var Vbk = function () {
    var plugin = function(){
         $('#defaultrange').daterangepicker({
                opens: ('left'),
                format: 'DD-MM-YYYY',
                separator: ' TO ',
                startDate: "<?=date('01-m-Y')?>",
                endDate: "<?=date('t-m-Y')?>",
            },
            function (start, end) {
                $('#defaultrange input').val(start.format('DD-MM-YYYY') + ' TO ' + end.format('DD-MM-YYYY'));
            }
        );
        $("select.select2meFilter,select.select2").select2({allowClear: true});
        $("input.MaskMoney").maskMoney({prefix:'Rp ',thousands:'.',decimal:',',precision:0});
        $("input.touchspin").TouchSpin({ min: 1});

        // $("select.select2meFilter,select.select2").select2({allowClear: true});
    }
    var crudBk = function(){
        // Del BK
        $("div#p0").on("click","a.del_bk",function(){
            inv_val=$(this).attr('id');
            swal_help("Apakah anda yakin ?","Hapus Transaksi ini !","warning",function(){
                e_post("<?=Url::toRoute('/materialdetail/del_byod')?>",{inv:inv_val},function(){
        			$('#w0').yiiGridView('applyFilter');
                    swal("Dihapus!", "Transaksi telah di hapus.", "success"); 
                });
            });
           
        });

        // View Nota
        $("div#p0").on("click","a.print_tr_bk",function(){
            start();
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPostPrint("<?=Url::toRoute('materialdetail/nota')?>",{inv:$(this).attr('id'),tr:'bk'},"Bukti Transaksi");
        });

        // View Detail BK
        $("div#p0").on("click","a.detail_bk",function(){
            $("#basic .modal-dialog").addClass("modal-lg");
            modalPost("<?=Url::toRoute('materialdetail/detail_tr')?>",{inv:$(this).attr('id'),tr:'bk'},"Detail Transaksi");
        });

        // shipping
        $("button.item_ship").click(function(){
            $("#basic .modal-dialog ").addClass("modal-lg");
            var checked=$("div#p0").find("input.check_item:checked");
            if(checked.length > 0){
                var data=checked.serialize();
                modalGet("<?=Url::toRoute('materialdetail/shipping')?>",data,"Konfirmasi Pengiriman");
            }else{
                toast(" Anda belum pilih ceklist di table !",'danger','warning');
                $("#basic .modal-dialog ").removeClass("modal-lg");
            }
        });

        $("#basic .modal-content .modal-body").on("submit","form#form_shipping",function(e){
            e.preventDefault();
            start("#basic .modal-content");
            var form = $(this);
            $.ajax({
                type:'POST',
                url:"<?=Url::toRoute('materialdetail/shipping')?>",
                data: new FormData(form[0]),
                contentType:false,
                processData:false,
                dataType:'json',
                success:function(response){
                    reloadGridview(response.pesan,response.status);

                }
            });
        });
           
        // barang di terima/DONE
        $("button.inv_done").click(function(){
            var checked=$("div#p0").find("input.check_item:checked");
            if(checked.length > 0){
                var data=checked.serialize();
                swal_help("Barang diterima pembeli.","Apakah anda yakin ?","info",function(){
                    // start();
                    e_post("<?=Url::toRoute('/materialdetail/tr_done')?>",data,function(response){
                        swal("Transaksi selesai !", "Barang sudah diterima oleh pembeli, transaksi ini akan mempengaruhi jumlah stok ", "success"); 
                        // end();
                        $('#w0').yiiGridView('applyFilter');
                    },'json');
                },"CEK LAGI","SELESAI");
            }else{
                toast(" Anda belum pilih ceklist di table !",'danger','warning');
                $("#basic .modal-dialog ").removeClass("modal-lg");
            }
        });

     
        
        // ship print loop
         $("button.print_loop").click(function(){
            $("#basic .modal-dialog ").addClass("modal-lg");
            var checked=$("div#p0").find("input.check_item:checked");
            if(checked.length > 0){
                var data=checked.serialize();
                modalPostPrint("<?=Url::toRoute('materialdetail/ship_print_loop')?>",data,"Cetak Transaksi");
            }else{
                toast(" Anda belum pilih ceklist di table !",'danger','warning');
                $("#basic .modal-dialog ").removeClass("modal-lg");
            }
        });

    }

    return {
        init: function () {
            crudBk();
        }
    }   
}();
</script>