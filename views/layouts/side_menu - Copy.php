<?php
    use yii\Helpers\UrL;
    use app\models\Permissions;

    $user=Yii::$app->user->identity;
    $menu=Permissions::find()->joinWith('menu')->where(['role_id'=>$user->role,'parent_id'=>0])->orderBy(['menu.no_urut'=>SORT_ASC])->all();
    foreach ($menu as $item) {
        if($item->menu->is_heading<1){
?>  
    <li class="<?=$item->menu->class_add?>"> 
        <a href="<?=url::to('@web/'.$item->menu->link)?>" class="waves-effect hidden-print">
            <i  class="mdi <?=$item->menu->icon;?> fa-fw"></i> <span class="hide-menu"><?=$item->menu->name_as?></span>
        </a> 
    </li>
<?php
        }else{
?>
            <li class="<?=$item->menu->class_add?>"> 
                <a href="javascript:void(0)" class="waves-effect hidden-print">
                    <i class="mdi <?=$item->menu->icon;?> fa-fw" data-icon="v"></i> 
                    <span class="hide-menu"> <?=$item->menu->name_as?>  <span class="fa fa-caret-down"></span> 
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <?php
                    if($item->menu->child)
                        foreach ($item->menu->child as $child) {
?>
                            <li class="<?=$item->menu->class_add?>"> 
                               
                                 <a href="<?=url::to('@web/'.$child->link)?>" class="waves-effect hidden-print">
                                    <i  class="mdi <?=$child->icon;?> fa-fw"></i> 
                                    <span class="hide-menu"><?=$child->name_as?></span>
                                </a>
                            </li>

<?php
                        }
                    ?>
                    
                </ul>
            </li>
<?php
        }
    }    
?>
