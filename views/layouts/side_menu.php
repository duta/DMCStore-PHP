<?php
    use yii\Helpers\UrL;
    use app\models\Permissions;
    use yii\helpers\ArrayHelper;

    $user=Yii::$app->user->identity;
    $menu=Permissions::find()->joinWith('menu')->where(['role_id'=>$user->id_role,'parent_id'=>0])->orderBy(['menu.no_urut'=>SORT_ASC])->all();
    $menuChild=Permissions::find()->joinWith('menu')->where('role_id='.$user->id_role.' AND parent_id>0')->orderBy(['menu.no_urut'=>SORT_ASC])->asArray()->all();

    foreach ($menu as $item) {
        if($item->menu->is_heading<1){
?>  
    <li class="<?=$item->menu->class_add?>"> 
        <a href="<?=url::to('@web/'.$item->menu->link)?>" class="waves-effect hidden-print">
            <i  class="mdi <?=$item->menu->icon;?> fa-fw"></i> <span class="hide-menu"><?=$item->menu->name_as?></span>
        </a> 
    </li>
<?php
        }else{
?>
            <li class="<?=$item->menu->class_add?>"> 
                <a href="javascript:void(0)" class="waves-effect hidden-print">
                    <i class="mdi <?=$item->menu->icon;?> fa-fw" data-icon="v"></i> 
                    <span class="hide-menu"> <?=$item->menu->name_as?>  <span class="fa fa-caret-down"></span> 
                    </span>
                </a>
                <ul class="nav nav-second-level">
                    <?php
                    if($item->menu->child)
                        foreach ($item->menu->child as $child) {    
                            foreach (ArrayHelper::getColumn($menuChild,'menu_id') as $menuChild_id) {
                                if($child->id==$menuChild_id){
                                    ?>
                                    <li class="<?=$item->menu->class_add?>">
                                       
                                         <a href="<?=url::to('@web/'.$child->link)?>" class="waves-effect hidden-print">
                                            <i  class="mdi <?=$child->icon;?> fa-fw"></i> 
                                            <span class="hide-menu"><?=$child->name_as?></span>
                                        </a>
                                    </li>

                                    <?php
                                }
                            }                      
                        }
                    ?>
                    
                </ul>
            </li>
<?php
        }
    }    
?>
 