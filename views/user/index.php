
<?php
use yii\helpers\Html;
use yii\helpers\Url;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use app\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$link=Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
$menu=Menu::find()->with(['permit_action'])->where(['link'=>$link])->one();
$this->title = Yii::t('app', $menu->name); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi '.$menu->icon.' fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;



$sess = Yii::$app->session;
$sess->set('current_action',$link);
$sess->set('menu',$menu);

?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <?php
                if(isset($menu->parent)){
            ?>
                    <li class=""><?=$menu->parent->name?></li>
            <?php
                }
            ?>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                <?php
                    foreach ($menu->permit_action  as $value) {
                        if($value->menu_action->tipe=='add'){
                            
                            echo '<button type="button" class="btn  btn-circle btn-sm pull-right '.$value->menu_action->class_add.'"  data-toggle="tooltip" data-original-title="'.ucwords($value->menu_action->name).'" data-placement="top"><i class="fa '.$value->menu_action->icon.'"></i></button>';
                        }
                    }
                ?>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                          <?php Pjax::begin(); ?>    <?= GridView::widget([
                                'tableOptions'=>['class'=>'table table-condensed table-hover '],

                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'responsive'=>true,
                                'responsiveWrap'=>false,
                                'condensed'=>true,
                                'bordered'=>false,
                                'export'=>false,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    // Starter column
                                        // [
                                        //     'header'=> '',
                                        //     'format'=>'',
                                        //     'value'=>function($model,$key,$i){
                                        //         return '';
                                        //     },
                                        //     'filter'=>'<input type="text" class="form-control input-sm" name="" value="" placeholder="" >'
                                    //     // ],
                                    // 'id',
                                    // 'password',
                                    'username', 
                                    'fullname',
                                    'email',
                                    
                                    [
                                            'header'=> 'Role',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return $model->role->fullname;
                                            },
                                            // 'filter'=>'<input type="text" class="form-control input-sm" name="" value="" placeholder="" >'
                                        ],
                                    // 'authKey',
                                    // 'accessToken',
                                    // 'active',
                                    // 'fullname:ntext',
                                    // 'role',
                                    // 'created_by',
                                    // 'created_at',
                                    // 'updated_at',

                                    // ['class' => 'yii\grid\ActionColumn'],
                                    [   'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                        'headerOptions' => ['width' => '100'],

                                        'class' => 'yii\grid\ActionColumn',
                                        'template'=>'<center>{action}</center>', /*{update} {delete} */

                                        'buttons'=>[
                                            // 'update'=>function($url,$model){
                                            //     return '<button type="button" class="btn btn-primary btn-circle btn-sm update_user" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Update User" data-placement="top"><i class="fa fa-pencil"></i></button>';
                                            // },
                                            // 'delete'=>function($url,$model){
                                            //     return '<button type="button" class="btn btn-danger btn-circle btn-sm del_user" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Hapus User" data-placement="top"><i class="fa fa-trash"></i></button>';
                                            // },
                                            'action'=>function($url,$model) use ($menu) {
                                                $html="";
                                                foreach ($menu->permit_action  as $value) {
                                                    if($value->menu_action->tipe!='add'){

                                                        $html.='<button type="button" class="btn  btn-circle btn-sm '.$value->menu_action->class_add.'" id="'.$model[$value->menu_action->column_param].'" data-toggle="tooltip" data-original-title="'.ucwords($value->menu_action->name).'" data-placement="top"><i class="fa '.$value->menu_action->icon.'"></i></button>';
                                                    }
                                                }
                                                return $html;
                                            }
                                        ],
                                    ],
                                ],
                            ]); ?>
                        <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                    <?php
                        // echo $sess->get('current_action');
                        // echo "<pre>";
                        // print_r($menu->permit_action);
                        // foreach ($menu->permit_action as $value) {
                        //     echo $value->menu->name.' => '.$value->menu_action->name."<br>";
                        // }
                        
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>