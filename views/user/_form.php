<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;

    use app\models\Role;
?>

<form class="form-horizontal" id="form_user" action="<?=($model->isNewRecord) ? Url::toRoute('user/create') : Url::toRoute('user/update').'/'.$model['id']; ?>" role="form">
    <div class="form-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="User[fullname]" value="<?=$model['fullname']?>" placeholder="Nama Lengkap" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="User[username]" value="<?=$model['username']?>" placeholder="username" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control" name="User[email]" value="<?=$model['email']?>" placeholder="email" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <?php
            if($model->isNewRecord){
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="User[password]" value="<?=$model['password']?>" placeholder="password" required="required">
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
         <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="col-md-12">
                         <?=Html::DropDownList("User[id_role]",$model['id_role'],ArrayHelper::map(Role::find()->all(), 'id', 'fullname'),['id'=>'id_role','class'=>'form-control','required'=>'required','prompt'=>'-- Pilih Role --']);?>
                        <span class="help-block">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <hr class="light-grey-hr">
    
    <div class="form-actions">
        <div class="row">
            <div class="text-center col-md-12">
                <button type="submit" class="btn btn-success"><?=($model->isNewRecord ? 'Create' : 'Save'); ?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</form>