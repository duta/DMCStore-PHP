<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VPb */

$this->title = $model->kd_tagihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vpbs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vpb-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->kd_tagihan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->kd_tagihan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_tagihan',
            'kd_transaksi',
            'tipe_transfer',
            'tipe_transaksi',
            'ppn',
            'pph',
            'bea_masuk',
            'hgtot_dasar',
            'hgtot_beli',
            'ongkir_impor',
            'qty_total',
            'id_sup',
            'id_status',
            'created_at',
        ],
    ]) ?>

</div>
