<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use app\components\Help;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pembelian'); /*TITLE*/
$icon = Yii::t('app', ''); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                <button class="add_bm btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="ti-plus"></span> 
                </button>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                          <?php Pjax::begin(); ?>    <?= GridView::widget([
                                'tableOptions'=>['class'=>'table table-condensed table-hover'],
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'responsive'=>true,
                                'responsiveWrap'=>false,
                                'condensed'=>true,
                                'bordered'=>false,
                                'export'=>false,
                                'rowOptions'=>function($model, $key, $index, $grid){
                                        if($model->id_status==3){
                                            return array('key'=>$key,'index'=>$index,'class'=>"");
                                        }
                                    },

                                'columns' => [
                                    // action column
                                    [
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                            'headerOptions' => ['width' => '150'],
                                            'class' => 'yii\grid\ActionColumn',
                                            'contentOptions' => ['style' => 'width:15%;'],

                                            'template'=>'<center>{price}{update}{detail}{delete}</center>',
                                            'buttons'=>[
                                                'action'=>function($url,$model){
                                                     return ' <div class="btn-group dropup">
                                                            <button aria-expanded="false" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light" type="button">
                                                                    <i class="fa fa-cog m-r-5"></i>
                                                                    <span class="caret"></span>
                                                            </button>
                                                            <ul role="menu" class="dropdown-menu animated flipInX">
                                                                <li class="disabled"><a href="javascript:void(0)">TR : #'.$model->kd_tagihan.'</a></li>
                                                                <li class="divider"></li>
                                                                <li >
                                                                        <a href="javascript:void(0)" id="'.$model->kd_tagihan.'" class="price_item"><i class="fa fa-percent fa-fw"></i>Atur harga</a>
                                                                </li>
                                                               

                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kd_tagihan.'" class="edit_bm"><i class="fa fa-pencil fa-fw"></i>Perbarui Pembelian</a>
                                                                </li>

                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kd_tagihan.'" class="detail_bm"><i class="fa fa-search fa-fw"></i>Detail Pembelian</a>
                                                                </li>

                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kd_tagihan.'" class="del_bm"><i class="fa fa-trash fa-fw"></i>Hapus Pembelian</a>
                                                                </li>
                                                              
                                                                
                                                            </ul>
                                                        </div>';
                                                },
                                                'price'=>function($url,$model){
                                                    return '<a href="javascript:void(0)" class="btn btn-success btn-circle btn-sm price_item" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Atur harga" data-placement="top"><i class="fa fa-percent"></i></a>';
                                                },

                                                'update'=>function($url,$model){
                                                    // if($model->id_status==3){
                                                        return '<a href="javascript:void(0)" class="btn btn-info btn-circle btn-sm edit_bm" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Perbarui Pembelian" data-placement="top"><i class="fa fa-pencil"></i></a>';
                                                    // }
                                                },
                                                'detail'=>function($url,$model){
                                                    return '<a href="javascript:void(0)" class="btn btn-warning btn-circle btn-sm detail_bm" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Detail Pembelian" data-placement="top"><i class="fa fa-search"></i></a>';
                                                },
                                                'delete'=>function($url,$model){
                                                    // return Html::button('<i class="fa fa-trash"></i>',['class'=>'btn btn-danger btn-circle btn-sm del_materialdetail']);
                                                    return '<a href="javascript:void(0)" class="btn btn-danger btn-circle btn-sm del_bm" id="'.$model->kd_tagihan.'" data-toggle="tooltip" data-original-title="Hapus Pembelian" data-placement="top"><i class="fa fa-trash"></i></a>';
                                                }
                                            ],
                                        ],
                                    ['class' => 'yii\grid\SerialColumn','contentOptions' => ['style' => 'width:1%;']],
                                    // INVOICE
                                    [
                                        'header'=> 'Supplier',
                                        'format'=>'raw',

                                        'value'=>function($model,$key,$i){
                                            // $pb='<b>TR :</b> #'.$model->kd_tagihan;
                                            $pb='';
                                            $pb.='<b>'.$model->supplier->nm_sup.'</b>';
                                            $pb.='<br> <small>'.Help::dateindoShort($model->order_at).'<small>';
                                            return $pb;
                                        },
                                        'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[nm_sup]" value="'.$searchModel->nm_sup.'" placeholder="search by suppliers" >'
                                    ],
                                    // Supplier
                                    // [
                                    //     'header'=> 'Supplier',
                                    //     // 'attribute'=>'id_sup',
                                    //     'value'=>function($model,$key,$i){
                                    //         return $model->supplier->nm_sup;
                                    //     },
                                    //     'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[nm_sup]" value="'.$searchModel->nm_sup.'" placeholder="search by suppliers" >'
                                    // ],

                                    [
                                        'header'=> 'Barang ',
                                        'format'=>'raw',
                                        'value'=>function($model,$key,$i){
                                            $arr=[];

                                            foreach ($model->md as $item) {
                                                if(isset($item->v_material->nama)){
                                                    $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;
                                                }
                                            }
                                           
                                            return implode(array_unique($arr), '<br>');
                                        },
                                        'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                    ],
                                    // Varian
                                    [
                                        'header'=> '(QTY) Varian ',
                                        'format'=>'raw',
                                        'value'=>function($model,$key,$i){
                                            $val='';
                                            foreach ($model->md as $item) {
                                                if(isset($item->v_material->nama)){
                                                    $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                                                    $val.='('.$item->qty_order_bm.') '.$item->v_material->varianname.' <br>';
                                                }
                                            }
                                            return $val;
                                        },
                                        // 'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                    ],

                                     [
                                        'header'=> 'Harga modal ',
                                        'format'=>'raw',
                                        'value'=>function($model,$key,$i){
                                            $val='';
                                            foreach ($model->md as $item) {
                                                $val.=Help::toRp($item->harga_mod).'<br>';
                                                $val.="";
                                            }
                                            return $val;
                                        },
                                        // 'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                    ],

                                    


                                    // 'id',
                                    // 'kd_tagihan',
                                    // 'kd_transaksi',
                                    // 'tipe_transfer',
                                    // 'tipe_transaksi',
                                    // 'ppn',
                                    // 'pph',
                                    // 'bea_masuk',
                                    // 'hgtot_dasar',
                                    // 'hgtot_beli',
                                    // 'ongkir_impor',
                                    // 'qty_total',
                                    // 'id_sup',
                                    // 'id_status',
                                    // 'created_at',

                                    
                                ],
                            ]); ?>
                        <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>