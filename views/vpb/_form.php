<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VPb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vpb-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_tagihan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_transaksi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipe_transfer')->textInput() ?>

    <?= $form->field($model, 'tipe_transaksi')->textInput() ?>

    <?= $form->field($model, 'ppn')->textInput() ?>

    <?= $form->field($model, 'pph')->textInput() ?>

    <?= $form->field($model, 'bea_masuk')->textInput() ?>

    <?= $form->field($model, 'hgtot_dasar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hgtot_beli')->textInput() ?>

    <?= $form->field($model, 'ongkir_impor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qty_total')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_sup')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
