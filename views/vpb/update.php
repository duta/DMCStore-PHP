<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VPb */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vpb',
]) . $model->kd_tagihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vpbs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_tagihan, 'url' => ['view', 'id' => $model->kd_tagihan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vpb-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
