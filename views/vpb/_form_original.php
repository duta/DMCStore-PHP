
    <?= $form->field($model, 'gi_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gi_no')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gi_date')->textInput() ?>

    <?= $form->field($model, 'gi_nosuratjln')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gi_nofaktur')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gi_expiredate')->textInput() ?>

    <?= $form->field($model, 'gi_sourcewh_id')->textInput() ?>

    <?= $form->field($model, 'gi_receiving_person')->textInput() ?>

    <?= $form->field($model, 'gi_checked_by')->textInput() ?>

    <?= $form->field($model, 'gi_approved_by')->textInput() ?>

    <?= $form->field($model, 'gi_approved_finance_by')->textInput() ?>

    <?= $form->field($model, 'gi_receiving_location')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'gi_lastupdate')->textInput() ?>

    <?= $form->field($model, 'gi_description')->textarea(['rows' => 6]) ?>