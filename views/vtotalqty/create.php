<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VTotalqty */

$this->title = Yii::t('app', 'Create Vtotalqty');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vtotalqties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vtotalqty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
