<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VTotalqty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vtotalqty-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_tagihan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_material')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qty_unacc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qty_acc')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
