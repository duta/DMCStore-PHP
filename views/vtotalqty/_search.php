<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VTotalqtySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vtotalqty-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kd_tagihan') ?>

    <?= $form->field($model, 'id_material') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'qty_order_bm') ?>

    <?= $form->field($model, 'qty_unacc') ?>

    <?php // echo $form->field($model, 'qty_acc') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
