<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VTotalqty */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vtotalqty',
]) . $model->kd_tagihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vtotalqties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_tagihan, 'url' => ['view', 'id' => $model->kd_tagihan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vtotalqty-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
