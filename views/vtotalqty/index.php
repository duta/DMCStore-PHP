
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Barang masuk'); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi mdi-file fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="">Laporan</li>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>

             <!--    <button class="add_od btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="ti-plus"></span> 
                </button> -->
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                        <?php Pjax::begin(); ?>    <?= GridView::widget([
                                'tableOptions'=>['class'=>'table table-condensed table-hover'],
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    'kd_tagihan',
                                    // 'id_material',
                                    'fullname:ntext',
                                    'qty_order_bm',
                                    'qty_unacc',
                                    'qty_acc',

                                    // ['class' => 'yii\grid\ActionColumn'],
                                ],
                            ]); ?>
                        <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>