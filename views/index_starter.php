
<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;

use yii\widgets\Pjax;
use app\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$link=Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
$menu=Menu::find()->where(['link'=>$link])->one();
$this->title = Yii::t('app', strtolower($menu->name)); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi '.$menu->icon.' fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <?php
                if(isset($menu->parent)){
            ?>
                    <li class=""><?=$menu->parent->name?></li>
            <?php
                }
            ?>
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>

                <button class="add_<?=$this->title?> btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="ti-plus"></span> 
                </button>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                            <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'responsive'=>true,
                                    'responsiveWrap'=>false,
                                    'condensed'=>true,
                                    'bordered'=>false,
                                    'export'=>false,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'id',
                                        'name',
                                        [
                                            'header'=> 'Lokasi ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return $model->lokasi->name;
                                            },
                                        ],
                                        // 'created_at',
                                        // 'updated_at',

                                        [   
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',

                                            'class' => 'yii\grid\ActionColumn',
                                            'template'=>'<center>{update} {delete}</center>',
                                            'buttons'=>[
                                                'update'=>function($url,$model){
                                                    return '<button type="button" class="btn btn-primary btn-circle btn-sm update_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Update '.$this->title.'" data-placement="top"><i class="fa fa-pencil"></i></button>';
                                                },
                                                'delete'=>function($url,$model){
                                                    return '<button type="button" class="btn btn-danger btn-circle btn-sm del_'.$this->title.'" id="'.$model->id.'" data-toggle="tooltip" data-original-title="Hapus '.$this->title.'" data-placement="top"><i class="fa fa-trash"></i></button>';
                                                }
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                          
                                    <!-- 'tableOptions'=>['class'=>'table table-condensed table-hover'], -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

