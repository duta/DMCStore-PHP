<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kurir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kurir-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nm_kurir')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'layanan')->textInput(['maxlength' => true]) ?>

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
