<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kurir */

$this->title = Yii::t('app', 'Create Kurir');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kurirs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
