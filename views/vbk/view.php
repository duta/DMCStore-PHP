<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VBk */

$this->title = $model->kd_tagihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vbks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vbk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->kd_tagihan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->kd_tagihan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'kd_tagihan',
            'tipe_transfer',
            'tipe_transaksi',
            'qty',
            'ongkir_buyer',
            'total_belanja',
            'total_tagihan_buyer',
            'buyer',
            'buyer_note:ntext',
            'id_market',
            'id_kurir',
            'resi',
            'id_status',
            'created_at',
            'created_by',
        ],
    ]) ?>

</div>
