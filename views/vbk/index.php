<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Barang Keluar');
$icon = Yii::t('app', '<i class="mdi mdi-basket-unfill fa-fw"></i>');
// $this->params['breadcrumbs'][] = $this->title;
if($user->id_role==1){
    $give="show";
    $send="show";
    $done="show";
    $print="show";
}elseif ($user->id_role==2) {
    $give="hidden";
    $send="hidden";
    $done="show";
    $print="hidden";

}elseif ($user->id_role==3) {
    $give="hidden";
    $send="show";
    $done="hidden";
    $print="show";
}
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
       

                <button class="<?=$done?> pull-right inv_done btn btn-sm btn-success btn-outline  waves-effect waves-light" data-toggle="tooltip" data-original-title="Barang telah diterima & Transaksi selesai" data-placement="left">
                    <i class="mdi mdi-clipboard-check"></i> </button>
                    
                <button class="<?=$send?> pull-right m-r-10 item_ship btn btn-sm btn-primary btn-outline  waves-effect waves-light" data-toggle="tooltip" data-original-title="Sudah dikirim" data-placement="left">
                            <i class="mdi mdi-truck-delivery"></i> </button>


                <button class="<?=$print?> pull-right print_loop m-r-10 btn btn-sm btn-success btn-outline  waves-effect waves-light" data-toggle="tooltip" data-original-title="Cetak untuk pengiriman" data-placement="left">
                    <i class="mdi mdi-printer"></i> </button>
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                            <?php Pjax::begin(); ?>    <?= GridView::widget([
                                    'tableOptions'=>['class'=>'table table-condensed table-hover '],

                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'responsive'=>true,
                                    'responsiveWrap'=>false,
                                    'condensed'=>true,
                                    'bordered'=>false,
                                    'export'=>false,
                                    'columns' => [
                                         // action Column
                                        [
                                            'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                            // 'headerOptions' => ['width' => '100'],
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '<center>{action}</center>',
                                            'buttons' => [
                                                'action'=>function($url,$model)  use ($user){
                                                    $action=' <div class="btn-group dropup">
                                                            <button aria-expanded="false" data-toggle="dropdown" class="btn btn-sm btn-primary dropdown-toggle waves-effect waves-light" type="button">
                                                                    <i class="fa fa-cog m-r-5"></i>
                                                                    <span class="caret"></span>
                                                            </button>
                                                            <ul role="menu" class="dropdown-menu animated flipInX">
                                                                <li class="disabled"><a href="javascript:void(0)">TR : #'.$model->kd_order.'</a></li>
                                                                <li class="divider"></li>';
                                                        if($model->is_gived!=1 && $user->id_role!=3){
                                                            $action.='<li>
                                                                        <a href="javascript:void(0)" id="'.$model->kd_order.'" class="detail_bk"><i class="fa fa-search fa-fw"></i>Detail Transaksi</a>
                                                                </li>

                                                                <li>
                                                                        <a href="javascript:void(0)" id="'.$model->kd_order.'" class="print_tr_bk"><i class="fa fa-print fa-fw"></i>Cetak Transaksi</a>
                                                                </li>';
                                                        }
                                                        if($user->id_role==1){
                                                               $action.='<li>
                                                                        <a href="javascript:void(0)" id="'.$model->kd_order.'" class="del_bk"><i class="fa fa-trash fa-fw"></i>Hapus Transaksi</a>
                                                                </li>';
                                                        }

                                                        if($model->resi_photo!=null || $model->resi_photo!=''){
                                                        
                                                            $action.=' <li>
                                                                                <a href="javascript:void(0);" data-filename="'.$model->resi_photo.'" id="'.$model->kd_order.'"  class="view_resi">
                                                                                <i class="fa fa-file-image-o fa-fw"></i>Lihat Bukti Resi
                                                                                </a>
                                                                        </li>';
                                                        }
                                                        $action.='</ul>
                                                        </div>';
                                                    return $action;
                                                },
                                                'view' => function($url,$model)  use ($user){
                                                    if($model->is_gived!=1 && $user->id_role!=3){
                                                        return '<button type="button" class="btn btn-info btn-circle btn-sm detail_bk" id="'.$model->kd_order.'" data-toggle="tooltip" data-original-title="Detail Transaksi" data-placement="top"><i class="fa fa-search"></i></button>';
                                                    }
                                                },
                                                'print' => function($url,$model) use ($user){

                                                    if($model->is_gived!=1 && $user->id_role!=3){
                                                        return '<button type="button" class="btn btn-success btn-circle btn-sm print_tr_bk" id="'.$model->kd_order.'" data-toggle="tooltip" data-original-title="Cetak Transaksi" data-placement="top"><i class="fa fa-print"></i></button>';
                                                    }
                                                },
                                                'del' => function($url,$model) use($user){
                                                    if($user->id_role==1){
                                                        return '<button type="button" class="btn btn-danger btn-circle btn-sm del_bk" id="'.$model->kd_order.'" data-toggle="tooltip" data-original-title="Hapus Transaksi" data-placement="top"><i class="fa fa-trash"></i></button>';
                                                    }
                                                }
                                            ]
                                        ],
                                        ['class' => 'yii\grid\SerialColumn'],
                                        // Starter column
                                        // [
                                        //     'header'=> '',
                                        //     'format'=>'',
                                        //     'value'=>function($model,$key,$i){
                                        //         return '';
                                        //     },
                                        //     'filter'=>'<input type="text" class="form-control input-sm" name="" value="" placeholder="" >'
                                        // ],

                                         // Barang
                                       
                                        [
                                            'header'=> 'Barang ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                $arr=[];

                                                foreach ($model->md as $item) {
                                                    $arr[]='<i class="fa fa-circle fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>'.$item->v_material->nama;
                                                }
                                               
                                                return implode(array_unique($arr), '<br>');
                                            },
                                            'filter'=>'<input type="text" class="form-control input-sm" name="VBkSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                        ],

                                         // Varian
                                        [
                                            'header'=> '(QTY) Varian ',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                $val='';
                                                foreach ($model->md as $item) {
                                                    $val.='<i class="fa fa-chevron-right fa-fw" style="color:'.$item->v_material->kode_warna.'"></i>';
                                                    $val.='<b>('.$item->qty.')</b> '.$item->v_material->varianname.' <br>';
                                                }
                                                return $val;
                                            },
                                            // 'filter'=>'<input type="text" class="form-control input-sm" name="VPbSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                        ],
                                        // // QTY
                                        // [
                                        //     'header'=> 'QTY ',
                                        //     'format'=>'raw',
                                        //     'value'=>function($model,$key,$i){
                                        //         $val='';
                                        //         foreach ($model->md as $item) {
                                        //             $val.=$item->qty.'<br>';
                                        //         }
                                        //         return $val;
                                        //     },
                                        //     // 'filter'=>'<input type="text" class="form-control input-sm" name="VBkSearch[barang]" value="'.$searchModel->barang.'" placeholder="Search by items" >'
                                        // ],



                                        // Pengiriman
                                        [
                                            'header'=> 'Pengiriman',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                if($model->is_gived!=1){
                                                    $resi=($model->resi=='')?'belum dikirim':$model->resi;
                                                    $kurir=($model->kurir->layanan=='')?'NO KURIR':$model->kurir->layanan;
                                                    $val='
                                                        <table>
                                                            <tr>
                                                                <td style="border-right:1px solid #e4e7ea"><b>'.$kurir.'</b> &emsp;<br><i class="text-danger">'.$resi.'</i>&emsp;</td>
                                                                <td>&emsp;<b>'.$model->plg->nama.'</b><br>&emsp;'.$model->plg->prov.', '.$model->plg->kota_kab.', '.$model->plg->kec.'</td>
                                                            </tr>
                                                        </table>';
                                                    return $val;
                                                }else{
                                                    return '<span class="label label-info">Given to : <b>'.$model->plg->nama.'</b></span>';
                                                }
                                            },
                                            'filter'=>'<input type="text" class="form-control input-sm" name="VBkSearch[pengiriman]" value="'.$searchModel->pengiriman.'" placeholder="Search by : kurir / resi / pelanggan / alamat">'
                                        ],

                                        // Status
                                        [
                                            'header'=> 'Status',
                                            'attribute' => 'id_status',
                                            'format'=>'raw',
                                            'value'=>function($model,$key,$i){
                                                return '<span class="label label-sm popovers '.$model->status->class.'"  data-trigger="hover" data-placement="left" data-content="'.$model->status->ket_status.'" data-original-title="'.$model->status->nm_status.'">'.$model->status->nm_status.' </span>';
                                            },

                                            'filter'=>[24 => 'Belum dikirim', 3 => 'Dikirim', 4 => 'Selesai'],
                                            'filterInputOptions' => [
                                                'class' => 'form-control input-sm',
                                            ],
                                            // 'filter'=>Html::activeDropDownList($searchModel,"buyer",$cm,['class'=>'form-control select2 input-sm','prompt' => 'By Customers']),
                                        ],
                                        // checkbox
                                        [
                                            'class' => 'yii\grid\CheckboxColumn',
                                            'checkboxOptions' => function($model,$key,$i){
                                                    $inv=$model->kd_order;
                                                    return ['value'=>$inv,'class'=>'check_item'];
                                            },
                                           
                                        ],
                                        // 'kd_tagihan',
                                        // 'tipe_transfer',
                                        // 'tipe_transaksi',
                                        // 'qty',
                                        // 'ongkir_buyer',
                                        // 'total_belanja',
                                        // 'total_tagihan_buyer',
                                        // 'buyer',
                                        // 'buyer_note:ntext',
                                        // 'id_market',
                                        // 'id_kurir',
                                        // 'resi',
                                        // 'id_status',
                                        // 'created_at',
                                        // 'created_by',
                                        
                                       
                                    ],
                                ]); ?>
                            <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
