<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VBk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vbk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_tagihan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipe_transfer')->textInput() ?>

    <?= $form->field($model, 'tipe_transaksi')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ongkir_buyer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_belanja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_tagihan_buyer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buyer')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buyer_note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_market')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_kurir')->textInput() ?>

    <?= $form->field($model, 'resi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
