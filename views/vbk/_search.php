<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VBkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vbk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'kd_tagihan') ?>

    <?= $form->field($model, 'tipe_transfer') ?>

    <?= $form->field($model, 'tipe_transaksi') ?>

    <?= $form->field($model, 'qty') ?>

    <?= $form->field($model, 'ongkir_buyer') ?>

    <?php // echo $form->field($model, 'total_belanja') ?>

    <?php // echo $form->field($model, 'total_tagihan_buyer') ?>

    <?php // echo $form->field($model, 'buyer') ?>

    <?php // echo $form->field($model, 'buyer_note') ?>

    <?php // echo $form->field($model, 'id_market') ?>

    <?php // echo $form->field($model, 'id_kurir') ?>

    <?php // echo $form->field($model, 'resi') ?>

    <?php // echo $form->field($model, 'id_status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
