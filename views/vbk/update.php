<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VBk */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Vbk',
]) . $model->kd_tagihan;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vbks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kd_tagihan, 'url' => ['view', 'id' => $model->kd_tagihan]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="vbk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
