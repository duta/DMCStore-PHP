<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>PRODUCT</th>
                <th>STOCK</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $rank=1;
                foreach ($models as $key => $model) {
                    
            ?>
                    <tr>
                        <td style="width: 1%">#<?=$rank?></td>
                        <td class="txt-oflo"><?=$model['fullname']?></td>
                        <td>
                            <span class="label label-warning label-rouded"><?=$model['stok_akhir']?></span> 
                        </td>
                    </tr>
            <?php
                $rank++;
                }
            ?>
            
        </tbody>
    </table>
</div>