<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VMaterial */

$this->title = Yii::t('app', 'Create Vmaterial');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vmaterials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vmaterial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
