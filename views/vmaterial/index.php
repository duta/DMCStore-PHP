
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MaterialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Barang'); /*TITLE*/
$icon = Yii::t('app', '<i class="mdi mdi-puzzle fa-fw"></i>'); /*icon*/ /*<i class="mdi mdi-basket-unfill fa-fw"></i>*/
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><?=$icon?><?=$this->title?></h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        
        <ol class="breadcrumb">
            <li class="active"><?=$this->title?></li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">
                <?=$icon?>Kelola <?=$this->title?>
                <button class="add_material btn  btn-success btn-icon-anim btn-circle pull-right" data-toggle="tooltip" data-original-title="Tambah <?=$this->title?>" data-placement="left">
                    <span class="ti-plus"></span> 
                </button>

           
            </h3>
             <hr>
            <div class="table-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="table-responsive"> -->
                          <?php Pjax::begin(); ?>    <?= GridView::widget([
                                'tableOptions'=>['class'=>'table table-condensed table-hover'],
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,

                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                     // Nama
                                    [
                                        'header'=>'Barang',
                                        'attribute'=>'material.nama',
                                        'value'=>'nama',
                                        'filter'=>'<input type="text" class="form-control input-sm" name="VMaterialSearch[nama]" value="'.$searchModel->nama.'" placeholder="By Barang" >',
                                        'enableSorting'=>true,
                                    ],

                                    // // HARGA RETAIL
                                    // [
                                    //     'header'=>'Harga',
                                    //     'attribute'=>'material.harga',
                                    //     'value'=>function($model,$key,$index){
                                    //         return Yii::$app->help->toRp($model->hgsat_retail);  
                                    //     },
                                    //     'filter'=>'<input type="number" class="form-control input-sm" name="MaterialSearch[hgsat_retail]" value="'.$searchModel->hgsat_retail.'" placeholder="By Harga" >',
                                    //     'enableSorting'=>true,
                                    // ],

                                    // Merk                                                        
                                        // [
                                        //     'header'=>'Merk',
                                        //     'attribute'=>'material.merk',

                                        //     'value'=>function($model,$key,$index){
                                        //         return $model->merk;  
                                        //     },
                                        //     'filter'=>'<input type="text" class="form-control input-sm" name="VMaterialSearch[merk]" value="'.$searchModel->merk.'" placeholder="By Merk" >',
                                        //     'enableSorting'=>true,
                                        // ],
                                    // 'id',
                                    // 'kode',
                                    // 'nama:ntext',
                                    // 'merk',
                                    // 'foto',
                                    // 'model',
                                    // 'hgsat_retail',
                                    // 'hgsat_reseller',
                                    // 'berat',
                                    // 'satuan_berat',
                                    // 'satuan',
                                    // 'diskon',
                                    // 'buyer_id',
                                    // 'id_ktg',
                                    // 'id_lokasi',
                                    // 'id_grup',
                                    // 'id_tipe',
                                    // 'stok_awal',
                                    // 'created_by',
                                    // 'created_at',
                                    // 'updated_at',
                                    // 'transaksi',
                                    // 'hg',
                                    // 'kondisi',
                                    // 'buyer_note:ntext',
                                    // 'note:ntext',
                                    // 'keterangan:ntext',
                                    // 'warna',
                                    // 'size',
                                    // 'qty_in',
                                    // 'qty_out',
                                    // 'stok_akhir',

                                    [   'header'=>'<center><i class="fa fa-cogs"></i></center>',
                                        'class' => 'yii\grid\ActionColumn',
                                        'template'=>'<center>{update} {delete}</center>',
                                        'buttons'=>[
                                            'update'=>function($url,$model){
                                                return '<button type="button" class="btn btn-primary btn-circle btn-sm edit_item" id="'.$model->kode.'" data-toggle="tooltip" data-original-title="Update Barang" data-placement="top"><i class="fa fa-pencil"></i></button>';
                                            },
                                            'delete'=>function($url,$model){
                                                return '<button type="button" class="btn btn-danger btn-circle btn-sm del_item" id="'.$model->kode.'" data-toggle="tooltip" data-original-title="Hapus Barang" data-placement="top"><i class="fa fa-trash"></i></button>';
                                            }
                                        ],
                                    ],
                                ],
                            ]); ?>
                        <?php Pjax::end(); ?>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>