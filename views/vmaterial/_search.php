<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VMaterialSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vmaterial-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kode') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'merk') ?>

    <?= $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'hgsat_retail') ?>

    <?php // echo $form->field($model, 'hgsat_reseller') ?>

    <?php // echo $form->field($model, 'berat') ?>

    <?php // echo $form->field($model, 'satuan_berat') ?>

    <?php // echo $form->field($model, 'satuan') ?>

    <?php // echo $form->field($model, 'diskon') ?>

    <?php // echo $form->field($model, 'buyer_id') ?>

    <?php // echo $form->field($model, 'id_ktg') ?>

    <?php // echo $form->field($model, 'id_lokasi') ?>

    <?php // echo $form->field($model, 'id_grup') ?>

    <?php // echo $form->field($model, 'id_tipe') ?>

    <?php // echo $form->field($model, 'stok_awal') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'transaksi') ?>

    <?php // echo $form->field($model, 'hg') ?>

    <?php // echo $form->field($model, 'kondisi') ?>

    <?php // echo $form->field($model, 'buyer_note') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'warna') ?>

    <?php // echo $form->field($model, 'size') ?>

    <?php // echo $form->field($model, 'qty_in') ?>

    <?php // echo $form->field($model, 'qty_out') ?>

    <?php // echo $form->field($model, 'stok_akhir') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
