<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VMaterial */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vmaterials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vmaterial-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kode',
            'nama:ntext',
            'merk',
            'foto',
            'model',
            'hgsat_retail',
            'hgsat_reseller',
            'berat',
            'satuan_berat',
            'satuan',
            'diskon',
            'buyer_id',
            'id_ktg',
            'id_lokasi',
            'id_grup',
            'id_tipe',
            'stok_awal',
            'created_by',
            'created_at',
            'updated_at',
            'transaksi',
            'hg',
            'kondisi',
            'buyer_note:ntext',
            'note:ntext',
            'keterangan:ntext',
            'warna',
            'size',
            'qty_in',
            'qty_out',
            'stok_akhir',
        ],
    ]) ?>

</div>
