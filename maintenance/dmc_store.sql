/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : dmc_store

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-02-01 16:34:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_status
-- ----------------------------
DROP TABLE IF EXISTS `app_status`;
CREATE TABLE `app_status` (
  `id` tinyint(3) unsigned NOT NULL,
  `nm_status` char(50) NOT NULL,
  `ket_status` text NOT NULL,
  `class` char(50) NOT NULL,
  `ktg_status` char(50) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_status
-- ----------------------------
INSERT INTO `app_status` VALUES ('0', 'Menunggu', 'Barang telah dipesan dan menunggu pembayaran oleh pembeli', 'label-warning', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('1', 'Diproses', 'Pembayaran telah di terima , pesanan sedang disiapkan untuk dikirim ', 'label-primary', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('2', 'Dibatalkan', 'Transaksi dibatalkan', 'label-danger', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('3', 'Dikirim', 'Barang telah diproses oleh kurir, tunggu kedatangan barang & silahkan melakukan konfirmasi terima barang ', 'label-warning', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('4', 'Selesai', 'Barang telah diterima & transaksi selesai', 'label-success', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('9', 'Internal Orders', 'Transaksi RO ke user', 'label-warning', 'tipe_transfer', '0');
INSERT INTO `app_status` VALUES ('10', 'Customer Orders', 'Transaksi dengan customer', 'label-success', 'tipe_transfer', '0');
INSERT INTO `app_status` VALUES ('14', 'in', 'Material Masuk', 'label-primary', 'tipe_transaksi', '0');
INSERT INTO `app_status` VALUES ('15', 'out', 'Material Keluar', 'label-warning', 'tipe_transaksi', '0');
INSERT INTO `app_status` VALUES ('16', 'Proses retur', 'retur material sedang diproses', 'label-primary', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('17', 'Dikirim (Retur)', 'retur material  dalam proses pengiriman oleh kurir', 'label-info', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('18', 'Selesai (Retur)', 'material berhasil di retur', 'label-success', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('24', 'Proses', 'Barang sudah dibayar, pesanan akan segera dikirim', 'label-info', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('25', 'Refunded', 'Uang pembeli dikembalikan', 'label-primary', 'status_transaksi', '0');
INSERT INTO `app_status` VALUES ('30', 'Ganti baru', 'pembeli ingin ganti barang baru', 'label-info', 'tipe_retur', '0');
INSERT INTO `app_status` VALUES ('31', 'Pengembalian Dana', 'pembeli ingin pengembalian dana', 'label-primary', 'tiper_retur', '0');

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `source` varchar(255) NOT NULL,
  `source_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk-auth-user_id-user-id` (`user_id`),
  CONSTRAINT `fk-auth-user_id-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth
-- ----------------------------
INSERT INTO `auth` VALUES ('1', '17', 'google', '115461662729408218804');

-- ----------------------------
-- Table structure for br_ktg
-- ----------------------------
DROP TABLE IF EXISTS `br_ktg`;
CREATE TABLE `br_ktg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nm_ktg` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of br_ktg
-- ----------------------------
INSERT INTO `br_ktg` VALUES ('1', 'Elektronik');
INSERT INTO `br_ktg` VALUES ('2', 'Fashion Pria');
INSERT INTO `br_ktg` VALUES ('3', 'Fashion Wanita');
INSERT INTO `br_ktg` VALUES ('4', 'Peralatan Rumah Tangga');
INSERT INTO `br_ktg` VALUES ('5', 'Kesehatan & Kecantikan');
INSERT INTO `br_ktg` VALUES ('6', 'Bayi & Mainan Anak');
INSERT INTO `br_ktg` VALUES ('7', 'Olahraga & Travel');
INSERT INTO `br_ktg` VALUES ('8', 'Groceries, Media & Pets');
INSERT INTO `br_ktg` VALUES ('9', 'Mobil & Motor');

-- ----------------------------
-- Table structure for br_status
-- ----------------------------
DROP TABLE IF EXISTS `br_status`;
CREATE TABLE `br_status` (
  `id` tinyint(3) unsigned NOT NULL,
  `nm_status` char(50) NOT NULL,
  `ket_status` text NOT NULL,
  `class` char(50) NOT NULL,
  `ktg_status` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of br_status
-- ----------------------------
INSERT INTO `br_status` VALUES ('1', 'Published', 'brg di jual', 'label-success', 'status_jual');
INSERT INTO `br_status` VALUES ('2', 'Draft', 'brg di simpan dulu sebelum dijual', 'label-warning', 'status_jual');
INSERT INTO `br_status` VALUES ('3', 'Sold', 'brg terjual', '', 'status_jual');

-- ----------------------------
-- Table structure for br_subktg
-- ----------------------------
DROP TABLE IF EXISTS `br_subktg`;
CREATE TABLE `br_subktg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_ktg` int(11) unsigned NOT NULL,
  `sub_ktg` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of br_subktg
-- ----------------------------
INSERT INTO `br_subktg` VALUES ('1', '1', 'celana');
INSERT INTO `br_subktg` VALUES ('2', '1', 'jaket');
INSERT INTO `br_subktg` VALUES ('3', '2', 'jilbab');
INSERT INTO `br_subktg` VALUES ('4', '2', 'tas wanita');

-- ----------------------------
-- Table structure for br_subsubktg
-- ----------------------------
DROP TABLE IF EXISTS `br_subsubktg`;
CREATE TABLE `br_subsubktg` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_subktg` int(11) unsigned NOT NULL,
  `subsub_ktg` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of br_subsubktg
-- ----------------------------
INSERT INTO `br_subsubktg` VALUES ('1', '1', 'celana pendek');
INSERT INTO `br_subsubktg` VALUES ('2', '1', 'celana panjang');
INSERT INTO `br_subsubktg` VALUES ('3', '3', 'segi empat');
INSERT INTO `br_subsubktg` VALUES ('4', '3', 'ciput');

-- ----------------------------
-- Table structure for br_supplier
-- ----------------------------
DROP TABLE IF EXISTS `br_supplier`;
CREATE TABLE `br_supplier` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kd_sup` varchar(150) NOT NULL,
  `nm_sup` varchar(150) NOT NULL,
  `tlp` varchar(150) NOT NULL,
  `qq` varchar(150) DEFAULT NULL,
  `wechat` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of br_supplier
-- ----------------------------
INSERT INTO `br_supplier` VALUES ('1', 'LP(', 'LED POPLOR (YIWU)', 'QQ : 179813631', null, '', '2018-01-04 08:55:25');
INSERT INTO `br_supplier` VALUES ('3', 'LCG', 'Li Cheng (Guangzhou Market)', 'Wechat : licheng4799', null, '', '2018-01-04 08:55:25');
INSERT INTO `br_supplier` VALUES ('4', 'H1', 'Hong 1835 (Yiwu)', 'wechat: junshuaipiju', null, '', '2018-01-24 06:51:39');
INSERT INTO `br_supplier` VALUES ('5', 'YESD', 'YIWU Extension Step Digital', '', '', 'wxid_o9rz4f', '2018-01-24 06:54:22');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` char(150) DEFAULT NULL,
  `email` char(100) DEFAULT NULL,
  `tlp` char(12) DEFAULT NULL,
  `prov` char(150) DEFAULT NULL,
  `kota_kab` char(150) DEFAULT NULL,
  `kec` char(150) DEFAULT NULL,
  `pos` int(5) DEFAULT NULL,
  `alamat` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES ('97', 'Rizal Widiantoro', 'rijalboy98@gmail.com', '081249780453', 'Jawa Timur', 'Surabaya', 'Sawahan', '2222', 'Jl. Petemon 1 59/b RT 04 , RW 08 ( atau Cari Rumah Pak.Misnur = ketua RT 04),(atau Cari Rumah Aji ) \r\nKec. Sawahan, Kota/Kab. Surabaya \r\nJawa Timur, 60252 ', '2017-11-13 13:54:52');
INSERT INTO `customers` VALUES ('98', 'Pelanggan A', 'plg@yahoo.co.id', '081249780453', 'Jawa Timur', 'Surabaya', 'Karangpilang', '0', 'Perumahan Maharaja Village BLOK BM 20, Griya kebraon tengah, Kebraon 5, Dekat masjid Madina Luhur. \r\nKec. Karangpilang, Kota/Kab. Surabaya \r\nJawa Timur, 60222 \r\n', '2017-11-16 12:02:36');
INSERT INTO `customers` VALUES ('99', 'Plg B', 'plgb@gmail.com', '081999222999', 'Jioij', 'Jiijjio', 'Jiojioijo', null, 'address PLG B', '2018-01-23 15:05:59');
INSERT INTO `customers` VALUES ('100', 'Jatmiko Pdam', null, '082777222888', null, null, null, null, null, '2018-01-24 11:20:13');

-- ----------------------------
-- Table structure for forwarder
-- ----------------------------
DROP TABLE IF EXISTS `forwarder`;
CREATE TABLE `forwarder` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kd_fw` varchar(150) NOT NULL,
  `nm_fw` varchar(150) NOT NULL,
  `tlp` varchar(150) NOT NULL,
  `qq` varchar(150) DEFAULT NULL,
  `wechat` varchar(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of forwarder
-- ----------------------------
INSERT INTO `forwarder` VALUES ('1', 'F1', 'fw 1', '052262', 'qweqwe', '223', '2018-01-30 11:24:54');
INSERT INTO `forwarder` VALUES ('2', 'F2', 'fw 2', '066567', 'eqhwiuehqiu', 'uiiuewqr', '2018-01-30 11:25:07');

-- ----------------------------
-- Table structure for grosir
-- ----------------------------
DROP TABLE IF EXISTS `grosir`;
CREATE TABLE `grosir` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_md` int(11) unsigned DEFAULT NULL,
  `qty` smallint(5) unsigned DEFAULT NULL,
  `harga` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of grosir
-- ----------------------------
INSERT INTO `grosir` VALUES ('19', '1', '50', '9000', '2018-01-23 11:48:14');
INSERT INTO `grosir` VALUES ('20', '1', '60', '8000', '2018-01-23 11:48:14');
INSERT INTO `grosir` VALUES ('21', '2', '10', '10000', '2018-01-23 11:49:48');
INSERT INTO `grosir` VALUES ('22', '1', '50', '9500', '2018-01-23 11:52:53');
INSERT INTO `grosir` VALUES ('24', '2', '10', '80000', '2018-01-23 11:53:20');
INSERT INTO `grosir` VALUES ('25', '57', '12', '33', '2018-01-26 13:57:03');
INSERT INTO `grosir` VALUES ('26', '57', '3', '55', '2018-01-26 13:57:03');
INSERT INTO `grosir` VALUES ('27', '55', '20', '15000', '2018-01-30 13:09:42');
INSERT INTO `grosir` VALUES ('28', '55', '30', '13000', '2018-01-30 13:09:42');
INSERT INTO `grosir` VALUES ('29', '55', '40', '11000', '2018-01-30 14:34:56');
INSERT INTO `grosir` VALUES ('30', '55', '50', '10000', '2018-01-30 14:34:56');

-- ----------------------------
-- Table structure for item_attr
-- ----------------------------
DROP TABLE IF EXISTS `item_attr`;
CREATE TABLE `item_attr` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nm_attr` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item_attr
-- ----------------------------
INSERT INTO `item_attr` VALUES ('1', 'warna', '2018-01-10 14:09:13');
INSERT INTO `item_attr` VALUES ('2', 'bahan', '2018-01-10 14:09:19');
INSERT INTO `item_attr` VALUES ('3', 'size', '2018-01-10 14:09:26');
INSERT INTO `item_attr` VALUES ('5', 'daya', '2018-01-17 14:37:31');
INSERT INTO `item_attr` VALUES ('6', 'Merk', '2018-01-26 14:03:31');

-- ----------------------------
-- Table structure for item_varian
-- ----------------------------
DROP TABLE IF EXISTS `item_varian`;
CREATE TABLE `item_varian` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_attr` int(11) unsigned DEFAULT NULL,
  `nm_varian` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item_varian
-- ----------------------------
INSERT INTO `item_varian` VALUES ('2', '2', 'katun', '2018-01-10 14:09:19');
INSERT INTO `item_varian` VALUES ('3', '3', 'XL', '2018-01-10 14:09:26');
INSERT INTO `item_varian` VALUES ('4', '3', 'L', '2018-01-10 14:18:32');
INSERT INTO `item_varian` VALUES ('5', '3', 'S', '2018-01-10 14:18:38');
INSERT INTO `item_varian` VALUES ('6', '3', 'M', '2018-01-10 14:18:46');
INSERT INTO `item_varian` VALUES ('7', '1', 'Hitam', '2018-01-10 14:21:47');
INSERT INTO `item_varian` VALUES ('8', '1', 'Merah', '2018-01-10 14:21:47');
INSERT INTO `item_varian` VALUES ('9', '1', 'Biru', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('10', '1', 'Putih', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('11', '1', 'Hijau', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('12', '1', 'Maroon', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('13', '1', 'Pink', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('14', '1', 'Navy', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('15', '1', 'Tosca', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('16', '1', 'Ungu', '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES ('22', '5', '5 watt', '2018-01-17 14:38:05');
INSERT INTO `item_varian` VALUES ('23', '5', '9 watt', '2018-01-17 14:38:13');
INSERT INTO `item_varian` VALUES ('24', '5', '12 watt', '2018-01-17 14:38:19');
INSERT INTO `item_varian` VALUES ('27', '2', 'kulit buaya', '2018-01-29 13:07:10');
INSERT INTO `item_varian` VALUES ('28', '2', 'kulit domba', '2018-01-29 13:07:10');
INSERT INTO `item_varian` VALUES ('29', '6', 'Sony', '2018-01-29 13:11:34');
INSERT INTO `item_varian` VALUES ('30', '6', 'Xiaomi', '2018-01-29 13:11:34');
INSERT INTO `item_varian` VALUES ('31', '6', 'sharp', '2018-01-29 13:11:34');

-- ----------------------------
-- Table structure for item_varmap
-- ----------------------------
DROP TABLE IF EXISTS `item_varmap`;
CREATE TABLE `item_varmap` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kd_material` char(50) DEFAULT NULL,
  `id_attr` int(11) DEFAULT NULL,
  `id_varian` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item_varmap
-- ----------------------------
INSERT INTO `item_varmap` VALUES ('1', '25', '1', '15', '2018-01-10 14:14:22');
INSERT INTO `item_varmap` VALUES ('2', '25', '1', '16', '2018-01-10 14:25:43');
INSERT INTO `item_varmap` VALUES ('3', '25', '3', '6', '2018-01-10 14:26:28');
INSERT INTO `item_varmap` VALUES ('4', '25', '3', '5', '2018-01-10 15:00:00');

-- ----------------------------
-- Table structure for kurir
-- ----------------------------
DROP TABLE IF EXISTS `kurir`;
CREATE TABLE `kurir` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nm_kurir` char(150) NOT NULL,
  `layanan` char(150) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kurir
-- ----------------------------
INSERT INTO `kurir` VALUES ('1', 'JNE Express', 'JNE Reguler', '2017-10-03 09:27:17', '2017-11-09 09:55:04');
INSERT INTO `kurir` VALUES ('2', 'JNE Express', 'JNE Yes', '2017-11-09 09:55:35', '2017-11-09 09:55:46');
INSERT INTO `kurir` VALUES ('3', 'TIKI', 'TIKI Reg', '2017-10-03 09:27:17', '2017-11-09 10:12:22');
INSERT INTO `kurir` VALUES ('4', 'TIKI', 'TIKI ONS', '2017-11-09 10:12:47', '2017-11-09 10:12:52');
INSERT INTO `kurir` VALUES ('6', 'POS INDONESIA', 'Pos Kilat Khusus', '2017-11-10 15:51:26', '2017-11-10 15:51:41');
INSERT INTO `kurir` VALUES ('7', 'POS INDONESIA', 'Pos Express Next Day Service', '2017-11-10 15:51:27', '2017-11-10 15:51:42');

-- ----------------------------
-- Table structure for marketplace
-- ----------------------------
DROP TABLE IF EXISTS `marketplace`;
CREATE TABLE `marketplace` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nm_market` char(50) NOT NULL,
  `nm_singkat` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of marketplace
-- ----------------------------
INSERT INTO `marketplace` VALUES ('1', 'Bukalapak', 'BL');
INSERT INTO `marketplace` VALUES ('2', 'Tokopedia', 'TOPED');
INSERT INTO `marketplace` VALUES ('3', 'Lazada', 'LZD');
INSERT INTO `marketplace` VALUES ('4', 'Shopee', 'SHP');
INSERT INTO `marketplace` VALUES ('5', 'Blibli', 'BB');
INSERT INTO `marketplace` VALUES ('6', 'Hatobe', 'HTB');

-- ----------------------------
-- Table structure for master_material
-- ----------------------------
DROP TABLE IF EXISTS `master_material`;
CREATE TABLE `master_material` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama` char(255) NOT NULL,
  `id_ktg` int(11) NOT NULL,
  `merk` char(50) NOT NULL,
  `berat` decimal(5,0) unsigned NOT NULL,
  `satuan_berat` char(5) NOT NULL,
  `keterangan` text NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_material
-- ----------------------------
INSERT INTO `master_material` VALUES ('4', 'Lampu Smart Charge', '1', '', '150', 'g', 'Lampu LED Emergency', '10', '2018-01-24 06:39:06');
INSERT INTO `master_material` VALUES ('5', 'Tas Tumi Saratoga', '2', '', '800', 'g', 'Sling bag Pria Tumi Saratoga', '10', '2018-01-24 07:15:04');
INSERT INTO `master_material` VALUES ('6', 'Tas Tumi Luanda', '3', '', '220', 'g', 'Tas Wanita Tumi Luanda Flightbag', '10', '2018-01-24 07:16:42');

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode` char(50) NOT NULL,
  `foto` char(50) NOT NULL,
  `id_attr` int(11) NOT NULL,
  `id_varian` int(11) NOT NULL,
  `stok_awal` smallint(5) NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of material
-- ----------------------------
INSERT INTO `material` VALUES ('42', '4', 'e73ca62d9d6c73eb688bd6648aa18a20.png', '5', '22', '0', '10', '2018-01-24 06:39:06');
INSERT INTO `material` VALUES ('43', '4', 'e73ca62d9d6c73eb688bd6648aa18a20.png', '5', '23', '0', '10', '2018-01-24 06:39:06');
INSERT INTO `material` VALUES ('44', '4', 'e73ca62d9d6c73eb688bd6648aa18a20.png', '5', '24', '0', '10', '2018-01-24 06:39:06');
INSERT INTO `material` VALUES ('45', '5', '4e8184139b688438ef195ad8d7d3da65.png', '1', '7', '0', '10', '2018-01-24 07:15:04');
INSERT INTO `material` VALUES ('46', '5', '43d344f7023c4f5faba69ab06e9e5175.png', '1', '9', '0', '10', '2018-01-24 07:15:04');
INSERT INTO `material` VALUES ('47', '6', 'aaccbeebed454bbd4a8007015d52651d.jpg', '1', '7', '0', '10', '2018-01-24 07:16:42');
INSERT INTO `material` VALUES ('48', '6', '2982573c97bd5274dfefedf89828f85e.jpg', '1', '8', '0', '10', '2018-01-24 07:16:42');

-- ----------------------------
-- Table structure for material_detail
-- ----------------------------
DROP TABLE IF EXISTS `material_detail`;
CREATE TABLE `material_detail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kd_tagihan` varchar(150) DEFAULT NULL,
  `kd_order` varchar(150) DEFAULT NULL,
  `buyer` int(11) unsigned DEFAULT NULL,
  `id_material` int(11) unsigned DEFAULT NULL,
  `id_sup` int(11) unsigned DEFAULT NULL,
  `id_fw` int(11) DEFAULT NULL,
  `id_market` int(11) unsigned DEFAULT NULL,
  `hgsat_dasar` int(11) DEFAULT NULL,
  `hgtot_dasar` int(11) unsigned DEFAULT NULL,
  `hgtot_beli` int(11) DEFAULT NULL,
  `hgsat_jual` int(11) unsigned DEFAULT NULL,
  `harga_mod` int(11) unsigned DEFAULT NULL,
  `keterangan` text,
  `qty` int(11) unsigned DEFAULT NULL,
  `qty_order_bm` int(11) DEFAULT NULL,
  `read` tinyint(3) unsigned DEFAULT NULL,
  `id_kurir` int(11) DEFAULT NULL,
  `resi` varchar(150) DEFAULT NULL,
  `total_pay` int(11) DEFAULT NULL,
  `paid_photo` char(50) DEFAULT NULL,
  `paid_note` text,
  `tipe_transfer` tinyint(3) DEFAULT NULL,
  `status` tinyint(3) unsigned DEFAULT NULL,
  `tipe_transaksi` tinyint(3) DEFAULT NULL,
  `buyer_note` text,
  `return_at` datetime DEFAULT NULL,
  `done_at` datetime DEFAULT NULL,
  `sent_at` datetime DEFAULT '0000-00-00 00:00:00',
  `sent_photo` char(50) DEFAULT NULL,
  `done_photo` char(50) DEFAULT NULL,
  `approved_by` int(11) unsigned DEFAULT NULL,
  `rejected_by` int(11) unsigned DEFAULT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `approved_at` datetime DEFAULT '0000-00-00 00:00:00',
  `rejected_at` datetime DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `accept_at` date DEFAULT NULL,
  `order_at` timestamp NULL DEFAULT NULL,
  `ppn` tinyint(3) DEFAULT NULL,
  `pph` tinyint(3) DEFAULT NULL,
  `bea_masuk` tinyint(3) DEFAULT NULL,
  `dll` int(11) DEFAULT NULL,
  `ongkir_impor` int(11) unsigned DEFAULT NULL,
  `ongkir_buyer` int(11) unsigned DEFAULT NULL,
  `hgsat_retail` int(11) DEFAULT NULL,
  `hgsat_reseller` int(11) DEFAULT NULL,
  `is_grosir` tinyint(1) unsigned DEFAULT '0',
  `is_gived` tinyint(1) DEFAULT '0',
  `cancel_note` text,
  `retur_note` text,
  `tipe_retur` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of material_detail
-- ----------------------------
INSERT INTO `material_detail` VALUES ('62', '4944504451', null, null, '42', '1', null, null, '6090', null, '7117000', null, '12554', null, '50', '100', null, null, null, null, null, null, '9', '2', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '10', '2018-01-30 15:24:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 11:56:18', '2018-01-29', '2018-01-23 23:44:03', '0', '0', '0', null, '3232000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('63', '4944504451', null, null, '43', '1', null, null, '7560', null, '7117000', null, '14024', null, '60', '200', null, null, null, null, null, null, '9', '2', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '10', '2018-01-30 15:24:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 11:56:18', '2018-01-29', '2018-01-23 23:44:03', '0', '0', '0', null, '3232000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('64', '4944504451', null, null, '44', '1', null, null, '8820', null, '7117000', null, '15284', null, '70', '200', null, null, null, null, null, null, '9', '2', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '10', '2018-01-30 15:24:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 11:56:18', '2018-01-29', '2018-01-23 23:44:03', '0', '0', '0', null, '3232000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('65', '4944504451', null, null, '42', '1', null, null, '6090', null, '7117000', null, '12554', null, '50', '100', null, null, null, null, null, null, '9', '2', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '10', '2018-01-30 15:24:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 11:56:18', '2018-01-30', '2018-01-23 23:44:03', '0', '0', '0', null, '3232000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('66', '4944504451', null, null, '43', '1', null, null, '7560', null, '7117000', null, '14024', null, '140', '200', null, null, null, null, null, null, '9', '2', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '10', '2018-01-30 15:24:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 11:56:18', '2018-01-30', '2018-01-23 23:44:03', '0', '0', '0', null, '3232000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('67', '4944504451', null, null, '44', '1', null, null, '8820', null, '7117000', null, '15284', null, '130', '200', null, null, null, null, null, null, '9', '2', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '10', '2018-01-30 15:24:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 11:56:18', '2018-01-30', '2018-01-23 23:44:03', '0', '0', '0', null, '3232000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('74', '7441754176', null, null, '42', '3', '2', null, '30000', null, '24739000', null, '42754', null, '90', '100', null, null, null, null, null, null, '9', '3', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-01-31 13:41:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 13:42:09', null, '2018-01-31 07:41:56', '10', '10', '10', '20000', '19000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('75', '7441754176', null, null, '43', '3', '2', null, '50000', null, '24739000', null, '62754', null, '190', '200', null, null, null, null, null, null, '9', '3', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-01-31 13:41:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 13:42:09', null, '2018-01-31 07:41:56', '10', '10', '10', '20000', '19000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('76', '7441754176', null, null, '44', '3', '2', null, '40000', null, '24739000', null, '52754', null, '140', '150', null, null, null, null, null, null, '9', '3', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-01-31 13:41:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 13:42:09', null, '2018-01-31 07:41:56', '10', '10', '10', '20000', '19000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('77', '7441754176', null, null, '42', '3', null, null, '30000', null, '24739000', null, '42754', null, '10', '100', null, null, null, null, null, null, '9', '4', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-01-31 13:42:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 13:42:09', '2018-01-31', '2018-01-31 07:41:56', '10', '10', '10', '20000', '19000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('78', '7441754176', null, null, '43', '3', null, null, '50000', null, '24739000', null, '62754', null, '10', '200', null, null, null, null, null, null, '9', '4', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-01-31 13:42:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 13:42:09', '2018-01-31', '2018-01-31 07:41:56', '10', '10', '10', '20000', '19000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('79', '7441754176', null, null, '44', '3', null, null, '40000', null, '24739000', null, '52754', null, '10', '150', null, null, null, null, null, null, '9', '4', '14', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-01-31 13:42:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 13:42:09', '2018-01-31', '2018-01-31 07:41:56', '10', '10', '10', '20000', '19000', null, null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('80', '7441754176', '8042814282', '97', '42', null, null, '1', null, null, null, '30000', null, null, '5', null, '0', '1', null, null, '04bebea4f3a0f79cdc9008cab87d8d71_paid.png', 'werewew', null, '25', '15', 'erer', null, null, '2018-01-31 07:50:14', null, null, null, null, '17', '2018-01-31 13:42:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 16:43:28', null, null, null, null, null, null, null, '19000', null, null, '0', '0', null, 'wqwqwq', '31');
INSERT INTO `material_detail` VALUES ('81', '7441754176', '8042814282', '97', '43', null, null, '1', null, null, null, '50000', null, null, '2', null, '0', '1', null, null, '04bebea4f3a0f79cdc9008cab87d8d71_paid.png', 'werewew', null, '25', '15', 'erer', null, null, '2018-01-31 07:50:14', null, null, null, null, '17', '2018-01-31 13:42:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 16:43:28', null, null, null, null, null, null, null, '19000', null, null, '0', '0', null, 'wqwqwq', '31');
INSERT INTO `material_detail` VALUES ('82', '7441754176', '8042814282', '97', '44', null, null, '1', null, null, null, '60000', null, null, '3', null, '0', '1', null, null, '04bebea4f3a0f79cdc9008cab87d8d71_paid.png', 'werewew', null, '25', '15', 'erer', null, null, '2018-01-31 07:50:14', null, null, null, null, '17', '2018-01-31 13:42:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-01-31 16:43:28', null, null, null, null, null, null, null, '19000', null, null, '0', '0', null, 'wqwqwq', '31');
INSERT INTO `material_detail` VALUES ('83', '7441754176', '830284', '97', '42', null, null, '2', null, null, null, '10000', null, null, '4', null, '0', '1', '4556722', null, '8765ed2f477029297026ff2944416b8d_paid.png', 'ddsadsa', null, '3', '15', 'sadads', null, null, '2018-02-01 05:27:32', null, null, null, null, '17', '2018-02-01 10:02:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-01 11:27:32', null, null, null, null, null, null, null, '30000', null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('84', '7441754176', '830284', '97', '43', null, null, '2', null, null, null, '20000', null, null, '3', null, '0', '1', '4556722', null, '8765ed2f477029297026ff2944416b8d_paid.png', 'ddsadsa', null, '3', '15', 'sadads', null, null, '2018-02-01 05:27:32', null, null, null, null, '17', '2018-02-01 10:02:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-01 11:27:32', null, null, null, null, null, null, null, '30000', null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('85', '7441754176', '850386', '98', '42', null, null, '5', null, null, null, '20000', null, null, '2', null, '0', '4', '97897998', null, 'cf1ae3c10fa8d6b37a001ad5d7435bef_paid.png', 's', null, '3', '15', 'fdsfd', null, null, '2018-02-01 05:27:32', null, null, null, null, '17', '2018-02-01 10:03:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-01 11:27:32', null, null, null, null, null, null, null, '33333', null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('86', '7441754176', '850386', '98', '43', null, null, '5', null, null, null, '3000', null, null, '2', null, '0', '4', '97897998', null, 'cf1ae3c10fa8d6b37a001ad5d7435bef_paid.png', 's', null, '3', '15', 'fdsfd', null, null, '2018-02-01 05:27:32', null, null, null, null, '17', '2018-02-01 10:03:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-01 11:27:32', null, null, null, null, null, null, null, '33333', null, null, '0', '0', null, null, null);
INSERT INTO `material_detail` VALUES ('87', '7441754176', '87', '100', '43', null, null, null, null, null, null, '0', null, null, '2', null, null, null, null, null, null, null, null, '4', '15', null, null, null, '0000-00-00 00:00:00', null, null, null, null, '17', '2018-02-01 12:00:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-02-01 12:00:34', null, null, null, null, null, null, null, '0', null, null, '0', '1', null, null, null);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `link` varchar(50) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(50) DEFAULT NULL,
  `no_urut` int(11) NOT NULL,
  `is_heading` tinyint(1) NOT NULL DEFAULT '0',
  `class_add` char(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('8', 'Dashboard', 'home/index', '0', 'mdi-av-timer', '1', '0', null);
INSERT INTO `menu` VALUES ('9', 'Barang', 'mastermaterial/index', '0', 'mdi-puzzle', '2', '0', null);
INSERT INTO `menu` VALUES ('10', 'Barang Masuk', 'vbm/index', '0', 'mdi-playlist-check', '4', '0', null);
INSERT INTO `menu` VALUES ('11', 'Penjualan', 'vorders/index', '0', 'mdi-cart-plus', '5', '0', null);
INSERT INTO `menu` VALUES ('12', 'Barang Keluar', 'vbk/index', '0', 'mdi-basket-unfill', '6', '0', null);
INSERT INTO `menu` VALUES ('13', 'Laporan', null, '0', 'mdi-file-multiple', '7', '1', '');
INSERT INTO `menu` VALUES ('14', 'Stok Barang', 'vmaterialstok/index', '13', 'mdi-file', '1', '0', null);
INSERT INTO `menu` VALUES ('15', 'Pengaturan', null, '0', 'mdi-wrench', '8', '1', null);
INSERT INTO `menu` VALUES ('16', 'User', 'user/index', '15', 'mdi-account-settings-variant', '1', '0', '');
INSERT INTO `menu` VALUES ('17', 'Pembelian', 'vpb/index', '0', 'mdi-basket-fill', '3', '0', null);
INSERT INTO `menu` VALUES ('18', 'Kategori barang', 'brktg/index', '15', 'mdi-arrange-send-backward', '5', '0', null);
INSERT INTO `menu` VALUES ('19', 'Supplier', 'brsupplier/index', '15', 'mdi-account-network', '2', '0', '');
INSERT INTO `menu` VALUES ('20', 'Pelanggan', 'customers/index', '15', 'mdi-account-multiple', '4', '0', null);
INSERT INTO `menu` VALUES ('21', 'Atribut & varian', 'itemattr/index', '15', 'mdi-arrange-bring-to-front', '6', '0', null);
INSERT INTO `menu` VALUES ('22', 'Kurir', 'kurir/index', '15', 'mdi-truck-delivery', '7', '0', null);
INSERT INTO `menu` VALUES ('23', 'Marketplace', 'marketplace/index', '15', 'mdi-store', '8', '0', null);
INSERT INTO `menu` VALUES ('24', 'Barang masuk', 'vtotalqty/index', '13', 'mdi-file', '2', '0', null);
INSERT INTO `menu` VALUES ('25', 'Forwarder', 'forwarder/index', '15', 'mdi-account-convert', '3', '0', null);

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `menu_id` smallint(5) NOT NULL,
  `role_id` smallint(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', '8', '1');
INSERT INTO `permissions` VALUES ('12', '9', '1');
INSERT INTO `permissions` VALUES ('15', '10', '1');
INSERT INTO `permissions` VALUES ('16', '11', '1');
INSERT INTO `permissions` VALUES ('17', '12', '1');
INSERT INTO `permissions` VALUES ('18', '13', '1');
INSERT INTO `permissions` VALUES ('19', '14', '1');
INSERT INTO `permissions` VALUES ('22', '8', '2');
INSERT INTO `permissions` VALUES ('23', '11', '2');
INSERT INTO `permissions` VALUES ('24', '8', '3');
INSERT INTO `permissions` VALUES ('25', '10', '3');
INSERT INTO `permissions` VALUES ('26', '12', '3');
INSERT INTO `permissions` VALUES ('27', '15', '1');
INSERT INTO `permissions` VALUES ('29', '17', '1');
INSERT INTO `permissions` VALUES ('30', '12', '2');
INSERT INTO `permissions` VALUES ('31', '13', '2');
INSERT INTO `permissions` VALUES ('32', '14', '2');
INSERT INTO `permissions` VALUES ('33', '24', '1');
INSERT INTO `permissions` VALUES ('34', '23', '1');
INSERT INTO `permissions` VALUES ('35', '22', '1');
INSERT INTO `permissions` VALUES ('36', '21', '1');
INSERT INTO `permissions` VALUES ('37', '20', '1');
INSERT INTO `permissions` VALUES ('38', '19', '1');
INSERT INTO `permissions` VALUES ('39', '18', '1');
INSERT INTO `permissions` VALUES ('40', '16', '1');
INSERT INTO `permissions` VALUES ('41', '25', '1');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` char(50) NOT NULL,
  `shortname` char(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'ADMINISTRATOR', 'ADMIN');
INSERT INTO `role` VALUES ('2', 'CUSTOMER SERVICE', 'CS');
INSERT INTO `role` VALUES ('3', 'GENERAL SUPPORT', 'GS');

-- ----------------------------
-- Table structure for toko
-- ----------------------------
DROP TABLE IF EXISTS `toko`;
CREATE TABLE `toko` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nm_toko` char(50) NOT NULL,
  `id_marketplace` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of toko
-- ----------------------------
INSERT INTO `toko` VALUES ('1', 'TOKO A', '1');
INSERT INTO `toko` VALUES ('2', 'TOKO B', '1');
INSERT INTO `toko` VALUES ('3', 'TOKO C', '3');
INSERT INTO `toko` VALUES ('7', 'TOKO D', '5');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(100) DEFAULT NULL,
  `password` char(100) DEFAULT NULL,
  `email` char(150) NOT NULL,
  `authKey` varchar(100) DEFAULT NULL,
  `accessToken` varchar(100) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1:aktif & 0:tidak aktif',
  `fullname` text,
  `id_role` smallint(5) NOT NULL,
  `github` varchar(255) DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('10', 'admin', '$2y$13$yopyHdgMBt9V8ns0q0.yoettTZyzbqU3lVtLMXQHnTgxJUF2.qOmC', '2rijalboy98@gmail.com', '', '', '1', 'admin_fullname', '1', null, '0', '2017-10-02 11:25:47', '2018-01-16 11:39:21');
INSERT INTO `user` VALUES ('11', 'agus', '$2y$13$82nMF6irRg4Ujjo4xYsEt.I3nhpykfqcx1aUesKAZCEDEXaVnR4vS', '3rijalboy98@gmail.com', '', '', '1', 'komandan agus', '3', null, '0', '2017-10-02 11:25:47', '2018-01-16 11:39:24');
INSERT INTO `user` VALUES ('12', 'cs', '$2y$13$Vn6jPY.1AqLcOLgLTnbOu.YvGjJj0XfNnX3S5nKdA5THszC8x6/CW', '123rijalboy98@gmail.com', '', '', '1', 'cs_fullname', '2', null, '0', '2018-01-03 10:12:04', '2018-01-22 10:34:20');
INSERT INTO `user` VALUES ('17', 'rijal', '$2y$13$fyyxmWY/L902Gn20jIaFhuBwTlva3Yl9DzxUCPLDbyL6CrFncCr7u', 'rijalboy98@gmail.com', null, null, '1', 'Rizal Widiantoro', '1', null, '0', '2018-01-04 00:25:18', '2018-01-16 10:39:57');
INSERT INTO `user` VALUES ('25', 'test123', '$2y$13$Co5hvXQMm/Ilt42.k5dYCuaG4jRXVWx3gf.GHPKuwZqsAQvG70IXC', 'tes2@uysdguyasgd', 'SNsLcezrzZFqwOUMlezHVlVL7ns2nW2k', null, '1', 'test', '1', null, '0', '2018-01-25 16:08:40', '2018-01-25 16:08:40');

-- ----------------------------
-- View structure for status
-- ----------------------------
DROP VIEW IF EXISTS `status`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost`  VIEW `status` AS SELECT id,ktg_status,nm_status,ket_status,class FROM app_status ORDER BY app_status.ktg_status ASC ;

-- ----------------------------
-- View structure for v_bk
-- ----------------------------
DROP VIEW IF EXISTS `v_bk`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_bk` AS SELECT
	md.kd_order,
	md.tipe_transfer,
	md.tipe_transaksi,
	SUM(md.qty) qty,
	md.ongkir_buyer,
	SUM(hgsat_jual * qty) total_belanja,
	(SUM(hgsat_jual * qty)) + ongkir_buyer total_tagihan_buyer,
	md.buyer,
	md.buyer_note,
	md.id_market,
	md.id_kurir,
	md.resi,
	md.`status` id_status,
	md.is_gived,
	md.created_at,
	md.created_by
FROM
	material_detail md
WHERE md.tipe_transaksi=15
AND md.`status` IN (24,3)
GROUP BY
	md.kd_order
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_bm
-- ----------------------------
DROP VIEW IF EXISTS `v_bm`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_bm` AS SELECT
	md.id,
	md.kd_tagihan,
	
	md.tipe_transfer,
	md.tipe_transaksi,
	md.ppn,
	md.pph,
	md.dll,
	md.bea_masuk,
	md.hgtot_dasar,
	md.hgtot_beli,
	md.ongkir_impor,
	SUM(md.qty) qty_total,
	md.id_sup,
	md.`status` id_status,
	md.accept_at,
	md.created_at,
	md.order_at
	
FROM
	material_detail md
WHERE md.tipe_transaksi=14 AND md.`status` IN (3)
GROUP BY
	md.kd_tagihan
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_material
-- ----------------------------
DROP VIEW IF EXISTS `v_material`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_material` AS SELECT
	mm.nama,
	CONCAT(
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		varian.nm_varian,
		' '
	) fullname,
	CONCAT(
		attr.nm_attr,
		' : ',
		varian.nm_varian
	) varianname,
	mm.berat,
	mm.satuan_berat,
	m.*, IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND tipe_transaksi = 14
			AND STATUS = 4
		),
		0
	) qty_in,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND tipe_transaksi = 15
			AND STATUS = 4
		),
		0
	) qty_out,
	stok_akhir_func (
		m.stok_awal,
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND tipe_transaksi = 14
				AND STATUS = 4
			),
			0
		),
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND tipe_transaksi = 15
				AND STATUS = 4
			),
			0
		)
	) stok_akhir
FROM
	material m
LEFT JOIN master_material mm ON mm.kode = m.kode
LEFT JOIN item_attr attr ON attr.id = m.id_attr
LEFT JOIN item_varian varian ON varian.id = m.id_varian ;

-- ----------------------------
-- View structure for v_material_stok
-- ----------------------------
DROP VIEW IF EXISTS `v_material_stok`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_material_stok` AS SELECT
	md.id,
	CONCAT(md.id,'_',m.id) kd_md,
-- 	MD5(CONCAT(mm.kode,m.id,md.kd_tagihan)) kd_stok,
	md.kd_tagihan,
	CONCAT('#',md.kd_tagihan) as kd_tagihan_view,
	CONCAT('TR : #',md.kd_tagihan,' (',DATE_FORMAT(md.order_at,'%d/%m/%y'),')') as kd_tagihan_2,
	mm.nama,
	CONCAT(
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		varian.nm_varian,
		' '
	) fullname,
	CONCAT(
		' (#',
		md.kd_tagihan,') ',
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		varian.nm_varian
		
	) fullname_inv,
	CONCAT(
		attr.nm_attr,
		' : ',
		varian.nm_varian
	) varianname,
	md.harga_mod,
	md.hgsat_reseller,
	md.hgsat_retail,
	md.is_grosir,
	md.id_sup,
	md.order_at,
-- 	MATERIAL 
	m.id id_material,
	m.kode,
	m.foto,
	m.id_attr,
	m.id_varian,
	m.stok_awal,
 
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND kd_tagihan = md.kd_tagihan
			AND tipe_transaksi = 14
			AND STATUS = 4
		),
		0
	) qty_in,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND kd_tagihan = md.kd_tagihan
			AND tipe_transaksi = 15
			AND STATUS = 4
		),
		0
	) qty_out,
	stok_akhir_func (
		0,
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND kd_tagihan = md.kd_tagihan
				AND tipe_transaksi = 14
				AND STATUS = 4
			),
			0
		),
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND kd_tagihan = md.kd_tagihan
				AND tipe_transaksi = 15
				AND STATUS = 4
			),
			0
		)
	) stok_akhir,
	md.created_by,
	md.created_at
FROM
	material m
RIGHT JOIN material_detail md ON md.id_material = m.id
LEFT JOIN master_material mm ON mm.kode = m.kode
LEFT JOIN item_attr attr ON attr.id = m.id_attr
LEFT JOIN item_varian varian ON varian.id = m.id_varian
GROUP BY
	kd_tagihan,
	id_material ;

-- ----------------------------
-- View structure for v_orders
-- ----------------------------
DROP VIEW IF EXISTS `v_orders`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_orders` AS SELECT
	md.kd_order,
	md.tipe_transfer,
	md.tipe_transaksi,
	SUM(md.qty) qty,
	md.ongkir_buyer,
	SUM(hgsat_jual * qty) total_belanja,
	(SUM(hgsat_jual * qty)) + ongkir_buyer total_tagihan_buyer,
	md.buyer,
	md.buyer_note,
	md.id_market,
	md.id_kurir,
	md.resi,
	md.`status` id_status,
	
	md.is_gived,
	md.created_at,
	
	md.created_by
FROM
	material_detail md
WHERE md.tipe_transaksi=15
AND md.`status` IN (0,2,24,25,16,3,4)
GROUP BY
	md.kd_order
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_pb
-- ----------------------------
DROP VIEW IF EXISTS `v_pb`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_pb` AS SELECT
	-- 	md.id,
	md.kd_tagihan,
	md.tipe_transfer,
	md.tipe_transaksi,
	md.ppn,
	md.pph,
	md.dll,
	md.bea_masuk,
	md.hgtot_dasar,
	md.hgtot_beli,
	md.ongkir_impor,
	SUM(md.qty) qty_total,
	md.id_sup,
	md.`status` id_status,
	-- 	md.accept_at,
	-- 	md.created_at
	md.order_at
FROM
	material_detail md
WHERE
	md.tipe_transaksi = 14
-- AND md.`status` IN (0, 1, 2, 3, 4)
GROUP BY
	md.kd_tagihan
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_totalqty
-- ----------------------------
DROP VIEW IF EXISTS `v_totalqty`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `v_totalqty` AS SELECT
	-- 	id,
	md.kd_tagihan,
	md.id_material,
	vm.fullname,
	md.qty_order_bm,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				kd_tagihan = md.kd_tagihan
			AND id_material = md.id_material
			AND tipe_transaksi=14
			AND `status` = 3
		),
		0
	) qty_unacc,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				kd_tagihan = md.kd_tagihan
			AND id_material = md.id_material
			AND tipe_transaksi=14
			AND `status` = 4
		),
		0
	) qty_acc
FROM
	material_detail md
LEFT JOIN v_material vm ON vm.id = md.id_material
WHERE md.tipe_transaksi=14
GROUP BY
	md.kd_tagihan,
	md.id_material
ORDER BY
	md.kd_tagihan,
	md.id_material ASC ;

-- ----------------------------
-- Function structure for stok_akhir_func
-- ----------------------------
DROP FUNCTION IF EXISTS `stok_akhir_func`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `stok_akhir_func`(`stok_awal` int,`masuk` int,`keluar` int) RETURNS int(11)
BEGIN
	RETURN (stok_awal + masuk)-keluar;
END
;;
DELIMITER ;
