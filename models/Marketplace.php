<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marketplace".
 *
 * @property string $id
 * @property string $nm_market
 * @property string $nm_singkat
 */
class Marketplace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marketplace';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_market', 'nm_singkat'], 'required'],
            [['nm_market'], 'string', 'max' => 50],
            [['nm_singkat'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_market' => 'Nama Market',
            'nm_singkat' => 'Nama Singkat',
        ];
    }
}
