<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trval_this_week".
 *
 * @property string $kode
 * @property string $item
 * @property string $qty_sales
 * @property string $total_sales
 * @property string $total_earnings
 */
class TrvalThisWeek extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trval_this_week';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'item'], 'required'],
            [['qty_sales', 'total_sales', 'total_earnings'], 'number'],
            [['kode'], 'string', 'max' => 50],
            [['item'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'item' => 'Item',
            'qty_sales' => 'Qty Sales',
            'total_sales' => 'Total Sales',
            'total_earnings' => 'Total Earnings',
        ];
    }
}
