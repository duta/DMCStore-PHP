<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Vspending;

/**
 * VspendingFilter represents the model behind the search form about `app\models\Vspending`.
 */
class VspendingFilter extends Vspending
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'coa_id', 'debit', 'kredit', 'status_id'], 'integer'],
            [['date', 'fullname', 'coa_name', 'keterangan', 'bukti', 'is_cf', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user=Yii::$app->user->identity;

        $query = Vspending::find()->where(['store_id'=>$user->store_id]);
        
        if($user->id_role!=1){
            $query->where(['user_id'=>$user->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'user_id' => $this->user_id,
            'coa_id' => $this->coa_id,
            'debit' => $this->debit,
            'kredit' => $this->kredit,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'coa_name', $this->coa_name])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            ->andFilterWhere(['like', 'bukti', $this->bukti])
            ->andFilterWhere(['like', 'is_cf', $this->is_cf]);

        return $dataProvider;
    }
}
