<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "store".
 *
 * @property string $id
 * @property string $store_name
 * @property string $store_desc
 * @property integer $user_id
 * @property string $created_at
 */
class Store extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_name', 'user_id'], 'required'],
            [['store_desc'], 'string'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['store_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_name' => 'Nama Toko',
            'store_desc' => 'Deskripsi Toko',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
