<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $link
 * @property integer $parent_id
 * @property string $icon
 * @property integer $no_urut
 * @property integer $is_heading
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'no_urut'], 'required'],
            [['parent_id', 'no_urut', 'is_heading'], 'integer'],
            [['name', 'link', 'icon'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */

    // get child menu
    public function getChild(){
        $user=Yii::$app->user->identity;
        return $this->hasMany(Menu::className(),['parent_id'=>'id'])
        ->leftJoin('permissions','permissions.menu_id=menu.id')
        ->where(['permissions.role_id'=>$user->role])
        ->orderBy(['no_urut'=>SORT_ASC]);
    }

    public function getChild_pure(){
        return $this->hasMany(Menu::className(),['parent_id'=>'id'])
        // ->leftJoin('permissions','permissions.menu_id=menu.id')
        // ->where(['permissions.role_id'=>$user->role])
        ->orderBy(['no_urut'=>SORT_ASC]);
    }
    // get parent Menu
    public function getParent(){
        return $this->hasOne(Menu::className(),['id'=>'parent_id'])->orderBy(['no_urut'=>SORT_ASC]);
    }

    // get Menu Action
    public function getPermit_action(){
        $user=Yii::$app->user->identity;
        return $this->hasMany(PermissionAction::className(),['menu_id'=>'id'])
        ->where(['permission_action.role_id'=>$user->role]);
    }

    // public function get(){
        // return $this->hasMany(Menu::className(),['parent_id'=>'id'])->orderBy(['no_urut'=>SORT_ASC]);
    // }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'link' => 'Link',
            'parent_id' => 'Parent ID',
            'icon' => 'Icon',
            'no_urut' => 'No Urut',
            'is_heading' => 'Is Heading',
        ];
    }
}
