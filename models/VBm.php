<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_bm".
 *
 * @property string $id
 * @property string $kd_tagihan
 * @property string $kd_transaksi
 * @property integer $tipe_transfer
 * @property integer $tipe_transaksi
 * @property integer $ppn
 * @property integer $pph
 * @property integer $bea_masuk
 * @property integer $hgtot_beli
 * @property string $qty_total
 * @property string $ongkir_impor
 * @property string $id_sup
 * @property string $hgsat_modal
 * @property integer $status
 * @property string $created_at
 */
class VBm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'v_bm';
    }

    public static function primaryKey(){
        return ["kd_tagihan"];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipe_transfer', 'tipe_transaksi', 'ppn', 'pph', 'bea_masuk', 'hgtot_dasar', 'hgtot_beli', 'ongkir_impor', 'id_sup', 'status'], 'integer'],
            [['kd_tagihan', 'kd_transaksi', 'tipe_transfer', 'ppn', 'pph', 'bea_masuk', 'hgtot_dasar', 'hgtot_beli', 'ongkir_impor', 'id_sup'], 'required'],
            [['qty_total'], 'number'],
            [['created_at'], 'safe'],
            [['kd_tagihan'], 'string', 'max' => 100],
            [['kd_transaksi'], 'string', 'max' => 40],
        ];
    }

    public function getMd(){
        return $this->hasMany(MaterialDetail::className(),['kd_tagihan'=>'kd_tagihan'])->where(['status'=>[3]]);  
    }

    public function getTotalqty(){
        return $this->hasMany(VTotalqty::className(),['kd_tagihan'=>'kd_tagihan']);  
    }

    public function getSupplier(){
        return $this->hasOne(BrSupplier::className(),['id'=>'id_sup']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_tagihan' => 'Kd Tagihan',
            'kd_transaksi' => 'Kd Transaksi',
            'tipe_transfer' => 'Tipe Transfer',
            'tipe_transaksi' => 'Tipe Transaksi',
            'ppn' => 'Ppn',
            'pph' => 'Pph',
            'bea_masuk' => 'Bea Masuk',
            'hgtot_beli' => 'Hgtot Beli',
            'qty_total' => 'Qty Total',
            'ongkir_impor' => 'Ongkir Impor',
            'id_sup' => 'Id Sup',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
