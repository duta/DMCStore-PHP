<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales_this_week_by_item".
 *
 * @property integer $kode
 * @property string $item
 * @property string $qty
 * @property string $period_date
 * @property string $created_by
 * @property string $created_at
 * @property string $done_at
 */
class SalesThisWeekByItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sales_this_week_by_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'created_by'], 'integer'],
            [['item'], 'required'],
            [['qty'], 'number'],
            [['created_at', 'done_at'], 'safe'],
            [['item'], 'string', 'max' => 255],
            [['period_date'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'item' => 'Item',
            'qty' => 'Qty',
            'period_date' => 'Period Date',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'done_at' => 'Done At',
        ];
    }

    public  static function getChart($item=null){
        $user=Yii::$app->user->identity;

        $where=($item==null)?'':' AND (slw.kode='.$item.' OR slw.kode IS NULL)';
        $sql="SELECT * FROM (
                select *,DATE_FORMAT(date_field, '%d/%m') period_field from
                (
                    select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) date_field from
                    (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                    (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                    (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                    (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                    (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4
                ) t_date where YEARWEEK(date_field, 1) = YEARWEEK(CURDATE(), 1)
            ) tdw
            LEFT JOIN sales_this_week_by_item slw  ON DATE(slw.done_at)=tdw.date_field
            WHERE slw.store_id=".$user->store_id." AND tdw.date_field <> '' ".$where." 
            ORDER BY tdw.date_field ASC";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
