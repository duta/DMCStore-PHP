<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MaterialDetail;

/**
 * MaterialDetailSearch represents the model behind the search form about `app\models\MaterialDetail`.
 */
class MaterialDetailSearch extends MaterialDetail
{
    public $vm_nama;
    public $hg_satuan;

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['id', 'buyer', 'id_material', 'id_sup', 'id_market', 'hgsat_dasar', 'hgtot_beli', 'hgsat_jual', 'qty', 'status', 'read', 'id_kurir', 'total_pay', 'tipe_transfer', 'tipe_transaksi', 'created_by', 'approved_by', 'rejected_by', 'ppn', 'pph', 'bea_masuk', 'ongkir_impor', 'ongkir_buyer'], 'integer'],
            [[
            'kd_tagihan',
            'kd_transaksi',
            'keterangan',
            'resi',
            'sent_at',
            'return_at',
            'done_at',
            'paid_photo',
            'buyer_note',
            'sent_photo',
            'done_photo',
            'created_at',
            'approved_at',
            'rejected_at',
            'updated_at',
            // add manually
            'vm_nama',
            'hg_satuan',
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaterialDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);

        $query->joinWith('v_material');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'v_material.nama' => $this->vm_nama,
            // 'id' => $this->id,
            // 'buyer' => $this->buyer,
            // 'id_material' => $this->id_material,
            // 'id_sup' => $this->id_sup,
            // 'id_market' => $this->id_market,
            // 'hgsat_dasar' => $this->hgsat_dasar,
            // 'hgtot_beli' => $this->hgtot_beli,
            // 'hgsat_jual' => $this->hgsat_jual,
            // 'qty' => $this->qty,
            // 'status' => $this->status,
            // 'read' => $this->read,
            // 'id_kurir' => $this->id_kurir,
            // 'sent_at' => $this->sent_at,
            // 'return_at' => $this->return_at,
            // 'done_at' => $this->done_at,
            // 'total_pay' => $this->total_pay,
            // 'tipe_transfer' => $this->tipe_transfer,
            // 'tipe_transaksi' => $this->tipe_transaksi,
            // 'created_by' => $this->created_by,
            // 'approved_by' => $this->approved_by,
            // 'rejected_by' => $this->rejected_by,
            // 'created_at' => $this->created_at,
            // 'approved_at' => $this->approved_at,
            // 'rejected_at' => $this->rejected_at,
            // 'updated_at' => $this->updated_at,
            // 'ppn' => $this->ppn,
            // 'pph' => $this->pph,
            // 'bea_masuk' => $this->bea_masuk,
            // 'ongkir_impor' => $this->ongkir_impor,
            // 'ongkir_buyer' => $this->ongkir_buyer,
        ]);

        $query
            ->andFilterWhere(['like', 'v_material.nama', $this->vm_nama]); 
            // ->andFilterWhere(['like', 'kd_tagihan', $this->kd_tagihan])
            // ->andFilterWhere(['like', 'kd_transaksi', $this->kd_transaksi])
            // ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            // ->andFilterWhere(['like', 'resi', $this->resi])
            // ->andFilterWhere(['like', 'paid_photo', $this->paid_photo])
            // ->andFilterWhere(['like', 'buyer_note', $this->buyer_note])
            // ->andFilterWhere(['like', 'sent_photo', $this->sent_photo])
            // ->andFilterWhere(['like', 'done_photo', $this->done_photo]);

        return $dataProvider;
    }
}
