<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales_today".
 *
 * @property string $kd_order
 * @property integer $kode
 * @property string $id_material
 * @property string $item
 * @property string $atribut
 * @property string $varian
 * @property string $qty
 * @property string $created_by
 * @property string $created_at
 * @property string $done_at
 */
class SalesToday extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sales_today';
    }

    public static function primaryKey(){
        return ["kd_order"];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'id_material', 'created_by'], 'integer'],
            [['item'], 'required'],
            [['qty'], 'number'],
            [['created_at', 'done_at'], 'safe'],
            [['kd_order', 'atribut', 'varian'], 'string', 'max' => 150],
            [['item'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_order' => 'Kd Order',
            'kode' => 'Kode',
            'id_material' => 'Id Material',
            'item' => 'Item',
            'atribut' => 'Atribut',
            'varian' => 'Varian',
            'qty' => 'Qty',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'done_at' => 'Done At',
        ];
    }
}
