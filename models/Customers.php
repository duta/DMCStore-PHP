<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property string $id
 * @property string $nama
 * @property string $email
 * @property string $tlp
 * @property string $prov
 * @property string $kota_kab
 * @property string $kec
 * @property integer $pos
 * @property string $alamat
 * @property string $created_at
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 
                // 'email', 
                // 'tlp', 
                // 'prov', 
                // 'kota_kab', 
                // 'kec', 
                // 'pos', 
            // 'alamat'
            ], 'required'],
            [['pos'], 'number'],
            [['alamat'], 'string'],
            [['created_at'], 'safe'],
            [['nama', 'prov', 'kota_kab', 'kec'], 'string', 'max' => 150],
            [['email'], 'string', 'max' => 100],
            [['tlp'], 'string', 'max' => 12],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'email' => 'Email',
            'tlp' => 'no. Tlp/Hp',
            'prov' => 'Provinsi',
            'kota_kab' => 'Kota/Kabupaten',
            'kec' => 'Kecamatan',
            'pos' => 'Kode Pos',
            'alamat' => 'Alamat',
            'created_at' => 'Created At',
        ];
    }
}
