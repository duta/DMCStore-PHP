<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_bk".
 *
 * @property string $kd_order
 * @property integer $tipe_transfer
 * @property integer $tipe_transaksi
 * @property string $qty
 * @property string $ongkir_buyer
 * @property string $total_belanja
 * @property string $total_tagihan_buyer
 * @property string $buyer
 * @property string $buyer_note
 * @property string $id_market
 * @property integer $id_kurir
 * @property string $resi
 * @property integer $id_status
 * @property string $created_at
 * @property string $created_by
 */
class VBk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_bk';
    }
    
    public static function primaryKey(){
        return ["kd_order"];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipe_transfer', 'tipe_transaksi', 'ongkir_buyer', 'buyer', 'id_market', 'id_kurir', 'id_status', 'created_by'], 'integer'],
            [['qty', 'total_belanja', 'total_tagihan_buyer'], 'number'],
            [['buyer_note'], 'string'],
            [['created_at'], 'safe'],
            [['kd_order'], 'string', 'max' => 100],
            [['resi'], 'string', 'max' => 150],
        ];
    }
    // get MaterialDetail
    public function getMd(){
        $user=Yii::$app->user->identity;
        $where=[];
        if($user->id_role==3){
            $where=['material_detail.status'=>24];
        }else{
            $where=['material_detail.status'=>[24,3,4]];
        }
        return $this->hasMany(MaterialDetail::className(),['kd_order'=>'kd_order'])->where($where);
    }
    // get buyer/pelanggan
    public function getPlg(){
        return $this->hasOne(Customers::className(),['id'=>'buyer']);
    }
    // get market
    public function getMkt(){
        return $this->hasOne(Marketplace::className(),['id'=>'id_market']);
    }
    // get status
    public function getStatus(){
        return $this->hasOne(AppStatus::className(),['id'=>'id_status']);
    }
    // get kurir
    public function getKurir(){
        return $this->hasOne(Kurir::className(),['id'=>'id_kurir']);
    }

    // get Sending
    public function getSender(){
        return $this->hasOne(Dropshipper::className(),['id'=>'send_by']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_order' => 'Kd Order',
            'tipe_transfer' => 'Tipe Transfer',
            'tipe_transaksi' => 'Tipe Transaksi',
            'qty' => 'Qty',
            'ongkir_buyer' => 'Ongkir Buyer',
            'total_belanja' => 'Total Belanja',
            'total_tagihan_buyer' => 'Total Tagihan Buyer',
            'buyer' => 'Buyer',
            'buyer_note' => 'Buyer Note',
            'id_market' => 'Id Market',
            'id_kurir' => 'Id Kurir',
            'resi' => 'Resi',
            'id_status' => 'Id Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
