<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permission_action".
 *
 * @property string $id
 * @property integer $role_id
 * @property integer $menu_action_id
 */
class PermissionAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'permission_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'menu_action_id'], 'integer'],
        ];
    }


     // get Menu 
    public function getMenu(){
        return $this->hasOne(Menu::className(),['id'=>'menu_id']);
    }

    // get Menu Action
    public function getMenu_action(){
        return $this->hasOne(MenuAction::className(),['id'=>'menu_action_id']);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
            'menu_action_id' => 'Menu Action ID',
        ];
    }
}
