<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_material_stok".
 *
 * @property string $kd_stok
 * @property string $kd_tagihan
 * @property string $nama
 * @property string $fullname
 * @property string $varianname
 * @property string $id
 * @property string $kode
 * @property string $foto
 * @property integer $hg
 * @property integer $id_attr
 * @property integer $id_varian
 * @property integer $stok_awal
 * @property string $created_by
 * @property string $created_at
 * @property string $qty_in
 * @property string $qty_out
 * @property integer $stok_akhir
 */
class VMaterialStok extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_material_stok';
    }

    public static function primaryKey(){
        return ["id"];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname'], 'string'],
            [['id', 'hg', 'id_attr', 'id_varian', 'stok_awal', 'created_by', 'stok_akhir'], 'integer'],
            [['created_at'], 'safe'],
            [['qty_in', 'qty_out'], 'number'],
            [['kd_stok'], 'string', 'max' => 32],
            [['kd_tagihan'], 'string', 'max' => 150],
            [['nama'], 'string', 'max' => 255],
            [['varianname'], 'string', 'max' => 303],
            [['kode', 'foto'], 'string', 'max' => 50],
        ];
    }

    public function getGrosir(){
        return $this->hasMany(Grosir::className(),['id_md'=>'id']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            // 'kd_stok' => 'Kd Stok',
            'kd_tagihan' => 'KODE TR',
            'nama' => 'Nama Barang',
            'fullname' => 'Fullname',
            'varianname' => 'Varian',
            'id' => 'ID',
            'kode' => 'Kode',
            'foto' => 'Foto',
            'hg' => 'Hg',
            'id_attr' => 'Id Attr',
            'id_varian' => 'Id Varian',
            'stok_awal' => 'Stok Awal',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'qty_in' => 'Qty In',
            'qty_out' => 'Qty Out',
            'stok_akhir' => 'Stok',
        ];
    }
}
