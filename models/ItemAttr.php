<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_attr".
 *
 * @property string $id
 * @property string $nm_attr
 * @property string $created_at
 */
class ItemAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['nm_attr'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getVarian(){
        return $this->hasMany(ItemVarian::className(),['id_attr'=>'id']);
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_attr' => 'Atribut',
            'created_at' => 'Created At',
        ];
    }
}
