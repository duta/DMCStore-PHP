<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dropshipper".
 *
 * @property integer $id
 * @property string $nm_ds
 * @property string $tlp
 * @property string $created_at
 */
class Dropshipper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dropshipper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['nm_ds'], 'string', 'max' => 255],
            [['tlp'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_ds' => 'Nm Dropshipper',
            'tlp' => 'Tlp/Hp',
            'created_at' => 'Created At',
        ];
    }
}
