<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_totalqty".
 *
 * @property string $kd_tagihan
 * @property string $id_material
 * @property string $qty_unacc
 * @property string $qty_acc
 */
class VTotalqty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_totalqty';
    }

    public static function primaryKey(){
        return ["kd_tagihan"];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_material'], 'integer'],
            [['qty_unacc', 'qty_acc'], 'number'],
            [['kd_tagihan'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getV_material(){
        return $this->hasOne(VMaterial::className(),['id'=>'id_material']);
    }
    
    public function attributeLabels()
    {
        return [
            'kd_tagihan' => 'KODE TR',
            'id_material' => 'id_material',
            'fullname' => 'Barang',
            'qty_order_bm' => 'QTY yg dipesan',
            'qty_unacc' => 'QTY belum datang',
            'qty_acc' => 'Qty Sudah datang',
        ];
    }
}
