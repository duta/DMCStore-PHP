<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VPb;

/**
 * VPbSearch represents the model behind the search form about `app\models\VPb`.
 */
class VPbSearch extends VPb
{
    /**
     * @inheritdoc
     */
    public $barang;
    public $nm_sup;

    public function rules()
    {
        return [
            [[ 'tipe_transfer', 'tipe_transaksi', 'ppn', 'pph', 'bea_masuk', 'hgtot_dasar', 'hgtot_beli', 'ongkir_impor', 'id_sup', 'id_status'], 'integer'],
            [['kd_tagihan', 'kd_transaksi', 'created_at','nm_sup','barang'], 'safe'],
            [['qty_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user=Yii::$app->user->identity;

        $query = VPb::find()->where(['v_pb.store_id'=>$user->store_id])->joinWith(['md'=>function($query){
            $query->joinWith('v_material');
        },'supplier'])->groupBy('kd_tagihan');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>10
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            // 'tipe_transfer' => $this->tipe_transfer,
            // 'tipe_transaksi' => $this->tipe_transaksi,
            // 'ppn' => $this->ppn,
            // 'pph' => $this->pph,
            // 'bea_masuk' => $this->bea_masuk,
            // 'hgtot_dasar' => $this->hgtot_dasar,
            // 'hgtot_beli' => $this->hgtot_beli,
            // 'ongkir_impor' => $this->ongkir_impor,
            // 'qty_total' => $this->qty_total,
            // 'id_sup' => $this->id_sup,
            // 'id_status' => $this->id_status,
            // 'created_at' => $this->created_at,
        ]);
        
        $query->andFilterWhere(['like', 'material_detail.kd_tagihan', $this->kd_tagihan])
            ->andFilterWhere(['like', 'v_material.nama', $this->barang])
            ->andFilterWhere(['like', 'br_supplier.nm_sup', $this->nm_sup]);

        return $dataProvider;
    }
}
