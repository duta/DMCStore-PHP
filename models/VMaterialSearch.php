<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VMaterial;

/**
 * VMaterialSearch represents the model behind the search form about `app\models\VMaterial`.
 */
class VMaterialSearch extends VMaterial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hgsat_retail', 'hgsat_reseller', 'diskon', 'buyer_id', 'id_ktg', 'id_lokasi', 'id_grup', 'id_tipe', 'stok_awal', 'created_by', 'transaksi', 'hg', 'kondisi', 'stok_akhir'], 'integer'],
            [['kode', 'nama', 'merk', 'foto', 'model', 'satuan_berat', 'satuan', 'created_at', 'updated_at', 'buyer_note', 'note', 'keterangan', 'warna', 'size'], 'safe'],
            [['berat', 'qty_in', 'qty_out'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VMaterial::find()->groupBy('kode')->orderBy(['nama'=>SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            // 'hgsat_retail' => $this->hgsat_retail,
            // 'hgsat_reseller' => $this->hgsat_reseller,
            // 'berat' => $this->berat,
            // 'diskon' => $this->diskon,
            // 'buyer_id' => $this->buyer_id,
            // 'id_ktg' => $this->id_ktg,
            // 'id_lokasi' => $this->id_lokasi,
            // 'id_grup' => $this->id_grup,
            // 'id_tipe' => $this->id_tipe,
            // 'stok_awal' => $this->stok_awal,
            // 'created_by' => $this->created_by,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
            // 'transaksi' => $this->transaksi,
            // 'hg' => $this->hg,
            // 'kondisi' => $this->kondisi,
            // 'qty_in' => $this->qty_in,
            // 'qty_out' => $this->qty_out,
            // 'stok_akhir' => $this->stok_akhir,
        ]);

        $query->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'nama', $this->nama]);
            // ->andFilterWhere(['like', 'merk', $this->merk])
            // ->andFilterWhere(['like', 'foto', $this->foto])
            // ->andFilterWhere(['like', 'model', $this->model])
            // ->andFilterWhere(['like', 'satuan_berat', $this->satuan_berat])
            // ->andFilterWhere(['like', 'satuan', $this->satuan])
            // ->andFilterWhere(['like', 'buyer_note', $this->buyer_note])
            // ->andFilterWhere(['like', 'note', $this->note])
            // ->andFilterWhere(['like', 'keterangan', $this->keterangan])
            // ->andFilterWhere(['like', 'warna', $this->warna])
            // ->andFilterWhere(['like', 'size', $this->size]);

        return $dataProvider;
    }
}
