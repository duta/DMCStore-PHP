<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VOrders;
use yii\db\Expression;

/**
 * VOrdersSearch represents the model behind the search form about `app\models\VOrders`.
 */
class VOrdersSearch extends VOrders
{
    public $barang;
    // public $market;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_order', 'buyer_note', 'resi', 'created_at','barang'], 'safe'],
            [['tipe_transfer', 'tipe_transaksi', 'ongkir_buyer', 'buyer', 'id_market', 'id_kurir', 'status', 'created_by',], 'integer'],
            [['qty', 'total_belanja','total_tagihan_buyer'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user=Yii::$app->user->identity;

        $query = VOrders::find()->where(['v_orders.store_id'=>$user->store_id])
        ->joinWith(['md'=>function($query){
            $query->joinWith('v_material');
        }])->groupBy('kd_order')->orderBy([new Expression(" field(v_orders.id_status,0,24,3,2,4) ")]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>10
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'tipe_transfer' => $this->tipe_transfer,
            // 'tipe_transaksi' => $this->tipe_transaksi,
            // 'qty' => $this->qty,
            // 'ongkir_buyer' => $this->ongkir_buyer,
            // 'total_belanja' => $this->total_belanja,
            'v_orders.buyer' => $this->buyer,
            'total_tagihan_buyer' => $this->total_tagihan_buyer,
            'v_orders.id_market' => $this->id_market,
            // 'id_kurir' => $this->id_kurir,
            // 'status' => $this->status,
            // 'created_at' => $this->created_at,
            // 'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'v_material.nama', $this->barang]);
        //     ->andFilterWhere(['like', 'buyer_note', $this->buyer_note])
        //     ->andFilterWhere(['like', 'resi', $this->resi]);

        return $dataProvider;
    }
}
