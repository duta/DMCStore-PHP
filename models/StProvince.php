<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "st_province".
 *
 * @property string $id
 * @property string $province
 */
class StProvince extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'st_province';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['province'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'province' => 'Province',
        ];
    }
}
