<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_spending".
 *
 * @property integer $id
 * @property string $date
 * @property integer $user_id
 * @property string $fullname
 * @property integer $coa_id
 * @property string $coa_name
 * @property integer $debit
 * @property integer $kredit
 * @property string $keterangan
 * @property integer $status_id
 * @property string $bukti
 * @property integer $is_cf
 * @property string $created_at
 * @property string $updated_at
 */
class Vspending extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_spending';
    }

    public static function primaryKey(){
        return ['id'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'coa_id', 'debit', 'kredit', 'status_id'], 'integer'],
            [['date'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['fullname', 'keterangan'], 'string'],
            [['coa_name'], 'string', 'max' => 35],
            [['bukti'], 'string', 'max' => 150],
            [['is_cf'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'user_id' => 'User ID',
            'fullname' => 'Fullname',
            'coa_id' => 'Coa ID',
            'coa_name' => 'Coa',
            'debit' => 'Debit',
            'kredit' => 'Kredit',
            'keterangan' => 'Keterangan',
            'status_id' => 'Status ID',
            'bukti' => 'Bukti',
            'is_cf' => 'Is Cf',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
