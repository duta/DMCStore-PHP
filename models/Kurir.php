<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kurir".
 *
 * @property string $id
 * @property string $nm_kurir
 * @property string $layanan
 * @property string $created_at
 * @property string $updated_at
 */
class Kurir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kurir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_kurir', 'layanan'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['nm_kurir', 'layanan'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_kurir' => 'Nm Kurir',
            'layanan' => 'Layanan',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return KurirQuery the active query used by this AR class.
     */
    // public static function find()
    // {
    //     return new KurirQuery(get_called_class());
    // }
}
