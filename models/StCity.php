<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "st_city".
 *
 * @property string $id
 * @property string $city
 * @property string $type
 * @property string $postal_code
 * @property string $province_id
 */
class StCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'st_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'province_id'], 'required'],
            [['city', 'postal_code'], 'string'],
            [['province_id'], 'integer'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'type' => 'Type',
            'postal_code' => 'Postal Code',
            'province_id' => 'Province ID',
        ];
    }
}
