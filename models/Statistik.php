<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "statistik".
 *
 * @property integer $id
 * @property string $nama
 * @property string $sql
 * @property string $icon
 * @property string $bg_class
 * @property string $class_add
 * @property string $id_add
 * @property integer $display
 */
class Statistik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sql'], 'string'],
            [['display'], 'integer'],
            [['nama'], 'string', 'max' => 50],
            [['icon', 'class_add', 'id_add'], 'string', 'max' => 20],
            [['bg_class'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'sql' => 'Sql',
            'icon' => 'Icon',
            'bg_class' => 'Bg Class',
            'class_add' => 'Class Add',
            'id_add' => 'Id Add',
            'display' => 'Display',
        ];
    }

    public static function getScalar($sql){
        $exc=Yii::$app->db->createCommand($sql)->queryScalar();
        return ($exc!==false) ? $exc : 0;
    }
}
