<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_cashflow".
 *
 * @property string $kd_cf
 * @property string $date
 * @property string $keterangan
 * @property integer $status_id
 * @property integer $is_cf
 * @property string $created_at
 * @property string $updated_at
 */
class Vcashflow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_cashflow';
    }

    public static function primaryKey(){
        return ['kd_cf'];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['keterangan'], 'string'],
            [['kd_cf'], 'string', 'max' => 32],
            [['status_id'], 'string', 'max' => 3],
            [['is_cf'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_cf' => 'Kd Cf',
            'date' => 'Date',
            'keterangan' => 'Keterangan',
            'status_id' => 'Status ID',
            'is_cf' => 'Is Cf',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getOprs()
    {
        return $this->hasMany(Opr::className(),['kd_cf'=>'kd_cf']);
    }
}
