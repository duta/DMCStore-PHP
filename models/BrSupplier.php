<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "br_supplier".
 *
 * @property string $id
 * @property string $nm_sup
 */
class BrSupplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'br_supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_sup'], 'required'],
            [['nm_sup'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_sup' => 'Supplier',
            'tlp' => 'Tlp/Hp',
        ];
    }
}
