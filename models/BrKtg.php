<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "br_ktg".
 *
 * @property string $id
 * @property string $nm_ktg
 */
class BrKtg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'br_ktg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nm_ktg'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_ktg' => 'Nama Kategori',
        ];
    }
}
