<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_action".
 *
 * @property string $id
 * @property integer $menu_id
 * @property string $name
 * @property string $column_param
 * @property string $tipe
 * @property string $icon
 * @property string $display_class
 * @property string $class_add
 */
class MenuAction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_action';
    }

    /**
     * @inheritdoc
     */

    // get Menu
    public function getMenu(){
        return $this->hasOne(Menu::className(),['id'=>'menu_id']);
    }


    public function rules()
    {
        return [
            [['menu_id'], 'integer'],
            [['tipe'], 'string'],
            [['name', 'column_param', 'icon', 'display_class'], 'string', 'max' => 50],
            [['class_add'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'name' => 'Name',
            'column_param' => 'Column Param',
            'tipe' => 'Tipe',
            'icon' => 'Icon',
            'display_class' => 'Display Class',
            'class_add' => 'Class Add',
        ];
    }
}
