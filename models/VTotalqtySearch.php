<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VTotalqty;

/**
 * VTotalqtySearch represents the model behind the search form about `app\models\VTotalqty`.
 */
class VTotalqtySearch extends VTotalqty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_tagihan', 'fullname'], 'safe'],
            [['id_material', 'qty_order_bm'], 'integer'],
            [['qty_unacc', 'qty_acc'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user=Yii::$app->user->identity;
        
        $query = VTotalqty::find()->where(['store_id'=>$user->store_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>10
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_material' => $this->id_material,
            'qty_order_bm' => $this->qty_order_bm,
            'qty_unacc' => $this->qty_unacc,
            'qty_acc' => $this->qty_acc,
        ]);

        $query->andFilterWhere(['like', 'kd_tagihan', $this->kd_tagihan])
            ->andFilterWhere(['like', 'fullname', $this->fullname]);

        return $dataProvider;
    }
}
