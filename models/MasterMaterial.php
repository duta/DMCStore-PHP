<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_material".
 *
 * @property integer $kode
 * @property string $nm_material
 * @property string $created_at
 */
class MasterMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['created_at'], 'safe'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getV_material(){
        return $this->hasMany(VMaterial::className(),['kode'=>'kode']);
    }
    public function getKtg(){
        return $this->hasOne(BrKtg::className(),['id'=>'id_ktg']);
    }


    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'nama' => 'Nama',
            'created_at' => 'Created At',
        ];
    }

    public static function qexcel(){
        $sql="  SELECT
                    ktg.nm_ktg kategori,
                    t.* 
                FROM
                    (
                        SELECT
                            kode,
                            id_ktg,
                            nama,
                            varian,
                            macam,
                            0 qty_varian 
                        FROM
                            v_material UNION
                        SELECT
                            kode,
                            id_ktg,
                            nama,
                            '' varian,
                            '' macam,
                            ( SELECT COUNT( ID ) FROM v_material vm WHERE vm.kode = mm.kode ) qty_varian 
                        FROM
                            master_material mm 
                        HAVING
                            qty_varian < 1 
                    ) t
                    INNER JOIN br_ktg ktg ON ktg.id = t.id_ktg";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
