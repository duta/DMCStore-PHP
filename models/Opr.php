<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "opr".
 *
 * @property integer $id
 * @property string $date
 * @property integer $user_id
 * @property integer $coa_id
 * @property integer $debit
 * @property integer $kredit
 * @property string $keterangan
 * @property integer $status_id
 * @property string $bukti
 * @property integer $is_cf
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Coa $coa
 * @property User $user
 */
class Opr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'opr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['date', 'is_cf'], 'required'],
            // [['date', 'created_at', 'updated_at'], 'safe'],
            // [['user_id', 'coa_id', 'debit', 'kredit', 'status_id'], 'default'],
            // [['keterangan'], 'string'],
            // [['bukti'], 'string', 'max' => 150],
            // [['is_cf'], 'string', 'max' => 1],
            // [['coa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Coa::className(), 'targetAttribute' => ['coa_id' => 'id']],
            // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'user_id' => 'User ID',
            'coa_id' => 'Coa ID',
            'debit' => 'Debit',
            'kredit' => 'Kredit',
            'keterangan' => 'Keterangan',
            'status_id' => 'Status ID',
            'bukti' => 'Bukti',
            'is_cf' => 'Is Cf',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoa()
    {
        return $this->hasOne(Coa::className(), ['id' => 'coa_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
