<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_status".
 *
 * @property integer $id
 * @property string $nm_status
 * @property string $ket_status
 * @property string $class
 * @property string $ktg_status
 */
class AppStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nm_status', 'ket_status', 'class', 'ktg_status'], 'required'],
            [['id'], 'integer'],
            [['ket_status'], 'string'],
            [['nm_status', 'class', 'ktg_status'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nm_status' => 'Nm Status',
            'ket_status' => 'Ket Status',
            'class' => 'Class',
            'ktg_status' => 'Ktg Status',
        ];
    }
}
