<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grosir".
 *
 * @property string $id
 * @property string $id_md
 * @property integer $qty
 * @property string $harga
 * @property string $created_at
 */
class Grosir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grosir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_md', 'qty', 'harga'], 'default']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_md' => 'Id MD',
            'qty' => 'Qty',
            'harga' => 'Harga',
            'created_at' => 'Created At',
        ];
    }
}
