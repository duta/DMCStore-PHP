<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_varian".
 *
 * @property string $id
 * @property string $id_attr
 * @property string $nm_varian
 * @property string $created_at
 */
class ItemVarian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_varian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_attr'], 'integer'],
            [['created_at'], 'safe'],
            [['nm_varian'], 'string', 'max' => 150],
        ];
    }

    public function getAttr(){
        return $this->hasOne(ItemAttr::className(),['id'=>'id_attr']);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_attr' => 'Id Attr',
            'nm_varian' => 'Nm Varian',
            'created_at' => 'Created At',
        ];
    }
}
