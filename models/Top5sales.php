<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "top5sales".
 *
 * @property integer $kode
 * @property string $item
 * @property string $sold
 */
class Top5sales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'top5sales';
    }

    public static function primaryKey(){
        return ["kode"];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode'], 'integer'],
            [['item'], 'required'],
            [['sold'], 'number'],
            [['item'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode' => 'Kode',
            'item' => 'Item',
            'sold' => 'Sold',
        ];
    }
}
