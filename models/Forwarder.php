<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forwarder".
 *
 * @property string $id
 * @property string $kd_fw
 * @property string $nm_fw
 * @property string $tlp
 * @property string $qq
 * @property string $wechat
 * @property string $created_at
 */
class Forwarder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forwarder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_fw', 'nm_fw', 'tlp', 'wechat'], 'default'],
            [['created_at'], 'safe'],
            [['kd_fw', 'nm_fw', 'tlp', 'qq', 'wechat'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_fw' => 'Kd Fw',
            'nm_fw' => 'Name',
            'tlp' => 'Tlp',
            'qq' => 'Qq',
            'wechat' => 'Wechat',
            'created_at' => 'Created At',
        ];
    }
}
