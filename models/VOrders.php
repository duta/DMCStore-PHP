<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_orders".
 *
 * @property string $kd_order
 * @property integer $tipe_transfer
 * @property integer $tipe_transaksi
 * @property string $qty
 * @property string $ongkir_buyer
 * @property string $total_belanja
 * @property string $total_tagihan_buyer
 * @property string $buyer
 * @property string $buyer_note
 * @property string $id_market
 * @property integer $id_kurir
 * @property string $resi
 * @property integer $status
 * @property string $created_at
 * @property string $created_by
 */
class VOrders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_orders';
    }

    public static function primaryKey(){
        return ["kd_order"];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_order', 'tipe_transfer', 'ongkir_buyer', 'buyer', 'buyer_note', 'id_market', 'id_kurir', 'resi', 'created_by'], 'required'],
            [['tipe_transfer', 'tipe_transaksi', 'ongkir_buyer', 'buyer', 'id_market', 'id_kurir', 'status', 'created_by'], 'integer'],
            [['qty', 'total_belanja', 'total_tagihan_buyer'], 'number'],
            [['buyer_note'], 'string'],
            [['created_at'], 'safe'],
            [['kd_order'], 'string', 'max' => 100],
            [['resi'], 'string', 'max' => 150],
        ];
    }
    // get MaterialDetail
    public function getMd(){
        return $this->hasMany(MaterialDetail::className(),['kd_order'=>'kd_order']);
    }
    // get buyer/pelanggan
    public function getPlg(){
        return $this->hasOne(Customers::className(),['id'=>'buyer']);
    }
     // get Created by
    public function getCsby(){
        return $this->hasOne(User::className(),['id'=>'created_by']);
    }
    // get market
    public function getMkt(){
        return $this->hasOne(Marketplace::className(),['id'=>'id_market']);
    }
    // get status
    public function getStatus(){
        return $this->hasOne(AppStatus::className(),['id'=>'id_status']);
    }
    // get kurir
    public function getKurir(){
        return $this->hasOne(Kurir::className(),['id'=>'id_kurir']);
    }
    // get v_material_stok
    public function getVmstok(){
        return $this->hasMany(VMaterialStok::className(),['kd_order'=>'kd_order']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_order' => 'Kd Tagihan',
            'tipe_transfer' => 'Tipe Transfer',
            'tipe_transaksi' => 'Tipe Transaksi',
            'qty' => 'Qty',
            'ongkir_buyer' => 'Ongkir Buyer',
            'total_belanja' => 'Total Belanja',
            'total_tagihan_buyer' => 'Total Tagihan Buyer',
            'buyer' => 'Buyer',
            'buyer_note' => 'Buyer Note',
            'id_market' => 'Id Market',
            'id_kurir' => 'Id Kurir',
            'resi' => 'Resi',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }

    public static function salesthisyear(){
        $user=Yii::$app->user->identity;

        $sql="SELECT 
                mt.month_name,
                -- mt.month_field,
                -- IFNULL(tr.qty_sales,200000) qty,
                IFNULL(tr.total_sales,0) OMZET,
                IFNULL(tr.total_earnings,0) MARGIN
                FROM (
                    select 
                    DATE_FORMAT(date_field, '%m') month_only,
                    DATE_FORMAT(date_field, '%m-%Y') month_field,
                    DATE_FORMAT(date_field,'%b') month_name                    
                    from
                    (
                        select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) date_field from
                        (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                        (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                        (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                        (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                        (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4
                    ) t_date 
                    where  
                    YEAR(date_field) = YEAR(CURDATE())
                    GROUP BY DATE_FORMAT(date_field, '%m-%Y') ASC
                ) mt
                LEFT JOIN 
                (
                    SELECT 
                    SUM(qty_sales) qty_sales,
                    SUM(total_sales) total_sales,
                    SUM(total_earnings) total_earnings,
                    `month_field` 
                    FROM trval_this_year WHERE store_id=".$user->store_id." GROUP BY `month_field` 
                ) tr ON mt.month_field=tr.month_field";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
