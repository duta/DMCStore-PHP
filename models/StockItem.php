<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stock_item".
 *
 * @property integer $iditem
 * @property string $store_url
 * @property string $item_url
 * @property string $item_updated
 */
class StockItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stock_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iditem', 'store_url', 'item_url'], 'required'],
            [['iditem'], 'integer'],
            [['item_updated'], 'safe'],
            [['store_url', 'item_url'], 'string', 'max' => 240],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'iditem' => 'Iditem',
            'store_url' => 'Store Url',
            'item_url' => 'Item Url',
            'item_updated' => 'Item Updated',
        ];
    }
}
