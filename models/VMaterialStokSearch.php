<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VMaterialStok;

/**
 * VMaterialStokSearch represents the model behind the search form about `app\models\VMaterialStok`.
 */
class VMaterialStokSearch extends VMaterialStok
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'kd_tagihan', 'nama', 'fullname', 'varianname', 'kode', 'foto', 'created_at'], 'safe'],
            [['id', 'is_grosir', 'id_attr', 'id_varian', 'stok_awal', 'created_by', 'stok_akhir'], 'integer'],
            [['qty_in', 'qty_out'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user=Yii::$app->user->identity;
        
        $query = VMaterialStok::find()->where(['store_id'=>$user->store_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>10
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_attr' => $this->id_attr,
            'id_varian' => $this->id_varian,
            'stok_awal' => $this->stok_awal,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'qty_in' => $this->qty_in,
            'qty_out' => $this->qty_out,
            'stok_akhir' => $this->stok_akhir,
        ]);

        $query->andFilterWhere(['like', 'kd_tagihan', $this->kd_tagihan])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'varianname', $this->varianname])
            ->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
