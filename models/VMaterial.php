<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_material".
 *
 * @property string $id
 * @property string $kode
 * @property string $nama
 * @property string $merk
 * @property string $foto
 * @property string $model
 * @property integer $hgsat_retail
 * @property integer $hgsat_reseller
 * @property string $berat
 * @property string $satuan_berat
 * @property string $satuan
 * @property integer $diskon
 * @property integer $buyer_id
 * @property integer $id_ktg
 * @property integer $id_lokasi
 * @property integer $id_grup
 * @property integer $id_tipe
 * @property string $stok_awal
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $transaksi
 * @property integer $hg
 * @property integer $kondisi
 * @property string $buyer_note
 * @property string $note
 * @property string $keterangan
 * @property string $warna
 * @property string $size
 * @property string $qty_in
 * @property string $qty_out
 * @property integer $stok_akhir
 */
class VMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_material';
    }

   
    /**
     * @inheritdoc$primaryKey
     */
    public static function primaryKey()
    {
        return ["id"];
    }

    public function rules()
    {
        return [
            [['id', 'hgsat_retail', 'hgsat_reseller', 'diskon', 'buyer_id', 'id_ktg', 'id_lokasi', 'id_grup', 'id_tipe', 'stok_awal', 'created_by', 'transaksi', 'hg', 'kondisi', 'stok_akhir'], 'integer'],
            [['kode', 'nama', 'merk', 'foto', 'model', 'hgsat_retail', 'hgsat_reseller', 'berat', 'satuan_berat', 'diskon', 'buyer_id', 'id_ktg', 'id_lokasi', 'id_grup', 'id_tipe', 'stok_awal', 'created_by', 'transaksi', 'hg', 'kondisi', 'buyer_note', 'note', 'keterangan', 'warna', 'size'], 'required'],
            [['nama', 'buyer_note', 'note', 'keterangan'], 'string'],
            [['berat', 'qty_in', 'qty_out'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode', 'merk', 'foto', 'model', 'warna', 'size'], 'string', 'max' => 50],
            [['satuan_berat'], 'string', 'max' => 5],
            [['satuan'], 'string', 'max' => 20],
        ];
    }
    public function getAttr(){
        return $this->hasOne(ItemAttr::className(),['id'=>'id_attr']);
    }

    public function getVarian(){
        return $this->hasOne(ItemVarian::className(),['id'=>'id_varian']);
    }

    public function getGrosir(){
        return $this->hasMany(Grosir::className(),['id_material'=>'id']);
    }
    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'merk' => 'Merk',
            'foto' => 'Foto',
            'model' => 'Model',
            'hgsat_retail' => 'Hgsat Retail',
            'hgsat_reseller' => 'Hgsat Reseller',
            'berat' => 'Berat',
            'satuan_berat' => 'Satuan Berat',
            'satuan' => 'Satuan',
            'diskon' => 'Diskon',
            'buyer_id' => 'Buyer ID',
            'id_ktg' => 'Id Ktg',
            'id_lokasi' => 'Id Lokasi',
            'id_grup' => 'Id Grup',
            'id_tipe' => 'Id Tipe',
            'stok_awal' => 'Stok Awal',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'transaksi' => '0: material ini belum ada kegiatan order, 1 : sebaliknya',
            'hg' => 'Hg',
            'kondisi' => 'Kondisi',
            'buyer_note' => 'Buyer Note',
            'note' => 'Note',
            'keterangan' => 'Keterangan',
            'warna' => 'Warna',
            'size' => 'Size',
            'qty_in' => 'Qty In',
            'qty_out' => 'Qty Out',
            'stok_akhir' => 'Stok Akhir',
        ];
    }
}
