<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permissions".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $role_id
 */
class Permissions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'permissions';
    }
    public static function primaryKey()
    {
        return ["id"];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'role_id'], 'required'],
            [['menu_id', 'role_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */

     // get menu
    public function getMenu(){
        return $this->hasOne(Menu::className(),['id'=>'menu_id']);
    }
    // get role
    public function getRole(){
        return $this->hasOne(Role::className(),['id'=>'role_id']);
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'role_id' => 'Role ID',
        ];
    }
}
