<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_detail".
 *
 * @property string $id
 * @property string $kd_tagihan
 * @property string $kd_transaksi
 * @property string $buyer
 * @property string $id_material
 * @property string $id_sup
 * @property string $id_market
 * @property integer $hgsat_dasar
 * @property integer $hgtot_beli
 * @property string $hgsat_jual
 * @property string $keterangan
 * @property string $qty
 * @property integer $status
 * @property integer $read
 * @property integer $id_kurir
 * @property string $resi
 * @property string $sent_at
 * @property string $return_at
 * @property string $done_at
 * @property integer $total_pay
 * @property string $paid_photo
 * @property integer $tipe_transfer
 * @property integer $tipe_transaksi
 * @property string $buyer_note
 * @property string $sent_photo
 * @property string $done_photo
 * @property string $created_by
 * @property string $approved_by
 * @property string $rejected_by
 * @property string $created_at
 * @property string $approved_at
 * @property string $rejected_at
 * @property string $updated_at
 * @property integer $ppn
 * @property integer $pph
 * @property integer $bea_masuk
 * @property string $ongkir_impor
 * @property string $ongkir_buyer
 */
class MaterialDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'kd_tagihan', 'kd_order', 'buyer', 'id_material', 'id_sup', 'id_market', 'hgsat_dasar', 'hgtot_beli', 'hgsat_jual', 'keterangan', 'qty', 'read', 'id_kurir', 'resi', 'return_at', 'done_at', 'total_pay', 'paid_photo', 'tipe_transfer', 'buyer_note', 'sent_photo', 'done_photo', 'created_by', 'approved_by', 'rejected_by', 'ppn', 'pph', 'bea_masuk', 'ongkir_impor', 'ongkir_buyer'
            ], 'default'],
            [[
                'buyer', 'id_material', 'id_sup', 'id_market', 'hgsat_dasar', 'hgtot_beli', 'hgsat_jual', 'qty', 'status', 'read', 'id_kurir', 'total_pay', 'tipe_transfer', 'tipe_transaksi', 'created_by', 'approved_by', 'rejected_by', 'ppn', 'pph', 'bea_masuk', 'ongkir_impor', 'ongkir_buyer'
            ], 'default'],
            [[
                'keterangan', 'buyer_note'
            ], 'string'],
            [[
                'sent_at', 'return_at', 'done_at', 'created_at', 'approved_at', 'rejected_at', 'updated_at'
            ], 'safe'],
            [[
                'kd_tagihan'
            ], 'string', 'max' => 100],
            [[
                'kd_order'
            ], 'string', 'max' => 40],
            [[
                'resi'
            ], 'string', 'max' => 150],
            [[
                'paid_photo', 'sent_photo', 'done_photo'
            ], 'string', 'max' => 50],
            [[
                'id_material', 'qty', 'total_pay', 'buyer', 'created_at'
            ], 'unique', 'targetAttribute' => ['id_material', 'qty', 'total_pay', 'buyer', 'created_at'
            ], 'message' => 'The combination of Buyer, Id Material, Qty, Total Pay and Created At has already been taken.'],
        ];
    }



    public function getV_material(){
        return $this->hasOne(VMaterial::className(),['id'=>'id_material']);
    }

    public function getV_materialbyinv(){
        return $this->hasOne(VMaterialStok::className(),['id_material'=>'id_material','kd_tagihan'=>'kd_tagihan']);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_tagihan' => 'Kd Tagihan',
            'kd_order' => 'Kd Transaksi',
            'buyer' => 'Buyer',
            'id_material' => 'Id Material',
            'id_sup' => 'Id Sup',
            'id_market' => 'Id Market',
            'hgsat_dasar' => 'Hgsat Dasar',
            'hgtot_beli' => 'Hgtot Beli',
            'hgsat_jual' => 'Hgsat Jual',
            'keterangan' => 'Keterangan',
            'qty' => 'Qty',
            'status' => 'Status',
            'read' => 'Read',
            'id_kurir' => 'Id Kurir',
            'resi' => 'Resi',
            'sent_at' => 'Sent At',
            'return_at' => 'Return At',
            'done_at' => 'Done At',
            'total_pay' => 'Total Pay',
            'paid_photo' => 'Paid Photo',
            'tipe_transfer' => 'Tipe Transfer',
            'tipe_transaksi' => 'Tipe Transaksi',
            'buyer_note' => 'Buyer Note',
            'sent_photo' => 'Sent Photo',
            'done_photo' => 'Done Photo',
            'created_by' => 'Created By',
            'approved_by' => 'Approved By',
            'rejected_by' => 'Rejected By',
            'created_at' => 'Created At',
            'approved_at' => 'Approved At',
            'rejected_at' => 'Rejected At',
            'updated_at' => 'Updated At',
            'ppn' => 'Ppn',
            'pph' => 'Pph',
            'bea_masuk' => 'Bea Masuk',
            'ongkir_impor' => 'Ongkir Impor',
            'ongkir_buyer' => 'Ongkir Buyer',
            
        ];
    }

    public static function kd_tr(){
        $string='#DMC'.Yii::$app->getSecurity()->generateRandomString(5).'TR';
        if(MaterialDetail::findOne(['kd_order'=>$string])===null){
            return $string;
        }else{
            return MaterialDetail::kd_transaksi();
        }

    }

}
