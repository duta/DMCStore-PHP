<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales_this_week".
 *
 * @property string $kd_order
 * @property integer $kode
 * @property string $id_material
 * @property string $item
 * @property string $atribut
 * @property string $varian
 * @property string $qty
 * @property string $created_by
 * @property string $created_at
 * @property string $done_at
 */
class SalesThisWeek extends \yii\db\ActiveRecord
{
    /**
     * @inheritdo   c
     */
    public static function tableName()
    {
        return 'sales_this_week';
    }
    public static function primaryKey(){
        return ["kd_order"];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'id_material', 'created_by'], 'integer'],
            [['item'], 'required'],
            [['qty'], 'number'],
            [['created_at', 'done_at'], 'safe'],
            [['kd_order', 'atribut', 'varian'], 'string', 'max' => 150],
            [['item'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_order' => 'Kd Order',
            'kode' => 'Kode',
            'id_material' => 'Id Material',
            'item' => 'Item',
            'atribut' => 'Atribut',
            'varian' => 'Varian',
            'qty' => 'Qty',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'done_at' => 'Done At',
        ];
    }

    public  static function getChart($item=null){
        $where=($item==null)?'':' AND (slw.kode='.$item.' OR slw.kode IS NULL)';
        $sql="SELECT * FROM (
                select *,DATE_FORMAT(date_field, '%d/%m') period_field from
                (
                    select adddate('1970-01-01',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) date_field from
                    (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                    (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                    (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                    (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                    (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4
                ) t_date where YEARWEEK(date_field, 1) = YEARWEEK(CURDATE(), 1)
            ) tdw
            LEFT JOIN sales_this_week slw ON DATE(slw.done_at)=tdw.date_field
            WHERE tdw.date_field <> '' ".$where." 
            ORDER BY tdw.date_field ASC";
        return Yii::$app->db->createCommand($sql)->queryAll();
    }
}
