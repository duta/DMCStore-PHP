<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "v_pb".
 *
 * @property string $id
 * @property string $kd_tagihan
 * @property string $kd_transaksi
 * @property integer $tipe_transfer
 * @property integer $tipe_transaksi
 * @property integer $ppn
 * @property integer $pph
 * @property integer $bea_masuk
 * @property string $hgtot_dasar
 * @property integer $hgtot_beli
 * @property string $ongkir_impor
 * @property string $qty_total
 * @property string $id_sup
 * @property integer $id_status
 * @property string $created_at
 */
class VPb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'v_pb';
    }

    public static function primaryKey(){
        return ["kd_tagihan"];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tipe_transfer', 'tipe_transaksi', 'ppn', 'pph', 'bea_masuk', 'hgtot_dasar', 'hgtot_beli', 'ongkir_impor', 'id_sup', 'id_status'], 'integer'],
            [['qty_total'], 'number'],
            [['created_at'], 'safe'],
            [['kd_tagihan'], 'string', 'max' => 100],
            [['kd_transaksi'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMd(){
        return $this->hasMany(MaterialDetail::className(),['kd_tagihan'=>'kd_tagihan'])->where(['status'=>[3,4]])
        ->groupBy(['material_detail.kd_tagihan','id_material']);
    }

    
    public function getSupplier(){
        return $this->hasOne(BrSupplier::className(),['id'=>'id_sup']);
    }

    public function getFw(){
        return $this->hasOne(Forwarder::className(),['id'=>'id_fw']);
    }

    
    

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_tagihan' => 'Kd Tagihan',
            'kd_transaksi' => 'Kd Transaksi',
            'tipe_transfer' => 'Tipe Transfer',
            'tipe_transaksi' => 'Tipe Transaksi',
            'ppn' => 'Ppn',
            'pph' => 'Pph',
            'bea_masuk' => 'Bea Masuk',
            'hgtot_dasar' => 'Hgtot Dasar',
            'hgtot_beli' => 'Hgtot Beli',
            'ongkir_impor' => 'Ongkir Impor',
            'qty_total' => 'Qty Total',
            'id_sup' => 'Id Sup',
            'id_status' => 'Id Status',
            'created_at' => 'Created At',
        ];
    }
}
