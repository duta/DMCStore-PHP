<?php

namespace app\controllers;

use Yii;
use app\models\Role;
use app\models\RoleFilter;
use app\models\Menu;
use app\models\Permissions;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use yii\web\Response;
use yii\widgets\ActiveForm;
use app\components\Help;
/**
 * RoleController implements the CRUD actions for Role model.
 */
class RoleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Role models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RoleFilter();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Role model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Role model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user=Yii::$app->user->identity;
        $model = new Role();

      
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) ) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $validate=Help::errorForm(ActiveForm::validate($model));
            if(count($validate)<1){
                $model->store_id=$user->store_id;
                $model->save();

            }
            return $validate;
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Role model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) ) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $validate=Help::errorForm(ActiveForm::validate($model));
            if(count($validate)<1){
                $model->save();
            }
            return $validate;
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Role model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    public function actionChange_permit(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['menus']=Menu::find()->where(['parent_id'=>0])->all();
        $data['role_id']=Yii::$app->request->post('role_id');
        $model=Permissions::find()->where(['role_id'=>$data['role_id']])->asArray()->all();
        $data['model']=ArrayHelper::index($model,'menu_id');
        // echo "<pre>";
        // print_r($data['model']);
        // echo count($data['model'][15]);
        return $this->renderPartial('_form_permit',$data);
    }

    public function actionPermit_added(){
        $menu=Yii::$app->request->post('menu');
        
        $val="";
        foreach ($menu as $menu_id) {
            $val.=" (".$menu_id.",".Yii::$app->request->post('role_id')."),";
        }

        $sql="INSERT IGNORE INTO permissions (menu_id,role_id) VALUES ".rtrim($val,',');
        Permissions::deleteAll(['role_id'=>Yii::$app->request->post('role_id')]);
        Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * Finds the Role model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Role the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
