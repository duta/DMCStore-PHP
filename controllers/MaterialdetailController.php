<?php

namespace app\controllers;

use Yii;
use app\models\MaterialDetail;
use app\models\MaterialDetailSearch;
use app\models\VMaterial;
use app\models\BrSupplier;
use app\models\Forwarder;
use app\models\Kurir;
use app\models\Customers;
use app\models\Marketplace;
use app\models\VOrders;
use app\models\VBk;
use app\models\VPb;
use app\models\VBm;
use app\models\VMaterialStok;
use app\models\Grosir;
use app\models\Dropshipper;
use app\models\StProvince;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\helpers\ArrayHelper;

use app\components\Help;
/**
 * MaterialdetailController implements the CRUD actions for MaterialDetail model.
 */
class MaterialdetailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MaterialDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaterialDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBm()
    {
        $searchModel = new MaterialDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bm', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrders()
    {
        $searchModel = new MaterialDetailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('orders', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single MaterialDetail model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MaterialDetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MaterialDetail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate_bm(){
        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        $r = Yii::$app->request->post('md');
        if($r!=null){
            $transaction=Yii::$app->db->beginTransaction();
            try {
                foreach ($r['qty'] as $id_material => $value) {       
                    $md=new MaterialDetail();   
                    $md->id_material=$id_material;
                    $md->hgsat_dasar=Help::toNumberOnly($r['harga_beli'][$id_material]);
                    $md->ongkir_impor=Help::toNumberOnly($r['ongkir']);
                    $md->hgtot_dasar=Help::toNumberOnly($r['hatot_dasar']);
                    $md->hgtot_beli=Help::toNumberOnly($r['hatot']);
                    $md->harga_mod=Help::toNumberOnly($r['hgmod'][$id_material]);
                    $md->ppn=$r['ppn'];
                    $md->pph=$r['pph'];
                    $md->bea_masuk=$r['bm'];
                    $md->dll=Help::toNumberOnly($r['dll']);
                    $md->id_sup=$r['id_sup'];
                    $md->id_fw=$r['id_fw'];
                    $md->status=3;
                    $md->tipe_transaksi=14;
                    $md->qty=$value;
                    $md->qty_order_bm=$value;
                    $md->created_by=$user['id'];
                    $md->order_at=date('Y-m-d H:i:s');
                    $md->tipe_transfer=9;
                    $md->store_id=$user->store_id;
                    $md->save();
                    $id_inserted[]=$md->id;

                }
                $inv=implode(date('i'),$id_inserted);
                Yii::$app->db->createCommand()->update('material_detail',['kd_tagihan'=>$inv]," id IN (".implode(',',$id_inserted).")")->execute();
                $transaction->commit();
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1,'inv'=>$inv];
            }catch(exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);

        }else{
            $data['Barang']=ArrayHelper::map(VMaterial::find()->where(['store_id'=>$user->store_id])->All(), 'id', 'fullname');
            return $this->renderPartial('_form_bm_od',$data);  
        }
    }

    public function actionEdit_bm(){
        $model=VPb::findOne(Yii::$app->request->post('inv'));
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r = Yii::$app->request->post('md');
        $inv = Yii::$app->request->post('inv');
        if($r!=null){

            $transaction=Yii::$app->db->beginTransaction();
            try {
                foreach ($r['qty'] as $id_md => $value) {       
                    $md=MaterialDetail::findOne($id_md);   
                    $md->hgsat_dasar=Help::toNumberOnly($r['harga_beli'][$id_md]);
                    $md->ongkir_impor=Help::toNumberOnly($r['ongkir']);
                    $md->hgtot_beli=Help::toNumberOnly($r['hatot']);
                    $md->harga_mod=Help::toNumberOnly($r['hgmod'][$id_md]);
                    $md->ppn=$r['ppn'];
                    $md->pph=$r['pph'];
                    $md->bea_masuk=$r['bm'];
                    $md->dll=Help::toNumberOnly($r['dll']);
                    $md->id_sup=$r['id_sup'];
                    $md->id_fw=$r['id_fw'];
                    $md->status=3;
                    $md->tipe_transaksi=14;
                    $md->qty=$value;
                    $md->tipe_transfer=9;
                    $md->save();
                }
             
                $transaction->commit();
                $pesan=['pesan'=>'Pembelian Berhasil di perbarui !','status'=>1];
            }catch(exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Pembelian gagal di perbarui !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);

        }else{
            // echo "<pre>";
            // print_r($model);
            $data['Forwarder']=ArrayHelper::map(Forwarder::find()->where(['store_id'=>$user->store_id])->All(),'id','nm_fw');
            
            $data['Supplier']=ArrayHelper::map(BrSupplier::find()->where(['store_id'=>$user->store_id])->All(),'id','nm_sup');
            return $this->renderPartial('_form_bm_edit',$data);  
        }
    }
    public function actionCreate_od(){
        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        $md = Yii::$app->request->post('md');
        $cm = Yii::$app->request->post('customer');
        $qty = Yii::$app->request->post('qty');
        $inv = Yii::$app->request->post('inv');
        $hj = Yii::$app->request->post('hgsat_jual');
        $hgmod = Yii::$app->request->post('hgmod');
        // print_r($rq->bodyParams);
        // foreach ($md['id_material'] as $id_item) {
        //     echo $md['qty'][$id_item].'//';
        // }

        if($md!=null){
            $transaction=Yii::$app->db->beginTransaction();
            try {
                if($cm['id_c']==null || $cm['id_c']==''){
                    $c=new Customers;
                    $c->nama=ucwords(strtolower($cm['nama']));
                    $c->email=$cm['email'];
                    $c->tlp=$cm['tlp'];
                    $c->prov=ucwords(strtolower($cm['prov']));
                    $c->kota_kab=ucwords(strtolower($cm['kota']));
                    $c->kec=ucwords(strtolower($cm['kec']));
                    $c->alamat=$cm['alamat'];
                    $c->store_id=$user->store_id;
                    $c->save();
                    $c_id=$c->id;
                }else{
                    $c_id=$cm['id_c'];
                }
                foreach ($md['kd_md'] as $id_item) {
                    $kd_md=explode('_',$id_item);

                    $md=new MaterialDetail();   
                    $md->id_material=$kd_md[1];
                    $md->kd_tagihan=$inv[$id_item];
                    $md->buyer=$c_id;
                    $md->ongkir_buyer=Help::toNumberOnly($cm['ongkir_buyer']);
                    $md->hgsat_jual=Help::toNumberOnly($hj[$id_item]);
                    $md->id_market=$cm['id_market'];
                    $md->id_kurir=$cm['id_kurir'];
                    $md->status=0;
                    $md->harga_mod=$hgmod[$id_item];
                    // $md->tipe_transaksi=15 = OUT(KELUAR);
                    $md->tipe_transaksi=15;
                    $md->buyer_note=$cm['c_buyer_note'];
                    $md->qty=$qty[$id_item];
                    $md->created_by=$user['id'];
                    // SEND BY HATOBE is default
                    $md->send_by=1;
                    $md->store_id=$user->store_id;
                    $md->save();
                    $id_inserted[]=$md->id;
                }
                $inv=implode(date('i'),$id_inserted);
                Yii::$app->db->createCommand()->update('material_detail',['kd_order'=>$inv]," id IN (".implode(',',$id_inserted).")")->execute();
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1,'inv'=>$inv];
                $transaction->commit();
                   
                
            }catch(exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        }else{
            $data['Barang']=ArrayHelper::map(VMaterialStok::find()->where('stok_akhir > 0 ')->andWhere(['store_id'=>$user->store_id])->All(), 'kd_md', 'fullname_inv','kd_tagihan_2');
            return $this->renderPartial('_form_bm_od',$data);  
        }
    }
    public function actionEdit_od(){
        $model=VOrders::findOne(Yii::$app->request->post('kd_order'));
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $md = Yii::$app->request->post('md_id');
        $kd_order = Yii::$app->request->post('kd_order');
        if($md!=null){
            $transaction=Yii::$app->db->beginTransaction();
            try {
                
                foreach ( $md as $id) {
                    $model=MaterialDetail::findOne($id);
                    $model->hgsat_jual=Help::toNumberOnly(Yii::$app->request->post('hgsat_jual')[$id]);
                    $model->qty=Yii::$app->request->post('qty')[$id];

                    $model->buyer=Yii::$app->request->post('md')['id_c'];
                    $model->id_market=Yii::$app->request->post('md')['id_market'];
                    $model->id_kurir=Yii::$app->request->post('md')['id_kurir'];
                    $model->ongkir_buyer=Yii::$app->request->post('md')['ongkir_buyer'];
                    $model->save();
                }
             
                $transaction->commit();
                $pesan=['pesan'=>'Penjualan Berhasil di perbarui !','status'=>1];
            }catch(exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Penjualan gagal di perbarui !','status'=>0,'errors'=>'<pre>'.$e];
            }

            // echo "<pre>";
            // print_r($_POST);
            echo json_encode($pesan);

        }else{
            // echo "<pre>";
            $data['Kurir']=ArrayHelper::map(Kurir::find()->where(['<>','id',100])->andWhere(['store_id'=>$user->store_id])->All(),'id','layanan','nm_kurir');
            $data['Market']=ArrayHelper::map(Marketplace::find()->where(['<>','id',100])->andWhere(['store_id'=>$user->store_id])->All(),'id','nm_market');
            $data['Customers']=ArrayHelper::map(Customers::find()->andWhere(['store_id'=>$user->store_id])->All(),'id','nama');

            // print_r($model);
            // $data['Forwarder']=ArrayHelper::map(Forwarder::find()->All(),'id','nm_fw');
            return $this->renderPartial('_form_od_edit',$data);  
        }
    }
    public function actionCreate_gift(){
        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        $md = Yii::$app->request->post('md');
        $cm = Yii::$app->request->post('customer');
        $qty = Yii::$app->request->post('qty');
        $inv = Yii::$app->request->post('inv');
        $hj = Yii::$app->request->post('hgsat_jual');
        // print_r($rq->bodyParams);
        // foreach ($md['id_material'] as $id_item) {
        //     echo $md['qty'][$id_item].'//';
        // }

        if($md!=null){
            $transaction=Yii::$app->db->beginTransaction();
            try {
                if(Yii::$app->request->post('give')!==null){
                    // echo "<pre>";
                    // print_r($_POST);
                     if($cm['id_c']==null || $cm['id_c']==''){
                        $c=new Customers;
                        $c->nama=ucwords(strtolower($cm['nama']));
                        $c->tlp=$cm['tlp'];
                        $c->store_id=$user->store_id;
                        $c->save();
                        $c_id=$c->id;
                    }else{
                        $c_id=$cm['id_c'];
                    }
                     foreach ($md['kd_md'] as $id_item) {
                        $kd_md=explode('_',$id_item);

                        $md=new MaterialDetail();   
                        $md->id_material=$kd_md[1];
                        $md->kd_tagihan=$inv[$id_item];
                        $md->buyer=$c_id;
                        $md->ongkir_buyer=0;
                        $md->hgsat_jual=0;
                        $md->status=4;
                        // $md->tipe_transaksi=15 = OUT(KELUAR);
                        $md->tipe_transaksi=15;
                        $md->qty=$qty[$id_item];
                        $md->is_gived=1;
                        $md->created_by=$user['id'];
                        $md->done_at=date('Y-m-d H-i-s');
                        $md->store_id=$user->store_id;
                        $md->save();
                        $id_inserted[]=$md->id;
                    }
                    Yii::$app->db->createCommand()->update('material_detail',['kd_order'=>implode(date('i'),$id_inserted)]," id IN (".implode(',',$id_inserted).")")->execute();
                    $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];
                }
                $transaction->commit();
            }catch(exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        }else{
            $data['Barang']=ArrayHelper::map(VMaterialStok::find()->where('stok_akhir > 0 ')->All(), 'kd_md', 'fullname_inv','kd_tagihan_2');
            return $this->renderPartial('_form_bm_od',$data);  
        }
    }

    public function actionForm_bm(){
        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        $r=Yii::$app->request->post('id_material');
        if($r!=null && $r!=''){
            $id=($r!='')?$r:[];
            $data['Barang']=VMaterial::findAll($id);
            $data['Supplier']=ArrayHelper::map(BrSupplier::find()->where(['store_id'=>$user->store_id])->All(),'id','nm_sup');
            $data['Forwarder']=ArrayHelper::map(Forwarder::find()->where(['store_id'=>$user->store_id])->All(),'id','nm_fw');
            return $this->renderPartial('_form_bm',$data);
        }
    }

    public function actionForm_od(){
        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r=Yii::$app->request->post('kd_md');
        $give=Yii::$app->request->post('give');
        
        if($r!=null && $r!=''){ 
            $id=($r!='')?$r:[];
            $data['Barang']=VMaterialStok::find()->where('stok_akhir > 0 ')->andWhere(['id'=>$id])->All();
            $data['Customers']=ArrayHelper::map(Customers::find()->andWhere(['store_id'=>$user->store_id])->All(),'id','nama');
                        
            if($give != null){

                return $this->renderPartial('_form_give',$data);
            }else{    
                $data['Province']=ArrayHelper::map(StProvince::find()->All(),'id','province');
                // $data['City']=ArrayHelper::map(City::find()->All(),'id','layanan','nm_kurir');
                $data['Kurir']=ArrayHelper::map(Kurir::find()->where(['<>','id',100])->andWhere(['store_id'=>$user->store_id])->All(),'id','layanan','nm_kurir');
                $data['Market']=ArrayHelper::map(Marketplace::find()->where(['<>','id',100])->andWhere(['store_id'=>$user->store_id])->All(),'id','nm_market');
                
                return $this->renderPartial('_form_od',$data);
            }
        }

    }

    public function actionDel_byinv(){
        MaterialDetail::deleteAll(['kd_tagihan' => Yii::$app->request->post('inv')]);
    }

    public function actionDel_byod(){
        MaterialDetail::deleteAll(['kd_order' => Yii::$app->request->post('inv')]);
    }

    public function actionAcc_bm(){
        MaterialDetail::updateAll(['status'=>4,'accept_at'=>date('Y-m-d')],['kd_tagihan' => Yii::$app->request->post('inv')]);
    }

    public function actionCm_json(){
        $c=Customers::findOne(Yii::$app->request->post('c_id'));
        echo json_encode(ArrayHelper::toArray($c));
    }


    public function actionNota(){
        $pk=Yii::$app->request->post('inv');
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=(Yii::$app->request->post('tr')=='od')?VOrders::findOne($pk):VBk::findOne($pk);
        // echo "<pre>";
        // print_r($data);
       
        return $this->renderPartial("_nota",$data);
    }

    public function actionDetail_tr(){
        $pk=Yii::$app->request->post('inv');
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=(Yii::$app->request->post('tr')=='od')?VOrders::findOne($pk):VBk::findOne($pk);
        return $this->renderPartial('_view',$data);
    }

    public function actionDetail_bm(){
        $pk=Yii::$app->request->post('inv');
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=VPb::findOne($pk);
        // echo "<pre>";
        // print_r($data['model']);
        return $this->renderPartial('_detail_pb',$data);
    }
   
    public function actionPayment(){

        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        if(Yii::$app->request->isAjax){
            if(Yii::$app->request->post('inv') != null){
                $data['model']=VOrders::findOne(Yii::$app->request->post('inv'));
                return $this->renderPartial('_payment_admin',$data);
            }

            if(Yii::$app->request->post('paid') != null){
                $inv=Yii::$app->request->post('paid');
                $paid_note=Yii::$app->request->post('paid_note');
                $filename="";
                $tr=Yii::$app->db->beginTransaction();
                try {
                    if(isset($_FILES['paid_photo'])){
                        $dir="file_uploaded/transaksi/orders/".$inv;
                        $filename=Help::upload($_FILES['paid_photo'],'_paid',$dir);
                    }
                    MaterialDetail::updateAll(['paid_photo'=>$filename,'paid_note'=>$paid_note,'status'=>24],['kd_order' => $inv]);
                    $tr->commit();                   

                    $md=MaterialDetail::findOne($inv);
                    $c=Customers::findOne($md->buyer);
                    $k=Kurir::findOne($md->id_kurir);
                    $html="<h3>Ada Barang Yang Harus Anda Kirimkan Kepada : </h3>";

                    $html.="<b>Nama lengkap : </b>".$c->nama."<br>";
                    $html.="<b>Alamat : </b>".$c->alamat.",".$c->kec.",".$c->kota_kab.",".$c->prov."<br>";
                    $html.="<b>Jasa Pengiriman : </b>".$k->nm_kurir." > ".$k->layanan;
                    $html.="<br><br><br><br>";
                    $html.="<b>Klik link : </b> https://www.hatobe.dmc.zone/vbk/index <b>untuk melakukan aksi lebih lanjut.</b>";
                    $html.="<hr>";
                    $html.="<div style='float:right;font-weight:bold'>Best Regards,<br><u>Crew Hatobe</u></div>";

                    Help::mail("rijaldaebak@gmail.com","Notifikasi Hatobe",$html);

                    $pesan=['pesan'=>'Berhasil melakukan pembayaran !','status'=>1];                    
                }catch(exception $e){
                    $tr->rollback();
                    $pesan=['pesan'=>'Gagal melakukan pembayaran !','status'=>0,'errors'=>$e];
                }
                echo json_encode($pesan,JSON_PRETTY_PRINT);
            }
        }
    }

    public function actionShipping(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        if(Yii::$app->request->isAjax){
            $inv=Yii::$app->request->post('inv');
            if($inv!=null){
                // echo "<pre>";
                // print_r($inv);
                $tr=Yii::$app->db->beginTransaction();
                try {
                    foreach ($inv as $inv => $resi) {
                        $filename[$inv]=null;
                        if(isset($_FILES['resi_photo_'.$inv])){
                            $dir="file_uploaded/no_resi/".$inv;
                            $filename[$inv]=Help::upload($_FILES['resi_photo_'.$inv],'_resi',$dir);
                        }
                        // echo $inv.'='.$resi['resi'].'<br>';

                        // echo "<pre>";
                        // print_r();

                        MaterialDetail::updateAll(
                            [
                                'resi'=>$resi['resi'],
                                'resi_photo'=>$filename[$inv],
                                'status'=>3 ,
                                'sent_at'=>date('Y-m-d H:i:s') ,
                                'read'=>0
                            ],['kd_order' => $inv]);

                    }
                    
                    $tr->commit(); 
                    $pesan=['pesan'=>"Berhasil dikirim !",'status'=>1];
                } catch (Exception $e) {
                    $pesan=['pesan'=>'Gagal dikirim !','status'=>2];
                    $tr->rollback();
                }
                echo json_encode($pesan);
            }else{
                $checked=Yii::$app->request->get('selection');
                $data['models']=VBk::find()->where(['id_status'=>24,'kd_order'=>$checked])->all();
                return $this->renderPartial("_form_shipping",$data);
            }
        }
    }
    public function actionTr_done(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        if(Yii::$app->request->isAjax){
            $inv=Yii::$app->request->post('selection');
            if($inv!=null){
                $tr=Yii::$app->db->beginTransaction();
                try {

                    foreach ($inv as $kd_inv) {
                        MaterialDetail::updateAll(
                            [
                                'status'=>4 ,
                                'done_at'=>date('Y-m-d H:i:s') ,
                                'read'=>0
                            ],['kd_order' => $kd_inv]);

                    }
                    $tr->commit(); 
                    $pesan=['pesan'=>"Transaksi Selesai !",'status'=>1];
                } catch (Exception $e) {
                    $tr->rollback();
                    $pesan=['pesan'=>'Transaksi Gagal !','status'=>2];
                }
                echo json_encode($pesan);
            }
        }
    }
        public function actionAcc_direct(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;

        $inv=Yii::$app->request->post('inv');
        $vbm=VBm::findOne($inv);  

        $tr=Yii::$app->db->beginTransaction();
        try {
            foreach ($vbm->md as $mdl) {
                $id_md=$mdl->id;
                $qty=$mdl->qty;

                if($qty != null && $qty != ''){

                    // UPDATING
                    $model=MaterialDetail::findOne($id_md);
                    if(intval($model->qty_order_bm)==intval($qty)){
                        $model->status=4;
                    }else{
                        $model->qty=$model->qty-$qty;
                    }
                    $model->save();

                    if($model->status==3){
                        if($model->qty<1){
                            MaterialDetail::findOne($id_md)->delete();
                        }
                        // INSERT NEW Record
                        $md=new MaterialDetail;
                        $md->id_material=$model->id_material;
                        $md->hgsat_dasar=$model->hgsat_dasar;
                        $md->ongkir_impor=$model->ongkir_impor;
                        $md->dll=$model->dll;
                        $md->hgtot_dasar=$model->hgtot_dasar;
                        $md->hgtot_beli=$model->hgtot_beli;
                        $md->harga_mod=$model->harga_mod;
                        $md->ppn=$model->ppn;
                        $md->pph=$model->pph;
                        $md->bea_masuk=$model->bea_masuk;
                        $md->id_sup=$model->id_sup;
                        $md->tipe_transaksi=$model->tipe_transaksi;
                        // Changing status & qty & qty_order_bm, + accept_at ONLY
                        $md->status=4;
                        $md->qty=$qty;
                        $md->qty_order_bm=$model->qty_order_bm;
                        $md->accept_at=date('Y-m-d',strtotime(Yii::$app->request->post('accept_at')));
                        $md->kd_tagihan=$model->kd_tagihan;
                        $md->created_by=$model->created_by;
                        $md->order_at=$model->order_at;
                        $md->tipe_transfer=$model->tipe_transfer;
                        $md->save();
                    }
                }else{
                    // ACC ALL BM
                    $model=MaterialDetail::findOne($id_md);
                    $md->status=4;
                    $model->save();
                }
                
            }

            $tr->commit();
            $pesan=['pesan'=>"Berhasil diterima !",'status'=>1];
        } catch (Exception $e) {
            $tr->rollback();
            $pesan=['pesan'=>'Gagal diterima !','status'=>2];
        }
        echo json_encode($pesan);
            
        

    }
    public function actionAcc_some(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;

        $inv=Yii::$app->request->post('inv');
        $md_post=Yii::$app->request->post('md');
        if($inv!=null){
            $data['model']=VBm::findOne($inv);
            return $this->renderPartial('_form_acc_some',$data);
        }else{
            if($md_post!=null){
                $tr=Yii::$app->db->beginTransaction();
                try {
                    foreach ($md_post as $id_md => $qty) {
                        if($qty != null && $qty != ''){

                            // UPDATING
                            $model=MaterialDetail::findOne($id_md);
                            if(intval($model->qty_order_bm)==intval($qty)){
                                $model->status=4;
                            }else{
                                $model->qty=$model->qty-$qty;
                            }
                            $model->save();

                            if($model->status==3){
                                if($model->qty<1){
                                    MaterialDetail::findOne($id_md)->delete();
                                }
                                // INSERT NEW Record
                                $md=new MaterialDetail;
                                $md->id_material=$model->id_material;
                                $md->hgsat_dasar=$model->hgsat_dasar;
                                $md->ongkir_impor=$model->ongkir_impor;
                                $md->dll=$model->dll;
                                $md->hgtot_dasar=$model->hgtot_dasar;
                                $md->hgtot_beli=$model->hgtot_beli;
                                $md->harga_mod=$model->harga_mod;
                                $md->ppn=$model->ppn;
                                $md->pph=$model->pph;
                                $md->bea_masuk=$model->bea_masuk;
                                $md->id_sup=$model->id_sup;
                                $md->tipe_transaksi=$model->tipe_transaksi;
                                // Changing status & qty & qty_order_bm, + accept_at ONLY
                                $md->status=4;
                                $md->qty=$qty;
                                $md->qty_order_bm=$model->qty_order_bm;
                                $md->accept_at=date('Y-m-d',strtotime(Yii::$app->request->post('accept_at')));
                                $md->kd_tagihan=$model->kd_tagihan;
                                $md->created_by=$model->created_by;
                                $md->order_at=$model->order_at;
                                $md->tipe_transfer=$model->tipe_transfer;
                                $md->save();
                            }
                        }else{
                            // ACC ALL BM
                            $model=MaterialDetail::findOne($id_md);
                            $md->status=4;
                            $model->save();
                        }
                        
                    }

                    $tr->commit();
                    $pesan=['pesan'=>"Berhasil diterima !",'status'=>1];
                } catch (Exception $e) {
                    $tr->rollback();
                    $pesan=['pesan'=>'Gagal diterima !','status'=>2];
                }
                echo json_encode($pesan);
            }
        }

    }



    public function actionChangevarian()
    {
        $r=Yii::$app->request;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        if($r->post('varian')!=null){
            // echo "<pre>";
            // print_r($_FILES['varian']);
            $transaction=Yii::$app->db->beginTransaction();

            try {
                
                $filename='not_set.jpg';
                if(isset($_FILES['varian'])){
                    foreach ($r->post('varian') as $m_id) {
                        $fileArr=explode('.', $_FILES['varian']['name'][$m_id]);
                        $ext=end($fileArr);
                        $name=md5($fileArr[0].date('Y-m-d H:i:s').$user['id']);
                        $filename=$name.".".$ext;
                        // echo $filename."<br>";
                        move_uploaded_file($_FILES['varian']['tmp_name'][$m_id],"file_uploaded/material/".$filename);

                        $model=Material::findOne($m_id);
                        $model->foto=$filename;
                        $model->save();

                        // echo "<pre>";
                        // print_r($fileArr);
                    }
                    $transaction->commit();
                }
                $pesan=['pesan'=>'Foto Berhasil di perbarui !','status'=>1];            
            }catch (Exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Foto gagal di perbarui !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);
        }else{
            $data['model']=VMaterialgroup::findOne($r->post('kd'));
            $data['attr']=ArrayHelper::map(ItemAttr::find()->All(),"id","nm_attr");
            $data['varian']=ArrayHelper::map(ItemVarian::find()->where(['id_attr'=>$data['model']->id_attr])->All(),"id","nm_varian");
            
            // $varian=[];
            // $var_val=[];
            // foreach ($data['model']->v_material as $item) {
            //     $var_val[]=$item->id_varian;
            //     $varian[]=$item->varian['id']=$item->varian['nm_varian'];
            //     // echo $item->varian['nm_varian'];
            // }
            // $data['varian']=$varian;
            // $data['var_val']=$var_val;
            // echo "<pre>";
            // print_r($varian);
            return $this->renderPartial('_form_varian',$data);
       
        }
    }

    public function actionLoaditemvarian(){
        $r=Yii::$app->request;
        
        $data=[];
        $data['model']=VMaterialgroup::findOne($r->post('kd'));
        $data['varian']=ItemVarian::find()->where(['id'=>$r->post('varian')])->all();
        
        echo "<pre>";
        print_r($data['varian']);
        // return $this->renderPartial('_load_item_varian',$data);
    }

   
    public function actionVariantable(){
        $r=Yii::$app->request;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        if($r->post('inv')!=null){
            $data['model']=VMaterialStok::find()->where(['kd_tagihan'=>$r->post('inv')])->all();
            return $this->renderPartial('_list_varian',$data);
        }
    }
    
    public function actionChangeprice()
    {
        $r=Yii::$app->request;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        
        if($r->post('price')!=null){
            $g=$r->post('grosir');

            $transaction=Yii::$app->db->beginTransaction();
            try{
                $model=MaterialDetail::findOne($r->post('price')['id']);
                $model->hgsat_retail=Help::toNumberOnly($r->post('price')['hgsat_retail']);
                // $model->hgsat_reseller=Help::toNumberOnly($r->post('price')['hgsat_reseller']);
                if($r->post('new_qty')[0]!=''){
                    $model->is_grosir=1;
                }
                
                if($model->save()){
                
                    if($r->post('set_qty')!=null && $r->post('set_price')!=null){
                        foreach ($r->post('set_qty') as $id => $qty) {
                            $m_set=Grosir::findOne($id);
                            $m_set->qty=$qty;
                            $m_set->harga=Help::toNumberOnly($r->post('set_price')[$id]);
                            $m_set->save();
                        }
                    }

                    if($r->post('new_qty')[0]!=''){
                        $i=0;
                        foreach ($r->post('new_qty') as $qty) {
                            $m_new=new Grosir;
                            $m_new->id_md=$model->id;
                            $m_new->qty=$qty;
                            $m_new->harga=Help::toNumberOnly($r->post('new_price')[$i]);
                            $m_new->save();
                            $i++;
                        }
                    }
                }


                $transaction->commit();
                $pesan=['pesan'=>'Harga berhasil diperbarui !','status'=>1,'inv'=>$model->kd_tagihan];
                // echo "<pre>";
                // print_r($model);            
            }catch (Exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Harga gagal diperbarui !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);

        }else{
            $data['model']=VMaterialStok::findOne($r->post('id'));
            // $data['attr']=ArrayHelper::map(ItemAttr::find()->All(),"id","nm_attr");
            return $this->renderPartial('_form_price',$data);
            // echo "<pre>";
            // print_r($data['model']);
        }
    }

    public function actionDelgrosir(){
        Grosir::findOne(Yii::$app->request->post('id'));
    }
    /**
     * Updates an existing MaterialDetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionTrcancel(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;

        if(Yii::$app->request->post('cancel_note')!==null){
            try {
                MaterialDetail::updateAll(
                    ['status'=>2,
                    'cancel_note'=>Yii::$app->request->post('cancel_note')],
                    ['kd_order'=>Yii::$app->request->post('kd_order')]);
                $pesan=['pesan'=>'Transaksi berhasil dibatalkan !','status'=>1,Yii::$app->request->post('kd_order')];    
            } catch (Exception $e) {        
                $pesan=['pesan'=>'Transaksi gagal dibatalkan !','status'=>0,Yii::$app->request->post('kd_order')];    
            }
            echo json_encode($pesan);
        }else{
            $data['kd_order']=Yii::$app->request->post('kd');
            return $this->renderPartial('_form_cancel_note',$data);
        }
    }

    public function actionRetur(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;

        if(Yii::$app->request->post('retur_note')!==null){
            try {
                MaterialDetail::updateAll([
                    'status'=>16,
                    'retur_note'=>Yii::$app->request->post('retur_note')
                ],['kd_order'=>Yii::$app->request->post('kd_order')]);

                $pesan=['pesan'=>'Transaksi sedang proses retur !','status'=>1,Yii::$app->request->post('kd_order')];    
            } catch (Exception $e) {        
                $pesan=['pesan'=>'Retur gagal  !','status'=>0,Yii::$app->request->post('kd_order')];    
            }
            echo json_encode($pesan);
        }else{
            $data['kd_order']=Yii::$app->request->post('kd');
            return $this->renderPartial('_form_retur_note',$data);
        }
    }

    public function actionReturacc(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;

        if(Yii::$app->request->post('tipe_retur')!==null){
            try {
                // ganti barang baru
                if(Yii::$app->request->post('tipe_retur')==30){
                    MaterialDetail::updateAll([
                        'status'=>24,
                        'resi'=>null,
                        'tipe_retur'=>Yii::$app->request->post('tipe_retur')
                    ],['kd_order'=>Yii::$app->request->post('kd_order')]);
                }else{
                    MaterialDetail::updateAll([
                        'status'=>25,
                        'tipe_retur'=>Yii::$app->request->post('tipe_retur')
                    ],['kd_order'=>Yii::$app->request->post('kd_order')]);
                }
                $pesan=['pesan'=>'Transaksi retur selesai!','status'=>1,Yii::$app->request->post('kd_order')];    
            } catch (Exception $e) {        
                $pesan=['pesan'=>'Transksi retur gagal  !','status'=>0,Yii::$app->request->post('kd_order')];    
            }
            echo json_encode($pesan);
        }else{
            $data['kd_order']=Yii::$app->request->post('kd');
            return $this->renderPartial('_form_acc_retur',$data);
        }
    }

    public function actionShip_print_loop(){
        $data['model']=VBk::findAll(Yii::$app->request->post('selection'));
        return $this->renderPartial('_nota_shipping',$data);
    }

    public function actionSenderchanged(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;

        if(Yii::$app->request->post('ds')!==null){
            if(Yii::$app->request->post('ds')['ds_found']!=null){
                MaterialDetail::updateAll([
                        'send_by'=>Yii::$app->request->post('ds')['ds_found'],
                    ],['kd_order'=>Yii::$app->request->post('kd_order')]);    
            }else{
                $model=new Dropshipper;
                $model->nm_ds=Yii::$app->request->post('nm_ds');
                $model->tlp=Yii::$app->request->post('tlp');
                $model->save();

                MaterialDetail::updateAll([
                        'send_by'=>$model->id,
                    ],['kd_order'=>Yii::$app->request->post('kd_order')]);    
            }
        }else{
            $data['kd_order']=Yii::$app->request->post('kd');
            $data['Dropshipper']=ArrayHelper::map(Dropshipper::find()->where(['not in','id',1])->all(),'id','nm_ds');
            $data['model']=Dropshipper::findOne(1);
            return $this->renderPartial('_form_sending',$data);
        }
    }
    /**
     * Deletes an existing MaterialDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MaterialDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return MaterialDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaterialDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
