<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;




/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r=Yii::$app->request->post('User');
        if ($r!= null) {
            $model->fullname=$r['fullname'];
            $model->username=$r['username'];
            $model->email=$r['email'];
            $model->password=Yii::$app->getSecurity()->generatePasswordHash($r['password']);
            $model->id_role=$r['id_role'];
            $model->authKey=Yii::$app->security->generateRandomString();
            $model->store_id=$user->store_id;
            if($model->save()){
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];
            }else{
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r=Yii::$app->request->post('User');
        if ($r!= null) {
            $model->fullname=$r['fullname'];
            $model->username=$r['username'];
            $model->email=$r['email'];
            $model->id_role=$r['id_role'];
            if($model->save()){
                $pesan=['pesan'=>'Data Berhasil di update !','status'=>1];
            }else{
                $pesan=['pesan'=>'Data gagal di di update !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        } else {
            
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
