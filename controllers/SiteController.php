<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\MaterialDetail;
use app\models\VMaterial;
use linslin\yii2\curl;

use app\components\AuthHandler;
use app\components\Help;
use yii\helpers\Security;


use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','get'],
                ],
            ],

            // 'LoggableBehavior' => [
                // 'class' => 'sammaye\audittrail\LoggableBehavior',
            //     'ignored' => ['some_field'], // This ignores fields from a selection of all fields, not needed with allowed
            //     'allowed' => ['another_field'] // optional, not needed if you use ignore
            // ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        // echo "<p
        (new AuthHandler($client))->handle();
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->render('index');
        }else{
            return $this->redirect(['home/index']);
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->renderPartial('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
        // echo "hai";
    }
    public function actionGenerate_pass(){
        echo Yii::$app->getSecurity()->generatePasswordHash('aisyah_bydmc');
    }
    public function actionRegister(){
        if(Yii::$app->request->post('reg') !== null){
            // echo "<pre>";
            // print_r($_POST);
            $reg=Yii::$app->request->post('reg');
            $model=new User;
            $model->username=$reg['username'];
            $model->password=Yii::$app->getSecurity()->generatePasswordHash($reg['cpass']);
            $model->email=$reg['email'];
            $model->authKey=Yii::$app->getSecurity()->generateRandomString();
            $model->accessToken=Yii::$app->getSecurity()->generateRandomString(10);
            $model->fullname=$reg['fname'];
            $model->phone=$reg['phone'];
            $model->active=0;
            $model->id_role=4;
            $model->save();
            // if($model->save()){
                // return $this->redirect(['home/index']);
            // }


        }else{
            return $this->renderPartial('register');
        }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionTes(){
        echo Help::toRp('20000');
    }

    public function actionChangepass(){
        $user=Yii::$app->user->identity;
        if(Yii::$app->request->isAjax){
            if(Yii::$app->request->post('new_pass')){
                $model=User::findOne($user->id);
                $model->password=Yii::$app->getSecurity()->generatePasswordHash(Yii::$app->request->post('new_pass'));
                $model->save();
            }else{
                return $this->renderPartial('_form_change_pass');
            }
        }
    }
    public function actionSay(){

        // $model=ArrayHelper::getColumn(VMaterial::find()->where(['kode'=>4])->with('varian')->all(),'varian.nm_varian');
        // $hash = Yii::$app->getSecurity()->generateRandomString(10);
        // echo $hash;
        // $a=[
        //     'customers-nama'=>['Nama cannot be blank.'],
        //     'customers-pos'=>['Pos must be an integer.']
        // ];

// 
        // echo "<pre>";
        // print_r($model);
        // $errors=[];
        // $i=1;
        // foreach ($a as $key => $value) {
        //     foreach ($value as $error) {
        //         $errors[]=$i.'. '.$error;   
        //         $i++;
        //     }
        // }
        // $cc=implode($errors,'<br>');
        // print_r(['errors'=>$cc]);


//         echo '<html><div id="showBarcode"></div></html> ';
//         $optionsArray = array(
// 'elementId'=> 'showBarcode', /* div or canvas id*/
// 'value'=> '4797001018719',  value for EAN 13 be careful to set right values for each barcode type 
// 'type'=>'ean13',/*supported types  ean8, ean13, upc, std25, int25, code11, code39, code93, code128, codabar, msi, datamatrix*/
 
// );
        
        // var_dump(BarcodeGenerator::widget($optionsArray));

        // echo Help::inisial('PT DUTA MEDIA CIPTA');
            // set_time_limit(500);
        
        // echo Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
        // $response = $curl->setPostParams([
        //     'Id'=>'11',
        //     'Stock'=>'40',
        // ])->put('http://35.227.171.221/StockPublish/api/ItemAPI/');
        // $response = $curl->get('http://35.227.171.221/StockPublish/api/ItemAPI/10');
        // if ($curl->errorCode === null) {
            // echo "<pre>";
            // echo $response;
        // } else {
            // echo $response;
            // print_r($curl);
             // List of curl error codes here https://curl.haxx.se/libcurl/c/libcurl-errors.html
            // print_r($curl->errorCode);
            // switch ($curl->errorCode) {
            
            //     case 6:
            //         //host unknown example
            //         break;
            // }
        // }
        // $user=Yii::$app->user->identity;

        // $marketplace='Tokopedia';
        // $username_store="rijalboy98@gmail.com";
        // $store_url='https://www.tokopedia.com/store98';
        // $rest="api/ItemAPI";



        //     $curl = new curl\Curl();
        //     $response = $curl->setPostParams([
        //         'Id'=>'11',
        //         'Stock'=>'10',
        //     ])->put('http://35.227.171.221/StockPublish/'.$rest);

        //     // $response = $curl->setPostParams([
        //     //     'Marketplace'=>'Tokopedia',  
        //     //     'StoreUrl'=>'https://www.tokopedia.com/store98',  
        //     //     'Username'=>'rijalboy98@gmail.com',  
        //     //     'Password'=>'qwe123*+()',
        //     //     'UserId'=>'17',
        //     // ])->post('http://35.227.171.221/StockPublish/'.$rest);
        //     if($curl->errorCode === null) {
        //         echo $response;
        //     }else {
        //         switch ($curl->errorCode) {
                
        //             case 6:
        //                 //host unknown example
        //                 break;
        //         }
        //         // $sql="INSERT INTO error (marketplace,id_user,username_store,store_url,rest,error_text) 
        //         // VALUES ('".$marketplace."',".$user->id.",'".$username_store.",'".$store_url."','".$rest."','".$e."') ";
        //         // echo $sql;
        //     }
                // echo "<pre>";
                // print_r(curl_error($curl));
            // 
            // print_r($e);





        // $hash = Yii::$app->getSecurity()->generatePasswordHash('cs');
        // $hash = Yii::$app->getSecurity()->generatePasswordHash('agus');
        $pass = Yii::$app->getSecurity()->generatePasswordHash('123');
        // if (Yii::$app->getSecurity()->validatePassword($pass, $hash)) {
        //     echo " all good, logging user in <br>";
        // } else {
        //     echo "wrong password <br>";
        // }
        echo $pass;
        // $ses=Yii::$app->user->identity;
        // $obj=MaterialDetail::AttributeLabels();        
        // $obj=MaterialDetail::kd_tr();
        // echo "<br>_".$ses['username'];
        // echo Yii::$app->request->csrfToken;
        // $obj="";
        // echo "<pre>";
        // print_r($obj);

        // if($obj===null){
        //     echo "null";
        // }else{
        //     echo "sadas";
        // }
        // echo $hash;
        // echo Yii::$app->help->toNumberOnly(sha1(md5($hash)));
        // echo "#".Yii::$app->getSecurity()->generateRandomString(5);

    }
}
