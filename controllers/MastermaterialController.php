<?php

namespace app\controllers;

use Yii;
use app\models\MasterMaterial;
use app\models\MasterMaterialSearch;
use app\models\Material;
use app\models\VMaterial;
use app\models\BrKtg;
use app\models\ItemAttr;
use app\models\ItemVarian;
use app\models\Grosir;
use app\models\Varian;
use app\models\User;

use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use \moonland\phpexcel\Excel;


use app\components\Help;

/**
 * MastermaterialController implements the CRUD actions for MasterMaterial model.
 */
class MastermaterialController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterMaterial models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterMaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);      
    }

    /**
     * Displays a single MasterMaterial model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterMaterial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model=new MasterMaterial;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $m=Yii::$app->request->post('Material');
        $g=Yii::$app->request->post('grosir');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($m!=null)
        {
            // echo "<pre>";
            // print_r($m);
            $transaction=Yii::$app->db->beginTransaction();
            try{                
                $filename='not_set.png';
                if(isset($_FILES['Material_file'])){
                    $fileArr=explode('.', $_FILES['Material_file']['name']);
                    $ext=end($fileArr);
                    $name=md5($fileArr[0].date('Y-m-d H:i:s').$user['id']);
                    $filename=$name.".".$ext;
                    move_uploaded_file($_FILES['Material_file']['tmp_name'],"file_uploaded/material/".$filename);
                }
                $model=new MasterMaterial;
                $model->kode_warna =Help::get_random_color();
                $model->nama =$m['nama'];
                $model->id_ktg =$m['id_ktg'];
                $model->berat =$m['berat'];
                $model->satuan_berat =$m['satuan_berat'];              
                $model->keterangan =$m['keterangan'];
                $model->created_by = $user['id'];
                $model->is_dropship = isset($m['is_dropship'])?1:0;
                $model->store_id = $user->store_id;
                $model->save();
                $kode=$model->kode;
                

                if(isset($m['varian'])){
                    foreach ($m['varian'] as $varian) {
                        $model=new Material;
                        $model->kode=$kode;
                        $model->created_by = $user['id'];
                        $model->foto = $filename;
                        $model->id_attr = $m['attr'];
                        $model->id_varian = $varian;
                        $model->store_id = $user->store_id;

                        $model->save();
                    }
                }
                // Yii::$app->db->createCommand()->update('material',['kode'=>implode(date('i'),$id_inserted)]," id IN (".implode(',',$id_inserted).")")->execute();
                

                // if($g['jml'][0]!=''){
                //     $model->hg=1;
                // }
                
                // if($model->save()){
                //     $i=0;
                //     $id_brg=$model->id;
                //     if($g['jml'][0]!=''){
                        
                //         foreach ($g['jml'] as $item) {
                //             // echo $item.'/';
                //             // echo $g['hg'][$i].'<br>';
                //             $sql="INSERT INTO br_grosir(id_brg,jml,hrg) VALUES (".$id_brg.",".$item.",".Yii::$app->help->toNumberOnly($g['hg'][$i]).") ";
                //             Yii::$app->db->createCommand($sql)->execute();
                //             $i++;
                //         }
                //     }
                // }
                $transaction->commit();
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];           
            }catch (Exception $e){
                $transaction->rollback();
                echo $e;
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);

        }else{
            $data['ktg']=BrKtg::find()->All();
            $data['attr']=ItemAttr::find()->where(['store_id'=>$user->store_id])->All();
            return $this->renderPartial('_form',$data);
            // echo "<pre>";
            // print_r($data);
            
        }
    }

    /**
     * Updates an existing MasterMaterial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = MasterMaterial::findOne($id);

        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $m=Yii::$app->request->post('Material');

        if($m!=null)
        {
            $transaction=Yii::$app->db->beginTransaction();
            try{
                $filename='not_set.png';
                if(isset($_FILES['Material_file'])){
                    $fileArr=explode('.', $_FILES['Material_file']['name']);
                    $ext=end($fileArr);
                    $name=md5($fileArr[0].date('Y-m-d H:i:s').$user['id']);
                    $filename=$name.".".$ext;
                    move_uploaded_file($_FILES['Material_file']['tmp_name'],"file_uploaded/material/".$filename);
                }

                $model=MasterMaterial::findOne($id);
                $model->nama =$m['nama'];
                $model->id_ktg =$m['id_ktg'];
                $model->berat =$m['berat'];
                $model->satuan_berat =$m['satuan_berat'];              
                $model->keterangan =$m['keterangan'];
                $model->created_by = $user['id'];
                $model->is_dropship = isset($m['is_dropship'])?1:0;

                $model->save();
                $kode=$model->kode;
                

                if(isset($m['varian'])){
                    Material::deleteAll('kode='.$kode);
                    foreach ($m['varian'] as $varian) {
                        

                        $model=new Material;
                        $model->kode=$kode;
                        $model->created_by = $user['id'];
                        $model->foto = $filename;
                        $model->id_attr = $m['attr'];
                        $model->id_varian = $varian;
                        $model->store_id = $user->store_id;

                        $model->save();
                    }
                }
                $transaction->commit();
                $pesan=['pesan'=>'Data Berhasil di edit !','status'=>1];            
            }catch (Exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Data gagal di edit !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);

        }else{

            
            $data['ktg']=BrKtg::find()->All();
            $data['attr']=ItemAttr::find()->where(['store_id'=>$user->store_id])->All();
            if(count($model->v_material)>0){
                $data['varian']=ItemVarian::find()->where(['id_attr'=>$model->v_material[0]->id_attr,'store_id'=>$user->store_id])->All();
            }else{
                $data['varian']=ItemVarian::find()->where(['store_id'=>$user->store_id])->All();
            }
            return $this->renderPartial('_form',$data);

            // echo "<pre>";
            // ArrayHelper::getColumn($model->v_material,'id_varian');
            // echo "<pre>";
            // print_r($data['varian']);


        }
    }

    /**
     * Deletes an existing MasterMaterial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Material::deleteAll(['kode'=>$id]);
        // return $this->redirect(['index']);
    }

    public function actionChangephoto()
    {
        $r=Yii::$app->request;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        if($r->post('varian')!=null){
            // echo "<pre>";
            // print_r($_FILES['varian']);
            $transaction=Yii::$app->db->beginTransaction();

            try {
                
                $filename='not_set.jpg';
                if(isset($_FILES['varian'])){
                    foreach ($r->post('varian') as $m_id) {
                        $fileArr=explode('.', $_FILES['varian']['name'][$m_id]);
                        $ext=end($fileArr);
                        $name=md5($fileArr[0].date('Y-m-d H:i:s').$user['id']);
                        $filename=$name.".".$ext;
                        // echo $filename."<br>";
                        move_uploaded_file($_FILES['varian']['tmp_name'][$m_id],"file_uploaded/material/".$filename);

                        $model=Material::findOne($m_id);
                        $model->foto=$filename;
                        $model->save();

                        // echo "<pre>";
                        // print_r($fileArr);
                    }
                    $transaction->commit();
                }
                $pesan=['pesan'=>'Foto Berhasil di perbarui !','status'=>1];            
            }catch (Exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Foto gagal di perbarui !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);
        }else{
            $data['model']=MasterMaterial::findOne($r->post('kd'));
            $data['attr']=ArrayHelper::map(ItemAttr::find()->All(),"id","nm_attr");
            // $data['varian']=ArrayHelper::map(ItemVarian::find()->where(['id_attr'=>$data['model']->v_material->id_attr])->All(),"id","nm_varian");

            return $this->renderPartial('_form_photo',$data);
        }
    }

    public function actionViewphoto()
    {
        $r=Yii::$app->request;
        $data['model']=MasterMaterial::findOne($r->post('kd'));
        return $this->renderPartial('_view_photo',$data);
    }

    public function actionEx_xls(){
        $data=MasterMaterial::qexcel();
        // echo "<pre>";
        // print_r($data);

        Excel::export(
        [
                'models' => $data, 
                'fileName' => 'items_(exported at '.date('d-m-Y H.i').').xlsx', 
                'asAttachment' => true,
                'columns' => ['kategori','nama','varian','macam'],
                'headers' => ['kategori'=>'KATEGORI','nama' => 'BARANG','varian'=>'JENIS_VARIAN','macam'=>'VARIAN'],
        ]);
    }
    public function actionImp_xls(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(Yii::$app->request->isPost){
            $fl=$_FILES;
            // echo "start";
            $data = Excel::import($fl['xls_file']['tmp_name']);
            $tr=Yii::$app->db->beginTransaction();
            // $a=['nama'=>'BARANG','imp_ktg'=>'KATEGORI'];
            // echo Help::echoarr($data);
            $inject=[];
            foreach ($data as $row => $values) {
                foreach ($values as $key => $val) {
                    $inject[$row][$key]=$val;
                    $inject[$row]['kode_warna']=Help::get_random_color();
                }
            }
            // echo Help::echoarr($inject);
            try {
                Help::batch_insert("br_ktg",['nm_ktg'=>'KATEGORI'],$inject);
                Help::batch_insert("master_material",['nama'=>'BARANG','byimp_ktg'=>'KATEGORI','kode_warna'=>'kode_warna'],$inject);
                Help::batch_insert("item_attr",['nm_attr'=>'JENIS_VARIAN'],$inject);
                Help::batch_insert("item_varian",['nm_varian'=>'VARIAN','byimp_attr'=>'JENIS_VARIAN'],$inject);
                Help::batch_insert("material",['byimp_attr'=>'JENIS_VARIAN','byimp_varian'=>'VARIAN','byimp_mastermaterial'=>'BARANG'],$inject);

                $sql="UPDATE master_material mm
                    INNER JOIN br_ktg ktg ON ktg.nm_ktg = mm.byimp_ktg 
                    SET mm.id_ktg = ktg.id WHERE mm.byimp_ktg IS NOT NULL;

                    UPDATE master_material mc SET mc.byimp_ktg=NULL;";
                Yii::$app->db->createCommand($sql)->execute();

                $sql="UPDATE item_varian ivar
                    INNER JOIN item_attr attr ON attr.nm_attr = ivar.byimp_attr 
                    SET ivar.id_attr = attr.id 
                    WHERE
                        ivar.byimp_attr IS NOT NULL;
                        
                    UPDATE item_varian 
                    SET byimp_attr = NULL;";
                Yii::$app->db->createCommand($sql)->execute();

                $sql="UPDATE item_varian ivar
                    INNER JOIN item_attr attr ON attr.nm_attr = ivar.byimp_attr 
                    SET ivar.id_attr = attr.id 
                    WHERE
                        ivar.byimp_attr IS NOT NULL;
                        
                    UPDATE item_varian 
                    SET byimp_attr = NULL;";
                Yii::$app->db->createCommand($sql)->execute();
                                
                $sql="UPDATE material m
                    INNER JOIN item_attr attr ON attr.nm_attr = m.byimp_attr
                    INNER JOIN item_varian ivar ON ivar.id_attr = attr.id 
                    AND ivar.nm_varian = m.byimp_varian
                    INNER JOIN master_material mm ON mm.nama = m.byimp_mastermaterial 
                    SET m.id_attr = attr.id,
                    m.id_varian = ivar.id,
                    m.kode = mm.kode,
                    -- m.foto = 'not_set.png',
                    m.store_id = 1 
                    WHERE
                        m.byimp_attr IS NOT NULL;         
                        
                    UPDATE material
                    SET byimp_attr = NULL,byimp_varian=NULL,byimp_mastermaterial=NULL;";
                Yii::$app->db->createCommand($sql)->execute();
                
                $tr->commit();
                return ['pesan'=>"Import sukses !"];
            } catch (Exception $e) {
                $tr->rollback();
                return $e;
            }
            
        }else{
            return $this->renderPartial('_form_import');
        }     
    }

    /**
     * Finds the MasterMaterial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterMaterial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterMaterial::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
