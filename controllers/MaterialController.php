<?php

namespace app\controllers;

use Yii;
use app\models\Material;
use app\models\VMaterial;
use app\models\VMaterialgroup;
use app\models\MaterialSearch;
use app\models\BrKtg;
use app\models\ItemAttr;
use app\models\ItemVarian;
use app\models\Grosir;
use app\models\VMaterialStok;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use yii\data\ActiveDataProvider;

use app\components\AuthHandler;
use app\components\Help;



/**
 * MaterialController implements the CRUD actions for Material model.
 */
class MaterialController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all Material models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaterialSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Material model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $data=[];
        $data['user']=Yii::$app->user->identity;
        $data['item']=VMaterial::findOne($id);
        return $this->renderPartial('_view',$data);

        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
    }

    /**
     * Creates a new Material model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Material();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }

    // }

    public function actionCreate()
    {
        $model=new Material;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $m=Yii::$app->request->post('Material');
        $g=Yii::$app->request->post('grosir');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if($m!=null)
        {
            // echo "<pre>";
            // print_r($m);
            $transaction=Yii::$app->db->beginTransaction();
            try{                
                $filename='not_set.jpg';
                if(isset($_FILES['Material_file'])){
                    $fileArr=explode('.', $_FILES['Material_file']['name']);
                    $ext=end($fileArr);
                    $name=md5($fileArr[0].date('Y-m-d H:i:s').$user['id']);
                    $filename=$name.".".$ext;
                    move_uploaded_file($_FILES['Material_file']['tmp_name'],"file_uploaded/material/".$filename);
                }
                foreach ($m['varian'] as $varian) {
                    $model=new Material;
                    $model->nama =$m['nama'];
                    $model->id_ktg =$m['id_ktg'];
                    $model->berat =$m['berat'];
                    $model->satuan_berat =$m['satuan_berat'];              
                    $model->keterangan =$m['keterangan'];
                    $model->created_by = $user['id'];
                    $model->foto = $filename;
                    $model->id_attr = $m['attr'];
                    $model->id_varian = $varian;
                    $model->save();
                    $id_inserted[]=$model->id;

                }
                Yii::$app->db->createCommand()->update('material',['kode'=>implode(date('i'),$id_inserted)]," id IN (".implode(',',$id_inserted).")")->execute();
                

                if($g['jml'][0]!=''){
                    $model->hg=1;
                }
                
                if($model->save()){
                    $i=0;
                    $id_brg=$model->id;
                    if($g['jml'][0]!=''){
                        
                        foreach ($g['jml'] as $item) {
                            // echo $item.'/';
                            // echo $g['hg'][$i].'<br>';
                            $sql="INSERT INTO br_grosir(id_brg,jml,hrg) VALUES (".$id_brg.",".$item.",".Yii::$app->help->toNumberOnly($g['hg'][$i]).") ";
                            Yii::$app->db->createCommand($sql)->execute();
                            $i++;
                        }
                    }
                }
                $transaction->commit();
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];           
            }catch (Exception $e){
                $transaction->rollback();
                echo $e;
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);

        }else{
            $data['ktg']=BrKtg::find()->All();
            $data['attr']=ItemAttr::find()->All();
            return $this->renderPartial('_form',$data);
            // echo "<pre>";
            // print_r($data);
            
        }
    }

    /**
     * Updates an existing Material model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = VMaterialgroup::findOne($id);


        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $m=Yii::$app->request->post('Material');
        $g=Yii::$app->request->post('grosir');

        if($m!=null)
        {
            $transaction=Yii::$app->db->beginTransaction();
            try{
                foreach($model->v_material as $vm){
                    // $filename='not_set.jpg';
                    // if(isset($_FILES['Material_file'])){
                    //     $fileArr=explode('.', $_FILES['Material_file']['name']);
                    //     $ext=end($fileArr);
                    //     $name=md5($fileArr[0].date('Y-m-d H:i:s').$user['id']);
                    //     $filename=$name.".".$ext;
                    //     move_uploaded_file($_FILES['Material_file']['tmp_name'],"file_uploaded/material/".$filename);
                    //     $model->foto = $filename;
                    // }
                    $model=Material::findOne($vm->id);
                    $model->nama =$m['nama'];
                    // $model->merk =$m['merk'];
                    $model->berat =$m['berat'];
                    $model->satuan_berat =$m['satuan_berat'];
                    // $model->status_jual =$m['status_jual'];
                    $model->id_ktg =$m['id_ktg'];
                    // $model->warna =$m['warna'];
                    // $model->size =$m['size'];
                    $model->keterangan =$m['keterangan'];
                    // $model->hgsat_retail =Yii::$app->help->toNumberOnly($m['hgsat_retail']);
                    // $model->hgsat_reseller =Yii::$app->help->toNumberOnly($m['hgsat_reseller']);
                    $model->save();
                }                

                // if($g['jml'][0]!=''){
                //  $model->hg=1;
                // }
                
                // if($model->save()){
                //  $i=0;
                //  $id_brg=$model->id;
                //  if($g['jml'][0]!=''){

                //      foreach ($g['jml'] as $item) {
                //          // echo $item.'/';
                //          // echo $g['hg'][$i].'<br>';
                //          $sql="INSERT IGNORE INTO br_grosir(id_brg,jml,hrg) VALUES (".$id_brg.",".$item.",".Yii::$app->help->toNumberOnly($g['hg'][$i]).") ";
                //          Yii::$app->db->createCommand($sql)->execute();
                //          $i++;
                //      }
                //  }
                // }
                $transaction->commit();
                $pesan=['pesan'=>'Data Berhasil di edit !','status'=>1];            
            }catch (Exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Data gagal di edit !','status'=>0,'error'=>$e];
            }           
            echo json_encode($pesan);

        }else{
            $data['ktg']=BrKtg::find()->All();
            $data['attr']=ItemAttr::find()->All();
            return $this->renderPartial('_form',$data);
            // echo "<pre>";
            // print_r($data['model']->v_material);


        }
    }
    public function actionVariancombo()
    {
        if(Yii::$app->request->post('id_attr') !== null){
            $varian=ItemVarian::find()->where(['id_attr'=>Yii::$app->request->post('id_attr')])->all();

            if(Yii::$app->request->post('json') !== null){
                $data=[];
                foreach ($varian as $item) {
                    $a['id']=$item['id'];
                    $a['text']=$item['nm_varian'];
                    $data[]=$a;
                }
                // echo json_encode($data);
                echo json_encode($data,JSON_PRETTY_PRINT);
            }else{
                echo Html::DropDownList("Material[varian]",null,ArrayHelper::map($varian,'id','nm_varian'),['id'=>'varian','class'=>'itemselect2','multiple'=>'multiple','placeholder'=>'-- Pilih Varian --']); 
            }
        }

        // echo "<pre>";
        // print_r($data);
    }

    /**
     * Deletes an existing Material model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }
    public function actionDelbykode()
    {
        Material::deleteAll(['kode'=>Yii::$app->request->post('kd')]);

        // return $this->redirect(['index']);
    }

    public function actionProduct()
    {   
        $data=[];
        $data['data']=VMaterial::find()->All();
        return $this->renderPartial('_product',$data);
    }



    /**
     * Finds the Material model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Material the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Material::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
