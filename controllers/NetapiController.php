<?php

namespace app\controllers;

use Yii;
use arsa\restenhance\ActiveController;
use arsa\restenhance\GoogleBearerAuth;

class NetapiController extends ActiveController {

    public $modelClass = "\\app\\model\\Netapi";
    public $email = null;

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => GoogleBearerAuth::className(),
            'auth' => function ($email) {
                $this->email = $email; //piggybacking here since User class is not properly implemented yet
                return \app\models\User::find()->where(['email' => $email])->one();
            },
        ];
        return $behaviors;
    }

    public function actions() {
        $actions = parent::actions();
        $actions['index']['method'] = function() {
            $connection = Yii::$app->getDb();
            $userid = $connection->createCommand("
                select id from user where email = :email;", [':email' => $this->email])->queryScalar();
            //we crawled through other sessions to notify that we got ping'd (if userid is a match)
            session_write_close();
            foreach (new \DirectoryIterator(session_save_path()) as $file) {
                if (preg_match('/^sess_(.*)/', $file->getFilename(), $data)) {
                    session_id($data[1]);
                    session_start();
                    if (array_key_exists('__id', $_SESSION) && $_SESSION['__id'] == $userid) {
                        $_SESSION['last_ping'] = time();
                    }
                }
                session_write_close();
            }
            //now the real stuff
            $command = $connection->createCommand("
                SELECT *, (select stok_akhir from v_material where id=iditem) \"stock\" FROM stock_item si 
                join stock_store ss on si.store_url=ss.store_url 
                where user_id = (select id from user where email = :email) 
                and item_updated < (select max(updated_at) from material_detail where id_material = iditem);", [':email' => $this->email]);
            return $command->queryAll();
        };
        $actions['create']['method'] = function($params) {
            $idlist = [];
            foreach ($params as $item) {
                array_push($idlist, $item['iditem']);
            }
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                UPDATE stock_item SET item_updated=CURRENT_TIMESTAMP
                WHERE iditem in (" . implode(',', $idlist) . ")");
            $command->query();
        };
        return $actions;
    }

}
