<?php

namespace app\controllers;

use Yii;
use app\models\Dropshipper;
use app\models\DropshipperSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;
use yii\widgets\ActiveForm;
use app\components\Help;
/**
 * DropshipperController implements the CRUD actions for Dropshipper model.
 */
class DropshipperController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dropshipper models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DropshipperSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dropshipper model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dropshipper model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user=Yii::$app->user->identity;
        $model = new Dropshipper();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) ) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $validate=Help::errorForm(ActiveForm::validate($model));
            if(count($validate)<1){
                $model->store_id=$user->store_id;
                $model->save();
            }
            return $validate;
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Dropshipper model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) ) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $validate=Help::errorForm(ActiveForm::validate($model));
            if(count($validate)<1){
                $model->save();
            }
            return $validate;
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Dropshipper model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    public function actionFindonejson(){
        $model=$this->findModel(Yii::$app->request->post('id'))->toArray();        
        echo json_encode($model);
    }


    /**
     * Finds the Dropshipper model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dropshipper the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dropshipper::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
