<?php

namespace app\controllers;

use Yii;
use app\models\Vspending;
use app\models\VspendingFilter;
use app\models\Opr;
use app\models\OprFilter;
use app\models\User;
use app\models\Coa;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\Response;
use yii\widgets\ActiveForm;
use app\components\Help;
/**
 * VspendingController implements the CRUD actions for Vspending model.
 */
class VspendingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vspending models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VspendingFilter();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vspending model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vspending model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Opr();
        $data=[];
        $user=Yii::$app->user->identity;
        $data['user']=$user;
        if (Yii::$app->request->isPost  ) {
            $filename=null;
            if(isset($_FILES['bukti'])){
                // $model->bukti=Yii::$app->request->post('opr')['bukti'];
                if(isset($_FILES['bukti'])){
                    $dir="file_uploaded/opr/spending";
                    $filename=Help::upload($_FILES['bukti'],'_'.Yii::$app->request->post('opr')['user_id'],$dir);
                }
            }
            $model->date=date('Y-m-d',strtotime(Yii::$app->request->post('opr')['date']));
            $model->user_id=Yii::$app->request->post('opr')['user_id'];
            $model->coa_id=Yii::$app->request->post('opr')['coa_id'];
            $model->bukti=$filename;
            $model->kredit=Help::toNumberOnly(Yii::$app->request->post('opr')['kredit']);
            $model->keterangan=Yii::$app->request->post('opr')['keterangan'];
            $model->store_id=$user->store_id;
            $model->is_cf=0;

            if($model->save()){
                $pesan=['pesan'=>' Data telah ditambahkan !','status'=>1];
            }else{
                $pesan=['pesan'=>'Action Failed !','status'=>0];
            }

            echo json_encode($pesan);
        } else {
            $data['model'] = $model;
            // $data['cf'] = Yii::$app->request->get('cf');
            $data['user'] = ArrayHelper::map(User::find()->where(['store_id'=>$user->store_id])->all(),'id','fullname');
            $data['coa'] = ArrayHelper::map(Coa::find()->where(['store_id'=>$user->store_id])->all(),'id','name');
            // echo "<pre>";
            // print_r($data);
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Updates an existing Vspending model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Opr::findOne($id);
        $data=[];
        
        if (Yii::$app->request->isPost ) {
            $filename=null;
            if(isset($_FILES['bukti'])){
                // $model->bukti=Yii::$app->request->post('opr')['bukti'];
                if(isset($_FILES['bukti'])){
                    $dir="file_uploaded/opr/spending";
                    $filename=Help::upload($_FILES['bukti'],'_'.Yii::$app->request->post('opr')['user_id'],$dir);
                }
            }
            $model->date=date('Y-m-d',strtotime(Yii::$app->request->post('opr')['date']));
            $model->user_id=Yii::$app->request->post('opr')['user_id'];
            $model->coa_id=Yii::$app->request->post('opr')['coa_id'];
            $model->bukti=$filename;
            $model->kredit=Help::toNumberOnly(Yii::$app->request->post('opr')['kredit']);
            $model->keterangan=Yii::$app->request->post('opr')['keterangan'];
            $model->is_cf=0;

            if($model->save()){
                $pesan=['pesan'=>' Data telah diperbarui !','status'=>1];
            }else{
                $pesan=['pesan'=>'Action Failed !','status'=>0];
            }

            echo json_encode($pesan);
        } else {
            $data['model'] = $model;
            // $data['cf'] = Yii::$app->request->get('cf');
            $data['user'] = ArrayHelper::map(User::find()->where(['store_id'=>$user->store_id])->all(),'id','fullname');
            $data['coa'] = ArrayHelper::map(Coa::find()->where(['store_id'=>$user->store_id])->all(),'id','name');
            // echo "<pre>";
            // print_r($data);
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Deletes an existing Vspending model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Opr::findOne($id)->delete();

        // return $this->redirect(['index']);
    }
    public function actionPaid()
    {
        // echo "<pre>";
        // print_r($_POST);
        $id=Yii::$app->request->post('selection');
        Opr::updateAll(['status_id'=>1],['id'=>$id]);
    }
    /**
     * Finds the Vspending model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vspending the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vspending::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
