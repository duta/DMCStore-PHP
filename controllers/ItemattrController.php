<?php

namespace app\controllers;

use Yii;
use app\models\ItemAttr;
use app\models\ItemAttrSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\ItemVarian;
/**
 * ItemattrController implements the CRUD actions for ItemAttr model.
 */
class ItemattrController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ItemAttr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemAttrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ItemAttr model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ItemAttr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user=Yii::$app->user->identity;

        $model = new ItemAttr();
        $model->store_id=$user->store_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ItemAttr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $pesan=['pesan'=>'Data Berhasil di update !','status'=>1];
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionChangevarian(){
        $data=[];
        $data['model']=ItemAttr::findOne(Yii::$app->request->post('id'));
        // echo "<pre>";
        // print_r($data['model']);
        return $this->renderPartial('_form_varian',$data);
    }

    public function actionUpdatedvarian(){
        $user=Yii::$app->user->identity;
        $tr=Yii::$app->db->beginTransaction();
        try {
            if(Yii::$app->request->post('set')!=null){
                foreach (Yii::$app->request->post('set') as $id => $value) {
                    $m_set=ItemVarian::findOne($id);
                    $m_set->nm_varian=$value;
                    $m_set->save();
                }
            }
            if(Yii::$app->request->post('new')!=null){
                foreach (Yii::$app->request->post('new') as $value) {
                    $m_new=new ItemVarian;
                    $m_new->id_attr=Yii::$app->request->post('id_attr');
                    $m_new->nm_varian=$value;
                    $m_new->store_id=$user->store_id;
                    $m_new->save();
                }
            }
            $tr->commit();
            $pesan=['Berhasil di perbarui !','status'=>1];
        } catch (Exception $e) {
            $tr->rollback();
            $pesan=['Gagal di perbarui !','status'=>0];
            // echo "<pre>";   
            // print_r($_POST);
        }
        echo json_encode($pesan);

    }

    public function actionDelvar(){
        ItemVarian::findOne(Yii::$app->request->post('id'))->delete();
    }
    /**
     * Deletes an existing ItemAttr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $tr=Yii::$app->db->beginTransaction();
        try {
            $this->findModel($id)->delete();
            ItemVarian::deleteAll(['id_attr'=>$id]);
            $tr->commit();
        } catch (Exception $e) {
            $tr->rollback();
        }
        

        // return $this->redirect(['index']);
    }

    /**
     * Finds the ItemAttr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ItemAttr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ItemAttr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
