<?php

namespace app\controllers;

use Yii;
use app\models\Forwarder;
use app\models\ForwarderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\components\Help;


/**
 * ForwarderController implements the CRUD actions for Forwarder model.
 */
class ForwarderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Forwarder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ForwarderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Forwarder model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Forwarder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Forwarder();

        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

            
        $r=Yii::$app->request->post('Forwarder');
        if ($r!= null) {
            $model->kd_fw=Help::inisial($r['nm_fw']);
            $model->nm_fw=$r['nm_fw'];
            $model->tlp=$r['tlp'];
            $model->qq=$r['qq'];
            $model->wechat=$r['wechat'];
            $model->store_id=$user->store_id;
            
            if($model->save()){
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];
            }else{
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0];
            }
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', $data);
        };
    }

    /**
     * Updates an existing Forwarder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

       
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

            
        $r=Yii::$app->request->post('Forwarder');
        if ($r!= null) {
            $model->kd_fw=Help::inisial($r['nm_fw']);
            $model->nm_fw=$r['nm_fw'];
            $model->tlp=$r['tlp'];
            $model->qq=$r['qq'];
            $model->wechat=$r['wechat'];
            
            if($model->save()){
                $pesan=['pesan'=>'Data Berhasil di perbarui !','status'=>1];
            }else{
                $pesan=['pesan'=>'Data gagal di perbarui !','status'=>0];
            }
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', $data);
        };
    }

    /**
     * Deletes an existing Forwarder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    /**
     * Finds the Forwarder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Forwarder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Forwarder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
