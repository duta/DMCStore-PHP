<?php

namespace app\controllers;

use Yii;
use app\models\Vcashflow;
use app\models\VcashflowFilter;
use app\models\Opr;
use app\models\OprFilter;
use app\models\User;
use app\models\Coa;
use yii\helpers\ArrayHelper;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use yii\web\Response;
use yii\widgets\ActiveForm;
use app\components\Help;

/**
 * VcashflowController implements the CRUD actions for Vcashflow model.
 */
class VcashflowController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vcashflow models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VcashflowFilter();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vcashflow model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vcashflow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user=Yii::$app->user->identity;
        $data=[];
        $model = new Opr();
        
        if (Yii::$app->request->isPost  ) {
            $tr=Yii::$app->db->beginTransaction();
            try {
                $randomString=Help::uniqueString('opr','kd_cf');
                // DEBIT 
                $debit = new Opr();
                $debit->date=date('Y-m-d',strtotime(Yii::$app->request->post('opr')['date']));
                
                $debit->user_id=$user->id;
                $debit->debit=Help::toNumberOnly(Yii::$app->request->post('opr')['debit']);

                $debit->keterangan=Yii::$app->request->post('opr')['keterangan'];
                $debit->is_cf=1;
                $debit->kd_cf=$randomString;
                $debit->store_id=$user->store_id;
                $debit->save();

                // KREDIT
                $kredit = new Opr();
                $kredit->date=date('Y-m-d',strtotime(Yii::$app->request->post('opr')['date']));

                $kredit->user_id=Yii::$app->request->post('opr')['user_id'];
                $kredit->kredit=Help::toNumberOnly(Yii::$app->request->post('opr')['debit']);

                $kredit->keterangan=Yii::$app->request->post('opr')['keterangan'];
                $kredit->is_cf=1;
                $kredit->kd_cf=$randomString;
                $kredit->store_id=$user->store_id;
                $kredit->save();

                $tr->commit();
                $pesan=['pesan'=>' Data telah ditambahkan !','status'=>1];

            } catch (Exception $e) {
                $tr->rollback();
                $pesan=['pesan'=>'Action Failed !','status'=>0];
            }
           
            echo json_encode($pesan);
        } else {
            $data['model'] = $model;
            // $data['cf'] = Yii::$app->request->get('cf');
            $data['user'] = ArrayHelper::map(User::find()->all(),'id','fullname');
            // echo "<pre>";
            // print_r($data);
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Updates an existing Vcashflow model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Vcashflow model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Opr::deleteAll(['kd_cf' => Yii::$app->request->post('kd_cf')]);
        // $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    /**
     * Finds the Vcashflow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vcashflow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vcashflow::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
