<?php

namespace app\controllers;

use Yii;
use app\models\BrSupplier;
use app\models\BrSupplierSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\components\Help;

/**
 * BrsupplierController implements the CRUD actions for BrSupplier model.
 */
class BrsupplierController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BrSupplier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BrSupplierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BrSupplier model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BrSupplier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BrSupplier();

        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r=Yii::$app->request->post('BrSupplier');
        if ($r!= null) {
            $model->kd_sup=Help::inisial($r['nm_sup']);
            $model->nm_sup=$r['nm_sup'];
            $model->tlp=$r['tlp'];
            $model->qq=$r['qq'];
            $model->wechat=$r['wechat'];
            $model->store_id=$user->store_id;
            
            if($model->save()){
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];
            }else{
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Updates an existing BrSupplier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r=Yii::$app->request->post('BrSupplier');
        if ($r!= null) {
            $model->kd_sup=Help::inisial($r['nm_sup']);
            $model->nm_sup=$r['nm_sup'];
            $model->tlp=$r['tlp'];
            $model->qq=$r['qq'];
            $model->wechat=$r['wechat'];
            
            if($model->save()){
                $pesan=['pesan'=>'Data Berhasil di update !','status'=>1];
            }else{
                $pesan=['pesan'=>'Data gagal di update !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', $data);
        }
    }

    /**
     * Deletes an existing BrSupplier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        

        // return $this->redirect(['index']);
    }

    /**
     * Finds the BrSupplier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return BrSupplier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BrSupplier::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
