<?php

namespace app\controllers;

use yii;
use yii\filters\AccessControl;
use yii\Helpers\UrL;
use app\models\Permissions;
use app\models\Statistik;
use app\models\Store;
use yii\helpers\ArrayHelper;
class HomeController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionListmenu(){
        $user=Yii::$app->user->identity;
        $menu=Permissions::find()->joinWith('menu')->where(['role_id'=>$user->id_role,'parent_id'=>0])->orderBy(['menu.no_urut'=>SORT_ASC])->all();
        // echo "<pre>";
        // print_r($menu);
        foreach ($menu as $item) {
            echo $item->menu->name_as."<br>";
        }
    }
    public function actionIndex()
    {
        $r=Yii::$app->request;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['store']=1;

        if($r->isAjax){
            // $data['market_statistik']=Material::Market_statistik();
            // $items=[];
            // foreach ($data['market_statistik'] as $item) {
            //     $items['value']=$item['brg_terjual'];
            //     $items['color']=Yii::app()->help->get_random_color();
            //     $items['highlight']=$items['color'];
            //     $items['label']=$item[''];
            // }
            
            return $this->renderPartial("_index",$data);
            
        }else{
            if($data['store']!=null){
                if($user->id_role==3){
                    return $this->render("index_shipping",$data);
                }elseif($user->id_role==4){
                    return $this->redirect(['vmaterialstok/index']);
                }else{
            	   return $this->render("index",$data);
                }
            }else{
               return $this->render("index_nostore",$data);
            }
        }
    }

    public function actionStatistik(){
        $data=[];
        $data['models']=Statistik::find()->where(['display'=>1])->orderBy(['no_urut'=>SORT_ASC])->all();
        // echo "<pre>";
        // print_r($data);
        return $this->renderPartial("_statistik_loop",$data);
        // return $this->renderPartial("_statistik");
    }

    public function actionTest(){
        $user=Yii::$app->user->identity;
        $menuChild=Permissions::find()->joinWith('menu')->where('role_id=4 AND parent_id >0')->orderBy(['menu.no_urut'=>SORT_ASC])->asArray()->all();
        
        echo "<pre>";
        print_r(ArrayHelper::getColumn($menuChild,'menu_id'));
    }

}
