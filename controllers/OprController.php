<?php

namespace app\controllers;

use Yii;
use app\models\Opr;
use app\models\OprFilter;
use app\models\User;
use app\models\Coa;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use yii\web\Response;
use yii\widgets\ActiveForm;
use app\components\Help;
/**
 * OprController implements the CRUD actions for Opr model.
 */
class OprController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Opr models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OprFilter();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Opr model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Opr model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Opr();

        if (Yii::$app->request->isPost ) {

            $model->date=date('Y-m-d',strtotime(Yii::$app->request->post('opr')['date']));
            $model->user_id=Yii::$app->request->post('opr')['user_id'];
            if(Yii::$app->request->post('opr')['is_cf']==0){
                $model->coa_id=Yii::$app->request->post('opr')['coa_id'];
                // $model->bukti=Yii::$app->request->post('opr')['bukti'];
            }
            $model->kredit=Help::toNumberOnly(Yii::$app->request->post('opr')['kredit']);
            $model->keterangan=Yii::$app->request->post('opr')['keterangan'];
            $model->is_cf=Yii::$app->request->post('opr')['is_cf'];

            if($model->save()){
                $pesan=['pesan'=>' Data telah ditambahkan !','status'=>1];
            }else{
                $pesan=['pesan'=>'Action Failed !','status'=>0];
            }
           
            echo json_encode($pesan);
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
                'cf'=> Yii::$app->request->get('cf'),
                'user' => ArrayHelper::map(User::find()->all(),'id','fullname'),
                'coa' => ArrayHelper::map(Coa::find()->all(),'id','name'),
            ]);
        }
    }

    /**
     * Updates an existing Opr model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) ) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            $validate=Help::errorForm(ActiveForm::validate($model));
            if(count($validate)<1){
                $model->save();
            }
            return $validate;
        } else {
            return $this->renderPartial('_form', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Opr model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }

    /**
     * Finds the Opr model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Opr the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Opr::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
