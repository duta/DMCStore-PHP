<?php
namespace app\controllers;

use Yii;
use app\models\VOrders;
use app\models\VOrdersSearch;
use app\models\Customers;
use app\models\Marketplace;
use app\models\SalesThisWeek;
use app\models\SalesThisWeekByItem;
use app\models\SalesToday;
use app\models\ItemVarian;
use app\models\VMaterial;
use app\models\MasterMaterial;
use app\models\Top5sales;
use app\models\TrvalThisMonth;
use app\models\TrvalToday;
use app\models\User;
use app\models\MaterialDetail;
use app\models\StProvince;
use app\models\Kurir;

use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\components\Help;

/**
 * VordersController implements the CRUD actions for VOrders model.
 */
class VordersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VOrders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VOrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user'=>Yii::$app->user->identity,
            'cm'=>ArrayHelper::map(Customers::find()->All(),'id','nama'),
            'mp'=>ArrayHelper::map(Marketplace::find()->where(['<>','id',100])->All(),'id','nm_market'),
        ]);
    }

    /**
     * Displays a single VOrders model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VOrders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VOrders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kd_tagihan]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VOrders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->kd_tagihan]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSalesthisweek(){
        $user=Yii::$app->user->identity;
        $model=SalesThisWeekByItem::getChart();
        $chart=[];
        
        $var=ArrayHelper::getColumn(SalesThisWeekByItem::find()->where(['store_id'=>$user->store_id])->all(),'item');
        $key_chart=[];
        $label_chart=[];
        $color=[];
        $legend=[];

        foreach ($var as $item_name) {
            $key_chart[]=Help::toAlnumOnly($item_name);          
        }
        
        foreach ($model as $item) {
            $chart[$item['period_field']]['period']=$item['period_field'];
            if($item['item']!==null){
                foreach ($var as $varian) {
                    $a[Help::toAlnumOnly($varian).'_'.$item['period_field']]=0;      
                }
                $b[Help::toAlnumOnly($item['item']).'_'.$item['period_field']]=$item['qty'];
                foreach (array_replace($a,$b) as $key => $value) {
                    $key_arr=explode('_',$key);
                    if($key_arr[1]==$item['period_field']){
                        $chart[$item['period_field']][$key_arr[0]]=intval($value);
                    }
                }
            }else{
                foreach ($var as $varian) {                
                    $chart[$item['period_field']][Help::toAlnumOnly($varian)]=0;
                }
            }
        }
        foreach ($var as $item2) {
            $cl=Help::get_random_color();                
            $color[]=$cl;
            $legend[]="<li><h5><i class='fa fa-circle m-r-5' style='color: ".$cl."'></i>".$item2."</h5> </li>";
        }

        $chartfix=[];
        foreach ($chart as $key => $value) {
            $chartfix[]=$value;
        }

        $data=[];
        $data['data']=$chartfix;
        $data['keys']=$key_chart;
        $data['labels']=$var;
        $data['colors']=$color;
        $data['legends']=$legend;

        echo json_encode($data,JSON_PRETTY_PRINT);
    }

    public function actionSalesthisyear(){
        $user=Yii::$app->user->identity;
        $model=VOrders::salesthisyear();
        $field=array_keys($model[0]);
        $clean_field=array_diff($field,['month_field','month_name']);
        $data=[];
        $data['data']=$model;
        $data['xkey']='month_name';

        foreach ($clean_field as $item) {
            $cl=Help::get_random_color();                
            $color[]=$cl;
            $legend[]="<li><h5><i class='fa fa-circle m-r-5' style='color: ".$cl."'></i>".$item."</h5> </li>";
            $data['ykeys'][]=$item;
            $data['labels'][]=$item;
        }

        $data['colors']=$color;
        $data['legends']=$legend;

        echo json_encode($data,JSON_PRETTY_PRINT);

    }
    public function actionSalestoday(){
        $user=Yii::$app->user->identity;
        $model=TrvalToday::find()->where(['store_id'=>$user->store_id])->asArray()->all();
        $data=[];
        $chart=[];
        $color=[];

        foreach ($model as $item) {
            $chart_data['label']=$item['item'];
            $chart_data['value']=intval($item['qty_sales']);
            $chart[]=$chart_data;
            $color[]=Help::get_random_color();
        }

        $data['data']=$chart;
        $data['colors']=$color;

        // echo "<pre>";
        echo json_encode($data,JSON_PRETTY_PRINT);
    }

    public function actionTop5sales(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['models']=Top5sales::find()->where(['store_id'=>$user->store_id])->all();
        return $this->renderPartial('_top5sales',$data);
    }

    public function actionTrvalthismonth(){
        $user=Yii::$app->user->identity;
        $data=[];
        $data['models']=TrvalThisMonth::find()->where('qty_sales>0')->andWhere(['store_id'=>$user->store_id])->all();
        return $this->renderPartial('_trvalthismonth',$data);
    }

    /**
     * Deletes an existing VOrders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionReport(){
        $data=[];
        $user=Yii::$app->user->identity;

        if(Yii::$app->request->isAjax){
            $start=date('Y-m-d',strtotime(Yii::$app->request->post('start')));
            $end=date('Y-m-d',strtotime(Yii::$app->request->post('end')));
            
            $where="(DATE(v_orders.done_at) BETWEEN '".$start."' AND '".$end."') ";

            if(Yii::$app->request->post('cs') !== null && Yii::$app->request->post('cs') !== ''){
                $where.=" AND v_orders.created_by=".Yii::$app->request->post('cs');    
            }

            $data['model']=VOrders::find()->where(['v_orders.store_id'=>$user->store_id])
            ->joinWith(['md'=>function($md){
                if(Yii::$app->request->post('item') !== null && Yii::$app->request->post('item') !== ''){
                    $md->joinWith('v_material')
                    ->where(['v_material.kode'=>Yii::$app->request->post('item')]);
                }
            }])
            ->where($where)->orderBy("v_orders.done_at DESC")->all();       

            return $this->renderPartial("_report",$data);

            // echo "<pre>";
            // print_r($data['model']);
        }else{
            $data['item']=ArrayHelper::map(MasterMaterial::find()->where(['store_id'=>$user->store_id])->all(),'kode','nama');
            $data['cs']=ArrayHelper::map(User::find()->where(['id_role'=>2,'store_id'=>$user->store_id])->all(),'id','fullname');
            return $this->render('report',$data);
        }  
    }


    public function actionCreate_od_dropship(){

        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;
        $md = Yii::$app->request->post('md');
        $cm = Yii::$app->request->post('customer');
        $qty = Yii::$app->request->post('qty');
        $inv = Yii::$app->request->post('inv');
        $hj = Yii::$app->request->post('hgsat_jual');
        $hgmod = Yii::$app->request->post('hgmod');
        // print_r($rq->bodyParams);
        // foreach ($md['id_material'] as $id_item) {
        //     echo $md['qty'][$id_item].'//';
        // }

        if($md!=null){
            $transaction=Yii::$app->db->beginTransaction();
            try {
                if($cm['id_c']==null || $cm['id_c']==''){
                    $c=new Customers;
                    $c->nama=ucwords(strtolower($cm['nama']));
                    $c->email=$cm['email'];
                    $c->tlp=$cm['tlp'];
                    $c->prov=ucwords(strtolower($cm['prov']));
                    $c->kota_kab=ucwords(strtolower($cm['kota']));
                    $c->kec=ucwords(strtolower($cm['kec']));
                    $c->alamat=$cm['alamat'];
                    $c->store_id=$user->store_id;
                    $c->save();
                    $c_id=$c->id;
                }else{
                    $c_id=$cm['id_c'];
                }
                foreach ($md['id'] as $id_item) {
                    // $id=explode('_',$id_item);

                    $md=new MaterialDetail();   
                    $md->id_material=$id_item;
                    $md->kd_tagihan=$inv[$id_item];
                    $md->buyer=$c_id;
                    $md->ongkir_buyer=Help::toNumberOnly($cm['ongkir_buyer']);
                    $md->hgsat_jual=Help::toNumberOnly($hj[$id_item]);
                    $md->id_market=$cm['id_market'];
                    $md->id_kurir=$cm['id_kurir'];
                    $md->status=0;
                    $md->harga_mod=$hgmod[$id_item];
                    // $md->tipe_transaksi=15 = OUT(KELUAR);
                    $md->tipe_transaksi=15;
                    $md->buyer_note=$cm['c_buyer_note'];
                    $md->qty=$qty[$id_item];
                    $md->created_by=$user['id'];
                    // SEND BY HATOBE is default
                    $md->send_by=1;
                    $md->store_id=$user->store_id;
                    $md->save();
                    $id_inserted[]=$md->id;
                }
                Yii::$app->db->createCommand()->update('material_detail',['kd_order'=>implode(date('i'),$id_inserted)]," id IN (".implode(',',$id_inserted).")")->execute();
                $pesan=['pesan'=>'Data Berhasil di tambahkan !','status'=>1];
                $transaction->commit();
            }catch(exception $e){
                $transaction->rollback();
                $pesan=['pesan'=>'Data gagal di tambahkan !','status'=>0,'errors'=>'<pre>'.$e];
            }
            echo json_encode($pesan);
        }else{
            $data['Barang']=ArrayHelper::map(VMaterial::find()->All(), 'id', 'fullname');
            return $this->renderPartial('_form_bm_od',$data);  
        }
    }
     public function actionForm_od(){
        $model=new MaterialDetail;
        $user=Yii::$app->user->identity;
        $data=[];
        $data['user']=$user;
        $data['model']=$model;

        $r=Yii::$app->request->post('id');
        
        if($r!=null && $r!=''){ 
            $id=($r!='')?$r:[];
            $data['Barang']=VMaterial::find()->andWhere(['id'=>$id,'store_id'=>$user->store_id])->All();
            $data['Customers']=ArrayHelper::map(Customers::find()->andWhere(['store_id'=>$user->store_id])->All(),'id','nama');
            $data['Province']=ArrayHelper::map(StProvince::find()->All(),'id','province');
            // $data['City']=ArrayHelper::map(City::find()->All(),'id','layanan','nm_kurir');
            $data['Kurir']=ArrayHelper::map(Kurir::find()->where(['<>','id',100])->andWhere(['store_id'=>$user->store_id])->All(),'id','layanan','nm_kurir');
            $data['Market']=ArrayHelper::map(Marketplace::find()->where(['<>','id',100])->andWhere(['store_id'=>$user->store_id])->All(),'id','nm_market');
            return $this->renderPartial('_form_od',$data);
        }

    }

    
    /**
     * Finds the VOrders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return VOrders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VOrders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
