<?php

namespace app\modules\api\controllers;
use yii\rest\ActiveController;
use yii\web\Response;

// use app\models\Marketplace;

class MarketplaceController extends ActiveController
{

	public $modelClass = 'app\models\Marketplace';

    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

	// public function actionTes(){
	// 	return [1,2,2];
	// }
    // public function actionIndex()
    // {
    // 	// $return=[''];
    // 	$model=User::find()->all();
    // 	// echo "test";
    //     // return $this->render('index');
    //     \Yii::$app->response->format = Response::FORMAT_JSON;
    //     return $model;
    // }
}
