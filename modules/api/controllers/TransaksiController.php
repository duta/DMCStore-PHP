<?php

namespace app\modules\api\controllers;

use Yii;
use yii\rest\ActiveController;
// use yii\web\Response;

class TransaksiController extends ActiveController
{
	public $modelClass = 'app\models\MaterialDetail';

	// public function actions()
	// {
	//     $actions = parent::actions();

	//     // disable the /"delete" and "create" actions
	//     // unset($actions['view']);

	//     // customize the data provider preparation with the "prepareDataProvider()" method
	//     $actions['index']['test'] = [$this, 'test'];

	//     return $actions;
	// }

    public function actionPb($kd)
    {
    	return [$kd];
    }

}
