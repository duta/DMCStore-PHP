<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/web/ampleadmin/';
    public $baseUrl = '@web/web/ampleadmin/';
    public $css = [
            'bootstrap/dist/css/bootstrap.min.css',
            'plugins/bower_components/toast-master/css/jquery.toast.css',
            'https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css',
            'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
            'plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css',
            'plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css',
            'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css',
            'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',
            'plugins/bower_components/timepicker/bootstrap-timepicker.min.css',
            'plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css',
            'plugins/bower_components/multiselect/css/multi-select.css',
            'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',
            'plugins/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css',
            'plugins/bower_components/bootstrap-modal-carousel/bootstrap-modal-carousel.min.css',
            'plugins/bower_components/sweetalert/sweetalert.css',
            'css/animate.css',
            'css/style.css',
            'css/colors/green.css',
            'plugins/bower_components/morrisjs/morris.css',
            'plugins/bower_components/custom-select/custom-select.css',
            'plugins/bower_components/product-slider/css/style.css',
            'plugins/bower_components/morrisjs/morris.css',
    ];
    public $js = [
            // 'plugins/bower_components/jquery/dist/jquery.min.js',
            'plugins/bower_components/jquery.blockui/jquery.blockui.min.js',
            'bootstrap/dist/js/bootstrap.min.js',
            'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
            'js/jquery.slimscroll.js',
            'js/waves.js',
            'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
            'plugins/bower_components/datatables/jquery.dataTables.min.js',
            'plugins/bower_components/toast-master/js/jquery.toast.js',
            'js/toastr.js',
            'plugins/bower_components/moment/moment.js',
            'plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js',
            'plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js',
            'plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js',
            'plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js',
            'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js',
            'plugins/bower_components/timepicker/bootstrap-timepicker.min.js',
            'plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js',
            'plugins/bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js',
            'plugins/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js',
            'plugins/bower_components/jquery.pulsate/jquery.pulsate.min.js',
            'plugins/bower_components/multifile-master/jQuery.MultiFile.min.js',
            'plugins/bower_components/switchery/dist/switchery.min.js',
            'plugins/bower_components/custom-select/custom-select.min.js',
            'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',
            'plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
            'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
            'plugins/bower_components/multiselect/js/jquery.multi-select.js',
            'plugins/bower_components/sweetalert/sweetalert.min.js',
            'plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js',
            'plugins/bower_components/Chart.js/Chart.min.js',
            'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
            'plugins/bower_components/waypoints/lib/jquery.waypoints.js',
            'plugins/bower_components/counterup/jquery.counterup.min.js',
            'plugins/bower_components/product-slider/js/jqzoom.js',
            'plugins/bower_components/bootstrap-modal-carousel/bootstrap-modal-carousel.min.js',
            'plugins/bower_components/raphael/raphael-min.js',
            'plugins/bower_components/morrisjs/morris.js',
            // 'https://js.pusher.com/4.1/pusher.min.js',
            // 'js/morris-data.js',
            

            'js/cbpFWTabs.js',
            'js/custom.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
