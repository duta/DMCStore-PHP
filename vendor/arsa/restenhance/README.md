Enhance Yii rest module
=======================
Enhance Yii rest module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist arsa/yii2-rest.enhance "*"
```

or add

```
"arsa/yii2-rest.enhance": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \arsa\rest_enhance\AutoloadExample::widget(); ?>```