<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace arsa\restenhance;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\ServerErrorHttpException;

/**
 * UpdateAction implements the API endpoint for updating a model.
 *
 * For more details and usage information on UpdateAction, see the [guide article on rest controllers](guide:rest-controllers).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UpdateAction extends \yii\rest\UpdateAction {

    /**
     * @var string the scenario to be assigned to the model before it is validated and updated.
     */
    public $scenario = Model::SCENARIO_DEFAULT;
    public $method;

    /**
     * Updates an existing model.
     * @param string $id the primary key of the model.
     * @return \yii\db\ActiveRecordInterface the model being updated
     * @throws ServerErrorHttpException if there is any error when updating the model
     */
    public function run($id = "") {
        if ($this->method === null) {
            if (!empty($id))
                $model = $this->findModel($id);
            else
                $model = $this->findModelFromCondition();

            if ($this->checkAccess) {
                call_user_func($this->checkAccess, $this->id, $model);
            }

            $model->scenario = $this->scenario;
            $model->load(Yii::$app->getRequest()->getBodyParams(), '');
            if (empty($id)) {
                $modelClass = $this->modelClass;
                $modelClass::updateAll(Yii::$app->getRequest()->getBodyParams(), Yii::$app->getRequest()->getQueryParams());
                return $model;
            }
            if ($model->save() === false && !$model->hasErrors()) {
                throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
            }

            return $model;
        } else {
            return call_user_func($this->method, $id, Yii::$app->getRequest()->getBodyParams());
        }
    }

}
