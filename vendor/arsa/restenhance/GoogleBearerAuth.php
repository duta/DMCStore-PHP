<?php

namespace arsa\restenhance;
use yii\httpclient\Client;
use yii\filters\auth\AuthMethod;

class GoogleBearerAuth extends AuthMethod {

    /**
     * @var string A "realm" attribute MAY be included to indicate the scope
     * of protection in the manner described in HTTP/1.1 [RFC2617].  The "realm"
     * attribute MUST NOT appear more than once.
     */
    public $realm = 'api';

    /**
     * @var string Authorization header schema, default 'Bearer'
     */
    public $schema = 'Bearer';

    /**
     * @var callable a PHP callable that will authenticate the user with the JWT payload information
     *
     * ```php
     * function ($token, $authMethod) {
     *    return \app\models\User::findOne($token->getClaim('id'));
     * }
     * ```
     *
     * If this property is not set, the username information will be considered as an access token
     * while the password information will be ignored. The [[\yii\web\User::loginByAccessToken()]]
     * method will be called to authenticate and login the user.
     */
    public $auth;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function authenticate($user, $request, $response) {
        $authHeader = $request->getHeaders()->get('Authorization');
        if ($authHeader !== null && preg_match('/^' . $this->schema . '\s+(.*?)$/', $authHeader, $matches)) {
            $client = new Client();
            $response = $client->createRequest()
                    ->setMethod('GET')
                    ->setUrl('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token='.$matches[1])
                    ->send();
            if ($response->isOk) {
                $email = $response->data['email'];
            } else {
                return null;
            }

            return call_user_func($this->auth, $email, get_class($this));
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function challenge($response) {
        $response->getHeaders()->set(
                'WWW-Authenticate', "{$this->schema} realm=\"{$this->realm}\", error=\"invalid_token\", error_description=\"The access token invalid or expired\""
        );
    }

}
