<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace arsa\restenhance;

use Yii;
use yii\web\ServerErrorHttpException;

/**
 * DeleteAction implements the API endpoint for deleting a model.
 *
 * For more details and usage information on DeleteAction, see the [guide article on rest controllers](guide:rest-controllers).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DeleteAction extends \yii\rest\DeleteAction {

    public $method;

    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id = "") {
        if ($this->method === null) {
            if (!empty($id))
                $model = $this->findModel($id);
            else
                $model = $this->findModelFromCondition();

            if ($this->checkAccess) {
                call_user_func($this->checkAccess, $this->id, $model);
            }

            if (empty($id)) {
                $modelClass = $this->modelClass;
                $modelClass::deleteAll(Yii::$app->getRequest()->getQueryParams());
            } else if ($model->delete() === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            }

            Yii::$app->getResponse()->setStatusCode(204);
        } else {
            call_user_func($this->method, $id);
        }
    }

}
