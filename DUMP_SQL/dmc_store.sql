/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100113
 Source Host           : 127.0.0.1:3306
 Source Schema         : dmc_store

 Target Server Type    : MySQL
 Target Server Version : 100113
 File Encoding         : 65001

 Date: 14/12/2018 13:08:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_status
-- ----------------------------
DROP TABLE IF EXISTS `app_status`;
CREATE TABLE `app_status`  (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nm_status` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ket_status` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `class` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ktg_status` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of app_status
-- ----------------------------
INSERT INTO `app_status` VALUES (0, 'Menunggu', 'Barang telah dipesan dan menunggu pembayaran oleh pembeli', 'label-warning', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (1, 'Diproses', 'Pembayaran telah di terima , pesanan sedang disiapkan untuk dikirim ', 'label-primary', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (2, 'Dibatalkan', 'Transaksi dibatalkan', 'label-danger', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (3, 'Dikirim', 'Barang telah diproses oleh kurir, tunggu kedatangan barang & silahkan melakukan konfirmasi terima barang ', 'label-warning', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (4, 'Selesai', 'Barang telah diterima & transaksi selesai', 'label-success', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (9, 'Internal Orders', 'Transaksi RO ke user', 'label-warning', 'tipe_transfer', 0);
INSERT INTO `app_status` VALUES (10, 'Customer Orders', 'Transaksi dengan customer', 'label-success', 'tipe_transfer', 0);
INSERT INTO `app_status` VALUES (14, 'in', 'Material Masuk', 'label-primary', 'tipe_transaksi', 0);
INSERT INTO `app_status` VALUES (15, 'out', 'Material Keluar', 'label-warning', 'tipe_transaksi', 0);
INSERT INTO `app_status` VALUES (16, 'Proses retur', 'retur material sedang diproses', 'label-primary', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (17, 'Dikirim (Retur)', 'retur material  dalam proses pengiriman oleh kurir', 'label-info', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (18, 'Selesai (Retur)', 'material berhasil di retur', 'label-success', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (24, 'Proses', 'Barang sudah dibayar, pesanan akan segera dikirim', 'label-info', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (25, 'Refunded', 'Uang pembeli dikembalikan', 'label-primary', 'status_transaksi', 0);
INSERT INTO `app_status` VALUES (30, 'Ganti baru', 'pembeli ingin ganti barang baru', 'label-info', 'tipe_retur', 0);
INSERT INTO `app_status` VALUES (31, 'Pengembalian Dana', 'pembeli ingin pengembalian dana', 'label-primary', 'tiper_retur', 0);
INSERT INTO `app_status` VALUES (32, 'Employess', 'Karyawan dari toko', 'label-warning', 'is_owner', 0);
INSERT INTO `app_status` VALUES (33, 'Boss', 'Bos dari toko', 'label-success', 'is_owner', 0);

-- ----------------------------
-- Table structure for auth
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `source` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `source_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk-auth-user_id-user-id`(`user_id`) USING BTREE,
  CONSTRAINT `fk-auth-user_id-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for br_ktg
-- ----------------------------
DROP TABLE IF EXISTS `br_ktg`;
CREATE TABLE `br_ktg`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nm_ktg` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_ktg
-- ----------------------------
INSERT INTO `br_ktg` VALUES (1, 'Elektronik');
INSERT INTO `br_ktg` VALUES (2, 'Fashion Pria');
INSERT INTO `br_ktg` VALUES (3, 'Fashion Wanita');
INSERT INTO `br_ktg` VALUES (4, 'Peralatan Rumah Tangga');
INSERT INTO `br_ktg` VALUES (5, 'Kesehatan & Kecantikan');
INSERT INTO `br_ktg` VALUES (6, 'Bayi & Mainan Anak');
INSERT INTO `br_ktg` VALUES (7, 'Olahraga & Travel');
INSERT INTO `br_ktg` VALUES (8, 'Groceries, Media & Pets');
INSERT INTO `br_ktg` VALUES (9, 'Mobil & Motor');

-- ----------------------------
-- Table structure for br_status
-- ----------------------------
DROP TABLE IF EXISTS `br_status`;
CREATE TABLE `br_status`  (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nm_status` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ket_status` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `class` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ktg_status` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_status
-- ----------------------------
INSERT INTO `br_status` VALUES (1, 'Published', 'brg di jual', 'label-success', 'status_jual');
INSERT INTO `br_status` VALUES (2, 'Draft', 'brg di simpan dulu sebelum dijual', 'label-warning', 'status_jual');
INSERT INTO `br_status` VALUES (3, 'Sold', 'brg terjual', '', 'status_jual');

-- ----------------------------
-- Table structure for br_subktg
-- ----------------------------
DROP TABLE IF EXISTS `br_subktg`;
CREATE TABLE `br_subktg`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_ktg` int(11) UNSIGNED NOT NULL,
  `sub_ktg` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_subktg
-- ----------------------------
INSERT INTO `br_subktg` VALUES (1, 1, 'celana');
INSERT INTO `br_subktg` VALUES (2, 1, 'jaket');
INSERT INTO `br_subktg` VALUES (3, 2, 'jilbab');
INSERT INTO `br_subktg` VALUES (4, 2, 'tas wanita');

-- ----------------------------
-- Table structure for br_subsubktg
-- ----------------------------
DROP TABLE IF EXISTS `br_subsubktg`;
CREATE TABLE `br_subsubktg`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_subktg` int(11) UNSIGNED NOT NULL,
  `subsub_ktg` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_subsubktg
-- ----------------------------
INSERT INTO `br_subsubktg` VALUES (1, 1, 'celana pendek');
INSERT INTO `br_subsubktg` VALUES (2, 1, 'celana panjang');
INSERT INTO `br_subsubktg` VALUES (3, 3, 'segi empat');
INSERT INTO `br_subsubktg` VALUES (4, 3, 'ciput');

-- ----------------------------
-- Table structure for br_supplier
-- ----------------------------
DROP TABLE IF EXISTS `br_supplier`;
CREATE TABLE `br_supplier`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kd_sup` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nm_sup` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tlp` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qq` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wechat` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of br_supplier
-- ----------------------------
INSERT INTO `br_supplier` VALUES (1, 'SA', 'LED POPLOR (YIWU)', 'QQ : 179813631', NULL, '', 1, '2018-01-04 08:55:25');
INSERT INTO `br_supplier` VALUES (3, 'SC', 'Li Cheng (Guangzhou Market)', 'Wechat : licheng4799', NULL, '', 1, '2018-01-04 08:55:25');
INSERT INTO `br_supplier` VALUES (4, '', 'Hong 1835 (Yiwu)', 'wechat: junshuaipiju', NULL, '', 1, '2018-01-24 06:51:39');
INSERT INTO `br_supplier` VALUES (5, '', 'YIWU Extension Step Digital', 'wechat : wxid_o9rz4f', NULL, '', 1, '2018-01-24 06:54:22');
INSERT INTO `br_supplier` VALUES (6, 'L', 'Lulu', '', '', 'Junshuaipiju', 1, '2018-03-22 13:38:27');
INSERT INTO `br_supplier` VALUES (7, 'S', 'Sumbawa', '', '', '', 1, '2018-04-23 11:23:39');
INSERT INTO `br_supplier` VALUES (8, 'DH', 'Dian Hijab', '', '', '', 1, '2018-04-23 11:23:52');
INSERT INTO `br_supplier` VALUES (9, 'S', 'Syamna ', '', '', '', 1, '2018-04-23 11:24:02');
INSERT INTO `br_supplier` VALUES (10, 'Q', 'qreewerr', '213', 'dsfdsdsf', '23132', 19, '2018-07-17 12:34:38');

-- ----------------------------
-- Table structure for coa
-- ----------------------------
DROP TABLE IF EXISTS `coa`;
CREATE TABLE `coa`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of coa
-- ----------------------------
INSERT INTO `coa` VALUES (1, 'Konsumsi', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:40');
INSERT INTO `coa` VALUES (2, 'Material Project', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:41');
INSERT INTO `coa` VALUES (3, 'Transportasi', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:42');
INSERT INTO `coa` VALUES (4, 'Entertainment', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:43');
INSERT INTO `coa` VALUES (5, 'Komunikasi', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:48');
INSERT INTO `coa` VALUES (6, 'Penginapan', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:49');
INSERT INTO `coa` VALUES (7, 'Alat Tulis Kantor', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:50');
INSERT INTO `coa` VALUES (8, 'Maintenance & Perbaikan', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:51');
INSERT INTO `coa` VALUES (9, 'General Support', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:51');
INSERT INTO `coa` VALUES (10, 'Support Project', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:53');
INSERT INTO `coa` VALUES (11, 'Lain - Lain', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:54');
INSERT INTO `coa` VALUES (12, 'PAJAK', 1, '2018-05-14 11:11:26', '2018-05-14 11:11:55');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tlp` char(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `prov` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kota_kab` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kec` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pos` int(5) NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (97, 'Rizal Widiantoro', 'rijalboy98@gmail.com', '081249780453', 'Jawa Timur', 'Surabaya', 'Sawahan', 0, 'Bank Mandiri(Persero) : Tbk. Jl. Jendral Sudirman no. 11 Tegal 52131 ', 1, '2017-11-13 06:54:52');
INSERT INTO `customers` VALUES (98, 'Pelanggan A', 'plg@yahoo.co.id', '081249780453', 'Jawa Timur', 'Surabaya', 'Karangpilang', 0, 'Perumahan Maharaja Village BLOK BM 20, Griya kebraon tengah, Kebraon 5, Dekat masjid Madina Luhur. \r\nKec. Karangpilang, Kota/Kab. Surabaya \r\nJawa Timur, 60222 \r\n', 1, '2017-11-16 05:02:36');
INSERT INTO `customers` VALUES (99, 'Plg B', 'plgb@gmail.com', '081999222999', 'Jioij', 'Jiijjio', 'Jiojioijo', NULL, 'address PLG B', 1, '2018-01-23 08:05:59');
INSERT INTO `customers` VALUES (100, 'Jatmiko Pdam', NULL, '082777222888', NULL, NULL, NULL, NULL, NULL, 1, '2018-01-24 04:20:13');
INSERT INTO `customers` VALUES (101, 'Dmc Anang Bakti', 'anang.bakti@gmail.com', '08113080340', 'Jawa Timur', 'Surabaya', 'Karangpilang', NULL, 'Perumahan Maharaja Village BLOK BM 20, Griya kebraon tengah, Kebraon 5, Dekat masjid Madina Luhur. \r\nKec. Karangpilang, Kota/Kab. Surabaya \r\nJawa Timur, 60222 \r\n', 1, '2018-02-06 14:12:24');
INSERT INTO `customers` VALUES (102, 'Ujang', 'abc@gmail.com', '08113382070', 'Jawa Timur', 'Surabaya', 'Karang Pilang', NULL, 'Maharaja village Bm 20', 1, '2018-03-21 10:42:33');
INSERT INTO `customers` VALUES (103, 'Tri', 'abc@gmail.com', '081331041347', 'Jawa Timur', 'Surabaya', 'Karangpilang', NULL, 'Pondok maritim', 1, '2018-03-22 12:57:51');
INSERT INTO `customers` VALUES (104, 'Ahmad Muttaqien', 'abc@gmail.com', '085314241748', 'Jawa Tengah', 'Tegal', 'Tegal Selatan', NULL, 'Bank Mandiri(Persero) : Tbk. Jl. Jendral Sudirman no. 11 Tegal 52131', 1, '2018-03-27 09:04:13');
INSERT INTO `customers` VALUES (105, 'Manager Bsm', '', '1234', '', '', '', NULL, '', 1, '2018-03-27 13:11:59');
INSERT INTO `customers` VALUES (106, 'Jatmiko', '', '1234', '', '', '', NULL, '', 1, '2018-03-27 13:13:02');
INSERT INTO `customers` VALUES (107, 'Nita Febri', 'abc@gmail.com', '082135350570', 'DI Yogyakarta', 'Bantul', 'Umbulharjo', NULL, 'Jl. Balirejo 1 gang tunas melati griya timoho estate no B1 rt 022 rw 007 kel. Muja muju kec. Umbulharjo Yogyakarta 55165', 1, '2018-04-20 14:09:36');
INSERT INTO `customers` VALUES (108, 'Arifin', 'abc@gmail.com', '082122969655', 'Jawa Timur', 'Surabaya', 'Karangpilang', NULL, 'Maharaja village bm20', 1, '2018-04-23 10:36:26');
INSERT INTO `customers` VALUES (109, 'Ahmad Yusri', 'abc@gmail.com', '081241577075', 'Sulawesi Selatan', 'Palopo', 'Wara Selatan', NULL, 'Jl. Jend Sudirman VII (BTN Griya Situju) Blok D No. 06 Songka Kec. Wara Selatan Kota Palopo, KOTA PALOPO, WARA SELATAN, SULAWESI SELATAN, ID, 91959', 1, '2018-04-23 12:18:05');
INSERT INTO `customers` VALUES (110, 'Pras', 'abc@gmail.com', '08563651696', 'Jawa Timur', 'Surabaya', 'Rungkut', NULL, 'Pras  (+62) 8563651696\r\nJln. Bung tomo No.39 Surabaya. (STMJ bu Nunuk), KOTA SURABAYA, WONOKROMO, JAWA TIMUR, ID, 60246', 1, '2018-04-23 12:21:35');
INSERT INTO `customers` VALUES (111, 'Arinal Haqo', 'abc@gmail.com', '085850514161', 'Jawa Timur', 'Pasuruan', 'Winongan', NULL, 'Arinal Khaqqo  (+62) 85850514161\r\nklinik al syafi winongan pasuruan, KAB. PASURUAN, WINONGAN, JAWA TIMUR, ID, 67182', 1, '2018-04-23 12:25:39');
INSERT INTO `customers` VALUES (112, 'Fann', 'abc@gmail.com', '081333002434', 'Jawa Timur', 'Surabaya', 'Rungkut', NULL, 'Fann  (+62) 81333002434\r\nWisma kedung asem indah blok e 11 kedung baruk rungkut, KOTA SURABAYA, RUNGKUT, JAWA TIMUR, ID, 60298', 1, '2018-04-23 12:29:01');
INSERT INTO `customers` VALUES (113, 'Fann', 'abc@gmail.com', '081333002434', 'Jawa Timur', 'Surabaya', 'Rungkut', NULL, 'Fann  (+62) 81333002434\r\n						Wisma kedung asem indah blok e 11 kedung baruk rungkut, KOTA SURABAYA, RUNGKUT, JAWA TIMUR, ID, 60298', 1, '2018-04-23 12:31:19');
INSERT INTO `customers` VALUES (114, 'Nisa Nurulfalah', 'abc@gmail.com', '083822633774', 'Jawa Barat', 'Bandung', 'Ciroyom', NULL, 'Jl. And it gg. Sastra no 96/78 Rw 04 Rt 07 kel. Croom KOTA BANDUNG JAWABARAT', 1, '2018-04-28 15:24:54');
INSERT INTO `customers` VALUES (115, 'Shanti Nur', 'abc@gmail.com', '081233001612', 'Jawa Timur', 'Gresik', 'Menganti', NULL, 'menganti gresik', 1, '2018-04-28 15:57:09');
INSERT INTO `customers` VALUES (116, 'Joko Setyawan', 'abc@gmail.com', '082230227809', 'Jawa Timur', 'Sidoarjo', 'Buduran', NULL, 'Ds. Prasung Rt 01 Rw 01 Kec. Buduran Kab. Sidoarjo ( Rumah bapak samiadi ) Buduran Kab. Sidoarjo Jawatimur', 1, '2018-05-02 08:31:53');
INSERT INTO `customers` VALUES (117, 'Nella Zaimus', 'abc@gmail.com', '081357314171', 'Jawa Timur', 'Surabaya', 'Sukolilo', NULL, 'Manyar Jaya praja III blok D-32 Sukolilo Surabaya Jawatimur', 1, '2018-05-08 16:11:26');
INSERT INTO `customers` VALUES (118, 'Jefri Mikael Divanli', 'abc@gmail.com', '087893428633', 'Riau', 'Pekanbaru', 'Senapelan', NULL, 'Jalan Ahmad Yani 25, KOTA PEKANBARU, SANAPELAN, RIAU', 1, '2018-05-12 10:00:53');

-- ----------------------------
-- Table structure for dropshipper
-- ----------------------------
DROP TABLE IF EXISTS `dropshipper`;
CREATE TABLE `dropshipper`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_ds` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tlp` char(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dropshipper
-- ----------------------------
INSERT INTO `dropshipper` VALUES (1, 'Hatobe', '082122969655', 1, '2018-05-14 12:28:54');
INSERT INTO `dropshipper` VALUES (2, 'dropshipper A', '081999222999', 1, '2018-05-14 12:28:54');
INSERT INTO `dropshipper` VALUES (3, 'dropshipper B', '08223222222', 1, '2018-05-14 12:28:54');
INSERT INTO `dropshipper` VALUES (4, 'dropshipper C', '082999999999', 1, '2018-05-14 12:28:54');

-- ----------------------------
-- Table structure for forwarder
-- ----------------------------
DROP TABLE IF EXISTS `forwarder`;
CREATE TABLE `forwarder`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kd_fw` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nm_fw` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tlp` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qq` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wechat` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of forwarder
-- ----------------------------
INSERT INTO `forwarder` VALUES (1, 'YB', 'Yudi Batam', '081270799969', 'a', 'a', 1, '2018-03-22 13:39:51');
INSERT INTO `forwarder` VALUES (2, 'Y', 'Yudha  ', '081281667786', 'a', 'a', 1, '2018-03-22 13:40:41');
INSERT INTO `forwarder` VALUES (3, 'S', 'sadsadds', '2314342', 'dsd', 'werwre', 19, '2018-07-17 12:34:57');

-- ----------------------------
-- Table structure for grosir
-- ----------------------------
DROP TABLE IF EXISTS `grosir`;
CREATE TABLE `grosir`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_md` int(11) UNSIGNED NULL DEFAULT NULL,
  `qty` smallint(5) UNSIGNED NULL DEFAULT NULL,
  `harga` int(11) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of grosir
-- ----------------------------
INSERT INTO `grosir` VALUES (19, 1, 50, 9000, '2018-01-23 04:48:14');
INSERT INTO `grosir` VALUES (20, 1, 60, 8000, '2018-01-23 04:48:14');
INSERT INTO `grosir` VALUES (21, 2, 10, 10000, '2018-01-23 04:49:48');
INSERT INTO `grosir` VALUES (22, 1, 50, 9500, '2018-01-23 04:52:53');
INSERT INTO `grosir` VALUES (24, 2, 10, 80000, '2018-01-23 04:53:20');
INSERT INTO `grosir` VALUES (25, 49, 3, 31000, '2018-01-24 07:43:21');
INSERT INTO `grosir` VALUES (26, 49, 6, 25000, '2018-01-24 07:43:21');

-- ----------------------------
-- Table structure for item_attr
-- ----------------------------
DROP TABLE IF EXISTS `item_attr`;
CREATE TABLE `item_attr`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nm_attr` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of item_attr
-- ----------------------------
INSERT INTO `item_attr` VALUES (1, 'warna', 1, '2018-01-10 14:09:13');
INSERT INTO `item_attr` VALUES (2, 'bahan', 1, '2018-01-10 14:09:19');
INSERT INTO `item_attr` VALUES (3, 'size', 1, '2018-01-10 14:09:26');
INSERT INTO `item_attr` VALUES (5, 'daya', 1, '2018-01-17 14:37:31');
INSERT INTO `item_attr` VALUES (6, 'Merk', 1, '2018-01-26 14:03:31');
INSERT INTO `item_attr` VALUES (7, 'warna', 19, '2018-07-17 12:32:49');

-- ----------------------------
-- Table structure for item_varian
-- ----------------------------
DROP TABLE IF EXISTS `item_varian`;
CREATE TABLE `item_varian`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_attr` int(11) UNSIGNED NULL DEFAULT NULL,
  `nm_varian` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of item_varian
-- ----------------------------
INSERT INTO `item_varian` VALUES (2, 2, 'katun', 1, '2018-01-10 14:09:19');
INSERT INTO `item_varian` VALUES (3, 3, 'XL', 1, '2018-01-10 14:09:26');
INSERT INTO `item_varian` VALUES (4, 3, 'L', 1, '2018-01-10 14:18:32');
INSERT INTO `item_varian` VALUES (5, 3, 'S', 1, '2018-01-10 14:18:38');
INSERT INTO `item_varian` VALUES (6, 3, 'M', 1, '2018-01-10 14:18:46');
INSERT INTO `item_varian` VALUES (7, 1, 'Hitam', 1, '2018-01-10 14:21:47');
INSERT INTO `item_varian` VALUES (8, 1, 'Merah', 1, '2018-01-10 14:21:47');
INSERT INTO `item_varian` VALUES (9, 1, 'Biru', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (10, 1, 'Putih', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (11, 1, 'Hijau', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (12, 1, 'Maroon', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (13, 1, 'Pink', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (14, 1, 'Navy', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (15, 1, 'Tosca', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (16, 1, 'Ungu', 1, '2018-01-10 14:21:48');
INSERT INTO `item_varian` VALUES (22, 5, '5 watt', 1, '2018-01-17 14:38:05');
INSERT INTO `item_varian` VALUES (23, 5, '9 watt', 1, '2018-01-17 14:38:13');
INSERT INTO `item_varian` VALUES (24, 5, '12 watt', 1, '2018-01-17 14:38:19');
INSERT INTO `item_varian` VALUES (27, 2, 'kulit buaya', 1, '2018-01-29 13:07:10');
INSERT INTO `item_varian` VALUES (28, 2, 'kulit domba', 1, '2018-01-29 13:07:10');
INSERT INTO `item_varian` VALUES (29, 6, 'Sony', 1, '2018-01-29 13:11:34');
INSERT INTO `item_varian` VALUES (30, 6, 'Xiaomi', 1, '2018-01-29 13:11:34');
INSERT INTO `item_varian` VALUES (31, 6, 'sharp', 1, '2018-01-29 13:11:34');
INSERT INTO `item_varian` VALUES (33, 1, 'Grey', 1, '2018-03-22 13:32:50');
INSERT INTO `item_varian` VALUES (34, 7, 'kuning', 19, '2018-07-17 12:32:59');
INSERT INTO `item_varian` VALUES (35, 7, 'hijau', 19, '2018-07-17 12:32:59');

-- ----------------------------
-- Table structure for kurir
-- ----------------------------
DROP TABLE IF EXISTS `kurir`;
CREATE TABLE `kurir`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nm_kurir` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `layanan` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kurir
-- ----------------------------
INSERT INTO `kurir` VALUES (1, 'JNE Express', 'JNE Reguler', 1, '2017-10-03 09:27:17', '2018-05-14 12:34:35');
INSERT INTO `kurir` VALUES (2, 'JNE Express', 'JNE Yes', 1, '2017-11-09 09:55:35', '2018-05-14 12:34:35');
INSERT INTO `kurir` VALUES (3, 'TIKI', 'TIKI Reg', 1, '2017-10-03 09:27:17', '2018-05-14 12:34:35');
INSERT INTO `kurir` VALUES (4, 'TIKI', 'TIKI ONS', 1, '2017-11-09 10:12:47', '2018-05-14 12:34:35');
INSERT INTO `kurir` VALUES (6, 'POS INDONESIA', 'Pos Kilat Khusus', 1, '2017-11-10 15:51:26', '2018-05-14 12:34:35');
INSERT INTO `kurir` VALUES (7, 'POS INDONESIA', 'Pos Express Next Day Service', 1, '2017-11-10 15:51:27', '2018-05-14 12:34:35');
INSERT INTO `kurir` VALUES (100, 'HATOBE', 'Internal Hatobe', 1, '2018-02-26 11:43:34', '2018-05-14 12:34:35');

-- ----------------------------
-- Table structure for marketplace
-- ----------------------------
DROP TABLE IF EXISTS `marketplace`;
CREATE TABLE `marketplace`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nm_market` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nm_singkat` char(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of marketplace
-- ----------------------------
INSERT INTO `marketplace` VALUES (2, 'Tokopedia', 'TOPED', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (3, 'Lazada', 'LZD', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (4, 'Shopee', 'SHP', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (5, 'Blibli', 'BB', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (11, 'Bukalapak', 'BL', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (100, 'Hatobe', 'HTB', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (101, 'Lain lain', 'Lain lain', 1, '2018-05-07 11:08:00', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (102, 'Facebook / WA', 'FB / WA', 1, '2018-05-07 11:11:05', '2018-05-14 12:35:37');
INSERT INTO `marketplace` VALUES (103, 'OFFLINE', 'Offline', 1, '2018-05-14 12:35:26', '2018-05-14 12:35:37');

-- ----------------------------
-- Table structure for master_material
-- ----------------------------
DROP TABLE IF EXISTS `master_material`;
CREATE TABLE `master_material`  (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `kode_warna` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` char(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_ktg` int(11) NOT NULL,
  `merk` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `berat` decimal(5, 0) UNSIGNED NOT NULL,
  `satuan_berat` char(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_dropship` tinyint(1) NOT NULL DEFAULT 0,
  `store_id` int(11) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`kode`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 111 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of master_material
-- ----------------------------
INSERT INTO `master_material` VALUES (4, '#3e11ab', 'Lampu Smart Charge', 1, '', 150, 'g', 'Lampu LED Emergency', 0, 1, 10, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (5, '#b85eb0', 'Tas Tumi Saratoga', 2, '', 800, 'g', 'Sling bag Pria Tumi Saratoga', 0, 1, 10, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (6, '#7efad5', 'Tas Tumi Luanda', 3, '', 220, 'g', 'Tas Wanita Tumi Luanda Flightbag', 0, 1, 10, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (8, '#9fcbba', 'Slingbag Antitheft', 2, '', 300, 'g', '', 0, 1, 10, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (9, '#995bfa', 'Tas Tumi Luanda', 3, '', 200, 'g', '', 0, 1, 10, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (10, '#8fd84d', 'Tas lipat strawberry/Tas belanja', 4, '', 30, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (11, '#b0b2b0', 'Gantungan kunci siul on off', 1, '', 30, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (12, '#18df06', 'Angpao lebaran', 4, '', 10, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (13, '#e1fe20', 'Glue gun 110-220v', 1, '', 700, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (14, '#37ac48', 'Cetakan nasi bekal', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (15, '#f7bf6f', 'Cetakan animal', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (16, '#0a4101', 'Baterai ultrafire', 1, '', 30, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (17, '#edfea5', 'Keranjang laundry', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (18, '#871aff', 'Keranjang laundry', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (19, '#a0af25', 'Keranjang laundry', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (20, '#1f99ca', 'Keranjang laundry', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (21, '#20ff5d', 'Keset microfiber', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (22, '#ac1144', 'Keset microfiber', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (23, '#c13bd5', 'Keset microfiber', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (24, '#b86f4e', 'Keset microfiber', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (25, '#9a77b9', 'Macora skin care', 5, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (26, '#92d503', 'Macora skin care', 5, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (27, '#5eb21c', 'Khimar salsa', 3, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (28, '#906dab', 'Khimar salsa', 3, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (29, '#a2c641', 'Khimar salsa', 3, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (30, '#b72bf4', 'Khimar salsa', 3, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (31, '#1e9450', 'Kacamata Anti silau', 2, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (32, '#7390e7', 'Lampu nyamuk LED', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (33, '#5e76bc', 'Obeng set', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (34, '#436c5b', 'Segiempat Rempel kombi', 3, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (35, '#36be9f', 'Malika square', 3, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (36, '#46ee05', 'Aisyah square', 3, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (37, '#dcc7fa', 'Pompa gallon elektrik', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (38, '#f416d4', 'Kemoceng bisa dipanjangkan', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (39, '#8986d0', 'Card rider usb', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (40, '#221305', 'Blood pressure monitor', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (41, '#4093a4', 'test', 2, '', 1223, 'g', 'ssadsad', 1, 1, 10, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (42, '#c2cac9', 'kantong resleting baju', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (43, '#3484ae', 'Pemotong Semangka', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (44, '#9393b9', 'Lentera Terbang', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (45, '#dc708f', 'Meat Grinder Besar', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (46, '#d62b3a', 'Sarung Tangan Microfiner', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (47, '#aeb4ce', 'Catok Keriting Profesional', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (48, '#56b3e0', 'Serut Jagung', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (49, '#ea2a6c', 'Tas Sepatu Futsal', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (50, '#a2ecd9', 'Pad Egg / pemisah telur', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (51, '#7f2657', 'Vapur Botol', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (52, '#bab974', 'Sendok Travel', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (53, '#4ed81c', 'Clever Cutter', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (54, '#bf1773', 'Riddex Gen 2', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (55, '#bbecad', 'Topi Keramas Anak', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (56, '#2901fc', 'Lakban Bening', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (57, '#0b530a', 'Tutup Pentil Ban Nyala', 9, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (58, '#0e28f4', 'Ringstand Karakter', 8, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (59, '#631f30', 'Money Detector Kecil', 8, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (60, '#15387a', 'Tongsis Bluetooth Kabel', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (61, '#e1175f', 'Pompa Ban bentuk ban', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (62, '#84057c', 'Kabel data tali sepatu', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (63, '#5b00de', 'Otg Robot', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (64, '#3944a5', 'Charger Baterai Double', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (65, '#9aac59', 'Teflon Mini', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (66, '#f0e280', 'Catok Sisir Besar', 1, '', 150, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (67, '#35796b', 'Touch U Karakter', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (68, '#f853f9', 'Senter Police Mini', 1, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (69, '#2a048e', 'Pembersih Kerak Gosong', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (70, '#0be95c', 'Engrave I', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (71, '#b84a1f', 'Tempat Sikat Gigi Warna', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (72, '#5d036e', 'Dispen Sabun Double', 1, '', 150, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (73, '#773f31', 'Animal Duster', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (74, '#1a70cf', 'Potatto Cutter', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (75, '#8caf3c', 'Lensa Super  Wide', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (76, '#bf1638', 'Jam Moody', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (77, '#2f41a6', 'Dompet Kartu Aluminium', 1, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (78, '#7e400a', 'Charger Baterai Single', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (79, '#4481bd', 'Fix it Propen', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (80, '#aea056', 'Batok Kepala Adaptor', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (81, '#e7029e', 'Tas Kosmetik Polos', 3, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (82, '#15151a', 'CCTV Palsu', 1, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (83, '#41fea0', 'Tali Jemuran', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (84, '#80879d', 'Timbangan Dapur', 4, '', 200, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (85, '#463db1', 'Tirai Love', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (86, '#602440', 'Lampu Tempel', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (87, '#3b5426', 'Korek Kuping LED', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (88, '#e4bb02', 'RGB', 1, '', 200, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (89, '#f55309', 'EZ Jet Water Cannon', 1, '', 200, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (90, '#cd6b89', 'Kanebo Besar', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (91, '#757501', 'Disdon Gen I', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (92, '#42ed10', 'Disdon Gen 2', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (93, '#f22b71', 'Timbangan Gantung', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (94, '#4488ad', 'Saringan Filter Air', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (95, '#6a1034', 'Tasbih Kecil', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (96, '#ff9521', 'Fitting Bohlam', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (97, '#8204fc', 'Hanger Jemuran Baju', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (98, '#538c13', 'Obeng 3 in 1', 4, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (99, '#aa59fc', 'Lampu Nyamuk', 1, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (100, '#18425d', 'Kacamata Night View', 2, '', 100, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (101, '#99462a', 'Mangkok Masker', 4, '', 70, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (102, '#2476a1', 'Sikat WC Lengkung', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (103, '#c19b8d', 'Trip Grip', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (104, '#d1d1e3', 'Sisik Ikan Gen I', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (105, '#8b4f10', 'Card Rider 1 Slot', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (106, '#975d00', 'Kabel Hippo', 1, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (107, '#c9424c', 'Travel Pillow', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (108, '#21462c', 'Kemoceng Panjang Pendek', 4, '', 200, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (109, '#9e6548', 'Jas Hujan Kresek', 4, '', 50, 'g', '', 0, 1, 28, '2018-05-14 12:37:39');
INSERT INTO `master_material` VALUES (110, '#dc608d', 'test', 1, '', 12, 'g', 'adssad', 0, 19, 28, '2018-07-17 12:33:20');

-- ----------------------------
-- Table structure for material
-- ----------------------------
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kode` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_attr` int(11) NOT NULL,
  `id_varian` int(11) NOT NULL,
  `stok_awal` smallint(5) NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 156 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of material
-- ----------------------------
INSERT INTO `material` VALUES (42, '4', 'e5b3a7913057294a8cc584273712a421.jpg', 5, 22, 0, 1, 10, '2018-01-24 06:39:06');
INSERT INTO `material` VALUES (43, '4', 'ccbd6fb17d21924796844e940e4803d4.jpg', 5, 23, 0, 1, 10, '2018-01-24 06:39:06');
INSERT INTO `material` VALUES (44, '4', '0c8550e5f16e6604346addd5bcda8698.jpg', 5, 24, 0, 1, 10, '2018-01-24 06:39:06');
INSERT INTO `material` VALUES (45, '5', '4e8184139b688438ef195ad8d7d3da65.png', 1, 7, 0, 1, 10, '2018-01-24 07:15:04');
INSERT INTO `material` VALUES (46, '5', '43d344f7023c4f5faba69ab06e9e5175.png', 1, 9, 0, 1, 10, '2018-01-24 07:15:04');
INSERT INTO `material` VALUES (47, '6', 'aaccbeebed454bbd4a8007015d52651d.jpg', 1, 7, 0, 1, 10, '2018-01-24 07:16:42');
INSERT INTO `material` VALUES (48, '6', '2982573c97bd5274dfefedf89828f85e.jpg', 1, 8, 0, 1, 10, '2018-01-24 07:16:42');
INSERT INTO `material` VALUES (50, '8', 'not_set.png', 1, 7, 0, 1, 10, '2018-03-22 13:34:15');
INSERT INTO `material` VALUES (51, '8', 'not_set.png', 1, 33, 0, 1, 10, '2018-03-22 13:34:15');
INSERT INTO `material` VALUES (52, '9', 'not_set.png', 1, 7, 0, 1, 10, '2018-04-19 12:59:05');
INSERT INTO `material` VALUES (53, '9', 'not_set.png', 1, 8, 0, 1, 10, '2018-04-19 12:59:05');
INSERT INTO `material` VALUES (54, '10', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-23 10:04:03');
INSERT INTO `material` VALUES (55, '11', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-23 10:05:14');
INSERT INTO `material` VALUES (56, '12', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-23 10:06:04');
INSERT INTO `material` VALUES (57, '13', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:07:16');
INSERT INTO `material` VALUES (58, '14', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:08:25');
INSERT INTO `material` VALUES (59, '15', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-23 10:08:57');
INSERT INTO `material` VALUES (60, '16', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-23 10:09:27');
INSERT INTO `material` VALUES (61, '17', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-23 10:11:24');
INSERT INTO `material` VALUES (62, '18', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:12:17');
INSERT INTO `material` VALUES (63, '19', 'not_set.png', 1, 13, 0, 1, 28, '2018-04-23 10:18:32');
INSERT INTO `material` VALUES (64, '20', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-23 10:19:26');
INSERT INTO `material` VALUES (65, '21', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-23 10:20:48');
INSERT INTO `material` VALUES (66, '22', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:21:37');
INSERT INTO `material` VALUES (67, '23', 'not_set.png', 1, 13, 0, 1, 28, '2018-04-23 10:22:04');
INSERT INTO `material` VALUES (68, '24', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-23 10:22:25');
INSERT INTO `material` VALUES (69, '25', 'not_set.png', 1, 13, 0, 1, 28, '2018-04-23 10:29:23');
INSERT INTO `material` VALUES (70, '26', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:29:51');
INSERT INTO `material` VALUES (71, '27', 'not_set.png', 1, 13, 0, 1, 28, '2018-04-23 10:31:11');
INSERT INTO `material` VALUES (72, '28', 'not_set.png', 1, 33, 0, 1, 28, '2018-04-23 10:31:40');
INSERT INTO `material` VALUES (73, '29', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:32:05');
INSERT INTO `material` VALUES (74, '30', 'not_set.png', 1, 16, 0, 1, 28, '2018-04-23 10:32:41');
INSERT INTO `material` VALUES (75, '31', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-23 10:34:13');
INSERT INTO `material` VALUES (76, '32', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:39:17');
INSERT INTO `material` VALUES (77, '33', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 10:40:25');
INSERT INTO `material` VALUES (78, '34', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-23 11:38:09');
INSERT INTO `material` VALUES (79, '35', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-23 11:39:12');
INSERT INTO `material` VALUES (80, '36', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-23 11:40:01');
INSERT INTO `material` VALUES (81, '37', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 11:40:57');
INSERT INTO `material` VALUES (82, '38', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-23 11:41:47');
INSERT INTO `material` VALUES (83, '39', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-23 11:43:32');
INSERT INTO `material` VALUES (84, '40', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-23 11:45:20');
INSERT INTO `material` VALUES (85, '41', 'not_set.png', 1, 10, 0, 1, 10, '2018-04-25 15:14:45');
INSERT INTO `material` VALUES (86, '42', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:11:23');
INSERT INTO `material` VALUES (87, '43', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:12:30');
INSERT INTO `material` VALUES (88, '44', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:13:51');
INSERT INTO `material` VALUES (89, '45', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:14:31');
INSERT INTO `material` VALUES (90, '46', 'not_set.png', 1, 11, 0, 1, 28, '2018-04-27 14:15:21');
INSERT INTO `material` VALUES (91, '47', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:16:08');
INSERT INTO `material` VALUES (92, '48', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:17:05');
INSERT INTO `material` VALUES (93, '49', 'not_set.png', 1, 13, 0, 1, 28, '2018-04-27 14:17:53');
INSERT INTO `material` VALUES (94, '50', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:33:54');
INSERT INTO `material` VALUES (95, '51', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:35:24');
INSERT INTO `material` VALUES (96, '52', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:36:18');
INSERT INTO `material` VALUES (97, '53', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:37:36');
INSERT INTO `material` VALUES (98, '54', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:38:19');
INSERT INTO `material` VALUES (99, '55', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:39:04');
INSERT INTO `material` VALUES (100, '56', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:39:39');
INSERT INTO `material` VALUES (101, '57', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:45:24');
INSERT INTO `material` VALUES (102, '58', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-27 14:46:27');
INSERT INTO `material` VALUES (103, '59', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:47:04');
INSERT INTO `material` VALUES (104, '60', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:48:02');
INSERT INTO `material` VALUES (105, '61', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-27 14:48:40');
INSERT INTO `material` VALUES (106, '62', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 09:37:29');
INSERT INTO `material` VALUES (107, '63', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:38:50');
INSERT INTO `material` VALUES (108, '64', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:39:38');
INSERT INTO `material` VALUES (109, '65', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:40:04');
INSERT INTO `material` VALUES (110, '66', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:40:45');
INSERT INTO `material` VALUES (111, '67', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:41:29');
INSERT INTO `material` VALUES (112, '68', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:42:13');
INSERT INTO `material` VALUES (113, '69', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:43:25');
INSERT INTO `material` VALUES (114, '70', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:44:28');
INSERT INTO `material` VALUES (115, '71', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 09:45:39');
INSERT INTO `material` VALUES (116, '72', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 09:47:23');
INSERT INTO `material` VALUES (117, '73', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 09:47:53');
INSERT INTO `material` VALUES (118, '74', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 10:32:00');
INSERT INTO `material` VALUES (119, '75', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 10:33:03');
INSERT INTO `material` VALUES (120, '76', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 10:34:06');
INSERT INTO `material` VALUES (121, '77', 'not_set.png', 1, 13, 0, 1, 28, '2018-04-28 10:35:16');
INSERT INTO `material` VALUES (122, '78', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 10:36:01');
INSERT INTO `material` VALUES (123, '79', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 10:36:46');
INSERT INTO `material` VALUES (124, '80', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 10:37:21');
INSERT INTO `material` VALUES (125, '81', 'not_set.png', 1, 11, 0, 1, 28, '2018-04-28 10:38:03');
INSERT INTO `material` VALUES (126, '82', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 10:38:46');
INSERT INTO `material` VALUES (127, '83', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 10:39:38');
INSERT INTO `material` VALUES (128, '84', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 10:40:20');
INSERT INTO `material` VALUES (129, '85', 'not_set.png', 1, 16, 0, 1, 28, '2018-04-28 10:40:59');
INSERT INTO `material` VALUES (130, '86', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:05:14');
INSERT INTO `material` VALUES (131, '87', 'not_set.png', 1, 11, 0, 1, 28, '2018-04-28 11:06:00');
INSERT INTO `material` VALUES (132, '88', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:09:33');
INSERT INTO `material` VALUES (133, '89', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:10:56');
INSERT INTO `material` VALUES (134, '90', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:37:32');
INSERT INTO `material` VALUES (135, '91', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:38:30');
INSERT INTO `material` VALUES (136, '92', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:38:59');
INSERT INTO `material` VALUES (137, '93', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 11:41:05');
INSERT INTO `material` VALUES (138, '94', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 11:41:50');
INSERT INTO `material` VALUES (139, '95', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-28 11:42:14');
INSERT INTO `material` VALUES (140, '96', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 11:44:22');
INSERT INTO `material` VALUES (141, '97', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 11:44:52');
INSERT INTO `material` VALUES (142, '98', 'not_set.png', 1, 10, 0, 1, 28, '2018-04-28 12:30:33');
INSERT INTO `material` VALUES (143, '99', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 12:31:07');
INSERT INTO `material` VALUES (144, '100', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 12:31:51');
INSERT INTO `material` VALUES (145, '101', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 12:33:46');
INSERT INTO `material` VALUES (146, '102', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 12:34:16');
INSERT INTO `material` VALUES (147, '103', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 12:35:30');
INSERT INTO `material` VALUES (148, '104', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 12:36:16');
INSERT INTO `material` VALUES (149, '105', 'not_set.png', 1, 8, 0, 1, 28, '2018-04-28 12:36:56');
INSERT INTO `material` VALUES (150, '106', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 12:37:37');
INSERT INTO `material` VALUES (151, '107', 'not_set.png', 1, 7, 0, 1, 28, '2018-04-28 12:38:14');
INSERT INTO `material` VALUES (152, '108', 'not_set.png', 1, 11, 0, 1, 28, '2018-04-28 12:38:51');
INSERT INTO `material` VALUES (153, '109', 'not_set.png', 1, 9, 0, 1, 28, '2018-04-28 12:41:29');
INSERT INTO `material` VALUES (154, '110', 'not_set.png', 7, 34, 0, 19, 28, '2018-07-17 12:33:20');
INSERT INTO `material` VALUES (155, '110', 'not_set.png', 7, 35, 0, 19, 28, '2018-07-17 12:33:20');

-- ----------------------------
-- Table structure for material_detail
-- ----------------------------
DROP TABLE IF EXISTS `material_detail`;
CREATE TABLE `material_detail`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `kd_tagihan` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kd_order` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `buyer` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_material` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_sup` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_fw` int(11) NULL DEFAULT NULL,
  `id_market` int(11) UNSIGNED NULL DEFAULT NULL,
  `hgsat_dasar` int(11) NULL DEFAULT NULL,
  `hgtot_dasar` int(11) UNSIGNED NULL DEFAULT NULL,
  `hgtot_beli` int(11) NULL DEFAULT NULL,
  `hgsat_jual` int(11) UNSIGNED NULL DEFAULT NULL,
  `harga_mod` int(11) UNSIGNED NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `qty` int(11) UNSIGNED NULL DEFAULT NULL,
  `qty_order_bm` int(11) NULL DEFAULT NULL,
  `read` tinyint(3) UNSIGNED NULL DEFAULT NULL,
  `id_kurir` int(11) NULL DEFAULT NULL,
  `resi` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `resi_photo` char(70) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_pay` int(11) NULL DEFAULT NULL,
  `paid_photo` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paid_note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tipe_transfer` tinyint(3) NULL DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NULL DEFAULT NULL,
  `tipe_transaksi` tinyint(3) NULL DEFAULT NULL,
  `buyer_note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `return_at` datetime(0) NULL DEFAULT NULL,
  `done_at` datetime(0) NULL DEFAULT NULL,
  `sent_at` datetime(0) NULL,
  `sent_photo` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `done_photo` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `approved_by` int(11) UNSIGNED NULL DEFAULT NULL,
  `rejected_by` int(11) UNSIGNED NULL DEFAULT NULL,
  `created_by` int(11) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `approved_at` datetime(0) NULL,
  `rejected_at` datetime(0) NULL,
  `updated_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `accept_at` date NULL DEFAULT NULL,
  `order_at` timestamp(0) NULL DEFAULT NULL,
  `ppn` tinyint(3) NULL DEFAULT NULL,
  `pph` tinyint(3) NULL DEFAULT NULL,
  `bea_masuk` tinyint(3) NULL DEFAULT NULL,
  `dll` int(11) NULL DEFAULT NULL,
  `ongkir_impor` int(11) UNSIGNED NULL DEFAULT NULL,
  `ongkir_buyer` int(11) UNSIGNED NULL DEFAULT NULL,
  `hgsat_retail` int(11) NULL DEFAULT NULL,
  `hgsat_reseller` int(11) NULL DEFAULT NULL,
  `is_grosir` tinyint(1) UNSIGNED NULL DEFAULT 0,
  `is_gived` tinyint(1) NULL DEFAULT 0,
  `cancel_note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `retur_note` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tipe_retur` tinyint(3) NULL DEFAULT NULL,
  `send_by` int(11) NULL DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 202 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of material_detail
-- ----------------------------
INSERT INTO `material_detail` VALUES (49, '4944504451', '', NULL, 42, 1, NULL, NULL, 6090, NULL, 7117000, NULL, 12554, '', 100, 100, NULL, NULL, '', '', NULL, '', '', 9, 4, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-01-24 06:44:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '2018-01-23 23:44:03', 0, 0, 0, NULL, 3232000, NULL, 37500, 31000, 1, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (50, '4944504451', '', NULL, 43, 1, NULL, NULL, 7560, NULL, 7117000, NULL, 14024, '', 200, 200, NULL, NULL, '', '', NULL, '', '', 9, 4, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-01-24 06:44:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '2018-01-23 23:44:03', 0, 0, 0, NULL, 3232000, NULL, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (51, '4944504451', '', NULL, 44, 1, NULL, NULL, 8820, NULL, 7117000, NULL, 15284, '', 200, 200, NULL, NULL, '', '', NULL, '', '', 9, 4, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-01-24 06:44:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '2018-01-23 23:44:03', 0, 0, 0, NULL, 3232000, NULL, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (54, '4944504451', '54', 101, 43, NULL, NULL, 103, NULL, NULL, NULL, 28000, 14024, NULL, 7, NULL, 0, 100, NULL, '', NULL, NULL, '', NULL, 4, 15, ' ', '0000-00-00 00:00:00', '2018-02-06 08:30:51', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 26, '2018-02-06 14:12:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (55, '4944504451', '55', 101, 44, NULL, NULL, 103, NULL, NULL, NULL, 30500, 15284, NULL, 15, NULL, 0, 100, NULL, '', NULL, NULL, '', NULL, 4, 15, NULL, '0000-00-00 00:00:00', '2018-02-06 08:30:51', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 26, '2018-02-06 14:17:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (62, '4944504451', '62', 101, 43, NULL, NULL, 103, NULL, NULL, NULL, 28000, 14024, NULL, 2, NULL, 0, 100, NULL, '', NULL, NULL, '', NULL, 4, 15, NULL, '0000-00-00 00:00:00', '2018-02-12 07:37:39', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 26, '2018-02-11 18:53:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (65, '4944504451', '65', 103, 44, NULL, NULL, 103, NULL, NULL, NULL, 45000, 15284, NULL, 1, NULL, 0, 100, NULL, '', NULL, NULL, '', NULL, 4, 15, NULL, '0000-00-00 00:00:00', '2018-03-22 05:58:33', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-03-22 12:57:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, '', '', NULL, 1, 1);
INSERT INTO `material_detail` VALUES (66, '664267', '', NULL, 50, 6, 2, NULL, 90000, NULL, 9000000, NULL, 90000, '', 50, 50, NULL, NULL, '', '', NULL, '', '', 9, 4, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-03-22 13:42:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '2018-03-22 06:42:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (67, '664267', '', NULL, 51, 6, 2, NULL, 90000, NULL, 9000000, NULL, 90000, '', 50, 50, NULL, NULL, '', '', NULL, '', '', 9, 4, 14, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-03-22 13:42:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '2018-03-22 06:42:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (68, '4944504451', '68', 104, 44, NULL, NULL, 102, NULL, NULL, NULL, 45500, 15284, NULL, 2, NULL, 0, 3, '030091736673', 'c9d0751ccbf3236a7f21c641de334e78_resi.jpg', NULL, '0ff4647a23d8159440208c38504e2da9_paid.jpg', 'Lampu LED 12 watt', NULL, 4, 15, 'Lampu Ajaib 12 watt', '0000-00-00 00:00:00', '2018-04-23 03:38:07', '2018-03-27 09:08:45', NULL, NULL, NULL, NULL, 28, '2018-03-27 09:04:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, '', '', NULL, 1, 1);
INSERT INTO `material_detail` VALUES (70, '4944504451', '70', 105, 44, NULL, NULL, 100, NULL, NULL, NULL, 0, NULL, '', 1, NULL, NULL, 100, '', '', NULL, '', '', NULL, 4, 15, '', '0000-00-00 00:00:00', '2018-03-27 06:11:59', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-03-27 13:11:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (71, '4944504451', '711372', 106, 43, NULL, NULL, 100, NULL, NULL, NULL, 0, NULL, '', 1, NULL, NULL, 100, '', '', NULL, '', '', NULL, 4, 15, '', '0000-00-00 00:00:00', '2018-03-27 06:13:02', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-03-27 13:13:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (72, '4944504451', '711372', 106, 44, NULL, NULL, 100, NULL, NULL, NULL, 0, NULL, '', 1, NULL, NULL, 100, '', '', NULL, '', '', NULL, 4, 15, '', '0000-00-00 00:00:00', '2018-03-27 06:13:02', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-03-27 13:13:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (73, '664267', '73', 100, 50, NULL, NULL, 100, NULL, NULL, NULL, 0, NULL, '', 1, NULL, NULL, 100, '', '', NULL, '', '', NULL, 4, 15, '', '0000-00-00 00:00:00', '2018-03-27 06:16:28', '0000-00-00 00:00:00', '', '', NULL, NULL, 10, '2018-03-27 13:16:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '0000-00-00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 1, '', '', NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (74, '740275', NULL, NULL, 47, 3, NULL, NULL, 200000, NULL, 1200000, NULL, 200000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 10, '2018-04-19 13:02:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-19 06:02:05', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (75, '740275', NULL, NULL, 48, 3, NULL, NULL, 200000, NULL, 1200000, NULL, 200000, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 10, '2018-04-19 13:02:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-19 06:02:05', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (76, '740275', '76', 107, 47, NULL, NULL, 102, NULL, NULL, NULL, 600000, 200000, NULL, 1, NULL, 0, 3, '030092767860', NULL, NULL, '7b99ab033b035ca55c4e0b82c033e40d_paid.jpg', '', NULL, 4, 15, 'Abc', NULL, '2018-04-25 05:01:19', '2018-04-20 09:05:13', NULL, NULL, NULL, NULL, 28, '2018-04-20 14:09:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (77, '4944504451', '77', 108, 44, NULL, NULL, 103, NULL, NULL, NULL, 30500, 15284, NULL, 2, NULL, 0, 100, NULL, NULL, NULL, '93243baedebb7755760c2fa9760b0a26_paid.jpg', '', NULL, 4, 15, 'Abc', NULL, '2018-04-23 03:37:58', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 10:36:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (78, '78', NULL, NULL, 54, 7, NULL, NULL, 6500, NULL, 32500, NULL, 6500, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 11:27:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 04:27:38', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (79, '79', NULL, NULL, 84, 7, NULL, NULL, 125000, NULL, 125000, NULL, 125000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:01:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:01:51', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (80, '80', NULL, NULL, 77, 7, NULL, NULL, 11000, NULL, 11000, NULL, 11000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:02:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:02:53', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (81, '81', NULL, NULL, 78, 8, NULL, NULL, 21000, NULL, 84000, NULL, 21000, NULL, 4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:03:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:03:58', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (82, '82', NULL, NULL, 80, 8, NULL, NULL, 35000, NULL, 35000, NULL, 35000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:05:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:05:10', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (83, '83', NULL, NULL, 81, 7, NULL, NULL, 37000, NULL, 37000, NULL, 37000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:06:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:06:11', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (84, '84', NULL, NULL, 82, 7, NULL, NULL, 16000, NULL, 16000, NULL, 16000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:07:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:07:10', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (85, '85', NULL, NULL, 83, 7, NULL, NULL, 4500, NULL, 4500, NULL, 4500, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:08:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:08:42', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (87, '80', '87', 110, 77, NULL, NULL, 4, NULL, NULL, NULL, 11000, 11000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '7f8e706f429f570884412b45640fdb49_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-24 04:27:58', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:21:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (89, '82', '89', 112, 80, NULL, NULL, 4, NULL, NULL, NULL, 35000, 35000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '349eb4f670ce474502255eb51d4066b0_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-24 04:28:16', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:29:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (90, '83', '90', 113, 81, NULL, NULL, 4, NULL, NULL, NULL, 37000, 37000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '448ef9b23449ed63c03953c9e6941f88_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-24 09:04:47', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:31:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (91, '84', '91', 113, 82, NULL, NULL, 4, NULL, NULL, NULL, 16000, 16000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, 'ef6379680244d2c711fcf9e42830cf9f_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-24 09:04:47', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:32:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (92, '85', '92', 112, 83, NULL, NULL, 4, NULL, NULL, NULL, 4500, 4500, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, 'a1236180e07819211ce637055e751e15_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-24 09:04:47', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:33:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (93, '93', NULL, NULL, 55, 7, NULL, NULL, 9000, NULL, 9000, NULL, 9000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:37:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:37:43', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (94, '94', NULL, NULL, 56, 7, NULL, NULL, 1000, NULL, 1000, NULL, 1000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:38:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:38:18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (95, '95', NULL, NULL, 57, 7, NULL, NULL, 10000, NULL, 50000, NULL, 10000, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:39:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:39:04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (96, '96', NULL, NULL, 58, 7, NULL, NULL, 8000, NULL, 40000, NULL, 8000, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:39:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:39:51', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (97, '97', NULL, NULL, 59, 7, NULL, NULL, 13000, NULL, 65000, NULL, 13000, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:40:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:40:24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (98, '98', NULL, NULL, 60, 7, NULL, NULL, 9500, NULL, 47500, NULL, 9500, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:41:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:41:13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (99, '99', NULL, NULL, 61, 7, NULL, NULL, 14000, NULL, 14000, NULL, 14000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:41:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:41:58', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (100, '100', NULL, NULL, 62, 7, NULL, NULL, 14000, NULL, 28000, NULL, 14000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:42:28', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:42:28', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (101, '101', NULL, NULL, 63, 7, NULL, NULL, 14000, NULL, 14000, NULL, 14000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:43:30', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:43:30', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (102, '102', NULL, NULL, 64, 7, NULL, NULL, 14000, NULL, 14000, NULL, 14000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:44:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:44:01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (103, '103', NULL, NULL, 65, 7, NULL, NULL, 16250, NULL, 16250, NULL, 16250, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:44:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:44:53', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (104, '104', NULL, NULL, 66, 7, NULL, NULL, 16250, NULL, 32500, NULL, 16250, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:45:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:45:22', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (105, '105', NULL, NULL, 67, 7, NULL, NULL, 16250, NULL, 16250, NULL, 16250, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:45:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:45:49', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (106, '106', NULL, NULL, 68, 7, NULL, NULL, 16250, NULL, 16250, NULL, 16250, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:46:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:46:21', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (107, '107', NULL, NULL, 69, 7, NULL, NULL, 210000, NULL, 210000, NULL, 210000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:47:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:47:45', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (108, '108', NULL, NULL, 70, 7, NULL, NULL, 210000, NULL, 210000, NULL, 210000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 12:48:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 05:48:18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (109, '109', NULL, NULL, 71, 8, NULL, NULL, 46000, NULL, 46000, NULL, 46000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 13:02:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 06:02:04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (110, '110', NULL, NULL, 72, 8, NULL, NULL, 46000, NULL, 46000, NULL, 46000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 13:03:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 06:03:12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (111, '111', NULL, NULL, 73, 8, NULL, NULL, 46000, NULL, 46000, NULL, 46000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 13:03:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 06:03:36', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (112, '112', NULL, NULL, 74, 8, NULL, NULL, 46000, NULL, 46000, NULL, 46000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 13:04:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 06:04:06', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (113, '113', NULL, NULL, 75, 7, NULL, NULL, 12500, NULL, 12500, NULL, 12500, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 13:05:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 06:05:18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (114, '114', NULL, NULL, 76, 7, NULL, NULL, 13000, NULL, 13000, NULL, 13000, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-23 13:05:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-23 06:05:59', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (117, '79', '117', 109, 84, NULL, NULL, 4, NULL, NULL, NULL, 125000, 125000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '056d70a9b7e6cb92b84988049ab226cd_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-26 10:10:14', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-26 17:07:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 52000, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (118, '81', '118', 111, 78, NULL, NULL, 4, NULL, NULL, NULL, 21000, 21000, NULL, 4, NULL, 0, 1, NULL, NULL, NULL, 'effeb578dc7edb9cf8cb672fc38e6d8d_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-27 02:11:32', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 09:10:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (119, '119', NULL, NULL, 86, 7, NULL, NULL, 6500, NULL, 13000, NULL, 6500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:23:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:23:23', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (120, '120', NULL, NULL, 87, 7, NULL, NULL, 59000, NULL, 118000, NULL, 59000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:24:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:24:41', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (121, '121', NULL, NULL, 88, 7, NULL, NULL, 4500, NULL, 9000, NULL, 4500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:25:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:25:24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (122, '122', NULL, NULL, 89, 7, NULL, NULL, 31000, NULL, 62000, NULL, 31000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:26:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:26:39', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (123, '123', NULL, NULL, 90, 7, NULL, NULL, 9000, NULL, 18000, NULL, 9000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:28:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:28:04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (124, '124', NULL, NULL, 91, 7, NULL, NULL, 17500, NULL, 35000, NULL, 17500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:28:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:28:36', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (125, '125', NULL, NULL, 92, 7, NULL, NULL, 9000, NULL, 18000, NULL, 9000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:29:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:29:05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (126, '126', NULL, NULL, 93, 7, NULL, NULL, 12000, NULL, 24000, NULL, 12000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:29:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:29:41', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (127, '127', NULL, NULL, 94, 7, NULL, NULL, 8500, NULL, 17000, NULL, 8500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:49:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:49:59', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (128, '128', NULL, NULL, 95, 7, NULL, NULL, 5750, NULL, 11500, NULL, 5750, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:50:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:50:46', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (129, '129', NULL, NULL, 96, 7, NULL, NULL, 9500, NULL, 19000, NULL, 9500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:51:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:51:27', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (130, '130', NULL, NULL, 97, 7, NULL, NULL, 20500, NULL, 41000, NULL, 20500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:52:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:52:00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (131, '131', NULL, NULL, 98, 7, NULL, NULL, 26000, NULL, 52000, NULL, 26000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:52:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:52:31', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (132, '132', NULL, NULL, 99, 7, NULL, NULL, 8000, NULL, 16000, NULL, 8000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:53:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:53:05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (133, '133', NULL, NULL, 100, 7, NULL, NULL, 8000, NULL, 16000, NULL, 8000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:55:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:55:29', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (134, '134', NULL, NULL, 101, 7, NULL, NULL, 6500, NULL, 13000, NULL, 6500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:55:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:55:54', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (135, '135', NULL, NULL, 102, 7, NULL, NULL, 4000, NULL, 8000, NULL, 4000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:56:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:56:19', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (136, '136', NULL, NULL, 103, 7, NULL, NULL, 22000, NULL, 44000, NULL, 22000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:56:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:56:47', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (137, '137', NULL, NULL, 104, 7, NULL, NULL, 10000, NULL, 20000, NULL, 10000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:57:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:57:17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (138, '138', NULL, NULL, 105, 7, NULL, NULL, 70000, NULL, 140000, NULL, 70000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-27 14:57:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-27 07:57:46', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (139, '139', NULL, NULL, 106, 7, NULL, NULL, 5000, NULL, 10000, NULL, 5000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:51:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:51:24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (140, '140', NULL, NULL, 107, 7, NULL, NULL, 3300, NULL, 6600, NULL, 3300, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:53:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:53:58', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (141, '141', NULL, NULL, 108, 7, NULL, NULL, 18000, NULL, 36000, NULL, 18000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:54:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:54:39', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (142, '142', NULL, NULL, 109, 7, NULL, NULL, 12000, NULL, 24000, NULL, 12000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:55:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:55:50', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (143, '143', NULL, NULL, 110, 7, NULL, NULL, 43000, NULL, 86000, NULL, 43000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:56:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:56:26', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (144, '144', NULL, NULL, 111, 7, NULL, NULL, 3000, NULL, 6000, NULL, 3000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:56:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:56:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (145, '145', NULL, NULL, 112, 7, NULL, NULL, 14500, NULL, 29000, NULL, 14500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:57:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:57:24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (146, '146', NULL, NULL, 113, 7, NULL, NULL, 15000, NULL, 30000, NULL, 15000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:58:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:58:05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (147, '147', NULL, NULL, 114, 7, NULL, NULL, 13000, NULL, 26000, NULL, 13000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:58:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:58:36', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (148, '148', NULL, NULL, 115, 7, NULL, NULL, 4000, NULL, 8000, NULL, 4000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:59:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:59:13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (149, '149', NULL, NULL, 116, 7, NULL, NULL, 41000, NULL, 82000, NULL, 41000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 09:59:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 02:59:45', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (150, '150', NULL, NULL, 117, 7, NULL, NULL, 11000, NULL, 22000, NULL, 11000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:00:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:00:16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (151, '151', NULL, NULL, 118, 7, NULL, NULL, 7000, NULL, 14000, NULL, 7000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:43:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:43:06', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (152, '152', NULL, NULL, 119, 7, NULL, NULL, 8500, NULL, 17000, NULL, 8500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:44:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:44:05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (153, '153', NULL, NULL, 120, 7, NULL, NULL, 26500, NULL, 53000, NULL, 26500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:45:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:45:43', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (154, '154', NULL, NULL, 121, 7, NULL, NULL, 7500, NULL, 15000, NULL, 7500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:46:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:46:17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (155, '155', NULL, NULL, 122, 7, NULL, NULL, 8250, NULL, 16500, NULL, 8250, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:47:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:47:00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (156, '156', NULL, NULL, 123, 7, NULL, NULL, 7500, NULL, 15000, NULL, 7500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:47:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:47:33', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (157, '157', NULL, NULL, 124, 7, NULL, NULL, 4500, NULL, 9000, NULL, 4500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:48:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:48:13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (158, '158', NULL, NULL, 125, 7, NULL, NULL, 5000, NULL, 10000, NULL, 5000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:48:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:48:42', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (159, '159', NULL, NULL, 126, 7, NULL, NULL, 16000, NULL, 32000, NULL, 16000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:49:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:49:03', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (160, '160', NULL, NULL, 127, 7, NULL, NULL, 7500, NULL, 15000, NULL, 7500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:49:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:49:33', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (161, '161', NULL, NULL, 128, 7, NULL, NULL, 49000, NULL, 98000, NULL, 49000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:49:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:49:59', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (162, '162', NULL, NULL, 129, 7, NULL, NULL, 12500, NULL, 25000, NULL, 12500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 10:50:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 03:50:22', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (163, '163', NULL, NULL, 130, 7, NULL, NULL, 7500, NULL, 15000, NULL, 7500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 11:46:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 04:46:38', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (164, '164', NULL, NULL, 131, 7, NULL, NULL, 3000, NULL, 6000, NULL, 3000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 11:47:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 04:47:05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (165, '165', NULL, NULL, 132, 7, NULL, NULL, 76000, NULL, 152000, NULL, 76000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 11:47:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 04:47:37', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (166, '166', NULL, NULL, 134, 7, NULL, NULL, 6000, NULL, 12000, NULL, 6000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:19:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:19:49', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (167, '167', NULL, NULL, 135, 7, NULL, NULL, 18500, NULL, 37000, NULL, 18500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:20:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:20:23', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (168, '168', NULL, NULL, 136, 7, NULL, NULL, 20500, NULL, 41000, NULL, 20500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:20:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:20:51', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (169, '169', NULL, NULL, 137, 7, NULL, NULL, 30000, NULL, 60000, NULL, 30000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:21:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:21:23', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (170, '170', NULL, NULL, 138, 7, NULL, NULL, 7500, NULL, 15000, NULL, 7500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:21:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:21:54', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (171, '171', NULL, NULL, 139, 7, NULL, NULL, 4500, NULL, 9000, NULL, 4500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:22:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:22:15', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (172, '172', NULL, NULL, 140, 7, NULL, NULL, 300, NULL, 600, NULL, 300, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:22:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:22:47', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (173, '173', NULL, NULL, 141, 7, NULL, NULL, 3300, NULL, 6600, NULL, 3300, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:23:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:23:09', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (174, '174', NULL, NULL, 142, 7, NULL, NULL, 11000, NULL, 22000, NULL, 11000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:42:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:42:18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (176, '176', NULL, NULL, 144, 7, NULL, NULL, 12500, NULL, 25000, NULL, 12500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:43:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:43:42', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (177, '177', NULL, NULL, 145, 7, NULL, NULL, 12500, NULL, 25000, NULL, 12500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:45:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:45:05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (178, '178', NULL, NULL, 146, 7, NULL, NULL, 5500, NULL, 11000, NULL, 5500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:46:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:46:12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (179, '179', NULL, NULL, 147, 7, NULL, NULL, 15000, NULL, 30000, NULL, 15000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:46:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:46:42', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (180, '180', NULL, NULL, 148, 7, NULL, NULL, 4000, NULL, 8000, NULL, 4000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:47:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:47:19', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (181, '181', NULL, NULL, 149, 7, NULL, NULL, 2500, NULL, 5000, NULL, 2500, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:47:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:47:50', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (182, '182', NULL, NULL, 150, 7, NULL, NULL, 7000, NULL, 14000, NULL, 7000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:48:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:48:21', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (183, '183', NULL, NULL, 151, 7, NULL, NULL, 14000, NULL, 28000, NULL, 14000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:48:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:48:57', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (184, '184', NULL, NULL, 153, 7, NULL, NULL, 2000, NULL, 4000, NULL, 2000, NULL, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:50:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, '2018-04-28 05:50:00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (185, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:52:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (186, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (187, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (188, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (189, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (190, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (191, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (192, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:53:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (193, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:54:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (194, '175', NULL, NULL, 143, 7, NULL, NULL, 19000, NULL, 380000, NULL, 19000, NULL, 2, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 4, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 12:54:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', '2018-04-27', '2018-04-28 05:42:55', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 1);
INSERT INTO `material_detail` VALUES (195, '110', '195', 114, 72, NULL, NULL, 4, NULL, NULL, NULL, 46000, 46000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '7e651e2854d9148bb706be39fa3d0110_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-05-08 03:37:23', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 15:24:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (196, '112', '196', 114, 74, NULL, NULL, 4, NULL, NULL, NULL, 46000, 46000, NULL, 1, NULL, 0, 1, NULL, NULL, NULL, '73130e182cd5aa34711a014198f90cae_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-05-08 03:37:23', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 15:26:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (197, '177', '197', 115, 145, NULL, NULL, 103, NULL, NULL, NULL, 12500, 12500, NULL, 1, NULL, 0, 100, NULL, NULL, NULL, 'eb1b9f4de42e9de6341acd5937ba509c_paid.jpg', '', NULL, 4, 15, NULL, NULL, '2018-04-28 08:58:24', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-04-28 15:57:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (198, '4944504451', '198', 116, 44, NULL, NULL, 2, NULL, NULL, NULL, 32000, 15284, NULL, 2, NULL, 0, 1, '031460025917518', NULL, NULL, 'bc2c0f2a48ee8d78c7c7bebea6799463_paid.png', '', NULL, 4, 15, NULL, NULL, '2018-05-08 03:37:34', '2018-05-02 07:39:27', NULL, NULL, NULL, NULL, 28, '2018-05-02 08:31:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5000, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (199, '740275', '199', 117, 48, NULL, NULL, 2, NULL, NULL, NULL, 550000, 200000, NULL, 1, NULL, 0, 1, '031460027454018', NULL, NULL, '4405b28c2676833ee63887607b47d0c4_paid.png', '', NULL, 4, 15, NULL, NULL, '2018-05-12 03:03:06', '2018-05-09 09:11:58', NULL, NULL, NULL, NULL, 28, '2018-05-08 16:11:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5000, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (200, '136', '200', 118, 103, NULL, NULL, 100, NULL, NULL, NULL, 26000, 22000, NULL, 2, NULL, 0, 1, '031460027779818', NULL, NULL, 'b0f808cbbe08ae819a9c8e072414c0d4_paid.png', '', NULL, 3, 15, NULL, NULL, NULL, '2018-05-14 04:17:34', NULL, NULL, NULL, NULL, 28, '2018-05-12 10:00:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-05-14 12:41:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 35000, NULL, NULL, 0, 0, NULL, NULL, NULL, 1, 1);
INSERT INTO `material_detail` VALUES (201, '201', NULL, NULL, 155, 10, NULL, NULL, 57670, NULL, 317185, NULL, 63437, NULL, 5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 3, 14, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 28, '2018-07-17 12:35:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2018-07-17 12:35:19', NULL, '2018-07-17 12:35:19', NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 19);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_as` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `link` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_urut` int(11) NOT NULL,
  `is_heading` tinyint(1) NOT NULL DEFAULT 0,
  `class_add` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (8, 'Dashboard', 'Dashboard', 'home/index', 0, 'mdi-av-timer', 1, 0, NULL);
INSERT INTO `menu` VALUES (9, 'Barang', 'Barang', 'mastermaterial/index', 0, 'mdi-puzzle', 2, 0, NULL);
INSERT INTO `menu` VALUES (10, 'Barang Masuk', 'Barang Masuk', 'vbm/index', 0, 'mdi-playlist-check', 4, 0, NULL);
INSERT INTO `menu` VALUES (11, 'Penjualan', 'Penjualan', 'vorders/index', 0, 'mdi-cart-plus', 5, 0, NULL);
INSERT INTO `menu` VALUES (12, 'Barang Keluar', 'Barang Keluar', 'vbk/index', 0, 'mdi-basket-unfill', 6, 0, NULL);
INSERT INTO `menu` VALUES (13, 'Laporan', 'Laporan', NULL, 0, 'mdi-file-multiple', 8, 1, '');
INSERT INTO `menu` VALUES (14, 'Stok Barang', 'Stok Barang', 'vmaterialstok/index', 13, 'mdi-file', 1, 0, NULL);
INSERT INTO `menu` VALUES (15, 'Pengaturan', 'Pengaturan', NULL, 0, 'mdi-wrench', 9, 1, NULL);
INSERT INTO `menu` VALUES (16, 'Karyawan', 'User', 'user/index', 15, 'mdi-account-settings-variant', 1, 0, '');
INSERT INTO `menu` VALUES (17, 'Pembelian', 'Pembelian', 'vpb/index', 0, 'mdi-basket-fill', 3, 0, NULL);
INSERT INTO `menu` VALUES (18, 'Kategori barang', 'Kategori barang', 'brktg/index', 15, 'mdi-arrange-send-backward', 6, 0, NULL);
INSERT INTO `menu` VALUES (19, 'Supplier', 'Supplier', 'brsupplier/index', 15, 'mdi-account-network', 2, 0, '');
INSERT INTO `menu` VALUES (20, 'Pelanggan', 'Pelanggan', 'customers/index', 15, 'mdi-account-multiple', 5, 0, NULL);
INSERT INTO `menu` VALUES (21, 'Atribut & varian', 'Atribut & varian', 'itemattr/index', 15, 'mdi-arrange-bring-to-front', 7, 0, NULL);
INSERT INTO `menu` VALUES (22, 'Kurir', 'Kurir', 'kurir/index', 15, 'mdi-truck-delivery', 8, 0, NULL);
INSERT INTO `menu` VALUES (23, 'Marketplace', 'Marketplace', 'marketplace/index', 15, 'mdi-store', 9, 0, NULL);
INSERT INTO `menu` VALUES (24, 'Barang masuk', 'Barang masuk', 'vtotalqty/index', 13, 'mdi-file', 2, 0, NULL);
INSERT INTO `menu` VALUES (25, 'Forwarder', 'Forwarder', 'forwarder/index', 15, 'mdi-account-convert', 3, 0, NULL);
INSERT INTO `menu` VALUES (26, 'Dropshipper', 'Dropshipper', 'dropshipper/index', 15, 'mdi-account-star', 4, 0, NULL);
INSERT INTO `menu` VALUES (27, 'Role', 'Role', 'role/index', 15, 'mdi-account-circle', 10, 0, NULL);
INSERT INTO `menu` VALUES (28, 'Operasional', 'Operasional', '', 0, 'mdi-sync-alert', 7, 1, '');
INSERT INTO `menu` VALUES (31, 'Penjualan', 'Penjualan', 'vorders/report', 13, 'mdi-file', 3, 0, NULL);
INSERT INTO `menu` VALUES (32, 'Pengeluaran', 'Pengeluaran', 'vspending/index', 28, 'mdi-cash-multiple', 1, 0, NULL);
INSERT INTO `menu` VALUES (33, 'CashFlow', 'CashFlow', 'vcashflow/index', 28, 'mdi-clipboard-flow', 2, 0, NULL);

-- ----------------------------
-- Table structure for menu_action
-- ----------------------------
DROP TABLE IF EXISTS `menu_action`;
CREATE TABLE `menu_action`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NULL DEFAULT NULL,
  `name` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `url_action` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `column_param` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipe` enum('add','edit','delete','other') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'other',
  `icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `display_class` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'show',
  `class_add` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_action
-- ----------------------------
INSERT INTO `menu_action` VALUES (1, 16, 'tambah user', 'user/create', '', 'add', 'fa-plus', 'show', 'add_user btn-success');
INSERT INTO `menu_action` VALUES (2, 16, 'update user', 'user/update', 'id', 'edit', 'fa-pencil', 'show', 'update_user btn-primary');
INSERT INTO `menu_action` VALUES (3, 16, 'delete user', 'user/delete', 'id', 'delete', 'fa-trash', 'show', 'del_user btn-danger');
INSERT INTO `menu_action` VALUES (4, 19, 'tambah supplier', NULL, '', 'add', NULL, 'show', NULL);
INSERT INTO `menu_action` VALUES (5, 19, 'update supplier', NULL, 'id', 'edit', NULL, 'show', NULL);
INSERT INTO `menu_action` VALUES (6, 19, 'delete supplier', NULL, 'id', 'delete', NULL, 'show', NULL);

-- ----------------------------
-- Table structure for opr
-- ----------------------------
DROP TABLE IF EXISTS `opr`;
CREATE TABLE `opr`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `coa_id` int(11) NULL DEFAULT NULL,
  `debit` int(25) NULL DEFAULT 0,
  `kredit` int(25) NULL DEFAULT 0,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status_id` tinyint(3) NULL DEFAULT 0 COMMENT '0;Normal;1:Koreksi',
  `bukti` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kd_cf` char(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_cf` tinyint(1) NULL DEFAULT 0 COMMENT '1:cashflow',
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user`(`user_id`) USING BTREE,
  INDEX `coa`(`coa_id`) USING BTREE,
  CONSTRAINT `coa` FOREIGN KEY (`coa_id`) REFERENCES `coa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of opr
-- ----------------------------
INSERT INTO `opr` VALUES (1, '2018-11-08', 11, 3, 0, 3999, 'tewst', 0, NULL, NULL, 0, 1, '2018-11-08 10:41:44', '2018-11-08 10:41:44');
INSERT INTO `opr` VALUES (2, '2018-11-08', 10, NULL, 400000, 0, 'cf', 0, NULL, 'hz8iVMdx5wdgNP1lCPiRFdNb_GqMlnoA', 1, 1, '2018-11-08 10:42:20', '2018-11-08 10:42:20');
INSERT INTO `opr` VALUES (3, '2018-11-08', 11, NULL, 0, 400000, 'cf', 0, NULL, 'hz8iVMdx5wdgNP1lCPiRFdNb_GqMlnoA', 1, 1, '2018-11-08 10:42:20', '2018-11-08 10:42:20');

-- ----------------------------
-- Table structure for permission_action
-- ----------------------------
DROP TABLE IF EXISTS `permission_action`;
CREATE TABLE `permission_action`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `menu_id` int(11) NULL DEFAULT NULL,
  `menu_action_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permission_action
-- ----------------------------
INSERT INTO `permission_action` VALUES (1, 1, 16, 1);
INSERT INTO `permission_action` VALUES (2, 1, 16, 2);
INSERT INTO `permission_action` VALUES (3, 1, 16, 3);

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `role_id` smallint(5) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_menu`(`menu_id`) USING BTREE,
  INDEX `fk_role`(`role_id`) USING BTREE,
  CONSTRAINT `fk_menu` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 146 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (22, 8, 2);
INSERT INTO `permissions` VALUES (23, 11, 2);
INSERT INTO `permissions` VALUES (24, 8, 3);
INSERT INTO `permissions` VALUES (25, 10, 3);
INSERT INTO `permissions` VALUES (26, 12, 3);
INSERT INTO `permissions` VALUES (30, 12, 2);
INSERT INTO `permissions` VALUES (31, 13, 2);
INSERT INTO `permissions` VALUES (32, 14, 2);
INSERT INTO `permissions` VALUES (71, 28, 2);
INSERT INTO `permissions` VALUES (72, 28, 3);
INSERT INTO `permissions` VALUES (73, 32, 2);
INSERT INTO `permissions` VALUES (74, 32, 3);
INSERT INTO `permissions` VALUES (122, 8, 1);
INSERT INTO `permissions` VALUES (123, 9, 1);
INSERT INTO `permissions` VALUES (124, 10, 1);
INSERT INTO `permissions` VALUES (125, 11, 1);
INSERT INTO `permissions` VALUES (126, 12, 1);
INSERT INTO `permissions` VALUES (127, 13, 1);
INSERT INTO `permissions` VALUES (128, 14, 1);
INSERT INTO `permissions` VALUES (129, 24, 1);
INSERT INTO `permissions` VALUES (130, 31, 1);
INSERT INTO `permissions` VALUES (131, 15, 1);
INSERT INTO `permissions` VALUES (132, 16, 1);
INSERT INTO `permissions` VALUES (133, 19, 1);
INSERT INTO `permissions` VALUES (134, 25, 1);
INSERT INTO `permissions` VALUES (135, 26, 1);
INSERT INTO `permissions` VALUES (136, 20, 1);
INSERT INTO `permissions` VALUES (137, 18, 1);
INSERT INTO `permissions` VALUES (138, 21, 1);
INSERT INTO `permissions` VALUES (139, 22, 1);
INSERT INTO `permissions` VALUES (140, 23, 1);
INSERT INTO `permissions` VALUES (141, 27, 1);
INSERT INTO `permissions` VALUES (142, 17, 1);
INSERT INTO `permissions` VALUES (143, 28, 1);
INSERT INTO `permissions` VALUES (144, 32, 1);
INSERT INTO `permissions` VALUES (145, 33, 1);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `fullname` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `shortname` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'ADMINISTRATOR', 'ADMIN', 0, '2018-05-07 11:11:38', '2018-05-15 11:00:25');
INSERT INTO `role` VALUES (2, 'CUSTOMER SERVICE', 'CS', 1, '2018-05-07 11:11:38', '2018-05-14 11:24:12');
INSERT INTO `role` VALUES (3, 'GENERAL SUPPORT', 'GS', 1, '2018-05-07 11:11:38', '2018-05-14 11:24:13');

-- ----------------------------
-- Table structure for st_city
-- ----------------------------
DROP TABLE IF EXISTS `st_city`;
CREATE TABLE `st_city`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `postal_code` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `province_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 502 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of st_city
-- ----------------------------
INSERT INTO `st_city` VALUES (1, 'Aceh Barat', 'Kabupaten', '23681', 21);
INSERT INTO `st_city` VALUES (2, 'Aceh Barat Daya', 'Kabupaten', '23764', 21);
INSERT INTO `st_city` VALUES (3, 'Aceh Besar', 'Kabupaten', '23951', 21);
INSERT INTO `st_city` VALUES (4, 'Aceh Jaya', 'Kabupaten', '23654', 21);
INSERT INTO `st_city` VALUES (5, 'Aceh Selatan', 'Kabupaten', '23719', 21);
INSERT INTO `st_city` VALUES (6, 'Aceh Singkil', 'Kabupaten', '24785', 21);
INSERT INTO `st_city` VALUES (7, 'Aceh Tamiang', 'Kabupaten', '24476', 21);
INSERT INTO `st_city` VALUES (8, 'Aceh Tengah', 'Kabupaten', '24511', 21);
INSERT INTO `st_city` VALUES (9, 'Aceh Tenggara', 'Kabupaten', '24611', 21);
INSERT INTO `st_city` VALUES (10, 'Aceh Timur', 'Kabupaten', '24454', 21);
INSERT INTO `st_city` VALUES (11, 'Aceh Utara', 'Kabupaten', '24382', 21);
INSERT INTO `st_city` VALUES (12, 'Agam', 'Kabupaten', '26411', 32);
INSERT INTO `st_city` VALUES (13, 'Alor', 'Kabupaten', '85811', 23);
INSERT INTO `st_city` VALUES (14, 'Ambon', 'Kota', '97222', 19);
INSERT INTO `st_city` VALUES (15, 'Asahan', 'Kabupaten', '21214', 34);
INSERT INTO `st_city` VALUES (16, 'Asmat', 'Kabupaten', '99777', 24);
INSERT INTO `st_city` VALUES (17, 'Badung', 'Kabupaten', '80351', 1);
INSERT INTO `st_city` VALUES (18, 'Balangan', 'Kabupaten', '71611', 13);
INSERT INTO `st_city` VALUES (19, 'Balikpapan', 'Kota', '76111', 15);
INSERT INTO `st_city` VALUES (20, 'Banda Aceh', 'Kota', '23238', 21);
INSERT INTO `st_city` VALUES (21, 'Bandar Lampung', 'Kota', '35139', 18);
INSERT INTO `st_city` VALUES (22, 'Bandung', 'Kabupaten', '40311', 9);
INSERT INTO `st_city` VALUES (23, 'Bandung', 'Kota', '40111', 9);
INSERT INTO `st_city` VALUES (24, 'Bandung Barat', 'Kabupaten', '40721', 9);
INSERT INTO `st_city` VALUES (25, 'Banggai', 'Kabupaten', '94711', 29);
INSERT INTO `st_city` VALUES (26, 'Banggai Kepulauan', 'Kabupaten', '94881', 29);
INSERT INTO `st_city` VALUES (27, 'Bangka', 'Kabupaten', '33212', 2);
INSERT INTO `st_city` VALUES (28, 'Bangka Barat', 'Kabupaten', '33315', 2);
INSERT INTO `st_city` VALUES (29, 'Bangka Selatan', 'Kabupaten', '33719', 2);
INSERT INTO `st_city` VALUES (30, 'Bangka Tengah', 'Kabupaten', '33613', 2);
INSERT INTO `st_city` VALUES (31, 'Bangkalan', 'Kabupaten', '69118', 11);
INSERT INTO `st_city` VALUES (32, 'Bangli', 'Kabupaten', '80619', 1);
INSERT INTO `st_city` VALUES (33, 'Banjar', 'Kabupaten', '70619', 13);
INSERT INTO `st_city` VALUES (34, 'Banjar', 'Kota', '46311', 9);
INSERT INTO `st_city` VALUES (35, 'Banjarbaru', 'Kota', '70712', 13);
INSERT INTO `st_city` VALUES (36, 'Banjarmasin', 'Kota', '70117', 13);
INSERT INTO `st_city` VALUES (37, 'Banjarnegara', 'Kabupaten', '53419', 10);
INSERT INTO `st_city` VALUES (38, 'Bantaeng', 'Kabupaten', '92411', 28);
INSERT INTO `st_city` VALUES (39, 'Bantul', 'Kabupaten', '55715', 5);
INSERT INTO `st_city` VALUES (40, 'Banyuasin', 'Kabupaten', '30911', 33);
INSERT INTO `st_city` VALUES (41, 'Banyumas', 'Kabupaten', '53114', 10);
INSERT INTO `st_city` VALUES (42, 'Banyuwangi', 'Kabupaten', '68416', 11);
INSERT INTO `st_city` VALUES (43, 'Barito Kuala', 'Kabupaten', '70511', 13);
INSERT INTO `st_city` VALUES (44, 'Barito Selatan', 'Kabupaten', '73711', 14);
INSERT INTO `st_city` VALUES (45, 'Barito Timur', 'Kabupaten', '73671', 14);
INSERT INTO `st_city` VALUES (46, 'Barito Utara', 'Kabupaten', '73881', 14);
INSERT INTO `st_city` VALUES (47, 'Barru', 'Kabupaten', '90719', 28);
INSERT INTO `st_city` VALUES (48, 'Batam', 'Kota', '29413', 17);
INSERT INTO `st_city` VALUES (49, 'Batang', 'Kabupaten', '51211', 10);
INSERT INTO `st_city` VALUES (50, 'Batang Hari', 'Kabupaten', '36613', 8);
INSERT INTO `st_city` VALUES (51, 'Batu', 'Kota', '65311', 11);
INSERT INTO `st_city` VALUES (52, 'Batu Bara', 'Kabupaten', '21655', 34);
INSERT INTO `st_city` VALUES (53, 'Bau-Bau', 'Kota', '93719', 30);
INSERT INTO `st_city` VALUES (54, 'Bekasi', 'Kabupaten', '17837', 9);
INSERT INTO `st_city` VALUES (55, 'Bekasi', 'Kota', '17121', 9);
INSERT INTO `st_city` VALUES (56, 'Belitung', 'Kabupaten', '33419', 2);
INSERT INTO `st_city` VALUES (57, 'Belitung Timur', 'Kabupaten', '33519', 2);
INSERT INTO `st_city` VALUES (58, 'Belu', 'Kabupaten', '85711', 23);
INSERT INTO `st_city` VALUES (59, 'Bener Meriah', 'Kabupaten', '24581', 21);
INSERT INTO `st_city` VALUES (60, 'Bengkalis', 'Kabupaten', '28719', 26);
INSERT INTO `st_city` VALUES (61, 'Bengkayang', 'Kabupaten', '79213', 12);
INSERT INTO `st_city` VALUES (62, 'Bengkulu', 'Kota', '38229', 4);
INSERT INTO `st_city` VALUES (63, 'Bengkulu Selatan', 'Kabupaten', '38519', 4);
INSERT INTO `st_city` VALUES (64, 'Bengkulu Tengah', 'Kabupaten', '38319', 4);
INSERT INTO `st_city` VALUES (65, 'Bengkulu Utara', 'Kabupaten', '38619', 4);
INSERT INTO `st_city` VALUES (66, 'Berau', 'Kabupaten', '77311', 15);
INSERT INTO `st_city` VALUES (67, 'Biak Numfor', 'Kabupaten', '98119', 24);
INSERT INTO `st_city` VALUES (68, 'Bima', 'Kabupaten', '84171', 22);
INSERT INTO `st_city` VALUES (69, 'Bima', 'Kota', '84139', 22);
INSERT INTO `st_city` VALUES (70, 'Binjai', 'Kota', '20712', 34);
INSERT INTO `st_city` VALUES (71, 'Bintan', 'Kabupaten', '29135', 17);
INSERT INTO `st_city` VALUES (72, 'Bireuen', 'Kabupaten', '24219', 21);
INSERT INTO `st_city` VALUES (73, 'Bitung', 'Kota', '95512', 31);
INSERT INTO `st_city` VALUES (74, 'Blitar', 'Kabupaten', '66171', 11);
INSERT INTO `st_city` VALUES (75, 'Blitar', 'Kota', '66124', 11);
INSERT INTO `st_city` VALUES (76, 'Blora', 'Kabupaten', '58219', 10);
INSERT INTO `st_city` VALUES (77, 'Boalemo', 'Kabupaten', '96319', 7);
INSERT INTO `st_city` VALUES (78, 'Bogor', 'Kabupaten', '16911', 9);
INSERT INTO `st_city` VALUES (79, 'Bogor', 'Kota', '16119', 9);
INSERT INTO `st_city` VALUES (80, 'Bojonegoro', 'Kabupaten', '62119', 11);
INSERT INTO `st_city` VALUES (81, 'Bolaang Mongondow (Bolmong)', 'Kabupaten', '95755', 31);
INSERT INTO `st_city` VALUES (82, 'Bolaang Mongondow Selatan', 'Kabupaten', '95774', 31);
INSERT INTO `st_city` VALUES (83, 'Bolaang Mongondow Timur', 'Kabupaten', '95783', 31);
INSERT INTO `st_city` VALUES (84, 'Bolaang Mongondow Utara', 'Kabupaten', '95765', 31);
INSERT INTO `st_city` VALUES (85, 'Bombana', 'Kabupaten', '93771', 30);
INSERT INTO `st_city` VALUES (86, 'Bondowoso', 'Kabupaten', '68219', 11);
INSERT INTO `st_city` VALUES (87, 'Bone', 'Kabupaten', '92713', 28);
INSERT INTO `st_city` VALUES (88, 'Bone Bolango', 'Kabupaten', '96511', 7);
INSERT INTO `st_city` VALUES (89, 'Bontang', 'Kota', '75313', 15);
INSERT INTO `st_city` VALUES (90, 'Boven Digoel', 'Kabupaten', '99662', 24);
INSERT INTO `st_city` VALUES (91, 'Boyolali', 'Kabupaten', '57312', 10);
INSERT INTO `st_city` VALUES (92, 'Brebes', 'Kabupaten', '52212', 10);
INSERT INTO `st_city` VALUES (93, 'Bukittinggi', 'Kota', '26115', 32);
INSERT INTO `st_city` VALUES (94, 'Buleleng', 'Kabupaten', '81111', 1);
INSERT INTO `st_city` VALUES (95, 'Bulukumba', 'Kabupaten', '92511', 28);
INSERT INTO `st_city` VALUES (96, 'Bulungan (Bulongan)', 'Kabupaten', '77211', 16);
INSERT INTO `st_city` VALUES (97, 'Bungo', 'Kabupaten', '37216', 8);
INSERT INTO `st_city` VALUES (98, 'Buol', 'Kabupaten', '94564', 29);
INSERT INTO `st_city` VALUES (99, 'Buru', 'Kabupaten', '97371', 19);
INSERT INTO `st_city` VALUES (100, 'Buru Selatan', 'Kabupaten', '97351', 19);
INSERT INTO `st_city` VALUES (101, 'Buton', 'Kabupaten', '93754', 30);
INSERT INTO `st_city` VALUES (102, 'Buton Utara', 'Kabupaten', '93745', 30);
INSERT INTO `st_city` VALUES (103, 'Ciamis', 'Kabupaten', '46211', 9);
INSERT INTO `st_city` VALUES (104, 'Cianjur', 'Kabupaten', '43217', 9);
INSERT INTO `st_city` VALUES (105, 'Cilacap', 'Kabupaten', '53211', 10);
INSERT INTO `st_city` VALUES (106, 'Cilegon', 'Kota', '42417', 3);
INSERT INTO `st_city` VALUES (107, 'Cimahi', 'Kota', '40512', 9);
INSERT INTO `st_city` VALUES (108, 'Cirebon', 'Kabupaten', '45611', 9);
INSERT INTO `st_city` VALUES (109, 'Cirebon', 'Kota', '45116', 9);
INSERT INTO `st_city` VALUES (110, 'Dairi', 'Kabupaten', '22211', 34);
INSERT INTO `st_city` VALUES (111, 'Deiyai (Deliyai)', 'Kabupaten', '98784', 24);
INSERT INTO `st_city` VALUES (112, 'Deli Serdang', 'Kabupaten', '20511', 34);
INSERT INTO `st_city` VALUES (113, 'Demak', 'Kabupaten', '59519', 10);
INSERT INTO `st_city` VALUES (114, 'Denpasar', 'Kota', '80227', 1);
INSERT INTO `st_city` VALUES (115, 'Depok', 'Kota', '16416', 9);
INSERT INTO `st_city` VALUES (116, 'Dharmasraya', 'Kabupaten', '27612', 32);
INSERT INTO `st_city` VALUES (117, 'Dogiyai', 'Kabupaten', '98866', 24);
INSERT INTO `st_city` VALUES (118, 'Dompu', 'Kabupaten', '84217', 22);
INSERT INTO `st_city` VALUES (119, 'Donggala', 'Kabupaten', '94341', 29);
INSERT INTO `st_city` VALUES (120, 'Dumai', 'Kota', '28811', 26);
INSERT INTO `st_city` VALUES (121, 'Empat Lawang', 'Kabupaten', '31811', 33);
INSERT INTO `st_city` VALUES (122, 'Ende', 'Kabupaten', '86351', 23);
INSERT INTO `st_city` VALUES (123, 'Enrekang', 'Kabupaten', '91719', 28);
INSERT INTO `st_city` VALUES (124, 'Fakfak', 'Kabupaten', '98651', 25);
INSERT INTO `st_city` VALUES (125, 'Flores Timur', 'Kabupaten', '86213', 23);
INSERT INTO `st_city` VALUES (126, 'Garut', 'Kabupaten', '44126', 9);
INSERT INTO `st_city` VALUES (127, 'Gayo Lues', 'Kabupaten', '24653', 21);
INSERT INTO `st_city` VALUES (128, 'Gianyar', 'Kabupaten', '80519', 1);
INSERT INTO `st_city` VALUES (129, 'Gorontalo', 'Kabupaten', '96218', 7);
INSERT INTO `st_city` VALUES (130, 'Gorontalo', 'Kota', '96115', 7);
INSERT INTO `st_city` VALUES (131, 'Gorontalo Utara', 'Kabupaten', '96611', 7);
INSERT INTO `st_city` VALUES (132, 'Gowa', 'Kabupaten', '92111', 28);
INSERT INTO `st_city` VALUES (133, 'Gresik', 'Kabupaten', '61115', 11);
INSERT INTO `st_city` VALUES (134, 'Grobogan', 'Kabupaten', '58111', 10);
INSERT INTO `st_city` VALUES (135, 'Gunung Kidul', 'Kabupaten', '55812', 5);
INSERT INTO `st_city` VALUES (136, 'Gunung Mas', 'Kabupaten', '74511', 14);
INSERT INTO `st_city` VALUES (137, 'Gunungsitoli', 'Kota', '22813', 34);
INSERT INTO `st_city` VALUES (138, 'Halmahera Barat', 'Kabupaten', '97757', 20);
INSERT INTO `st_city` VALUES (139, 'Halmahera Selatan', 'Kabupaten', '97911', 20);
INSERT INTO `st_city` VALUES (140, 'Halmahera Tengah', 'Kabupaten', '97853', 20);
INSERT INTO `st_city` VALUES (141, 'Halmahera Timur', 'Kabupaten', '97862', 20);
INSERT INTO `st_city` VALUES (142, 'Halmahera Utara', 'Kabupaten', '97762', 20);
INSERT INTO `st_city` VALUES (143, 'Hulu Sungai Selatan', 'Kabupaten', '71212', 13);
INSERT INTO `st_city` VALUES (144, 'Hulu Sungai Tengah', 'Kabupaten', '71313', 13);
INSERT INTO `st_city` VALUES (145, 'Hulu Sungai Utara', 'Kabupaten', '71419', 13);
INSERT INTO `st_city` VALUES (146, 'Humbang Hasundutan', 'Kabupaten', '22457', 34);
INSERT INTO `st_city` VALUES (147, 'Indragiri Hilir', 'Kabupaten', '29212', 26);
INSERT INTO `st_city` VALUES (148, 'Indragiri Hulu', 'Kabupaten', '29319', 26);
INSERT INTO `st_city` VALUES (149, 'Indramayu', 'Kabupaten', '45214', 9);
INSERT INTO `st_city` VALUES (150, 'Intan Jaya', 'Kabupaten', '98771', 24);
INSERT INTO `st_city` VALUES (151, 'Jakarta Barat', 'Kota', '11220', 6);
INSERT INTO `st_city` VALUES (152, 'Jakarta Pusat', 'Kota', '10540', 6);
INSERT INTO `st_city` VALUES (153, 'Jakarta Selatan', 'Kota', '12230', 6);
INSERT INTO `st_city` VALUES (154, 'Jakarta Timur', 'Kota', '13330', 6);
INSERT INTO `st_city` VALUES (155, 'Jakarta Utara', 'Kota', '14140', 6);
INSERT INTO `st_city` VALUES (156, 'Jambi', 'Kota', '36111', 8);
INSERT INTO `st_city` VALUES (157, 'Jayapura', 'Kabupaten', '99352', 24);
INSERT INTO `st_city` VALUES (158, 'Jayapura', 'Kota', '99114', 24);
INSERT INTO `st_city` VALUES (159, 'Jayawijaya', 'Kabupaten', '99511', 24);
INSERT INTO `st_city` VALUES (160, 'Jember', 'Kabupaten', '68113', 11);
INSERT INTO `st_city` VALUES (161, 'Jembrana', 'Kabupaten', '82251', 1);
INSERT INTO `st_city` VALUES (162, 'Jeneponto', 'Kabupaten', '92319', 28);
INSERT INTO `st_city` VALUES (163, 'Jepara', 'Kabupaten', '59419', 10);
INSERT INTO `st_city` VALUES (164, 'Jombang', 'Kabupaten', '61415', 11);
INSERT INTO `st_city` VALUES (165, 'Kaimana', 'Kabupaten', '98671', 25);
INSERT INTO `st_city` VALUES (166, 'Kampar', 'Kabupaten', '28411', 26);
INSERT INTO `st_city` VALUES (167, 'Kapuas', 'Kabupaten', '73583', 14);
INSERT INTO `st_city` VALUES (168, 'Kapuas Hulu', 'Kabupaten', '78719', 12);
INSERT INTO `st_city` VALUES (169, 'Karanganyar', 'Kabupaten', '57718', 10);
INSERT INTO `st_city` VALUES (170, 'Karangasem', 'Kabupaten', '80819', 1);
INSERT INTO `st_city` VALUES (171, 'Karawang', 'Kabupaten', '41311', 9);
INSERT INTO `st_city` VALUES (172, 'Karimun', 'Kabupaten', '29611', 17);
INSERT INTO `st_city` VALUES (173, 'Karo', 'Kabupaten', '22119', 34);
INSERT INTO `st_city` VALUES (174, 'Katingan', 'Kabupaten', '74411', 14);
INSERT INTO `st_city` VALUES (175, 'Kaur', 'Kabupaten', '38911', 4);
INSERT INTO `st_city` VALUES (176, 'Kayong Utara', 'Kabupaten', '78852', 12);
INSERT INTO `st_city` VALUES (177, 'Kebumen', 'Kabupaten', '54319', 10);
INSERT INTO `st_city` VALUES (178, 'Kediri', 'Kabupaten', '64184', 11);
INSERT INTO `st_city` VALUES (179, 'Kediri', 'Kota', '64125', 11);
INSERT INTO `st_city` VALUES (180, 'Keerom', 'Kabupaten', '99461', 24);
INSERT INTO `st_city` VALUES (181, 'Kendal', 'Kabupaten', '51314', 10);
INSERT INTO `st_city` VALUES (182, 'Kendari', 'Kota', '93126', 30);
INSERT INTO `st_city` VALUES (183, 'Kepahiang', 'Kabupaten', '39319', 4);
INSERT INTO `st_city` VALUES (184, 'Kepulauan Anambas', 'Kabupaten', '29991', 17);
INSERT INTO `st_city` VALUES (185, 'Kepulauan Aru', 'Kabupaten', '97681', 19);
INSERT INTO `st_city` VALUES (186, 'Kepulauan Mentawai', 'Kabupaten', '25771', 32);
INSERT INTO `st_city` VALUES (187, 'Kepulauan Meranti', 'Kabupaten', '28791', 26);
INSERT INTO `st_city` VALUES (188, 'Kepulauan Sangihe', 'Kabupaten', '95819', 31);
INSERT INTO `st_city` VALUES (189, 'Kepulauan Seribu', 'Kabupaten', '14550', 6);
INSERT INTO `st_city` VALUES (190, 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 'Kabupaten', '95862', 31);
INSERT INTO `st_city` VALUES (191, 'Kepulauan Sula', 'Kabupaten', '97995', 20);
INSERT INTO `st_city` VALUES (192, 'Kepulauan Talaud', 'Kabupaten', '95885', 31);
INSERT INTO `st_city` VALUES (193, 'Kepulauan Yapen (Yapen Waropen)', 'Kabupaten', '98211', 24);
INSERT INTO `st_city` VALUES (194, 'Kerinci', 'Kabupaten', '37167', 8);
INSERT INTO `st_city` VALUES (195, 'Ketapang', 'Kabupaten', '78874', 12);
INSERT INTO `st_city` VALUES (196, 'Klaten', 'Kabupaten', '57411', 10);
INSERT INTO `st_city` VALUES (197, 'Klungkung', 'Kabupaten', '80719', 1);
INSERT INTO `st_city` VALUES (198, 'Kolaka', 'Kabupaten', '93511', 30);
INSERT INTO `st_city` VALUES (199, 'Kolaka Utara', 'Kabupaten', '93911', 30);
INSERT INTO `st_city` VALUES (200, 'Konawe', 'Kabupaten', '93411', 30);
INSERT INTO `st_city` VALUES (201, 'Konawe Selatan', 'Kabupaten', '93811', 30);
INSERT INTO `st_city` VALUES (202, 'Konawe Utara', 'Kabupaten', '93311', 30);
INSERT INTO `st_city` VALUES (203, 'Kotabaru', 'Kabupaten', '72119', 13);
INSERT INTO `st_city` VALUES (204, 'Kotamobagu', 'Kota', '95711', 31);
INSERT INTO `st_city` VALUES (205, 'Kotawaringin Barat', 'Kabupaten', '74119', 14);
INSERT INTO `st_city` VALUES (206, 'Kotawaringin Timur', 'Kabupaten', '74364', 14);
INSERT INTO `st_city` VALUES (207, 'Kuantan Singingi', 'Kabupaten', '29519', 26);
INSERT INTO `st_city` VALUES (208, 'Kubu Raya', 'Kabupaten', '78311', 12);
INSERT INTO `st_city` VALUES (209, 'Kudus', 'Kabupaten', '59311', 10);
INSERT INTO `st_city` VALUES (210, 'Kulon Progo', 'Kabupaten', '55611', 5);
INSERT INTO `st_city` VALUES (211, 'Kuningan', 'Kabupaten', '45511', 9);
INSERT INTO `st_city` VALUES (212, 'Kupang', 'Kabupaten', '85362', 23);
INSERT INTO `st_city` VALUES (213, 'Kupang', 'Kota', '85119', 23);
INSERT INTO `st_city` VALUES (214, 'Kutai Barat', 'Kabupaten', '75711', 15);
INSERT INTO `st_city` VALUES (215, 'Kutai Kartanegara', 'Kabupaten', '75511', 15);
INSERT INTO `st_city` VALUES (216, 'Kutai Timur', 'Kabupaten', '75611', 15);
INSERT INTO `st_city` VALUES (217, 'Labuhan Batu', 'Kabupaten', '21412', 34);
INSERT INTO `st_city` VALUES (218, 'Labuhan Batu Selatan', 'Kabupaten', '21511', 34);
INSERT INTO `st_city` VALUES (219, 'Labuhan Batu Utara', 'Kabupaten', '21711', 34);
INSERT INTO `st_city` VALUES (220, 'Lahat', 'Kabupaten', '31419', 33);
INSERT INTO `st_city` VALUES (221, 'Lamandau', 'Kabupaten', '74611', 14);
INSERT INTO `st_city` VALUES (222, 'Lamongan', 'Kabupaten', '64125', 11);
INSERT INTO `st_city` VALUES (223, 'Lampung Barat', 'Kabupaten', '34814', 18);
INSERT INTO `st_city` VALUES (224, 'Lampung Selatan', 'Kabupaten', '35511', 18);
INSERT INTO `st_city` VALUES (225, 'Lampung Tengah', 'Kabupaten', '34212', 18);
INSERT INTO `st_city` VALUES (226, 'Lampung Timur', 'Kabupaten', '34319', 18);
INSERT INTO `st_city` VALUES (227, 'Lampung Utara', 'Kabupaten', '34516', 18);
INSERT INTO `st_city` VALUES (228, 'Landak', 'Kabupaten', '78319', 12);
INSERT INTO `st_city` VALUES (229, 'Langkat', 'Kabupaten', '20811', 34);
INSERT INTO `st_city` VALUES (230, 'Langsa', 'Kota', '24412', 21);
INSERT INTO `st_city` VALUES (231, 'Lanny Jaya', 'Kabupaten', '99531', 24);
INSERT INTO `st_city` VALUES (232, 'Lebak', 'Kabupaten', '42319', 3);
INSERT INTO `st_city` VALUES (233, 'Lebong', 'Kabupaten', '39264', 4);
INSERT INTO `st_city` VALUES (234, 'Lembata', 'Kabupaten', '86611', 23);
INSERT INTO `st_city` VALUES (235, 'Lhokseumawe', 'Kota', '24352', 21);
INSERT INTO `st_city` VALUES (236, 'Lima Puluh Koto/Kota', 'Kabupaten', '26671', 32);
INSERT INTO `st_city` VALUES (237, 'Lingga', 'Kabupaten', '29811', 17);
INSERT INTO `st_city` VALUES (238, 'Lombok Barat', 'Kabupaten', '83311', 22);
INSERT INTO `st_city` VALUES (239, 'Lombok Tengah', 'Kabupaten', '83511', 22);
INSERT INTO `st_city` VALUES (240, 'Lombok Timur', 'Kabupaten', '83612', 22);
INSERT INTO `st_city` VALUES (241, 'Lombok Utara', 'Kabupaten', '83711', 22);
INSERT INTO `st_city` VALUES (242, 'Lubuk Linggau', 'Kota', '31614', 33);
INSERT INTO `st_city` VALUES (243, 'Lumajang', 'Kabupaten', '67319', 11);
INSERT INTO `st_city` VALUES (244, 'Luwu', 'Kabupaten', '91994', 28);
INSERT INTO `st_city` VALUES (245, 'Luwu Timur', 'Kabupaten', '92981', 28);
INSERT INTO `st_city` VALUES (246, 'Luwu Utara', 'Kabupaten', '92911', 28);
INSERT INTO `st_city` VALUES (247, 'Madiun', 'Kabupaten', '63153', 11);
INSERT INTO `st_city` VALUES (248, 'Madiun', 'Kota', '63122', 11);
INSERT INTO `st_city` VALUES (249, 'Magelang', 'Kabupaten', '56519', 10);
INSERT INTO `st_city` VALUES (250, 'Magelang', 'Kota', '56133', 10);
INSERT INTO `st_city` VALUES (251, 'Magetan', 'Kabupaten', '63314', 11);
INSERT INTO `st_city` VALUES (252, 'Majalengka', 'Kabupaten', '45412', 9);
INSERT INTO `st_city` VALUES (253, 'Majene', 'Kabupaten', '91411', 27);
INSERT INTO `st_city` VALUES (254, 'Makassar', 'Kota', '90111', 28);
INSERT INTO `st_city` VALUES (255, 'Malang', 'Kabupaten', '65163', 11);
INSERT INTO `st_city` VALUES (256, 'Malang', 'Kota', '65112', 11);
INSERT INTO `st_city` VALUES (257, 'Malinau', 'Kabupaten', '77511', 16);
INSERT INTO `st_city` VALUES (258, 'Maluku Barat Daya', 'Kabupaten', '97451', 19);
INSERT INTO `st_city` VALUES (259, 'Maluku Tengah', 'Kabupaten', '97513', 19);
INSERT INTO `st_city` VALUES (260, 'Maluku Tenggara', 'Kabupaten', '97651', 19);
INSERT INTO `st_city` VALUES (261, 'Maluku Tenggara Barat', 'Kabupaten', '97465', 19);
INSERT INTO `st_city` VALUES (262, 'Mamasa', 'Kabupaten', '91362', 27);
INSERT INTO `st_city` VALUES (263, 'Mamberamo Raya', 'Kabupaten', '99381', 24);
INSERT INTO `st_city` VALUES (264, 'Mamberamo Tengah', 'Kabupaten', '99553', 24);
INSERT INTO `st_city` VALUES (265, 'Mamuju', 'Kabupaten', '91519', 27);
INSERT INTO `st_city` VALUES (266, 'Mamuju Utara', 'Kabupaten', '91571', 27);
INSERT INTO `st_city` VALUES (267, 'Manado', 'Kota', '95247', 31);
INSERT INTO `st_city` VALUES (268, 'Mandailing Natal', 'Kabupaten', '22916', 34);
INSERT INTO `st_city` VALUES (269, 'Manggarai', 'Kabupaten', '86551', 23);
INSERT INTO `st_city` VALUES (270, 'Manggarai Barat', 'Kabupaten', '86711', 23);
INSERT INTO `st_city` VALUES (271, 'Manggarai Timur', 'Kabupaten', '86811', 23);
INSERT INTO `st_city` VALUES (272, 'Manokwari', 'Kabupaten', '98311', 25);
INSERT INTO `st_city` VALUES (273, 'Manokwari Selatan', 'Kabupaten', '98355', 25);
INSERT INTO `st_city` VALUES (274, 'Mappi', 'Kabupaten', '99853', 24);
INSERT INTO `st_city` VALUES (275, 'Maros', 'Kabupaten', '90511', 28);
INSERT INTO `st_city` VALUES (276, 'Mataram', 'Kota', '83131', 22);
INSERT INTO `st_city` VALUES (277, 'Maybrat', 'Kabupaten', '98051', 25);
INSERT INTO `st_city` VALUES (278, 'Medan', 'Kota', '20228', 34);
INSERT INTO `st_city` VALUES (279, 'Melawi', 'Kabupaten', '78619', 12);
INSERT INTO `st_city` VALUES (280, 'Merangin', 'Kabupaten', '37319', 8);
INSERT INTO `st_city` VALUES (281, 'Merauke', 'Kabupaten', '99613', 24);
INSERT INTO `st_city` VALUES (282, 'Mesuji', 'Kabupaten', '34911', 18);
INSERT INTO `st_city` VALUES (283, 'Metro', 'Kota', '34111', 18);
INSERT INTO `st_city` VALUES (284, 'Mimika', 'Kabupaten', '99962', 24);
INSERT INTO `st_city` VALUES (285, 'Minahasa', 'Kabupaten', '95614', 31);
INSERT INTO `st_city` VALUES (286, 'Minahasa Selatan', 'Kabupaten', '95914', 31);
INSERT INTO `st_city` VALUES (287, 'Minahasa Tenggara', 'Kabupaten', '95995', 31);
INSERT INTO `st_city` VALUES (288, 'Minahasa Utara', 'Kabupaten', '95316', 31);
INSERT INTO `st_city` VALUES (289, 'Mojokerto', 'Kabupaten', '61382', 11);
INSERT INTO `st_city` VALUES (290, 'Mojokerto', 'Kota', '61316', 11);
INSERT INTO `st_city` VALUES (291, 'Morowali', 'Kabupaten', '94911', 29);
INSERT INTO `st_city` VALUES (292, 'Muara Enim', 'Kabupaten', '31315', 33);
INSERT INTO `st_city` VALUES (293, 'Muaro Jambi', 'Kabupaten', '36311', 8);
INSERT INTO `st_city` VALUES (294, 'Muko Muko', 'Kabupaten', '38715', 4);
INSERT INTO `st_city` VALUES (295, 'Muna', 'Kabupaten', '93611', 30);
INSERT INTO `st_city` VALUES (296, 'Murung Raya', 'Kabupaten', '73911', 14);
INSERT INTO `st_city` VALUES (297, 'Musi Banyuasin', 'Kabupaten', '30719', 33);
INSERT INTO `st_city` VALUES (298, 'Musi Rawas', 'Kabupaten', '31661', 33);
INSERT INTO `st_city` VALUES (299, 'Nabire', 'Kabupaten', '98816', 24);
INSERT INTO `st_city` VALUES (300, 'Nagan Raya', 'Kabupaten', '23674', 21);
INSERT INTO `st_city` VALUES (301, 'Nagekeo', 'Kabupaten', '86911', 23);
INSERT INTO `st_city` VALUES (302, 'Natuna', 'Kabupaten', '29711', 17);
INSERT INTO `st_city` VALUES (303, 'Nduga', 'Kabupaten', '99541', 24);
INSERT INTO `st_city` VALUES (304, 'Ngada', 'Kabupaten', '86413', 23);
INSERT INTO `st_city` VALUES (305, 'Nganjuk', 'Kabupaten', '64414', 11);
INSERT INTO `st_city` VALUES (306, 'Ngawi', 'Kabupaten', '63219', 11);
INSERT INTO `st_city` VALUES (307, 'Nias', 'Kabupaten', '22876', 34);
INSERT INTO `st_city` VALUES (308, 'Nias Barat', 'Kabupaten', '22895', 34);
INSERT INTO `st_city` VALUES (309, 'Nias Selatan', 'Kabupaten', '22865', 34);
INSERT INTO `st_city` VALUES (310, 'Nias Utara', 'Kabupaten', '22856', 34);
INSERT INTO `st_city` VALUES (311, 'Nunukan', 'Kabupaten', '77421', 16);
INSERT INTO `st_city` VALUES (312, 'Ogan Ilir', 'Kabupaten', '30811', 33);
INSERT INTO `st_city` VALUES (313, 'Ogan Komering Ilir', 'Kabupaten', '30618', 33);
INSERT INTO `st_city` VALUES (314, 'Ogan Komering Ulu', 'Kabupaten', '32112', 33);
INSERT INTO `st_city` VALUES (315, 'Ogan Komering Ulu Selatan', 'Kabupaten', '32211', 33);
INSERT INTO `st_city` VALUES (316, 'Ogan Komering Ulu Timur', 'Kabupaten', '32312', 33);
INSERT INTO `st_city` VALUES (317, 'Pacitan', 'Kabupaten', '63512', 11);
INSERT INTO `st_city` VALUES (318, 'Padang', 'Kota', '25112', 32);
INSERT INTO `st_city` VALUES (319, 'Padang Lawas', 'Kabupaten', '22763', 34);
INSERT INTO `st_city` VALUES (320, 'Padang Lawas Utara', 'Kabupaten', '22753', 34);
INSERT INTO `st_city` VALUES (321, 'Padang Panjang', 'Kota', '27122', 32);
INSERT INTO `st_city` VALUES (322, 'Padang Pariaman', 'Kabupaten', '25583', 32);
INSERT INTO `st_city` VALUES (323, 'Padang Sidempuan', 'Kota', '22727', 34);
INSERT INTO `st_city` VALUES (324, 'Pagar Alam', 'Kota', '31512', 33);
INSERT INTO `st_city` VALUES (325, 'Pakpak Bharat', 'Kabupaten', '22272', 34);
INSERT INTO `st_city` VALUES (326, 'Palangka Raya', 'Kota', '73112', 14);
INSERT INTO `st_city` VALUES (327, 'Palembang', 'Kota', '31512', 33);
INSERT INTO `st_city` VALUES (328, 'Palopo', 'Kota', '91911', 28);
INSERT INTO `st_city` VALUES (329, 'Palu', 'Kota', '94111', 29);
INSERT INTO `st_city` VALUES (330, 'Pamekasan', 'Kabupaten', '69319', 11);
INSERT INTO `st_city` VALUES (331, 'Pandeglang', 'Kabupaten', '42212', 3);
INSERT INTO `st_city` VALUES (332, 'Pangandaran', 'Kabupaten', '46511', 9);
INSERT INTO `st_city` VALUES (333, 'Pangkajene Kepulauan', 'Kabupaten', '90611', 28);
INSERT INTO `st_city` VALUES (334, 'Pangkal Pinang', 'Kota', '33115', 2);
INSERT INTO `st_city` VALUES (335, 'Paniai', 'Kabupaten', '98765', 24);
INSERT INTO `st_city` VALUES (336, 'Parepare', 'Kota', '91123', 28);
INSERT INTO `st_city` VALUES (337, 'Pariaman', 'Kota', '25511', 32);
INSERT INTO `st_city` VALUES (338, 'Parigi Moutong', 'Kabupaten', '94411', 29);
INSERT INTO `st_city` VALUES (339, 'Pasaman', 'Kabupaten', '26318', 32);
INSERT INTO `st_city` VALUES (340, 'Pasaman Barat', 'Kabupaten', '26511', 32);
INSERT INTO `st_city` VALUES (341, 'Paser', 'Kabupaten', '76211', 15);
INSERT INTO `st_city` VALUES (342, 'Pasuruan', 'Kabupaten', '67153', 11);
INSERT INTO `st_city` VALUES (343, 'Pasuruan', 'Kota', '67118', 11);
INSERT INTO `st_city` VALUES (344, 'Pati', 'Kabupaten', '59114', 10);
INSERT INTO `st_city` VALUES (345, 'Payakumbuh', 'Kota', '26213', 32);
INSERT INTO `st_city` VALUES (346, 'Pegunungan Arfak', 'Kabupaten', '98354', 25);
INSERT INTO `st_city` VALUES (347, 'Pegunungan Bintang', 'Kabupaten', '99573', 24);
INSERT INTO `st_city` VALUES (348, 'Pekalongan', 'Kabupaten', '51161', 10);
INSERT INTO `st_city` VALUES (349, 'Pekalongan', 'Kota', '51122', 10);
INSERT INTO `st_city` VALUES (350, 'Pekanbaru', 'Kota', '28112', 26);
INSERT INTO `st_city` VALUES (351, 'Pelalawan', 'Kabupaten', '28311', 26);
INSERT INTO `st_city` VALUES (352, 'Pemalang', 'Kabupaten', '52319', 10);
INSERT INTO `st_city` VALUES (353, 'Pematang Siantar', 'Kota', '21126', 34);
INSERT INTO `st_city` VALUES (354, 'Penajam Paser Utara', 'Kabupaten', '76311', 15);
INSERT INTO `st_city` VALUES (355, 'Pesawaran', 'Kabupaten', '35312', 18);
INSERT INTO `st_city` VALUES (356, 'Pesisir Barat', 'Kabupaten', '35974', 18);
INSERT INTO `st_city` VALUES (357, 'Pesisir Selatan', 'Kabupaten', '25611', 32);
INSERT INTO `st_city` VALUES (358, 'Pidie', 'Kabupaten', '24116', 21);
INSERT INTO `st_city` VALUES (359, 'Pidie Jaya', 'Kabupaten', '24186', 21);
INSERT INTO `st_city` VALUES (360, 'Pinrang', 'Kabupaten', '91251', 28);
INSERT INTO `st_city` VALUES (361, 'Pohuwato', 'Kabupaten', '96419', 7);
INSERT INTO `st_city` VALUES (362, 'Polewali Mandar', 'Kabupaten', '91311', 27);
INSERT INTO `st_city` VALUES (363, 'Ponorogo', 'Kabupaten', '63411', 11);
INSERT INTO `st_city` VALUES (364, 'Pontianak', 'Kabupaten', '78971', 12);
INSERT INTO `st_city` VALUES (365, 'Pontianak', 'Kota', '78112', 12);
INSERT INTO `st_city` VALUES (366, 'Poso', 'Kabupaten', '94615', 29);
INSERT INTO `st_city` VALUES (367, 'Prabumulih', 'Kota', '31121', 33);
INSERT INTO `st_city` VALUES (368, 'Pringsewu', 'Kabupaten', '35719', 18);
INSERT INTO `st_city` VALUES (369, 'Probolinggo', 'Kabupaten', '67282', 11);
INSERT INTO `st_city` VALUES (370, 'Probolinggo', 'Kota', '67215', 11);
INSERT INTO `st_city` VALUES (371, 'Pulang Pisau', 'Kabupaten', '74811', 14);
INSERT INTO `st_city` VALUES (372, 'Pulau Morotai', 'Kabupaten', '97771', 20);
INSERT INTO `st_city` VALUES (373, 'Puncak', 'Kabupaten', '98981', 24);
INSERT INTO `st_city` VALUES (374, 'Puncak Jaya', 'Kabupaten', '98979', 24);
INSERT INTO `st_city` VALUES (375, 'Purbalingga', 'Kabupaten', '53312', 10);
INSERT INTO `st_city` VALUES (376, 'Purwakarta', 'Kabupaten', '41119', 9);
INSERT INTO `st_city` VALUES (377, 'Purworejo', 'Kabupaten', '54111', 10);
INSERT INTO `st_city` VALUES (378, 'Raja Ampat', 'Kabupaten', '98489', 25);
INSERT INTO `st_city` VALUES (379, 'Rejang Lebong', 'Kabupaten', '39112', 4);
INSERT INTO `st_city` VALUES (380, 'Rembang', 'Kabupaten', '59219', 10);
INSERT INTO `st_city` VALUES (381, 'Rokan Hilir', 'Kabupaten', '28992', 26);
INSERT INTO `st_city` VALUES (382, 'Rokan Hulu', 'Kabupaten', '28511', 26);
INSERT INTO `st_city` VALUES (383, 'Rote Ndao', 'Kabupaten', '85982', 23);
INSERT INTO `st_city` VALUES (384, 'Sabang', 'Kota', '23512', 21);
INSERT INTO `st_city` VALUES (385, 'Sabu Raijua', 'Kabupaten', '85391', 23);
INSERT INTO `st_city` VALUES (386, 'Salatiga', 'Kota', '50711', 10);
INSERT INTO `st_city` VALUES (387, 'Samarinda', 'Kota', '75133', 15);
INSERT INTO `st_city` VALUES (388, 'Sambas', 'Kabupaten', '79453', 12);
INSERT INTO `st_city` VALUES (389, 'Samosir', 'Kabupaten', '22392', 34);
INSERT INTO `st_city` VALUES (390, 'Sampang', 'Kabupaten', '69219', 11);
INSERT INTO `st_city` VALUES (391, 'Sanggau', 'Kabupaten', '78557', 12);
INSERT INTO `st_city` VALUES (392, 'Sarmi', 'Kabupaten', '99373', 24);
INSERT INTO `st_city` VALUES (393, 'Sarolangun', 'Kabupaten', '37419', 8);
INSERT INTO `st_city` VALUES (394, 'Sawah Lunto', 'Kota', '27416', 32);
INSERT INTO `st_city` VALUES (395, 'Sekadau', 'Kabupaten', '79583', 12);
INSERT INTO `st_city` VALUES (396, 'Selayar (Kepulauan Selayar)', 'Kabupaten', '92812', 28);
INSERT INTO `st_city` VALUES (397, 'Seluma', 'Kabupaten', '38811', 4);
INSERT INTO `st_city` VALUES (398, 'Semarang', 'Kabupaten', '50511', 10);
INSERT INTO `st_city` VALUES (399, 'Semarang', 'Kota', '50135', 10);
INSERT INTO `st_city` VALUES (400, 'Seram Bagian Barat', 'Kabupaten', '97561', 19);
INSERT INTO `st_city` VALUES (401, 'Seram Bagian Timur', 'Kabupaten', '97581', 19);
INSERT INTO `st_city` VALUES (402, 'Serang', 'Kabupaten', '42182', 3);
INSERT INTO `st_city` VALUES (403, 'Serang', 'Kota', '42111', 3);
INSERT INTO `st_city` VALUES (404, 'Serdang Bedagai', 'Kabupaten', '20915', 34);
INSERT INTO `st_city` VALUES (405, 'Seruyan', 'Kabupaten', '74211', 14);
INSERT INTO `st_city` VALUES (406, 'Siak', 'Kabupaten', '28623', 26);
INSERT INTO `st_city` VALUES (407, 'Sibolga', 'Kota', '22522', 34);
INSERT INTO `st_city` VALUES (408, 'Sidenreng Rappang/Rapang', 'Kabupaten', '91613', 28);
INSERT INTO `st_city` VALUES (409, 'Sidoarjo', 'Kabupaten', '61219', 11);
INSERT INTO `st_city` VALUES (410, 'Sigi', 'Kabupaten', '94364', 29);
INSERT INTO `st_city` VALUES (411, 'Sijunjung (Sawah Lunto Sijunjung)', 'Kabupaten', '27511', 32);
INSERT INTO `st_city` VALUES (412, 'Sikka', 'Kabupaten', '86121', 23);
INSERT INTO `st_city` VALUES (413, 'Simalungun', 'Kabupaten', '21162', 34);
INSERT INTO `st_city` VALUES (414, 'Simeulue', 'Kabupaten', '23891', 21);
INSERT INTO `st_city` VALUES (415, 'Singkawang', 'Kota', '79117', 12);
INSERT INTO `st_city` VALUES (416, 'Sinjai', 'Kabupaten', '92615', 28);
INSERT INTO `st_city` VALUES (417, 'Sintang', 'Kabupaten', '78619', 12);
INSERT INTO `st_city` VALUES (418, 'Situbondo', 'Kabupaten', '68316', 11);
INSERT INTO `st_city` VALUES (419, 'Sleman', 'Kabupaten', '55513', 5);
INSERT INTO `st_city` VALUES (420, 'Solok', 'Kabupaten', '27365', 32);
INSERT INTO `st_city` VALUES (421, 'Solok', 'Kota', '27315', 32);
INSERT INTO `st_city` VALUES (422, 'Solok Selatan', 'Kabupaten', '27779', 32);
INSERT INTO `st_city` VALUES (423, 'Soppeng', 'Kabupaten', '90812', 28);
INSERT INTO `st_city` VALUES (424, 'Sorong', 'Kabupaten', '98431', 25);
INSERT INTO `st_city` VALUES (425, 'Sorong', 'Kota', '98411', 25);
INSERT INTO `st_city` VALUES (426, 'Sorong Selatan', 'Kabupaten', '98454', 25);
INSERT INTO `st_city` VALUES (427, 'Sragen', 'Kabupaten', '57211', 10);
INSERT INTO `st_city` VALUES (428, 'Subang', 'Kabupaten', '41215', 9);
INSERT INTO `st_city` VALUES (429, 'Subulussalam', 'Kota', '24882', 21);
INSERT INTO `st_city` VALUES (430, 'Sukabumi', 'Kabupaten', '43311', 9);
INSERT INTO `st_city` VALUES (431, 'Sukabumi', 'Kota', '43114', 9);
INSERT INTO `st_city` VALUES (432, 'Sukamara', 'Kabupaten', '74712', 14);
INSERT INTO `st_city` VALUES (433, 'Sukoharjo', 'Kabupaten', '57514', 10);
INSERT INTO `st_city` VALUES (434, 'Sumba Barat', 'Kabupaten', '87219', 23);
INSERT INTO `st_city` VALUES (435, 'Sumba Barat Daya', 'Kabupaten', '87453', 23);
INSERT INTO `st_city` VALUES (436, 'Sumba Tengah', 'Kabupaten', '87358', 23);
INSERT INTO `st_city` VALUES (437, 'Sumba Timur', 'Kabupaten', '87112', 23);
INSERT INTO `st_city` VALUES (438, 'Sumbawa', 'Kabupaten', '84315', 22);
INSERT INTO `st_city` VALUES (439, 'Sumbawa Barat', 'Kabupaten', '84419', 22);
INSERT INTO `st_city` VALUES (440, 'Sumedang', 'Kabupaten', '45326', 9);
INSERT INTO `st_city` VALUES (441, 'Sumenep', 'Kabupaten', '69413', 11);
INSERT INTO `st_city` VALUES (442, 'Sungaipenuh', 'Kota', '37113', 8);
INSERT INTO `st_city` VALUES (443, 'Supiori', 'Kabupaten', '98164', 24);
INSERT INTO `st_city` VALUES (444, 'Surabaya', 'Kota', '60119', 11);
INSERT INTO `st_city` VALUES (445, 'Surakarta (Solo)', 'Kota', '57113', 10);
INSERT INTO `st_city` VALUES (446, 'Tabalong', 'Kabupaten', '71513', 13);
INSERT INTO `st_city` VALUES (447, 'Tabanan', 'Kabupaten', '82119', 1);
INSERT INTO `st_city` VALUES (448, 'Takalar', 'Kabupaten', '92212', 28);
INSERT INTO `st_city` VALUES (449, 'Tambrauw', 'Kabupaten', '98475', 25);
INSERT INTO `st_city` VALUES (450, 'Tana Tidung', 'Kabupaten', '77611', 16);
INSERT INTO `st_city` VALUES (451, 'Tana Toraja', 'Kabupaten', '91819', 28);
INSERT INTO `st_city` VALUES (452, 'Tanah Bumbu', 'Kabupaten', '72211', 13);
INSERT INTO `st_city` VALUES (453, 'Tanah Datar', 'Kabupaten', '27211', 32);
INSERT INTO `st_city` VALUES (454, 'Tanah Laut', 'Kabupaten', '70811', 13);
INSERT INTO `st_city` VALUES (455, 'Tangerang', 'Kabupaten', '15914', 3);
INSERT INTO `st_city` VALUES (456, 'Tangerang', 'Kota', '15111', 3);
INSERT INTO `st_city` VALUES (457, 'Tangerang Selatan', 'Kota', '15332', 3);
INSERT INTO `st_city` VALUES (458, 'Tanggamus', 'Kabupaten', '35619', 18);
INSERT INTO `st_city` VALUES (459, 'Tanjung Balai', 'Kota', '21321', 34);
INSERT INTO `st_city` VALUES (460, 'Tanjung Jabung Barat', 'Kabupaten', '36513', 8);
INSERT INTO `st_city` VALUES (461, 'Tanjung Jabung Timur', 'Kabupaten', '36719', 8);
INSERT INTO `st_city` VALUES (462, 'Tanjung Pinang', 'Kota', '29111', 17);
INSERT INTO `st_city` VALUES (463, 'Tapanuli Selatan', 'Kabupaten', '22742', 34);
INSERT INTO `st_city` VALUES (464, 'Tapanuli Tengah', 'Kabupaten', '22611', 34);
INSERT INTO `st_city` VALUES (465, 'Tapanuli Utara', 'Kabupaten', '22414', 34);
INSERT INTO `st_city` VALUES (466, 'Tapin', 'Kabupaten', '71119', 13);
INSERT INTO `st_city` VALUES (467, 'Tarakan', 'Kota', '77114', 16);
INSERT INTO `st_city` VALUES (468, 'Tasikmalaya', 'Kabupaten', '46411', 9);
INSERT INTO `st_city` VALUES (469, 'Tasikmalaya', 'Kota', '46116', 9);
INSERT INTO `st_city` VALUES (470, 'Tebing Tinggi', 'Kota', '20632', 34);
INSERT INTO `st_city` VALUES (471, 'Tebo', 'Kabupaten', '37519', 8);
INSERT INTO `st_city` VALUES (472, 'Tegal', 'Kabupaten', '52419', 10);
INSERT INTO `st_city` VALUES (473, 'Tegal', 'Kota', '52114', 10);
INSERT INTO `st_city` VALUES (474, 'Teluk Bintuni', 'Kabupaten', '98551', 25);
INSERT INTO `st_city` VALUES (475, 'Teluk Wondama', 'Kabupaten', '98591', 25);
INSERT INTO `st_city` VALUES (476, 'Temanggung', 'Kabupaten', '56212', 10);
INSERT INTO `st_city` VALUES (477, 'Ternate', 'Kota', '97714', 20);
INSERT INTO `st_city` VALUES (478, 'Tidore Kepulauan', 'Kota', '97815', 20);
INSERT INTO `st_city` VALUES (479, 'Timor Tengah Selatan', 'Kabupaten', '85562', 23);
INSERT INTO `st_city` VALUES (480, 'Timor Tengah Utara', 'Kabupaten', '85612', 23);
INSERT INTO `st_city` VALUES (481, 'Toba Samosir', 'Kabupaten', '22316', 34);
INSERT INTO `st_city` VALUES (482, 'Tojo Una-Una', 'Kabupaten', '94683', 29);
INSERT INTO `st_city` VALUES (483, 'Toli-Toli', 'Kabupaten', '94542', 29);
INSERT INTO `st_city` VALUES (484, 'Tolikara', 'Kabupaten', '99411', 24);
INSERT INTO `st_city` VALUES (485, 'Tomohon', 'Kota', '95416', 31);
INSERT INTO `st_city` VALUES (486, 'Toraja Utara', 'Kabupaten', '91831', 28);
INSERT INTO `st_city` VALUES (487, 'Trenggalek', 'Kabupaten', '66312', 11);
INSERT INTO `st_city` VALUES (488, 'Tual', 'Kota', '97612', 19);
INSERT INTO `st_city` VALUES (489, 'Tuban', 'Kabupaten', '62319', 11);
INSERT INTO `st_city` VALUES (490, 'Tulang Bawang', 'Kabupaten', '34613', 18);
INSERT INTO `st_city` VALUES (491, 'Tulang Bawang Barat', 'Kabupaten', '34419', 18);
INSERT INTO `st_city` VALUES (492, 'Tulungagung', 'Kabupaten', '66212', 11);
INSERT INTO `st_city` VALUES (493, 'Wajo', 'Kabupaten', '90911', 28);
INSERT INTO `st_city` VALUES (494, 'Wakatobi', 'Kabupaten', '93791', 30);
INSERT INTO `st_city` VALUES (495, 'Waropen', 'Kabupaten', '98269', 24);
INSERT INTO `st_city` VALUES (496, 'Way Kanan', 'Kabupaten', '34711', 18);
INSERT INTO `st_city` VALUES (497, 'Wonogiri', 'Kabupaten', '57619', 10);
INSERT INTO `st_city` VALUES (498, 'Wonosobo', 'Kabupaten', '56311', 10);
INSERT INTO `st_city` VALUES (499, 'Yahukimo', 'Kabupaten', '99041', 24);
INSERT INTO `st_city` VALUES (500, 'Yalimo', 'Kabupaten', '99481', 24);
INSERT INTO `st_city` VALUES (501, 'Yogyakarta', 'Kota', '55222', 5);

-- ----------------------------
-- Table structure for st_province
-- ----------------------------
DROP TABLE IF EXISTS `st_province`;
CREATE TABLE `st_province`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `province` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of st_province
-- ----------------------------
INSERT INTO `st_province` VALUES (1, 'Bali');
INSERT INTO `st_province` VALUES (2, 'Bangka Belitung');
INSERT INTO `st_province` VALUES (3, 'Banten');
INSERT INTO `st_province` VALUES (4, 'Bengkulu');
INSERT INTO `st_province` VALUES (5, 'DI Yogyakarta');
INSERT INTO `st_province` VALUES (6, 'DKI Jakarta');
INSERT INTO `st_province` VALUES (7, 'Gorontalo');
INSERT INTO `st_province` VALUES (8, 'Jambi');
INSERT INTO `st_province` VALUES (9, 'Jawa Barat');
INSERT INTO `st_province` VALUES (10, 'Jawa Tengah');
INSERT INTO `st_province` VALUES (11, 'Jawa Timur');
INSERT INTO `st_province` VALUES (12, 'Kalimantan Barat');
INSERT INTO `st_province` VALUES (13, 'Kalimantan Selatan');
INSERT INTO `st_province` VALUES (14, 'Kalimantan Tengah');
INSERT INTO `st_province` VALUES (15, 'Kalimantan Timur');
INSERT INTO `st_province` VALUES (16, 'Kalimantan Utara');
INSERT INTO `st_province` VALUES (17, 'Kepulauan Riau');
INSERT INTO `st_province` VALUES (18, 'Lampung');
INSERT INTO `st_province` VALUES (19, 'Maluku');
INSERT INTO `st_province` VALUES (20, 'Maluku Utara');
INSERT INTO `st_province` VALUES (21, 'Nanggroe Aceh Darussalam (NAD)');
INSERT INTO `st_province` VALUES (22, 'Nusa Tenggara Barat (NTB)');
INSERT INTO `st_province` VALUES (23, 'Nusa Tenggara Timur (NTT)');
INSERT INTO `st_province` VALUES (24, 'Papua');
INSERT INTO `st_province` VALUES (25, 'Papua Barat');
INSERT INTO `st_province` VALUES (26, 'Riau');
INSERT INTO `st_province` VALUES (27, 'Sulawesi Barat');
INSERT INTO `st_province` VALUES (28, 'Sulawesi Selatan');
INSERT INTO `st_province` VALUES (29, 'Sulawesi Tengah');
INSERT INTO `st_province` VALUES (30, 'Sulawesi Tenggara');
INSERT INTO `st_province` VALUES (31, 'Sulawesi Utara');
INSERT INTO `st_province` VALUES (32, 'Sumatera Barat');
INSERT INTO `st_province` VALUES (33, 'Sumatera Selatan');
INSERT INTO `st_province` VALUES (34, 'Sumatera Utara');

-- ----------------------------
-- Table structure for statistik
-- ----------------------------
DROP TABLE IF EXISTS `statistik`;
CREATE TABLE `statistik`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sql` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `no_urut` tinyint(3) NOT NULL,
  `icon` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bg_class` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `class_add` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_add` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `display` tinyint(1) NOT NULL DEFAULT 1,
  `type` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of statistik
-- ----------------------------
INSERT INTO `statistik` VALUES (2, 'ORDER', 'SELECT REPLACE(FORMAT(SUM(total_process),0),\',\',\'.\') from trval_this_month', 2, 'mdi-timer-sand', 'danger', '', '', 1, NULL);
INSERT INTO `statistik` VALUES (3, 'OMZET', 'SELECT REPLACE(FORMAT(SUM(total_sales),0),\',\',\'.\') from trval_this_month', 1, 'mdi-puzzle', 'warning', NULL, NULL, 1, 'rp');
INSERT INTO `statistik` VALUES (4, 'MARGIN', 'SELECT REPLACE(FORMAT(SUM(total_earnings),0),\',\',\'.\') from trval_this_month', 3, 'mdi-currency-usd', 'success', '', '', 1, 'rp');

-- ----------------------------
-- Table structure for stock_ip
-- ----------------------------
DROP TABLE IF EXISTS `stock_ip`;
CREATE TABLE `stock_ip`  (
  `ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mask` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `allowed` int(1) NOT NULL DEFAULT 1,
  `grouped` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ip`) USING BTREE,
  UNIQUE INDEX `ip_UNIQUE`(`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of stock_ip
-- ----------------------------
INSERT INTO `stock_ip` VALUES ('127.0.0.1', NULL, 1, 'REST');
INSERT INTO `stock_ip` VALUES ('139.192.193.34', NULL, 1, 'REST');
INSERT INTO `stock_ip` VALUES ('192.168.0.0', '255.255.0.0', 1, 'REST');
INSERT INTO `stock_ip` VALUES ('dmcstok.dmc.zone', NULL, 1, 'REST');
INSERT INTO `stock_ip` VALUES ('localhost', NULL, 1, 'REST');

-- ----------------------------
-- Table structure for stock_item
-- ----------------------------
DROP TABLE IF EXISTS `stock_item`;
CREATE TABLE `stock_item`  (
  `iditem` int(11) NOT NULL,
  `store_url` varchar(240) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `item_url` varchar(240) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `item_updated` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iditem`) USING BTREE,
  INDEX `stock_item_store_idx`(`store_url`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of stock_item
-- ----------------------------
INSERT INTO `stock_item` VALUES (42, 'https://www.tokopedia.com/ddctest', 'https://www.tokopedia.com/product-edit.pl?id=238476906', '2018-02-08 15:41:28');
INSERT INTO `stock_item` VALUES (43, 'https://www.bukalapak.com/u/firosyanrosyiddin', 'https://www.bukalapak.com/products/d4909t-jual-selelinux/edit', '2018-02-08 15:41:28');

-- ----------------------------
-- Table structure for stock_store
-- ----------------------------
DROP TABLE IF EXISTS `stock_store`;
CREATE TABLE `stock_store`  (
  `store_url` varchar(240) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `marketplace` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `store_name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `store_id` int(11) NOT NULL,
  PRIMARY KEY (`store_url`) USING BTREE,
  UNIQUE INDEX `store_url_UNIQUE`(`store_url`) USING BTREE,
  INDEX `userid_idx`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of stock_store
-- ----------------------------
INSERT INTO `stock_store` VALUES ('https://www.bukalapak.com/u/firosyanrosyiddin', 10, 'Bukalapak', 'fr.soerabaja@gmail.com', 'frasoerabajaagmailacopSpgLbuO5xzc9kUiHSfh5M=', 'Firosyan Rosyiddin', 0);
INSERT INTO `stock_store` VALUES ('https://www.tokopedia.com/ddctest', 10, 'Tokopedia', 'fr.soerabaja@gmail.com', 'frasoerabajaagmailacopSpgLbuO5xzc9kUiHSfh5M=', 'ddctest', 0);

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_name` char(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `store_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES (1, 'HATOBE', NULL, 10, '2018-02-21 10:28:45');
INSERT INTO `store` VALUES (17, 'RizalStore', '123', 30, '2018-05-04 11:04:15');
INSERT INTO `store` VALUES (18, 'DevStore', 'Store for developing activities', 32, '2018-05-15 10:56:33');
INSERT INTO `store` VALUES (19, 'toko saya', '1212', 28, '2018-07-17 12:25:11');

-- ----------------------------
-- Table structure for store_payment
-- ----------------------------
DROP TABLE IF EXISTS `store_payment`;
CREATE TABLE `store_payment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `method` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of store_payment
-- ----------------------------
INSERT INTO `store_payment` VALUES (1, 1, 'transfer', '2018-02-21 11:59:58');

-- ----------------------------
-- Table structure for store_shipping
-- ----------------------------
DROP TABLE IF EXISTS `store_shipping`;
CREATE TABLE `store_shipping`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL,
  `kurir_id` int(11) NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` char(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` char(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `authKey` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `accessToken` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1:aktif & 0:tidak aktif',
  `fullname` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `date_birth` date NULL DEFAULT NULL,
  `id_role` smallint(5) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT 0,
  `is_owner` tinyint(1) NOT NULL,
  `phone` char(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `github` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (10, 'admin', '$2y$13$lW50U.9br62bLZBv1GD3pedqZp33jhzQ5q.mi0U7GCRHmI3SpgzSK', 'rijalboy98@gmail.com', '', '', 1, 'admin_fullname', NULL, 1, 1, 0, '', '', 0, '2017-10-02 11:25:47', '2018-05-14 12:44:51');
INSERT INTO `user` VALUES (11, 'agus', '$2y$13$82nMF6irRg4Ujjo4xYsEt.I3nhpykfqcx1aUesKAZCEDEXaVnR4vS', 'aguskenti88@gmail.com', '', '', 1, 'Agus Suwarno', NULL, 3, 1, 0, '', '', 0, '2017-10-02 11:25:47', '2018-05-14 12:44:51');
INSERT INTO `user` VALUES (28, 'shanti', '$2y$13$JiUtJZkNRzc/168A9y7d1OxQq83vY1tsrMatoUluiQjqQrh.HSG0i', 'SHANTI', 'Fj2SlxfHZvUQVKWjar3palOagPRBngiS', '', 1, 'Shanti', NULL, 1, 19, 1, '', '', 0, '2018-03-21 10:28:36', '2018-07-17 12:25:11');
INSERT INTO `user` VALUES (30, 'jefri', '$2y$13$TkmaOQpbA78T5OVcfI9oxeIRcsDb/BuwkX/PtPJTyk.53Q8S4FB/2', 'dutamediacipta@gmail.com', 'jsirkgZGq1ribPdPyGypJ-0-hDCgbQk2', NULL, 1, 'Jefri Sumardiono', NULL, 1, 1, 0, NULL, NULL, 0, '2018-04-25 12:13:32', '2018-05-14 12:44:51');
INSERT INTO `user` VALUES (31, 'ujang', '$2y$13$pGhAvv3dE98Vf.5.wX0C5OxsvJTw/hfDk5V2qdzSyzejw8n1KTnJC', 'ujang.fajar77@gmail.com', '09fmihFym4-_zdB8hD8knGAWgb3-lZU1', NULL, 1, 'ujang', NULL, 1, 1, 1, NULL, NULL, 0, '2018-04-25 12:14:54', '2018-05-14 12:46:42');
INSERT INTO `user` VALUES (32, 'rijal', '$2y$13$LlSxpaLVcGTX6Qy8KDC2SOBSJo1Zgkro9aFhAD.cHSzIPkjnlf83m', 'rijalboy98@gmail.com', '2cXwCzXSrO3UTTsh8r2Nx6apTZpsoA9p', 'M9n3KsKkzo', 1, 'Rizal Widiantoro', NULL, 1, 0, 1, '081249780453', NULL, 0, '2018-05-15 10:52:09', '2018-05-15 11:01:57');

-- ----------------------------
-- View structure for sales_this_week
-- ----------------------------
DROP VIEW IF EXISTS `sales_this_week`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `sales_this_week` AS SELECT
	md.kd_order,
	md.store_id,
	s.store_name store,
	mm.kode,
	md.id_material,
	m.id_attr,
	m.id_varian,
	mm.nama item,
	CONCAT(
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		var.nm_varian,
		' '
	) fullname,
	attr.nm_attr atribut,
	var.nm_varian varian,
	SUM(md.qty) qty,
	-- 	md.qty,
	DATE_FORMAT(md.done_at, '%d/%m') period_date,
	md.created_by,
	md.created_at,
	md.done_at
FROM
	material_detail md
INNER JOIN material m ON m.id = md.id_material
INNER JOIN master_material mm ON mm.kode = m.kode
INNER JOIN item_varian var ON var.id = m.id_varian
INNER JOIN item_attr attr ON attr.id = var.id_attr
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 15
AND md.`status` = 4
AND YEARWEEK(md.done_at, 1) = YEARWEEK(CURDATE(), 1)
GROUP BY
	DATE(md.done_at),
	md.id_material ;

-- ----------------------------
-- View structure for sales_this_week_by_item
-- ----------------------------
DROP VIEW IF EXISTS `sales_this_week_by_item`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `sales_this_week_by_item` AS SELECT
	mm.kode,
	md.store_id,
	s.store_name store,
	mm.nama item,
	SUM(md.qty) qty,
	DATE_FORMAT(md.done_at, '%d/%m') period_date,
	md.created_by,
	md.created_at,
	md.done_at
FROM
	material_detail md
INNER JOIN material m ON m.id = md.id_material
INNER JOIN master_material mm ON mm.kode = m.kode
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 15
AND md.`status` = 4
AND YEARWEEK(md.done_at, 1) = YEARWEEK(CURDATE(), 1)
GROUP BY
	DATE(md.done_at),
	m.kode
ORDER BY
	DATE(md.done_at) ASC ;

-- ----------------------------
-- View structure for sales_today
-- ----------------------------
DROP VIEW IF EXISTS `sales_today`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `sales_today` AS SELECT
	md.kd_order,
	md.store_id,
	s.store_name store,
	mm.kode,
	md.id_material,
	mm.nama item,
	CONCAT(
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		var.nm_varian,
		' '
	) fullname,
	attr.nm_attr atribut,
	var.nm_varian varian,
	SUM(md.qty) qty,
	-- 	md.qty,
	md.created_by,
	md.created_at,
	md.done_at
FROM
	material_detail md
INNER JOIN material m ON m.id = md.id_material
INNER JOIN master_material mm ON mm.kode = m.kode
INNER JOIN item_varian var ON var.id = m.id_varian
INNER JOIN item_attr attr ON attr.id = var.id_attr
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 15
AND md.`status` = 4
AND DATE(md.done_at) = CURDATE()
GROUP BY
	md.id_material ;

-- ----------------------------
-- View structure for status
-- ----------------------------
DROP VIEW IF EXISTS `status`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `status` AS SELECT id,ktg_status,nm_status,ket_status,class FROM app_status ORDER BY app_status.ktg_status ASC ;

-- ----------------------------
-- View structure for top5sales
-- ----------------------------
DROP VIEW IF EXISTS `top5sales`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `top5sales` AS SELECT
	mm.kode,
	md.store_id,
	s.store_name store,
	mm.nama item,
	SUM(md.qty) sold -- 	md.qty,
FROM
	material_detail md
INNER JOIN material m ON m.id = md.id_material
INNER JOIN master_material mm ON mm.kode = m.kode
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 15
AND md.`status` = 4 -- AND YEARWEEK(md.done_at, 1) = YEARWEEK(CURDATE(), 1)
GROUP BY
	m.kode
ORDER BY
	SUM(md.qty) DESC
LIMIT 5 ;

-- ----------------------------
-- View structure for trval_this_month
-- ----------------------------
DROP VIEW IF EXISTS `trval_this_month`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `trval_this_month` AS SELECT
	mm.kode,
	mm.store_id,
	s.store_name store,
	mm.kode_warna,
	mm.nama item,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND DATE_FORMAT(md_sub.done_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) qty_sales,
	IFNULL(
		(
			SELECT
				SUM(hgsat_jual * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND DATE_FORMAT(md_sub.done_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) total_sales,
	IFNULL(
		(
			SELECT
				SUM((hgsat_jual - harga_mod) * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND DATE_FORMAT(md_sub.done_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) total_earnings,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` IN (24, 3)
			AND DATE_FORMAT(md_sub.created_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) qty_process,
	IFNULL(
		(
			SELECT
				SUM(hgsat_jual * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` IN (24, 3)
			AND DATE_FORMAT(md_sub.created_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) total_process
FROM
	master_material mm
INNER JOIN store s ON s.id = mm.store_id ;

-- ----------------------------
-- View structure for trval_this_week
-- ----------------------------
DROP VIEW IF EXISTS `trval_this_week`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `trval_this_week` AS SELECT
	mm.kode,
	mm.store_id,
	s.store_name store,
	mm.kode_warna,
	mm.nama item,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND YEARWEEK(md_sub.done_at, 1) = YEARWEEK(CURDATE(), 1)
		),
		0
	) qty_sales,
	IFNULL(
		(
			SELECT
				SUM(hgsat_jual * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND YEARWEEK(md_sub.done_at, 1) = YEARWEEK(CURDATE(), 1)
		),
		0
	) total_sales,
	IFNULL(
		(
			SELECT
				SUM((hgsat_jual - harga_mod) * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND YEARWEEK(md_sub.done_at, 1) = YEARWEEK(CURDATE(), 1)
		),
		0
	) total_earnings,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` IN (24, 3)
			AND YEARWEEK(md_sub.created_at, 1) = YEARWEEK(CURDATE(), 1)
		),
		0
	) qty_process,
	IFNULL(
		(
			SELECT
				SUM(hgsat_jual * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` IN (24, 3)
			AND YEARWEEK(md_sub.created_at, 1) = YEARWEEK(CURDATE(), 1)
		),
		0
	) total_process
FROM
	master_material mm
INNER JOIN store s ON s.id = mm.store_id
WHERE
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND YEARWEEK(md_sub.done_at, 1) = YEARWEEK(CURDATE(), 1)
		),
		0
	) > 0 ;

-- ----------------------------
-- View structure for trval_this_year
-- ----------------------------
DROP VIEW IF EXISTS `trval_this_year`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `trval_this_year` AS SELECT
	m.kode,
	mm.store_id,
	s.store_name store,
	mm.nama item,
	SUM(qty) qty_sales,
	SUM(hgsat_jual * qty) total_sales,
	SUM((hgsat_jual - harga_mod) * qty) total_earnings,
	DATE_FORMAT(md.done_at, '%m-%Y') `month_field`
FROM
	material_detail md
INNER JOIN material m ON m.id = md.id_material
INNER JOIN master_material mm ON mm.kode = m.kode
INNER JOIN store s ON s.id = mm.store_id
WHERE
	tipe_transaksi = 15
AND (
	hgsat_jual IS NOT NULL
	AND hgsat_jual > 0
)
AND md.`status` = 4
AND YEAR (md.done_at) = YEAR (CURDATE())
GROUP BY
	MONTH (md.done_at),
	m.kode
ORDER BY
	DATE_FORMAT(md.done_at, '%Y-%m') ASC ;

-- ----------------------------
-- View structure for trval_today
-- ----------------------------
DROP VIEW IF EXISTS `trval_today`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `trval_today` AS SELECT
	m.kode,
	mm.store_id,
	s.store_name store,
	mm.nama item,
	SUM(qty) qty_sales,
	SUM(hgsat_jual * qty) total_sales,
	SUM((hgsat_jual - harga_mod) * qty) total_earnings
FROM
	material_detail md
INNER JOIN material m ON m.id = md.id_material
INNER JOIN master_material mm ON mm.kode = m.kode
INNER JOIN store s ON s.id = mm.store_id
WHERE
	tipe_transaksi = 15
AND (
	hgsat_jual IS NOT NULL
	AND hgsat_jual > 0
)
AND md.`status` = 4
AND DATE(md.done_at) = CURDATE()
GROUP BY
	m.kode
ORDER BY
	SUM(qty) DESC ;

-- ----------------------------
-- View structure for v_bk
-- ----------------------------
DROP VIEW IF EXISTS `v_bk`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_bk` AS SELECT
	md.kd_order,
	md.store_id,
	s.store_name store,
	md.tipe_transfer,
	md.tipe_transaksi,
	SUM(md.qty) qty,
	md.ongkir_buyer,
	SUM(hgsat_jual * qty) total_belanja,
	(SUM(hgsat_jual * qty)) + ongkir_buyer total_tagihan_buyer,
	md.buyer,
	md.buyer_note,
	md.id_market,
	md.id_kurir,
	md.resi,
	md.resi_photo,
	md.`status` id_status,
	md.is_gived,
	md.send_by,
	md.created_at,
	md.created_by
FROM
	material_detail md
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 15
AND md.`status` IN (24, 3)
GROUP BY
	md.kd_order
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_bm
-- ----------------------------
DROP VIEW IF EXISTS `v_bm`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_bm` AS SELECT
	md.id,
	md.kd_tagihan,
	md.store_id,
	s.store_name store,
	md.tipe_transfer,
	md.tipe_transaksi,
	md.ppn,
	md.pph,
	md.dll,
	md.bea_masuk,
	md.hgtot_dasar,
	md.hgtot_beli,
	md.ongkir_impor,
	SUM(md.qty) qty_total,
	md.id_sup,
	md.`status` id_status,
	md.accept_at,
	md.created_at,
	md.order_at
FROM
	material_detail md
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 14
AND md.`status` IN (3)
GROUP BY
	md.kd_tagihan
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_cashflow
-- ----------------------------
DROP VIEW IF EXISTS `v_cashflow`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_cashflow` AS SELECT
	kd_cf,
	opr.store_id,
	s.store_name store,
-- 	opr.id,
	opr.date,
-- 	opr.user_id,
-- 	`user`.fullname,
-- 	opr.debit,
-- 	opr.kredit,
	opr.keterangan,
	opr.status_id,
	opr.is_cf,
	opr.created_at,
	opr.updated_at
FROM
	opr
INNER JOIN store s ON s.id = opr.store_id
WHERE
	opr.is_cf = 1
GROUP BY kd_cf
ORDER BY
	created_at DESC ;

-- ----------------------------
-- View structure for v_material
-- ----------------------------
DROP VIEW IF EXISTS `v_material`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_material` AS SELECT
	mm.nama,
	CONCAT(
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		varian.nm_varian,
		' '
	) fullname,
	CONCAT(
		attr.nm_attr,
		' : ',
		varian.nm_varian
	) varianname,
	mm.kode_warna,
	mm.berat,
	mm.satuan_berat,
	mm.is_dropship,
	s.store_name store,
	m.*, IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND tipe_transaksi = 14
			AND STATUS = 4
		),
		0
	) qty_in,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND tipe_transaksi = 15
			AND STATUS = 4
		),
		0
	) qty_out,
	stok_akhir_func (
		m.stok_awal,
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND tipe_transaksi = 14
				AND STATUS = 4
			),
			0
		),
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND tipe_transaksi = 15
				AND STATUS = 4
			),
			0
		)
	) stok_akhir
FROM
	material m
LEFT JOIN master_material mm ON mm.kode = m.kode
LEFT JOIN item_attr attr ON attr.id = m.id_attr
LEFT JOIN item_varian varian ON varian.id = m.id_varian 
INNER JOIN store s ON s.id = m.store_id ;

-- ----------------------------
-- View structure for v_material_stok
-- ----------------------------
DROP VIEW IF EXISTS `v_material_stok`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_material_stok` AS SELECT
	md.id,
	CONCAT(md.id,'_',m.id) kd_md,
-- 	MD5(CONCAT(mm.kode,m.id,md.kd_tagihan)) kd_stok,
	md.kd_tagihan,
	CONCAT('#',md.kd_tagihan) as kd_tagihan_view,
	CONCAT('TR : #',md.kd_tagihan,' (',DATE_FORMAT(md.order_at,'%d/%m/%y'),')') as kd_tagihan_2,
	mm.kode_warna,
	mm.nama,
	CONCAT(
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		varian.nm_varian,
		' '
	) fullname,
	CONCAT(
		' (#',
		md.kd_tagihan,') ',
		mm.nama,
		' >  ',
		attr.nm_attr,
		' : ',
		varian.nm_varian
		
	) fullname_inv,
	CONCAT(
		attr.nm_attr,
		' : ',
		varian.nm_varian
	) varianname,
	md.harga_mod,
	md.hgsat_reseller,
	md.hgsat_retail,
	md.is_grosir,
	md.id_sup,
	md.order_at,
-- 	MATERIAL 
	m.id id_material,
	m.kode,
	m.foto,
	m.id_attr,
	m.id_varian,
	m.stok_awal,
	m.store_id,
	s.store_name store,
 
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND kd_tagihan = md.kd_tagihan
			AND tipe_transaksi = 14
			AND STATUS = 4
		),
		0
	) qty_in,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				id_material = m.id
			AND kd_tagihan = md.kd_tagihan
			AND tipe_transaksi = 15
			AND STATUS = 4
		),
		0
	) qty_out,
	stok_akhir_func (
		0,
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND kd_tagihan = md.kd_tagihan
				AND tipe_transaksi = 14
				AND STATUS = 4
			),
			0
		),
		IFNULL(
			(
				SELECT
					SUM(qty)
				FROM
					material_detail
				WHERE
					id_material = m.id
				AND kd_tagihan = md.kd_tagihan
				AND tipe_transaksi = 15
				AND STATUS = 4
			),
			0
		)
	) stok_akhir,
	md.created_by,
	md.created_at
FROM
	material m
RIGHT JOIN material_detail md ON md.id_material = m.id
LEFT JOIN master_material mm ON mm.kode = m.kode
LEFT JOIN item_attr attr ON attr.id = m.id_attr
LEFT JOIN item_varian varian ON varian.id = m.id_varian
INNER JOIN store s ON s.id = m.store_id 
-- WHERE mm.is_dropship =0
GROUP BY
	kd_tagihan,
	id_material ;

-- ----------------------------
-- View structure for v_orders
-- ----------------------------
DROP VIEW IF EXISTS `v_orders`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_orders` AS SELECT
	md.kd_order,
	md.store_id,
	s.store_name store,
	md.tipe_transfer,
	md.tipe_transaksi,
	SUM(md.qty) qty,
	md.ongkir_buyer,
	SUM(hgsat_jual * qty) total_belanja,
	SUM((hgsat_jual - harga_mod) * qty) total_margin,
	(SUM(hgsat_jual * qty)) + IFNULL(ongkir_buyer, 0) total_tagihan_buyer,
	md.buyer,
	md.buyer_note,
	md.id_market,
	md.id_kurir,
	md.resi,
	md.resi_photo,
	md.`status` id_status,
	md.is_gived,
	md.created_at,
	md.created_by,
	md.done_at
FROM
	material_detail md

INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 15
AND md.`status` IN (0, 2, 24, 25, 16, 3, 4)
GROUP BY
	md.kd_order
ORDER BY
	FIELD(md.`status`, 0, 24, 3, 2, 4) ;

-- ----------------------------
-- View structure for v_pb
-- ----------------------------
DROP VIEW IF EXISTS `v_pb`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_pb` AS SELECT
	-- 	md.id,

	md.kd_tagihan,
	md.store_id,
	s.store_name store,
	md.tipe_transfer,
	md.tipe_transaksi,
	md.ppn,
	md.pph,
	md.dll,
	md.bea_masuk,
	md.hgtot_dasar,
	md.hgtot_beli,
	md.ongkir_impor,
	SUM(md.qty) qty_total,
	md.id_sup,
	md.`status` id_status,
	-- 	md.accept_at,
	-- 	md.created_at
	md.id_fw,
	md.order_at
FROM
	material_detail md
INNER JOIN store s ON s.id = md.store_id
WHERE
	md.tipe_transaksi = 14 -- AND md.`status` IN (0, 1, 2, 3, 4)
GROUP BY
	md.kd_tagihan
ORDER BY
	md.created_at DESC ;

-- ----------------------------
-- View structure for v_spending
-- ----------------------------
DROP VIEW IF EXISTS `v_spending`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_spending` AS SELECT
	opr.id,
	opr.store_id,
	s.store_name store,
	opr.date,
	opr.user_id,
	`user`.fullname,
	opr.coa_id,
	coa.`name` coa_name,
	opr.debit,
	opr.kredit,
	opr.keterangan,
	opr.status_id,
	opr.bukti,
	opr.is_cf,
	opr.created_at,
	opr.updated_at
FROM
	opr
INNER JOIN `user` ON `user`.id = opr.user_id
INNER JOIN coa ON coa.id = opr.coa_id
INNER JOIN store s ON s.id = opr.store_id
ORDER BY
	opr.created_at DESC ;

-- ----------------------------
-- View structure for v_spend_group
-- ----------------------------
DROP VIEW IF EXISTS `v_spend_group`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_spend_group` AS SELECT
	opr.store_id,
	s.store_name store,
	opr.user_id,
	`user`.fullname,
	SUM(opr.debit) debit,
	SUM(opr.kredit) kredit
FROM
	opr
INNER JOIN `user` ON `user`.id = opr.user_id
INNER JOIN coa ON coa.id = opr.coa_id
INNER JOIN store s ON s.id = opr.store_id
WHERE
	opr.status_id = 0
ORDER BY
	opr.created_at DESC ;

-- ----------------------------
-- View structure for v_totalqty
-- ----------------------------
DROP VIEW IF EXISTS `v_totalqty`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_totalqty` AS SELECT
	-- 	id,
	md.kd_tagihan,
	md.store_id,
	s.store_name store,
	md.id_material,
	vm.fullname,
	md.qty_order_bm,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				kd_tagihan = md.kd_tagihan
			AND id_material = md.id_material
			AND tipe_transaksi=14
			AND `status` = 3
		),
		0
	) qty_unacc,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail
			WHERE
				kd_tagihan = md.kd_tagihan
			AND id_material = md.id_material
			AND tipe_transaksi=14
			AND `status` = 4
		),
		0
	) qty_acc
FROM
	material_detail md
LEFT JOIN v_material vm ON vm.id = md.id_material
INNER JOIN store s ON s.id = md.store_id
WHERE md.tipe_transaksi=14
GROUP BY
	md.kd_tagihan,
	md.id_material
ORDER BY
	md.kd_tagihan,
	md.id_material ASC ;

-- ----------------------------
-- View structure for v_tr_thismonth
-- ----------------------------
DROP VIEW IF EXISTS `v_tr_thismonth`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `v_tr_thismonth` AS SELECT
	mm.kode,
	mm.store_id,
	s.store_name store,
	mm.kode_warna,
	mm.nama item,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND DATE_FORMAT(md_sub.done_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) qty_sales,
	IFNULL(
		(
			SELECT
				SUM(hgsat_jual * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND DATE_FORMAT(md_sub.done_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) total_sales,
	IFNULL(
		(
			SELECT
				SUM((hgsat_jual - harga_mod) * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` = 4
			AND DATE_FORMAT(md_sub.done_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) total_earnings,
	IFNULL(
		(
			SELECT
				SUM(qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` IN (24, 3)
			AND DATE_FORMAT(md_sub.created_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) qty_process,
	IFNULL(
		(
			SELECT
				SUM(hgsat_jual * qty)
			FROM
				material_detail md_sub
			INNER JOIN material m_sub ON m_sub.id = md_sub.id_material
			WHERE
				md_sub.tipe_transaksi = 15
			AND m_sub.kode = mm.kode
			AND (
				md_sub.hgsat_jual IS NOT NULL
				AND md_sub.hgsat_jual > 0
			)
			AND md_sub.`status` IN (24, 3)
			AND DATE_FORMAT(md_sub.created_at, '%y-%m') = DATE_FORMAT(CURDATE(), '%y-%m')
		),
		0
	) total_process
FROM
	master_material mm 
INNER JOIN store s ON s.id = mm.store_id ;

-- ----------------------------
-- Function structure for stok_akhir_func
-- ----------------------------
DROP FUNCTION IF EXISTS `stok_akhir_func`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `stok_akhir_func`(`stok_awal` int,`masuk` int,`keluar` int) RETURNS int(11)
BEGIN
	RETURN (stok_awal + masuk)-keluar;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table material_detail
-- ----------------------------
DROP TRIGGER IF EXISTS `default_od`;
delimiter ;;
CREATE TRIGGER `default_od` BEFORE INSERT ON `material_detail` FOR EACH ROW BEGIN 

IF(NEW.tipe_transaksi=15) THEN
       IF(NEW.id_market IS NULL OR NEW.id_market ='') THEN
             SET NEW.id_market=100;
       END IF;
       IF(NEW.id_kurir IS NULL OR NEW.id_kurir ='') THEN
             SET NEW.id_kurir=100;
       END IF;       
END IF;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
