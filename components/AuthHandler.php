<?php
namespace app\components;

use Yii;

use app\models\Auth;
use app\models\User;
use yii\helpers\Url;

use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;
    public $rememberMe = true;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();

       

        if($this->client->name=='google'){
            $email_ext=ArrayHelper::getValue($attributes, 'emails.0.value');
            $id_ext=ArrayHelper::getValue($attributes, 'id');
            $nickname_ext=ArrayHelper::getValue($attributes, 'nickname');

        }

        $email = $email_ext;
        $id = $id_ext;
        $nickname = $nickname_ext;

        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id, 
        ])->one();

        // echo $this->client->getTitle();
        // echo "<pre>";
        // print_r($auth);
        // exit;
        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /* @var User $user */
                $user = $auth->user;
                $this->updateUserInfo($user);
                Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
            } else { // signup
                if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                    //  Yii::$app->session->setFlash('error',"
                    // <div class='form-group m-t-20'>
                    //     <div class='container-fluid'>
                    //     <center>
                    //         <span id='animationSandbox' style='display: block;' class=''>
                    //            <i class='fa fa-exclamation-triangle fa-fw'></i>emai google ! 
                    //         </span>
                    //     </center>
                    //     </div>
                    // </div>");
                    $user=User::find()->where(['email' => $email])->one();

                    if(!Auth::find()->where(['user_id'=>$user->id])->exists()){
                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $this->client->getId(),
                            'source_id' => (string)$id,
                        ]);
                        if ($auth->save()) {
                            Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
                        }
                    }

                    // return Yii::$app->getResponse()->redirect(['site/login']);


                    // Yii::$app->getSession()->setFlash('error', [
                    //     Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                    // ]);
                } else {
                      Yii::$app->session->setFlash('error',"
                    <div class='form-group m-t-20'>
                        <div class='container-fluid'>
                        <center>
                            <span id='animationSandbox' style='display: block;' class=''>
                               <i class='fa fa-exclamation-triangle fa-fw'></i>E-mail anda belum terdaftar di data kami ! 
                            </span>
                        </center>
                        </div>
                    </div>");
                    return Yii::$app->getResponse()->redirect(['site/login']);

                    // $password = Yii::$app->security->generateRandomString(6);
                    // $user = new User([
                    //     'username' => $nickname,
                    //     // 'github' => $nickname,
                    //     'email' => $email,
                    //     'password' => $password,
                    // ]);
                    // $user->generateAuthKey();
                    // $user->generatePasswordResetToken();

                    // $transaction = User::getDb()->beginTransaction();

                    // if ($user->save()) {
                    //     $auth = new Auth([
                    //         'user_id' => $user->id,
                    //         'source' => $this->client->getId(),
                    //         'source_id' => (string)$id,
                    //     ]);
                    //     if ($auth->save()) {
                    //         $transaction->commit();
                    //         Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
                    //     } else {
                    //         Yii::$app->getSession()->setFlash('error', [
                    //             Yii::t('app', 'Unable to save {client} account: {errors}', [
                    //                 'client' => $this->client->getTitle(),
                    //                 'errors' => json_encode($auth->getErrors()),
                    //             ]),
                    //         ]);
                    //     }
                    // } else {
                    //     Yii::$app->getSession()->setFlash('error', [
                    //         Yii::t('app', 'Unable to save user: {errors}', [
                    //             'client' => $this->client->getTitle(),
                    //             'errors' => json_encode($user->getErrors()),
                    //         ]),
                    //     ]);
                    // }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $this->client->getId(),
                    'source_id' => (string)$attributes['id'],
                ]);
                if ($auth->save()) {
                    /** @var User $user */
                    $user = $auth->user;
                    $this->updateUserInfo($user);
                    Yii::$app->getSession()->setFlash('success', [
                        Yii::t('app', 'Linked {client} account.', [
                            'client' => $this->client->getTitle()
                        ]),
                    ]);
                } else {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', 'Unable to link {client} account: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($auth->getErrors()),
                        ]),
                    ]);
                }
            } else { // there's existing auth
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app',
                        'Unable to link {client} account. There is another user using it.',
                        ['client' => $this->client->getTitle()]),
                ]);
            }
        }
    }

    /**
     * @param User $user
     */
    private function updateUserInfo(User $user)
    {
        $attributes = $this->client->getUserAttributes();
        $github = ArrayHelper::getValue($attributes, 'login');
        if ($user->github === null && $github) {
            $user->github = $github;
            $user->save();
        }
    }
}

?>