<?php

namespace app\components;
 
 
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Help extends Component {

	public static function get_my_info() {

	     //your code here
	     //your code here

	     return 0;
	}

	public static  function FormatRupiah($parm){
	    return 'Rp ' . number_format( floatval($parm), 0 , '' , '.' ) . ',00';
	}
	public static  function toRp($parm,$rp=1){
	    	return 'Rp ' . number_format( floatval($parm), 0 , '' , '.' );
		
	}

	public static  function FormatRupiahShort($parm){
	    return number_format( $parm, 0 , '' , '.' );
	}

	public static  function FormatHariIndonesia($parm){

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));
		return $array_hari[$dataHari];
	}

	public static function FormatTanggalIndonesia($parm){

		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));
		return date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm));

	}

	public static function FormatTanggalHariIndonesia($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "Belum Check Out";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		return $array_hari[$dataHari].", ".date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm));

	}
	public static function datetimeindo($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		$dataWaktu = date('H:i',strtotime($parm));

		return $array_hari[$dataHari].", ".date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm))." <i class='fa fa-clock-o'></i>".$dataWaktu;
	}
	public static function datetimeindoShort($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "";
		}
		$array_bulan = array(1=>"Jan","Feb","Mar", "Apr", "Mei", "Jun","Jul","Agu","Sep","Okt", "Nov","Des");
		$dataBulan = date('n',strtotime($parm));

		$dataWaktu = date('H:i',strtotime($parm));

		return date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm))." <i class='fa fa-clock-o'></i>".$dataWaktu;
	}
	public static function dateindoShort($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "";
		}
		$array_bulan = array(1=>"Jan","Feb","Mar", "Apr", "Mei", "Jun","Jul","Agu","Sep","Okt", "Nov","Des");
		$dataBulan = date('n',strtotime($parm));

		// $dataWaktu = date('H:i',strtotime($parm));

		return date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm));
	}

	public static function FormatBulanIndonesia($bulan){

		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		return $array_bulan[ltrim($bulan,'0')];

	}

	public static function FormatTanggalHariWaktuIndonesia($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "-";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		$dataWaktu = date('H:i',strtotime($parm));

		return $array_hari[$dataHari].", ".date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm))." ".$dataWaktu;

	}

	public static function FormatTanggalWaktuIndonesia($parm){

		if($parm == '0000-00-00 00:00:00'){
			return "Belum Check Out";
		}
		$array_bulan = array(1=>"Januari","Februari","Maret", "April", "Mei", "Juni","Juli","Agustus","September","Oktober", "November","Desember");
		$dataBulan = date('n',strtotime($parm));

		$array_hari = array(1=>"Senin","Selasa","Rabu","Kamis","Jumat", "Sabtu","Minggu");
		$dataHari = date('N',strtotime($parm));

		$dataWaktu = "<i class='fa fa-clock-o'></i>".date('H:i',strtotime($parm));

		return date('d',strtotime($parm))." ".$array_bulan[$dataBulan]." ".date('Y',strtotime($parm))." ".$dataWaktu;

	}

	public static  function FormatWaktuIndonesia($parm){
	    if($parm == '0000-00-00 00:00:00'){
			return "-";
		}

		return date('H:i',strtotime($parm));

	}

	public static function HitungLemburNormal($total,$upahPerJam){
		//Perhitungan untuk jam kerja tambahan
		$jamPertama = 1.5*$upahPerJam;
		$jamSisa = ($total-1)*2*$upahPerJam;
		
		$hasil = $jamPertama + $jamSisa;
		return $hasil;
	}

 	public static function HitungLemburHarian($total,$upahPerJam){
 		//Perhitungan untuk hari libur atau pengganti bagi yang shift
 		if($total<= 8){
 			$hasil = $total*2*$upahPerJam;
 		}else if($total <= 9){
 			$jamPertama =$total*2*$upahPerJam;
			$jamSisa = 3*$upahPerJam;
			$hasil = $jamPertama + $jamSisa;
 		}else if($total <= 11){
 			$jamPertama =$total*2*$upahPerJam;
			$jamKedua = 3*$upahPerJam;
			$jamSisa = ($total-9)*4*$upahPerJam;
			$hasil = $jamPertama + $jamKedua + $jamSisa;
 		}else{
 			$total = 11;
 			$jamPertama =$total*2*$upahPerJam;
			$jamKedua = 3*$upahPerJam;
			$jamSisa = ($total-9)*4*$upahPerJam;
			$hasil = $jamPertama + $jamKedua + $jamSisa;
 		}
 		return $hasil;
	}

	public static function DiffDateForHour($date1,$date2){
		//Menghitung Selisih Jam
		$t1 = StrToTime ( $date1 );
		$t2 = StrToTime ( $date2);
		$diff = $t1 - $t2;
		$hours = $diff / ( 60 * 60 );
		return (int)abs($hours);
	}
	
	public static function timeInterval($start,$end){
		$date1=date_create($start);
		$date2=date_create($end);
		$diff=date_diff($date1,$date2);
        $jam=$diff->format('%h')+($diff->format('%a')*24);
        $menit=$diff->format('%i')+($diff->format('%a')*24);
        $detik=$diff->format('%s')+($diff->format('%a')*24);
        $time=['jam'=>$jam,'menit'=>$menit,'detik'=>$detik];
        return $time;
	}

	public static function timeInterval24($start,$end){
		$date1=date_create($start);
		$date2=date_create($end);
		$diff=date_diff($date1,$date2);
        $jam=$diff->format('%h')+($diff->format('%a')*24);
        $menit=$diff->format('%i')+($diff->format('%a')*24);
        $detik=$diff->format('%s')+($diff->format('%a')*24);
       	$time=Date('H:i:s',strtotime($jam.':'.$menit.':'.$detik));
        return $time;
	}
	public static function calculateTime($timeArray=[]){
		
		$sum = strtotime('00:00:00');
		$sum2=0;
		foreach ($timeArray as $v){

		    $sum1=strtotime($v)-$sum;

		    $sum2 = $sum2+$sum1;
		}

		$sum3=$sum+$sum2;

		return date("H:i:s",$sum3);
	}


	public static function datePeriod($start,$end,$format=null){
        $data=[];
        if($format != null){
        	$format_fix=$format;
        }else{
        	$format_fix='Y-m-d';
        }
		$begin = new DateTime( date('Y-m-d',strtotime($start)) );
        $end = new DateTime( date('Y-m-d',strtotime($end. ' +1 day')) );
        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        foreach ($period as $date) {
            $data[]=$date->format($format_fix);
        }
        return $data;
	}

	public static function countRangeDate($date1,$date2){
		$start=date_create($date1);
		$end=date_create($date2);
		$diff=date_diff($start,$end);
        return $diff->format('%a')+1;
	}

	public static function pembulatanJam($waktu,$pembulatanKeatasDalamMenit=null){
		if($pembulatanKeatasDalamMenit==null){
			$parm=45;
		}else{
			$parm=$pembulatanKeatasDalamMenit;
		}
		$time=date('H:i',strtotime($waktu));
		$time_arr=explode(':',$time);
		if($time_arr[1] !='00' && $time_arr[1] < $parm){
			$timeFix=date('H:i',strtotime($time_arr[0].':00'));
		}else{
			if($time_arr[1] =='00'){
				$timeFix=date('H:i',strtotime($time));
			}else{
				$timeFix=date('H:i',strtotime($time_arr[0].':00 + 1 hour'));
			}
		}
		return $timeFix;
	}
	public static function timeToWord($timeParm){
		$time=explode(':',$timeParm);
        if($time[0] != 00){
            $jam=$time[0].' jam ';
        }else{
            $jam='';
        }
        if($time[1]!=00){
            $menit=$time[1].' menit ';
        }else{
            $menit='';
        }
        if($time[2]!=00){
            $detik=$time[2].' detik';
        }else{
            $detik='';
        }
        return $jam.$menit.$detik;
	}
	// public static function numberToWord($numberParm){
		
	// }
	public static function upahPerJamByUMK($umk){
		$upahPerJam=(int)$umk*1/173;
		return ceil($upahPerJam);
	}
	public static function upahPerJamByTHP($thp){
		$perhitunganPertama = (int)((int)$thp*(25/100));
		$perhitunganKedua = ($perhitunganPertama - (int)$thp);
		$perhitunganKetiga = (int)($perhitunganKedua/173);
		return abs($perhitunganKetiga);
	}
	public static function toNumberOnly($text){
       return preg_replace("/[^0-9]+/", "", $text);
	}
	public static function toAlphaOnly($text){
       return preg_replace("/[^a-zA-Z]+/", "", $text);
	}
	public static function toAlnumOnly($text){
		// ALPHABET & NUMBER ONLY
       return preg_replace("/[^a-zA-Z0-9]+/", "", $text);
	}
	public static function convertImageToJPG($originalImage, $outputImage, $quality){
	    // jpg, png, gif or bmp?
	    $exploded = explode('.',$originalImage);
	    $ext = strtolower(end($exploded)); 
	    if (preg_match('/jpg|jpeg/i',$ext))
	        $imageTmp=imagecreatefromjpeg($originalImage);
	    else if (preg_match('/png/i',$ext))
	        $imageTmp=imagecreatefrompng($originalImage);
	    else if (preg_match('/gif/i',$ext))
	        $imageTmp=imagecreatefromgif($originalImage);
	    else if (preg_match('/bmp/i',$ext))
	        $imageTmp=imagecreatefrombmp($originalImage);
	    else
	        return 0;
	    // quality is a value from 0 (worst) to 100 (best)
	    imagejpeg($imageTmp, $outputImage, $quality);
	    // imagedestroy($originalImage);
	    unlink($originalImage);
	    return 1;
	}
	public static function limit_words($string, $word_limit){
	    $words = explode(" ",$string);
	    return implode(" ",array_splice($words,0,$word_limit));
	}
	public static function limit_character($string, $character_limit){
		if(strlen($string) < $character_limit){
			$string_fix=$string;
		}else{
			$string_fix=substr($string,0,$character_limit).' ...';
		}
	    return $string_fix;
	}
	public static function base64ToImage($base64_string, $output_file) {
	    $file = fopen($output_file, "wb");
	    $data = explode(',', $base64_string);
	    fwrite($file, base64_decode($data[1]));
	    fclose($file);
	    return $output_file;
	}
	public static function stok_akhir($stok_awal,$in,$out){
		return ($stok_awal+$in) - $out;
	}

	public static function upload($files,$filename_additional='',$dir){
		if(!is_dir($dir)){
            mkdir($dir);
        }
		$filename="";
        if(is_dir($dir)){
			$fileArr=explode('.', $files['name']);
	   		$ext=end($fileArr);
	        $name=md5($fileArr[0].date('Y-m-d H:i')).$filename_additional;
	        $filename=$name.".".$ext;
	        move_uploaded_file($files['tmp_name'],$dir."/".$filename);
    	}
        return $filename;
	}

	public static function get_random_color(){
		// return '#'.dechex(rand(0x000000, 0xFFFFFF));
		// return '#'.sprintf('#%06X', mt_rand(0, 0xFFFFFF));
		$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
    	return '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
	}

	public static function inisial($string_what_ever){
		$pn = explode(' ' , $string_what_ever);
		$result='';
		foreach($pn as $i => $inisial) {
			$i > 0;
			$result.=substr($inisial,0,1);
		}
		return strtoupper($result);
	}

	public static function errorForm($arrayFromActiveForm=[]){

        $errors=[];
        $i=1;
        foreach ($arrayFromActiveForm as $key => $value) {
            foreach ($value as $error) {
                $errors[]=$i.'. '.$error;   
                $i++;
            }
        }

        return ( count($errors)>0)?['errors'=>implode($errors,'<br>')]:[];
	}


	public static function uniqueString($table=null,$attribute=null, $length = 32) {
				
		$randomString = Yii::$app->getSecurity()->generateRandomString($length);
		if($table===null){
			return $randomString;
		}else{
			$q=Yii::$app->db->createCommand('SELECT * FROM '.$table.' WHERE '.$attribute.'="'.$randomString.'"')->queryScalar();
			if(!$q){
				return $randomString;
			}else{
				return $this->uniqueString($table,$attribute, $length);
			}
		}
	}

	public static function mail($to,$subject,$html)
	{
		Yii::$app->mailer->compose()
		->setFrom(['hatobe@hatobe.dmc.zone'=>'Hatobe'])
        ->setTo($to)
        ->setSubject($subject)
        // ->setTextBody('Plain text content')
        ->setHtmlBody($html)
        ->send();
	}
	
	public static function echoarr($array){
		echo "<pre>";
		print_r($array);
	}

	public static function batch_insert($table='',$columns=[],$values=[]){
		$q="INSERT IGNORE INTO ".$table."(".implode(",", array_keys($columns)).") VALUES ";
		$q_val='';
		if(count($values)>0){
			foreach ($values as $val) {
				$i=0;
				$i_arr=[];
				$strval='';
				foreach ($columns as $key => $columnxls) {
					$i++;
					$i_arr[$i]=$columnxls;
					if($val[$i_arr[1]]!=''){
						$strval.='"'.$val[$columnxls].'"';
						if($i < count($columns)) $strval.=",";
					}
				}
				if($strval!=''){
					$q_val.='('.$strval.'),';
				}
	            
	            // echo $item['BARANG']."<BR>";
	        }
	        if(trim($q_val)!=''){
		        $q_fix=$q.rtrim($q_val,",");   
		        Yii::$app->db->createCommand($q_fix)->execute();
	    	}
		}
	}
}

?>
