<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
    ],
    'timeZone' => 'Asia/Jakarta',
    'components' => [
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'messageConfig' => [
                // 'charset' => 'UTF-8',
                // 'from' => ['hatobe@hatobe.dmc.zone' => 'Hatobe'],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'srv18.niagahoster.com',
                'username' => 'hatobe@hatobe.dmc.zone',
                'password' => 'hatobe',
                'port' => 465,
                'encryption' => 'ssl'
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Uf-5wB4ZPMz17RR7dtlEiMHb9CWK-xAn',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
      
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        // 'mailer' => [
        //     'class' => 'yii\swiftmailer\Mailer',
        //     // send all mails to a file by default. You have to set
        //     // 'useFileTransport' to false and configure a transport
        //     // for the mailer to send real emails.
        //     'useFileTransport' => true,
        // ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                // ['class' => 'yii\rest\UrlRule', 'controller' => 'api/transaksi'],
                [
                    'class' => 'yii\rest\UrlRule', 
                    'controller' => 'api/transaksi',
                    // 'pluralize' => false, 
                    'extraPatterns'=>[
                        'GET pb/{kd}'=>'pb'
                    ]
                ],

                '/' => 'site/index',                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',


                // 'api/<controller:\w+>' => 'api/<controller>/index',
                // 'api/<controller:\w+>/<id:\d+>' => 'api/<controller>/view',
                // 'api/<controller:\w+>/<action:\w+>/<id:\d+>' => 'api/<controller>/<action>',
                // 'api/<controller:\w+>/<action:\w+>' => 'api/<controller>/<action>',
                // 'api/<controller:\w+>/<action:\w+>' => 'api/<controller>/<action>',


                // 'api/<controller:\w+>/<id:\d+>' => 'api/<controller>/view',
                // 'api/<controller:\w+>/<action:\w+>/<id:\d+>' => 'api/<controller>/<action>',
                // 'api/<controller:\w+>/<action:\w+>' => 'api/<controller>/<action>',

            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        
        'help' => [
            'class' => 'app\components\Help', 
        ],

        'AuthHandler' => [
            'class' => 'app\components\AuthHandler', 
        ],

        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '866159874055-68gtd9iedao29ttfkhoku1ckaai30qgj.apps.googleusercontent.com',
                    'clientSecret' => 'Uji5nhLmNw0oT_RiobiDBFjQ',
                ],
            ],
        ],
      
        // 'view' => [
        //     'theme' => [
        //         'class' => 'yii\base\Theme',
        //         'pathMap' => ['@app/views' => '@app/themes/ampeadmin'],
        //         'baseUrl'   => '@web/../themes/ampeadmin'
        //     ]
        // ],  
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
    $config['bootstrap'][]='gridview';
    $config['modules']['gridview'] = [
        'class' => 'kartik\grid\Module',
    ];
    
}

return $config;
